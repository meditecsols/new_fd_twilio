//
//  ConsultaPresencialVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 21/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "ConsultaPresencialVC.h"
#import "PrescriptionTVC.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "ConsultationPatientTVC.h"
#import "Config.h"
#import "RMCustomViewActionController.h"
#import "DrugAddTVC.h"
#import "SexyTooltip.h"
#import "StatusConsultationCVC.h"
#import "NotesViewerVC.h"

#define ROW_HIEGHT_HEALTH 40
#define PLACEHOLDER_OBSERVATION @"*Tip: Este campo puede utilizarse para hacer observaciones, generar un justificante médico o constancia de buena salud."
#define PLACEHOLDER_NOTES @""

typedef enum{
    healthAllergy = 1,
    healthSurgery = 2,
    healthTransfusion=3,
    healthTraumatism = 4,
    healthFamiliar=5,
    
    
}typeHealth;
typedef enum{
    studyLab = 1,
    studyGab = 2,
    
}typeStudy;

typedef enum
{
    tableSectionDrugs=0,
    tableSectionLab=1,
    tableSectionGabinete=2,
    tableSectionObservations=3

} tableSection;

@interface ConsultaPresencialVC (){
    int movedHeight;
    UITextField *textFieldEditable;
    UITapGestureRecognizer *tap;
    NSMutableArray *drugsArray;
    NSMutableArray *labArray;
    NSMutableArray *gabineteArray;
    NSMutableArray *observationsArray;
    NSMutableArray *healthConditionDrugsArray;
    NSMutableArray *healthAndDrugsArray;
    
    NSMutableArray *arrayOfStatus;
    NSMutableArray *allergyArray;
    NSMutableArray *surgeryArray;
    NSMutableArray *transfusionArray;
    NSMutableArray *injuryArray;
    NSMutableArray *familiarArray;
    
    NSMutableArray *prescriptionArray;
    
    NSMutableArray *notesArray;
    
    NSMutableArray *historicArray;
    NSMutableArray *completeHistoricArray;
    int studyToAdd;
    int diagnoseSelected;
    int statusIndexSelected;
    
    UIView *menuView;
    
    UIView *firstView;
    UIView *secondView;
    UIView *thirdView;
    UIView *fourthView;
    
    int healthToAdd;
    UIView *addView;
    UIView *addViewStudy;
    UIView *addViewObservations;
    UIView *addViewMedicament;
    UIView *addViewDiagnose;
    UIView *pdfViewer;
    
    UIView *changeWeightView;
    UIView *changeHeightView;
    UIView *changeTempView;
    UIView *changeBloodPressureView;
    UIView *changeHeartFreqView;
    UIView *changeBreathFreqView;
    
    NSMutableArray *arrayBloodTypes;
    UIPickerView *bloodPicker;
    UIView *changeBloodView;

    UIView *noteViewer;

    UIView *preDiagnosticView;

    UIPickerView *pickerDiagnostic;
    
    UIView *diagnoseClosingView;
    UIView *closingPaymentView;
    UIView *addViewService;
    NSMutableArray *serviceArray;
    NSMutableArray *exitReasonArray;
    UIPickerView *pickerExitReasonsType;
    
    
    NSMutableArray *paymentTypeArray;
    UIPickerView *pickerPaymentType;
    int paymentTypeSelected;
    
    UIPickerView *pickerServiceList;
    int serviceSelected;
    NSMutableArray *serviceListArray;
    float originFrame;
    
    int exitTypeSelected;
    
    UIView *pickerContainer;
    UIPickerView *pickerPrescriptionList;
    int prescriptionSelected;
    NSMutableArray *prescriptionListArray;
    NSMutableArray *notesListArray;
    UIPickerView *pickerNotesList;
    BOOL closeNoPayConsultation;
}

@end

@implementation ConsultaPresencialVC

- (void)viewDidLoad {
    [super viewDidLoad];
    comeFromScan=NO;
    // Do any additional setup after loading the view.
    _tabBarMenu.delegate=self;
    
    _tabBarMenu.backgroundColor = [UIColor whiteColor];
    _tabBarMenu.selectedItem=[[_tabBarMenu items] objectAtIndex:0];
  
    
    originFrame=0;
    float versionIOS=[[[UIDevice currentDevice] systemVersion] floatValue];
    if (versionIOS<11) {
        originFrame=20;
    }

    
//    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
//    [self.view addGestureRecognizer:swipeLeft];
//    
//    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
//    [self.view addGestureRecognizer:swipeRight];
    
    [self registerForKeyboardNotifications];
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(hideKeyboard)];
    [self.viewContainer addGestureRecognizer:tap];
    tap.delegate=self;
    menuView=[[UIView alloc] init];
    menuView=[[[NSBundle mainBundle] loadNibNamed:@"MenuActionView" owner:self options:nil] objectAtIndex:0];
    
    firstView=[[UIView alloc] init];
    firstView=[[[NSBundle mainBundle] loadNibNamed:@"ExpedienteView" owner:self options:nil] objectAtIndex:0];
    secondView=[[UIView alloc] init];
    secondView=[[[NSBundle mainBundle] loadNibNamed:@"RecetaView" owner:self options:nil] objectAtIndex:0];
    thirdView=[[UIView alloc] init];
    thirdView=[[[NSBundle mainBundle] loadNibNamed:@"NotasView" owner:self options:nil] objectAtIndex:0];
    fourthView=[[UIView alloc] init];
    fourthView=[[[NSBundle mainBundle] loadNibNamed:@"HistorialView" owner:self options:nil] objectAtIndex:0];
    
    _lblName.text=[dictPatientPresential objectForKey:@"first_name"];
    _lblLastName.text=[NSString stringWithFormat:@"%@ %@",[dictPatientPresential objectForKey:@"last_name"],[dictPatientPresential objectForKey:@"second_last_name"]];
    _lblAge.text=@"";
    _lblGender.text=@"";
    _lblHeight.text=@"";
    _lblWeight.text=@"";
    _lblBlood.text=@"";
    _lblTemp.text=@"";
    _lblBloodPressure.text=@"";
    _lblHeartFreq.text=@"";
    _lblBreathFreq.text=@"";
    if ([[dictPatientPresential objectForKey:@"gender"] isEqualToString:@"male"]) {
        _lblGender.text=@"M";
        
    }
    else if ([[dictPatientPresential objectForKey:@"gender"] isEqualToString:@"female"]) {
        _lblGender.text=@"F";
    }
    _lblBlood.text=[dictPatientPresential objectForKey:@"blood_type"];
    
    if ([[dictPatientPresential objectForKey:@"date_of_birth"] length]>0) {
        _lblAge.text=[self formatDateToView:[dictPatientPresential objectForKey:@"date_of_birth"]];
    }
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                _lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>4) {
                    self->_lblWeight.text=[[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] substringToIndex:4];
                }
            }
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                _lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
            }
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
            }
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
            }
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
            }
            if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
            }
        }
    }
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(showAge)];
    [_viewAge setUserInteractionEnabled:YES];
    [_viewAge addGestureRecognizer:tap0];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(changeHeight)];
    [_viewHeight setUserInteractionEnabled:YES];
    [_viewHeight addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changeWeight)];
    [_viewWeight setUserInteractionEnabled:YES];
    [_viewWeight addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changeBlood)];
    [_imageViewBlood setUserInteractionEnabled:YES];
    [_imageViewBlood addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changePressureBlood)];
    [_viewPressureBlood setUserInteractionEnabled:YES];
    [_viewPressureBlood addGestureRecognizer:tap4];
    
    
    UITapGestureRecognizer *tap5 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changeCardiacFreq)];
    [_viewCardiacFrequency setUserInteractionEnabled:YES];
    [_viewCardiacFrequency addGestureRecognizer:tap5];
    
    UITapGestureRecognizer *tap6 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changeBreathFreq)];
    [_viewBreathingFrequency setUserInteractionEnabled:YES];
    [_viewBreathingFrequency addGestureRecognizer:tap6];
    
    UITapGestureRecognizer *tap7 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(changeTemp)];
    [_viewTemp setUserInteractionEnabled:YES];
    [_viewTemp addGestureRecognizer:tap7];
    
    
    if (dictOfPrescription) {
        if ([NSNumber numberWithInt:(int)[presentialConsultation objectForKey:@"id"]]!=[NSNumber numberWithInt:(int)[dictOfPrescription  objectForKey:@"consultationId"]]) {
            dictOfPrescription=[NSMutableDictionary new];
        }
    }
    
    drugsArray=[NSMutableArray new];
    
    if (dictOfPrescription) {
        if ([[dictOfPrescription objectForKey:@"drugs"] count]>0) {
            drugsArray=[dictOfPrescription objectForKey:@"drugs"];
        }
    }
    labArray=[NSMutableArray new];
    if (dictOfPrescription) {
        if ([[dictOfPrescription objectForKey:@"arrayLab"] count]>0) {
            labArray=[dictOfPrescription objectForKey:@"arrayLab"];
        }
    }
    gabineteArray=[NSMutableArray new];
    if (dictOfPrescription) {
        if ([[dictOfPrescription objectForKey:@"arrayGab"] count]>0) {
            gabineteArray=[dictOfPrescription objectForKey:@"arrayGab"];
        }
    }
    observationsArray=[NSMutableArray new];
    if (dictOfPrescription) {
        if ([[dictOfPrescription objectForKey:@"observations"] count]>0) {
            observationsArray=[dictOfPrescription objectForKey:@"observations"];
        }
    }
    
    self.tvPrescriptionItems.backgroundView.backgroundColor = [UIColor whiteColor];
    self.tvPrescriptionItems.backgroundColor = [UIColor whiteColor];
  
    
}
-(void)viewDidLayoutSubviews{
    
}
-(NSString *)formatBirthDateReceivedToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yy"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
-(NSString *)formatDateToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:formattedDate
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    NSString *ageString=[NSString stringWithFormat:@"%ld",(long)age];
    return ageString;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    

    [firstView setFrame:CGRectMake(0,originFrame, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height) ];
    [self.viewContainer addSubview:firstView];
    _txtTest.delegate=self;
    [self closeAllergyTab];
    [self closeSurgeryTab];
    [self closeTransfuTab];
    [self closeTraumatismTab];
    [self closeFamiliarTab];
    
    _tvAllerg.dataSource=self;
    _tvSurgery.dataSource=self;
    _tvTraumatism.dataSource=self;
    _tvTransfusion.dataSource=self;
    _tvFamiliarBg.dataSource=self;

    [self getHealthData];
    [self getHealthDataArray];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    needRefreshConsultations=YES;
}

- (void)swipe:(UISwipeGestureRecognizer *)swipeRecogniser
{
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionRight)
    {
        switch (_tabBarMenu.selectedItem.tag) {
            case 1:{
               [self showFirstView];
            }break;
            case 2:{
                [self showSecondView];
            }break;
            case 3:{
                [self showThirdView];
            }break;
            default:
                break;
        }
    }
    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionLeft)
    {
        switch (_tabBarMenu.selectedItem.tag) {
            case 0:{
                [self showSecondView];
            }break;
            case 1:{
               [self showThirdView];
            }break;
            case 2:{
                [self showFourthView];
            }break;

            default:
                break;
        }
    }
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    [self hideKeyboard];
    switch (item.tag) {
        case 0:{
            [self showFirstView];
        }break;
        case 1:{
            [self showSecondView];
        }break;
        case 2:{
            [self showThirdView];
        }break;
        case 3:{
            [self showFourthView];
        }break;
        default:
            break;
    }
}
-(void)showFirstView{

    [firstView setFrame:CGRectMake(0,originFrame, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height) ];
    
    [self.viewContainer addSubview:firstView];
    [_tabBarMenu setSelectedItem:[[_tabBarMenu items] objectAtIndex:0]];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(changeHeight)];
    [_viewHeight setUserInteractionEnabled:YES];
    [_viewHeight addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(changeWeight)];
    [_viewWeight setUserInteractionEnabled:YES];
    [_viewWeight addGestureRecognizer:tap2];
    
    
    UITapGestureRecognizer *tap0 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(showAge)];
    [_viewAge setUserInteractionEnabled:YES];
    [_viewAge addGestureRecognizer:tap0];
    
    _lblBirthDate.text=[self formatBirthDateReceivedToView:[dictPatientPresential objectForKey:@"date_of_birth"]];

    
}
-(void)showSecondView{
    [secondView setFrame:CGRectMake(0,originFrame, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height) ];
    [self.viewContainer addSubview:secondView];
    [_tabBarMenu setSelectedItem:[[_tabBarMenu items] objectAtIndex:1]];
    _tvPrescriptionItems.delegate=self;
    _tvPrescriptionItems.dataSource=self;
    _tvPrescriptionsHistory.dataSource=self;
    _tvPrescriptionsHistory.delegate=self;
    _layoutConstraintContainerHeight.constant=300;
    _btnReceta.layer.borderWidth=1;
    _btnReceta.layer.cornerRadius=5;
    _btnReceta.layer.borderColor=[UIColor whiteColor].CGColor;
    _btnReceta.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnReceta.titleLabel.numberOfLines = 2;
    _layoutConstraintTablePrescriptionHeight.constant=_layoutConstraintContainerHeight.constant;
//    if ([[presentialConsultation objectForKey:@"diagnoseArray"] count]<=0 && [healthConditionDrugsArray count]<=0) {
//        [_scrollRecetaItems setHidden:YES];
//    }
    [self getPrescriptionsWS];
}
-(void)showThirdView{
    [thirdView setFrame:CGRectMake(0,originFrame, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height) ];
    [self.viewContainer addSubview:thirdView];
    [_tabBarMenu setSelectedItem:[[_tabBarMenu items] objectAtIndex:2]];
    _tvNotes.dataSource=self;
    _tvNotes.delegate=self;
    _tvNotes.backgroundColor=[UIColor clearColor];
    _txtViewNote.delegate=self;
    _txtViewNote.backgroundColor=[UIColor whiteColor];
    [self getNotes];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if([touch.view isDescendantOfView:_tvNotes]){
        return NO;
    }

    if([touch.view isDescendantOfView:_collectionStatus]){
        return NO;
    }

    return YES;
    
}
-(void)showFourthView{
    [fourthView setFrame:CGRectMake(0,originFrame, self.viewContainer.frame.size.width, self.viewContainer.frame.size.height) ];
    [self.viewContainer addSubview:fourthView];
    [_tabBarMenu setSelectedItem:[[_tabBarMenu items] objectAtIndex:3]];
    _tvHistoric.dataSource=self;
    _tvHistoric.delegate=self;
    _tvHistoric.backgroundView.backgroundColor = [UIColor whiteColor];
     _tvHistoric.backgroundColor = [UIColor whiteColor];
    [self getHistoric];
}
- (IBAction)backAction:(id)sender {
//    [self.view endEditing:YES];
//
//    if([menuView isDescendantOfView:self.view]){
//        [menuView removeFromSuperview];
//        return;
//    }
//    int yForView=78;
//    [menuView setFrame:CGRectMake(0,yForView, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
//    [self.view addSubview:menuView];
//
//    UIBezierPath *maskPath = [UIBezierPath
//                              bezierPathWithRoundedRect:_viewButtonsContainer.bounds
//                              byRoundingCorners:( UIRectCornerBottomRight)
//                              cornerRadii:CGSizeMake(15,15)
//                              ];
//
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
//
//    maskLayer.frame = _viewButtonsContainer.bounds;
//    maskLayer.path = maskPath.CGPath;
//
//    _viewButtonsContainer.layer.mask = maskLayer;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)actionClose:(id)sender {
    
    RMActionControllerStyle style = RMActionControllerStyleWhite;
    RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
    
    
    RMAction *closeConsultation = [RMAction<UIView *> actionWithTitle:@"Cerrar consulta" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        [actionController dismissViewControllerAnimated:NO completion:^{
            self->closeNoPayConsultation=YES;
             [self actionOpenPayment];
        }];
       
    }];
    
    RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Cobrar consulta" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        self->closeNoPayConsultation=NO;
        [self actionOpenPayment];
    }];
    
    RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
        
    }];
    
    
    actionController.title = @"¿Qué deseas hacer?";
    actionController.message = @"";
    
    [actionController addAction:cancelAction];
    if ([[presentialConsultation objectForKey:@"payment_type"] isEqualToString:@"card"]) {
        [actionController addAction:payConsultation];
    }
    
    [actionController addAction:closeConsultation];
    [self presentViewController:actionController animated:YES completion:nil];
    
    
//    UIAlertController * alert=  [UIAlertController
//                                 alertControllerWithTitle:@""
//                                 message:@"¿Desea cerrar la consulta?"
//                                 preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction* noAction = [UIAlertAction
//                               actionWithTitle:@"No"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//                                   [alert dismissViewControllerAnimated:YES completion:nil];
//
//                               }];
//    UIAlertAction* okAction = [UIAlertAction
//                               actionWithTitle:@"Si"
//                               style:UIAlertActionStyleDefault
//                               handler:^(UIAlertAction * action)
//                               {
//                                   [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
//                                       InvokeService *invoke=[[InvokeService alloc] init];
//                                       NSMutableDictionary *dictConsultationToClose=[[NSMutableDictionary alloc] init];
//                                       [dictConsultationToClose setObject:@"closed" forKey:@"status"];
//                                       [dictConsultationToClose setObject:[presentialConsultation objectForKey:@"id"] forKey:@"id"];
//                                       [invoke updateConsultationWithData:dictConsultationToClose WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
//                                           dispatch_async(dispatch_get_main_queue(), ^{
//                                               [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
//                                                   [self dismissViewControllerAnimated:YES completion:nil];
//                                               }];
//                                           });
//
//                                       }];
//
//                                   }];
//                               }];
//    [alert addAction:noAction];
//    [alert addAction:okAction];
//    [self presentViewController:alert animated:YES completion:nil];
   
}
- (void)actionOpenPayment {
    diagnoseClosingView=[[UIView alloc] init];
    diagnoseClosingView=[[[NSBundle mainBundle] loadNibNamed:@"ClosingDiagnoseView" owner:self options:nil] objectAtIndex:0];
    [diagnoseClosingView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    [menuView removeFromSuperview];
    [self.view addSubview:diagnoseClosingView];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [diagnoseClosingView addGestureRecognizer:tap2];
    tap2.delegate=self;
    
    
    if ([[presentialConsultation objectForKey:@"payment_type"] isEqualToString:@"subscription"] ) {
        [self getStatusTypes];
    }
    else{
        _viewContainerStatus.hidden=YES;
        _layoutConstraintStatusContainerHeight.constant=0;
        [self.view layoutIfNeeded];
    }
    
    
    
    
    [self getExitReasonsTypes];
    if ([healthConditionDrugsArray count]>0) {
        healthConditionDrugsArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:healthConditionDrugsArray] mutableCopy];
        NSString *diagnoses=@"";
        for (NSDictionary *dictHealth in healthConditionDrugsArray) {
            if ([[dictHealth objectForKey:@"description"] length]>0) {
                diagnoses= [diagnoses stringByAppendingString:[NSString stringWithFormat:@"%@,",[dictHealth objectForKey:@"description"]]];
            }
        }
        diagnoses=[diagnoses substringToIndex:[diagnoses length]-1];
        _txtClosingDiagnose.text=diagnoses;
        _txtClosingDiagnose.enabled=NO;
        
    }
    
}

- (IBAction)actionAddDiagnose:(id)sender {
    addViewDiagnose=[[UIView alloc] init];
    addViewDiagnose=[[[NSBundle mainBundle] loadNibNamed:@"AddDiagnoseView" owner:self options:nil] objectAtIndex:0];
    [addViewDiagnose setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addViewDiagnose addGestureRecognizer:tap2];
    [self.view addSubview:addViewDiagnose];
    [_txtDiagnoseInput becomeFirstResponder];
}
-(void)getStatusTypes{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getStatusOfPatientWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                self->arrayOfStatus=[[NSMutableArray alloc] init];
                self->arrayOfStatus=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:responseData] mutableCopy];
                
                if ([self->arrayOfStatus count]<=0) {
                    return;
                }
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"id" ascending:YES];
                NSArray *arraySorted = [self->arrayOfStatus sortedArrayUsingDescriptors:@[descriptor]];
                self->arrayOfStatus = [arraySorted mutableCopy];
                
                self->_lblStatus.text=[[self->arrayOfStatus objectAtIndex:0] objectForKey:@"description"];
                self->statusIndexSelected=0;
                self->_collectionStatus.delegate=self;
                self->_collectionStatus.dataSource=self;
                [self->_collectionStatus registerNib:[UINib nibWithNibName:@"StatusConsultationCVC" bundle:nil] forCellWithReuseIdentifier:@"statusConsultationIdentifier"];
                [self setFlowCollectionView];
                [self.collectionStatus reloadData];
            });
        }];
    }];
    
}
-(void)getExitReasonsTypes{
    
    exitReasonArray=[[NSMutableArray alloc] init];
    NSMutableDictionary *dict1=[[NSMutableDictionary alloc] init];
    [dict1 setObject:@"Mejoría" forKey:@"title"];
    [dict1 setObject:@"health_improvement" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict1];
    
    NSMutableDictionary *dict2=[[NSMutableDictionary alloc] init];
    [dict2 setObject:@"En tratamiento" forKey:@"title"];
    [dict2 setObject:@"in_treatment" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict2];
    
    NSMutableDictionary *dict3=[[NSMutableDictionary alloc] init];
    [dict3 setObject:@"Traslado a Hospital" forKey:@"title"];
    [dict3 setObject:@"hospital_transportation" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict3];
    
    NSMutableDictionary *dict4=[[NSMutableDictionary alloc] init];
    [dict4 setObject:@"Alta voluntaria" forKey:@"title"];
    [dict4 setObject:@"voluntary_discharge" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict4];
    
    NSMutableDictionary *dict5=[[NSMutableDictionary alloc] init];
    [dict5 setObject:@"Otro" forKey:@"title"];
    [dict5 setObject:@"other" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict5];
    
    NSMutableDictionary *dict6=[[NSMutableDictionary alloc] init];
    [dict6 setObject:@"Pendiente" forKey:@"title"];
    [dict6 setObject:@"pending" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict6];
    
    NSMutableDictionary *dict7=[[NSMutableDictionary alloc] init];
    [dict7 setObject:@"No aplica" forKey:@"title"];
    [dict7 setObject:@"not_applicable" forKey:@"valueRequest"];
    [exitReasonArray addObject:dict7];
    
    pickerExitReasonsType = [[UIPickerView alloc]init];
    pickerExitReasonsType.tintColor = [UIColor whiteColor];
    pickerExitReasonsType.dataSource = self;
    pickerExitReasonsType.delegate = self;
    [_txtClosingExitReason setInputView:pickerExitReasonsType];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingExitReason)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    toolBar.tintColor = [UIColor darkGrayColor];
    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _txtClosingExitReason.inputAccessoryView = toolBar;
    
}
- (void) donePickingExitReason {
    int row = (int) [pickerExitReasonsType selectedRowInComponent:0];
    exitTypeSelected=row;
    _txtClosingExitReason.text =  [self pickerView:pickerExitReasonsType titleForRow:row forComponent:0];

    [self hideKeyboard];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow:(NSNotification*)notification{
    NSDictionary* info = [notification userInfo];
    
    CGRect kKeyBoardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    int KeyboardY=kKeyBoardFrame.size.height;
//    int endPositionText=textFieldEditable.frame.origin.y+textFieldEditable.frame.size.height;
//    if (endPositionText<KeyboardY) {
//        textFieldEditable=nil;
//        return;
//    }
//    int animatedDistance =40+endPositionText-KeyboardY;
//    const int movementDistance = animatedDistance;
//    const float movementDuration = 0.3f;
//    int movement = -movementDistance;
//    movedHeight=-movement;
    
    
    
    //[UIView beginAnimations: @"anim" context: nil];
    //[UIView setAnimationBeginsFromCurrentState: YES];
    //[UIView setAnimationDuration: movementDuration];
    //self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    _bottomConstraintTabBar.constant=KeyboardY;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        if (screenSize.height == 812.0f)
            _bottomConstraintTabBar.constant=KeyboardY-33;
    }
    //[UIView commitAnimations];
    
}
-(void)keyboardWillHide:(NSNotification*)notification{
//    if (textFieldEditable==nil) {
//        return;
//    }
//    const float movementDuration = 0.3f;
//    int movement =movedHeight;
    
    
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    //[UIView setAnimationDuration: movementDuration];
    //self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    _bottomConstraintTabBar.constant=0;
    [UIView commitAnimations];
    textFieldEditable=nil;
    
}
- (void) hideKeyboard{
    [self.view endEditing:YES];
}
- (void)openPrediagnosticView {
    preDiagnosticView=[[UIView alloc] init];
    preDiagnosticView=[[[NSBundle mainBundle] loadNibNamed:@"PreDiagnosticView" owner:self options:nil] objectAtIndex:0];
    [preDiagnosticView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    [self.view addSubview:preDiagnosticView];

    [_txtfieldPreDiagnostic becomeFirstResponder];
    

}
- (IBAction)backPrediagnostic:(id)sender {
    [preDiagnosticView removeFromSuperview];
}
- (IBAction)savePrediagnostic:(id)sender{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        NSMutableDictionary *dictConsultation=[[NSMutableDictionary alloc] init];
        [dictConsultation setObject:self->_txtfieldPreDiagnostic.text forKey:@"diagnostic_impression"];
        [dictConsultation setObject:[presentialConsultation objectForKey:@"id"] forKey:@"id"];
       
        [invoke updateConsultationWithData:dictConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                    [self->preDiagnosticView removeFromSuperview];
                }];
            });
            
        }];
        
    }];
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark- Tab Expediente
-(void)showAge{
    
    if([[dictPatientPresential objectForKey:@"date_of_birth"] length]>0){
        if ([[self formatBirthDateReceivedToView:[dictPatientPresential objectForKey:@"date_of_birth"]] length]>0) {
            NSAttributedString *text=[[NSAttributedString alloc] initWithString:[self formatBirthDateReceivedToView:[dictPatientPresential objectForKey:@"date_of_birth"]] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 18.0f]}];
            
            SexyTooltip *greetingsTooltip = [[SexyTooltip alloc] initWithAttributedString:text sizedToView:_viewAge withPadding:UIEdgeInsetsMake(0,5,0,5) andMargin:UIEdgeInsetsMake(0, 0, 0, 0)];
            greetingsTooltip.color=[UIColor colorWithRed:0.094 green:0.510 blue:0.675 alpha:1.00];
            [greetingsTooltip presentFromView:_viewAge
                                       inView:firstView
                                   withMargin:0
                                     animated:YES];
            [greetingsTooltip dismissInTimeInterval:3];
        }
        
    }
    
}
-(void)changeHeight{
    changeHeightView=[[UIView alloc] init];
    changeHeightView=[[[NSBundle mainBundle] loadNibNamed:@"AddHeightView" owner:self options:nil] objectAtIndex:0];
    [changeHeightView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeHeightView addGestureRecognizer:tap2];
    [self.view addSubview:changeHeightView];
}
-(void)changeWeight{
    changeWeightView=[[UIView alloc] init];
    changeWeightView=[[[NSBundle mainBundle] loadNibNamed:@"AddWeightView" owner:self options:nil] objectAtIndex:0];
    [changeWeightView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeWeightView addGestureRecognizer:tap2];
    [self.view addSubview:changeWeightView];
}
-(void)changeTemp{
    changeTempView=[[UIView alloc] init];
    changeTempView=[[[NSBundle mainBundle] loadNibNamed:@"AddTempView" owner:self options:nil] objectAtIndex:0];
    [changeTempView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeTempView addGestureRecognizer:tap2];
    [self.view addSubview:changeTempView];
}
-(void)changePressureBlood{
    changeBloodPressureView=[[UIView alloc] init];
    changeBloodPressureView=[[[NSBundle mainBundle] loadNibNamed:@"AddBloodPressureView" owner:self options:nil] objectAtIndex:0];
    [changeBloodPressureView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeBloodPressureView addGestureRecognizer:tap2];
    [self.view addSubview:changeBloodPressureView];
}
-(void)changeCardiacFreq{
    changeHeartFreqView=[[UIView alloc] init];
    changeHeartFreqView=[[[NSBundle mainBundle] loadNibNamed:@"AddHeartFreqView" owner:self options:nil] objectAtIndex:0];
    [changeHeartFreqView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeHeartFreqView addGestureRecognizer:tap2];
    [self.view addSubview:changeHeartFreqView];
}
-(void)changeBreathFreq{
    changeBreathFreqView=[[UIView alloc] init];
    changeBreathFreqView=[[[NSBundle mainBundle] loadNibNamed:@"AddBreathtFreqView" owner:self options:nil] objectAtIndex:0];
    [changeBreathFreqView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeBreathFreqView addGestureRecognizer:tap2];
    [self.view addSubview:changeBreathFreqView];
}

-(void)changeBlood{
    changeBloodView=[[UIView alloc] init];
    changeBloodView=[[[NSBundle mainBundle] loadNibNamed:@"AddBloodType" owner:self options:nil] objectAtIndex:0];
    [changeBloodView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [changeBloodView addGestureRecognizer:tap2];
    [self.view addSubview:changeBloodView];
    arrayBloodTypes=[[NSMutableArray alloc] initWithArray:@[@"A+",@"B+",@"O+",@"AB+",@"A-",@"B-",@"O-",@"AB-",]];
    
    if ([arrayBloodTypes count]>0) {
        bloodPicker = [[UIPickerView alloc]init];
        bloodPicker.tintColor = [UIColor whiteColor];
        bloodPicker.dataSource = self;
        bloodPicker.delegate = self;
        [_txtfieldAddBlood setInputView:bloodPicker];
        UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
        toolBar.barStyle = UIBarStyleDefault;
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingBlood)];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        toolBar.tintColor = [UIColor darkGrayColor];
        [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
        _txtfieldAddBlood.inputAccessoryView = toolBar;
        [_txtfieldAddBlood becomeFirstResponder];
    }
}
- (IBAction)backAddBlood:(id)sender{
    [changeBloodView removeFromSuperview];
}
- (IBAction)saveAddBlood:(id)sender{
    if ([_txtfieldAddBlood.text length]<=0) {
        [self showAlertWithMessage:@"Selecciona un valor."];
        return;
    }
    NSMutableDictionary *dictBlood=[NSMutableDictionary new];
    [dictBlood setObject:_txtfieldAddBlood.text forKey:@"blood_type"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke2 updatePatientWithData:dictBlood WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error){
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                    [self->changeBloodView removeFromSuperview];
                    self->_lblBlood.text=[dictPatientPresential objectForKey:@"blood_type"];
                });
                
            }
        }
    }];
    
}
- (IBAction)backAddHeight:(id)sender{
    [changeHeightView removeFromSuperview];
}
- (IBAction)saveAddHeight:(id)sender{
    if ([_txtfieldAddHeight.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor."];
        return;
    }
    if ([_txtfieldAddHeight.text intValue]>=230||[_txtfieldAddHeight.text intValue]<=20) {
        [self showAlertWithMessage:@"Ingrese un valor válido."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] forKey:@"weight"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"weight"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] forKey:@"temperature"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] forKey:@"blood_pressure"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] forKey:@"cardiac_frequency"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
        }

    }
    else{
        [dictMetrics setObject:@"0" forKey:@"weight"];
    }
    
    [dictMetrics setObject:_txtfieldAddHeight.text forKey:@"height"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeHeightView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                     
                     }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
    
}

- (IBAction)backAddWeight:(id)sender{
    [changeWeightView removeFromSuperview];
}
- (IBAction)saveAddWeight:(id)sender{
    if ([_txtfieldAddWeight.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor"];
        return;
    }
    if ([_txtfieldAddWeight.text floatValue]>=250||[_txtfieldAddWeight.text floatValue]<=1) {
        [self showAlertWithMessage:@"Ingrese un valor válido."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] forKey:@"height"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"height"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] forKey:@"temperature"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] forKey:@"blood_pressure"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] forKey:@"cardiac_frequency"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
        }
    }
    else{
      [dictMetrics setObject:@"0" forKey:@"height"];
    }
    
    [dictMetrics setObject:_txtfieldAddWeight.text forKey:@"weight"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeWeightView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                    if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>4) {
                                        self->_lblWeight.text=[[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] substringToIndex:4];
                                    }
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                    
                }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
}

- (IBAction)backAddTemp:(id)sender{
    [changeTempView removeFromSuperview];
}
- (IBAction)saveAddTemp:(id)sender{
    if ([_txtfieldAddTemp.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
       
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] forKey:@"weight"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"weight"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] forKey:@"height"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"height"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] forKey:@"blood_pressure"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] forKey:@"cardiac_frequency"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
        }
        
    }
    else{
        [dictMetrics setObject:@"0" forKey:@"weight"];
        [dictMetrics setObject:@"0" forKey:@"height"];
    }
    
    
    [dictMetrics setObject:_txtfieldAddTemp.text forKey:@"temperature"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeTempView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                    
                }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
}

- (IBAction)backAddBloodPressure:(id)sender{
    [changeBloodPressureView removeFromSuperview];
}
- (IBAction)saveAddBloodPressure:(id)sender{
    if ([_txtfieldAddBloodPressure.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] forKey:@"weight"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"weight"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] forKey:@"height"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"height"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] forKey:@"temperature"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] forKey:@"cardiac_frequency"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
        }
        
    }
    else{
        [dictMetrics setObject:@"0" forKey:@"weight"];
        [dictMetrics setObject:@"0" forKey:@"height"];
    }
    
    [dictMetrics setObject:_txtfieldAddBloodPressure.text forKey:@"blood_pressure"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeBloodPressureView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                    
                }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
}

- (IBAction)backAddHeartFreq:(id)sender{
    [changeHeartFreqView removeFromSuperview];
}
- (IBAction)saveAddHeartFreq:(id)sender{
    if ([_txtfieldAddHearthFreq.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] forKey:@"weight"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"weight"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] forKey:@"height"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"height"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] forKey:@"temperature"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] forKey:@"blood_pressure"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
        }
        
    }
    else{
        [dictMetrics setObject:@"0" forKey:@"weight"];
        [dictMetrics setObject:@"0" forKey:@"height"];
    }
    
    [dictMetrics setObject:_txtfieldAddHearthFreq.text forKey:@"cardiac_frequency"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeHeartFreqView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                    
                }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
}

- (IBAction)backAddBreathFreq:(id)sender{
    [changeBreathFreqView removeFromSuperview];
}
- (IBAction)saveAddBreathFreq:(id)sender{
    if ([_txtfieldAddBreathFreq.text length]<=0) {
        [self showAlertWithMessage:@"Ingrese un valor."];
        return;
    }
    
    NSMutableDictionary *dictMetrics=[NSMutableDictionary new];
    if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
        
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] forKey:@"weight"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"weight"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] forKey:@"height"];
        }
        else{
            [dictMetrics setObject:@"0" forKey:@"height"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] forKey:@"temperature"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] forKey:@"blood_pressure"];
        }
        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] forKey:@"cardiac_frequency"];
        }
//        if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
//            [dictMetrics setObject:[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] forKey:@"breathing_frequency"];
//        }
        
    }
    else{
        [dictMetrics setObject:@"0" forKey:@"weight"];
        [dictMetrics setObject:@"0" forKey:@"height"];
    }
    
    [dictMetrics setObject:_txtfieldAddBreathFreq.text forKey:@"breathing_frequency"];
    [dictMetrics setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    InvokeService *invoke2=[[InvokeService alloc] init];
    
    [invoke2 addMetricsWithData:dictMetrics WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if (!error) {
            if ([[responseData allKeys] count]>0) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self->changeBreathFreqView removeFromSuperview];
                        if ([[dictPatientPresential objectForKey:@"latest_metrics"] isKindOfClass:[NSDictionary class]]) {
                            if ([[dictPatientPresential objectForKey:@"latest_metrics"] allKeys]>0) {
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"] length]>0) {
                                    self->_lblWeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"weight"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"] length]>0) {
                                    self->_lblHeight.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"height"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"] length]>0) {
                                    self->_lblTemp.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"temperature"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"] length]>0) {
                                    self->_lblBloodPressure.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"blood_pressure"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"] length]>0) {
                                    self->_lblHeartFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"cardiac_frequency"];
                                }
                                if ([[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"] length]>0) {
                                    self->_lblBreathFreq.text=[[dictPatientPresential objectForKey:@"latest_metrics"] objectForKey:@"breathing_frequency"];
                                }
                            }
                        }
                    });
                    
                }];
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
            }
        }
    }];
}


-(void)getHealthData{
    allergyArray=[NSMutableArray new];
    surgeryArray=[NSMutableArray new];
    transfusionArray=[NSMutableArray new];
    injuryArray=[NSMutableArray new];
    familiarArray=[NSMutableArray new];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getHealthCondition:[dictPatientPresential objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        NSMutableArray *arrayHealthConditions=[[NSMutableArray alloc] initWithArray:[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]]];
        for (NSMutableDictionary *dictHealth in arrayHealthConditions) {
            if([[dictHealth objectForKey:@"condition_type"] isEqualToString:@"allergy"]){
                [self->allergyArray addObject:dictHealth];
            }
            else if([[dictHealth objectForKey:@"condition_type"] isEqualToString:@"surgery"]){
                [self->surgeryArray addObject:dictHealth];
            }
            else if([[dictHealth objectForKey:@"condition_type"] isEqualToString:@"transfusion"]){
                [self->transfusionArray addObject:dictHealth];
            }
            else if([[dictHealth objectForKey:@"condition_type"] isEqualToString:@"injury"]){
                [self->injuryArray addObject:dictHealth];
            }
            else if([[dictHealth objectForKey:@"condition_type"] isEqualToString:@"predisposition"]){
                [self->familiarArray addObject:dictHealth];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self->allergyArray count]>0) {
                [self openAllergyTab];
                self->_btnYesAllerg.selected=YES;
                self->_btnNoAllerg.selected=NO;
            }else{
                self->_btnYesAllerg.selected=NO;
                self->_btnNoAllerg.selected=YES;
            }
            if ([self->surgeryArray count]>0) {
                [self openSurgeryTab];
                self->_btnYesSurgery.selected=YES;
                self->_btnNoSurgery.selected=NO;
            }else{
                self->_btnYesSurgery.selected=NO;
                self->_btnNoSurgery.selected=YES;
            }
            if ([self->transfusionArray count]>0) {
                [self openTransfuTab];
                self->_btnYesTransfusion.selected=YES;
                self->_btnNoTransfusion.selected=NO;
            }else{
                self->_btnYesTransfusion.selected=NO;
                self->_btnNoTransfusion.selected=YES;
            }
            if ([self->injuryArray count]>0) {
                [self openTraumatismTab];
                self->_btnYesTraumatism.selected=YES;
                self->_btnNoTraumatism.selected=NO;
            }else{
                self->_btnYesTraumatism.selected=NO;
                self->_btnNoTraumatism.selected=YES;
            }
            if ([self->familiarArray count]>0) {
                [self openFamiliarTab];
                self->_btnYesFamiliarBg.selected=YES;
                self->_btnNoFamiliarBg.selected=NO;
            }else{
                self->_btnYesFamiliarBg.selected=NO;
                self->_btnNoFamiliarBg.selected=YES;
            }
            
            [self->_tvAllerg reloadData];
            [self->_tvSurgery reloadData];
            [self->_tvTransfusion reloadData];
            [self->_tvTraumatism reloadData];
            [self->_tvFamiliarBg reloadData];
            
        });
        
    }];
}
-(void)closeAllergyTab{
    _layoutConstraintHeightAllerg.constant=60;
    [self.view layoutIfNeeded];
    [_btnAddAllerg setHidden:YES];
    [_tvAllerg setHidden:YES];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)closeSurgeryTab{
    _layoutConstraintHeightSurgery.constant=60;
    [self.view layoutIfNeeded];
    [_btnAddSurgery setHidden:YES];
    [_tvSurgery setHidden:YES];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)closeTransfuTab{
    _layoutConstraintHeightTransfu.constant=60;
    [self.view layoutIfNeeded];
    [_btnAddTransfusion setHidden:YES];
    [_tvTransfusion setHidden:YES];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)closeTraumatismTab{
    _layoutConstraintHeightTraumatism.constant=60;
    [self.view layoutIfNeeded];
    [_btnAddTraumatism setHidden:YES];
    [_tvTraumatism setHidden:YES];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)closeFamiliarTab{
    _layoutConstraintHeightFamiliar.constant=60;
    [self.view layoutIfNeeded];
    [_btnAddFamiliarBg setHidden:YES];
    [_tvFamiliarBg setHidden:YES];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)openAllergyTab{
    _layoutConstraintHeightAllerg.constant=190;
    [self.view layoutIfNeeded];
    [_btnAddAllerg setHidden:NO];
    [_tvAllerg setHidden:NO];
    //_layoutConstraintHeightAllerg.constant=_layoutConstraintHeightAllerg.constant-_layoutConstraintHeightTableAllerg.constant+(ROW_HIEGHT_HEALTH*[allergyArray count]);
   // _layoutConstraintHeightTableAllerg.constant=ROW_HIEGHT_HEALTH*[allergyArray count];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)openSurgeryTab{
    _layoutConstraintHeightSurgery.constant=190;
    [self.view layoutIfNeeded];
    [_btnAddSurgery setHidden:NO];
    [_tvSurgery setHidden:NO];
   
   // _layoutConstraintHeightSurgery.constant=_layoutConstraintHeightSurgery.constant-_layoutConstraintHeightTableSurgery.constant+(ROW_HIEGHT_HEALTH*[surgeryArray count]);
   // _layoutConstraintHeightTableSurgery.constant=ROW_HIEGHT_HEALTH*[surgeryArray count];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)openTransfuTab{
    _layoutConstraintHeightTransfu.constant=190;
    [self.view layoutIfNeeded];
    [_btnAddTransfusion setHidden:NO];
    [_tvTransfusion setHidden:NO];
    
   // _layoutConstraintHeightTransfu.constant=_layoutConstraintHeightTransfu.constant-_layoutConstraintHeightTableTransfu.constant+(ROW_HIEGHT_HEALTH*[transfusionArray count]);
   // _layoutConstraintHeightTableTransfu.constant=ROW_HIEGHT_HEALTH*[transfusionArray count];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)openTraumatismTab{
    _layoutConstraintHeightTraumatism.constant=190;
    [self.view layoutIfNeeded];
    [_btnAddTraumatism setHidden:NO];
    [_tvTraumatism setHidden:NO];
    
    //_layoutConstraintHeightTraumatism.constant=_layoutConstraintHeightTraumatism.constant-_layoutConstraintHeightTableTraumatism.constant+(ROW_HIEGHT_HEALTH*[injuryArray count]);
    //_layoutConstraintHeightTableTraumatism.constant=ROW_HIEGHT_HEALTH*[injuryArray count];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
-(void)openFamiliarTab{
    _layoutConstraintHeightFamiliar.constant=190;
    [self.view layoutIfNeeded];
    [_btnAddFamiliarBg setHidden:NO];
    [_tvFamiliarBg setHidden:NO];
    
    //_layoutConstraintHeightFamiliar.constant=_layoutConstraintHeightFamiliar.constant-_layoutConstraintHeightTableFamiliar.constant+(ROW_HIEGHT_HEALTH*[familiarArray count]);
    //_layoutConstraintHeightTableFamiliar.constant=ROW_HIEGHT_HEALTH*[familiarArray count];
    _layoutConstraintHeightSecondContainer.constant=_layoutConstraintHeightAllerg.constant+_layoutConstraintHeightSurgery.constant+_layoutConstraintHeightTransfu.constant+_layoutConstraintHeightTraumatism.constant+_layoutConstraintHeightFamiliar.constant;
}
- (IBAction)actionYesAllergy:(id)sender{
    _btnYesAllerg.selected=YES;
    _btnNoAllerg.selected=NO;
    [self openAllergyTab];
    [_scrollContainerExp scrollRectToVisible:CGRectMake(0, 180, 0, 0) animated:YES];
}
- (IBAction)actionYesSurgery:(id)sender{
    _btnYesSurgery.selected=YES;
    _btnNoSurgery.selected=NO;
    [self openSurgeryTab];
}
- (IBAction)actionYesTransfuc:(id)sender{
    _btnYesTransfusion.selected=YES;
    _btnNoTransfusion.selected=NO;
    [self openTransfuTab];
    [_scrollContainerExp setContentOffset:CGPointMake(0,self.transfusionView.frame.origin.y)];
    [_scrollContainerExp setContentOffset:CGPointMake(0,self.surgeryView.frame.origin.y)];
}
- (IBAction)actionYesTraumatism:(id)sender{
    _btnYesTraumatism.selected=YES;
    _btnNoTraumatism.selected=NO;
    [self openTraumatismTab];
    [_scrollContainerExp setContentOffset:CGPointMake(0,self.traumatismView.frame.origin.y)];
}
- (IBAction)actionYesFamiliar:(id)sender{
    _btnYesFamiliarBg.selected=YES;
    _btnNoFamiliarBg.selected=NO;
    [self openFamiliarTab];
    [_scrollContainerExp setContentOffset:CGPointMake(0,self.familiarView.frame.origin.y)];
}
-(void)deleteHealthFromArray:(NSMutableArray *)array{
    if ([array count]>0) {
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            for (NSDictionary *dictHealth in array) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke deleteHealthCondition:[dictHealth objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                    if ([dictHealth isEqual:[array lastObject]]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                [self getHealthData];
                            }];
                        });
                    }
                }];
            }
        }];
    }
}
- (IBAction)actionNoAllergy:(id)sender{
    _btnYesAllerg.selected=NO;
    _btnNoAllerg.selected=YES;
    [self closeAllergyTab];
    [self deleteHealthFromArray:allergyArray];
}
- (IBAction)actionNoSurgery:(id)sender{
    _btnYesSurgery.selected=NO;
    _btnNoSurgery.selected=YES;
    [self closeSurgeryTab];
    [self deleteHealthFromArray:surgeryArray];
}
- (IBAction)actionNoTransfuc:(id)sender{
    _btnYesTransfusion.selected=NO;
    _btnNoTransfusion.selected=YES;
    [self closeTransfuTab];
    [self deleteHealthFromArray:transfusionArray];
}
- (IBAction)actionNoTraumatism:(id)sender{
    _btnYesTraumatism.selected=NO;
    _btnNoTraumatism.selected=YES;
    [self closeTraumatismTab];
    [self deleteHealthFromArray:injuryArray];
}
- (IBAction)actionNoFamiliar:(id)sender{
    _btnYesFamiliarBg.selected=NO;
    _btnNoFamiliarBg.selected=YES;
    [self closeFamiliarTab];
    [self deleteHealthFromArray:familiarArray];
}
-(void)openAddHealth{
    addView=[[UIView alloc] init];
    addView=[[[NSBundle mainBundle] loadNibNamed:@"AddHealthConditionView" owner:self options:nil] objectAtIndex:0];
    [addView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addView addGestureRecognizer:tap2];
    switch (healthToAdd) {
        case healthAllergy:
            _lblTitleHealth.text=@"Alergia";
            break;
        case healthSurgery:
            _lblTitleHealth.text=@"Cirugía";
            break;
        case healthTransfusion:
            _lblTitleHealth.text=@"Transfusión";
            break;
        case healthTraumatism:
            _lblTitleHealth.text=@"Traumatismo";
            break;
        case healthFamiliar:
            _lblTitleHealth.text=@"Antecendente Familiar";
            break;
            
        default:
            break;
    }
    
    [self.view addSubview:addView];
}
- (IBAction)actionAddAllergy :(id)sender{
    healthToAdd=healthAllergy;
    [self openAddHealth];
    
}
- (IBAction)actionAddSurgery :(id)sender{
    healthToAdd=healthSurgery;
    [self openAddHealth];
}
- (IBAction)actionAddTransfuction :(id)sender{
    healthToAdd=healthTransfusion;
    [self openAddHealth];
}
- (IBAction)actionAddTraumatism :(id)sender{
    healthToAdd=healthTraumatism;
    [self openAddHealth];
}
- (IBAction)actionAddFamiliar :(id)sender{
    healthToAdd=healthFamiliar;
    [self openAddHealth];
}
- (IBAction)cancelAddHealth:(id)sender {
    [addView removeFromSuperview];
}

- (IBAction)actionAddHealth:(id)sender {
    if (_txtDescHealth.text>0) {
        NSString *type=@"";
        switch (healthToAdd) {
            case healthAllergy:
                type=@"allergy";
                break;
            case healthSurgery:
                type=@"surgery";
                break;
            case healthTransfusion:
                type=@"transfusion";
                break;
            case healthTraumatism:
                type=@"injury";
                break;
            case healthFamiliar:
                type=@"predisposition";
                break;
                
            default:
                break;
        }
        
        NSMutableDictionary *dictHealthCondition=[[NSMutableDictionary alloc] init];
        [dictHealthCondition setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
        [dictHealthCondition setObject:type forKey:@"condition_type"];
        [dictHealthCondition setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"start_date"];
        [dictHealthCondition setObject:_txtDescHealth.text forKey:@"description"];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [invoke addHealthConditionWithData:dictHealthCondition WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        [self->addView removeFromSuperview];
                        [self getHealthData];
                    }];
                });
            }];
        }];
        
        
    }
    
    
    
}
#pragma mark - tab Receta
-(void)getPrescriptionsWS{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getPrescriptionsOfConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
            //dispatch_async(dispatch_get_main_queue(), ^{
               // [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
            if ([responseData isKindOfClass:[NSDictionary class]]) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    /* Do UI work here */
                     [[SharedFunctions sharedInstance] removeLoadingView];
                });
               
                return;
            }
            if ([responseData count]<=0) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    /* Do UI work here */
                     [[SharedFunctions sharedInstance] removeLoadingView];
                });
               
                return;
            }
            self->prescriptionArray=[[NSMutableArray alloc] initWithArray:[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_tvPrescriptionsHistory reloadData];
                self->_layoutConstraintContainerHeight.constant=[self->prescriptionArray count]*50+20;
                self->_layoutConstraintTablePrescriptionHeight.constant=self->_layoutConstraintContainerHeight.constant;
                CGPoint bottomOffset = CGPointMake(0, [self->prescriptionArray count]*50-self->_scrollViewHistoryPrescription.frame.size.height+20);
                [self->_scrollViewHistoryPrescription setContentOffset:bottomOffset animated:YES];
            });
            
                    
                    InvokeService *invoke2=[[InvokeService alloc] init];
                    [invoke2 getHealthCondition:[dictPatientPresential objectForKey:@"id"] AndConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                        self->healthConditionDrugsArray=[[NSMutableArray alloc] initWithArray:[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]]];
                        
                        self->healthAndDrugsArray=[[NSMutableArray alloc] init];
                        for (NSDictionary *dictHealth in self->healthConditionDrugsArray) {
                            NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
                            [dictToAdd setObject:[dictHealth objectForKey:@"description"] forKey:@"name"];
                            [dictToAdd setObject:@"diagnose" forKey:@"type"];
                            [self->healthAndDrugsArray addObject:dictToAdd];
                            for (NSDictionary *dictDrug in self->drugsArray) {
                                if ([[[dictDrug objectForKey:@"healthCondition"] objectForKey:@"id"] intValue]==[[dictHealth objectForKey:@"id"] intValue]) {
                                    NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
                                    [dictToAdd setObject:[dictDrug objectForKey:@"name"] forKey:@"name"];
                                    [dictToAdd setObject:@"drug" forKey:@"type"];
                                    [self->healthAndDrugsArray addObject:dictToAdd];
                                }
                            }
                                                 
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
//                                if([healthConditionDrugsArray count]>0){
//                                    [_scrollRecetaItems setHidden:NO];
//                                }
                                [self->_tvPrescriptionItems reloadData];
                            }];
                        });
                    }];
                    
               // }];
            //});
        }];
    }];
}


- (IBAction)addDiagnose:(id)sender {
    addViewDiagnose=[[UIView alloc] init];
    addViewDiagnose=[[[NSBundle mainBundle] loadNibNamed:@"AddDiagnoseView" owner:self options:nil] objectAtIndex:0];
    [addViewDiagnose setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addViewDiagnose addGestureRecognizer:tap2];
    [addViewMedicament addSubview:addViewDiagnose];
}
- (IBAction)addDiagnoseInput:(id)sender{
    if (_txtDiagnoseInput.text.length>0) {
        NSMutableDictionary *dictHealthCondition=[[NSMutableDictionary alloc] init];
        [dictHealthCondition setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
        [dictHealthCondition setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
        [dictHealthCondition setObject:@"other" forKey:@"condition_type"];
        [dictHealthCondition setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"start_date"];
        [dictHealthCondition setObject:_txtDiagnoseInput.text forKey:@"description"];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [invoke addHealthConditionWithData:dictHealthCondition WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        [self->addViewDiagnose removeFromSuperview];
                        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
                            InvokeService *invoke=[InvokeService new];
                            [invoke getHealthCondition:[dictPatientPresential objectForKey:@"id"] AndConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                                self->healthConditionDrugsArray=[[NSMutableArray alloc] initWithArray:[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]]];
                                
                                self->healthAndDrugsArray=[[NSMutableArray alloc] init];
                                for (NSDictionary *dictHealth in self->healthConditionDrugsArray) {
                                    NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
                                    [dictToAdd setObject:[dictHealth objectForKey:@"description"] forKey:@"name"];
                                    [dictToAdd setObject:@"diagnose" forKey:@"type"];
                                    [self->healthAndDrugsArray addObject:dictToAdd];
                                    for (NSDictionary *dictDrug in self->drugsArray) {
                                        if ([[[dictDrug objectForKey:@"healthCondition"] objectForKey:@"id"] intValue]==[[dictHealth objectForKey:@"id"] intValue]) {
                                            NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
                                            [dictToAdd setObject:[dictDrug objectForKey:@"name"] forKey:@"name"];
                                            [dictToAdd setObject:@"drug" forKey:@"type"];
                                            [self->healthAndDrugsArray addObject:dictToAdd];
                                        }
                                    }
                                    
                                }
                            
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                        [self->_tvPrescriptionItems reloadData];
                                    }];
                                });
                            }];
                        }];
                    }];
                });
            }];
        }];
    }
    
}
- (IBAction)actionCloseDiagnose:(id)sender{
    [addViewDiagnose removeFromSuperview];
    
}
- (IBAction)addMedicine:(id)sender {
    if (_txtDiagnose.text.length<=0) {
        [self showAlertWithMessage:@"Selecciona la valoración médica."];
        return;
    }
    if (_txtNameMedicine.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa el nombre del medicamento."];
        return;
    }
    if (_txtDose.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa la dosis."];
        return;
    }
    if (_txtAdministration.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa la vía de administración."];
        return;
    }
    if (_txtFrequency.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa la frecuencia del medicamento."];
        return;
    }
    if (_txtDuration.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa la duración del tratamiento."];
        return;
    }
    if (!dictOfPrescription) {
        dictOfPrescription=[NSMutableDictionary new];
        [dictOfPrescription setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultationId"];
    }
    NSMutableDictionary *dictDrug=[NSMutableDictionary new];
    [dictDrug setObject:_txtNameMedicine.text forKey:@"name"];
    [dictDrug setObject:_txtDose.text forKey:@"dose"];
    [dictDrug setObject:_txtAdministration.text forKey:@"administration"];
    [dictDrug setObject:_txtFrequency.text forKey:@"frequency"];
    [dictDrug setObject:_txtDuration.text forKey:@"duration"];
    [dictDrug setObject:[healthConditionDrugsArray objectAtIndex:diagnoseSelected] forKey:@"healthCondition"];
    
    [drugsArray addObject:dictDrug];
    [dictOfPrescription setObject:drugsArray forKey:@"drugs"];
    [addViewMedicament removeFromSuperview];
    
    healthAndDrugsArray=[[NSMutableArray alloc] init];
    for (NSDictionary *dictHealth in healthConditionDrugsArray) {
        NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
        [dictToAdd setObject:[dictHealth objectForKey:@"description"] forKey:@"name"];
        [dictToAdd setObject:@"diagnose" forKey:@"type"];
        [healthAndDrugsArray addObject:dictToAdd];
        for (NSDictionary *dictDrug in drugsArray) {
            if ([[[dictDrug objectForKey:@"healthCondition"] objectForKey:@"id"] intValue]==[[dictHealth objectForKey:@"id"] intValue]) {
                NSMutableDictionary *dictToAdd=[[NSMutableDictionary alloc] init];
                [dictToAdd setObject:[dictDrug objectForKey:@"name"] forKey:@"name"];
                [dictToAdd setObject:@"drug" forKey:@"type"];
                [healthAndDrugsArray addObject:dictToAdd];
            }
        }
        
    }
    [_tvPrescriptionItems reloadData];

    
}
- (IBAction)addStudy:(id)sender{
    if (_txtNameStudy.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa el nombre del estudio."];
        return;
    }
    if (!dictOfPrescription) {
        dictOfPrescription=[NSMutableDictionary new];
        [dictOfPrescription setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultationId"];
    }
    if (studyToAdd==studyLab) {
        [labArray addObject:_txtNameStudy.text];
        [dictOfPrescription setObject:labArray forKey:@"arrayLab"];
    }
    else if (studyToAdd==studyGab){
        [gabineteArray addObject:_txtNameStudy.text];
        [dictOfPrescription setObject:gabineteArray forKey:@"arrayGab"];
    }
    
    [addViewStudy removeFromSuperview];
    
    [_tvPrescriptionItems reloadData];
    
}
- (IBAction)addObservation:(id)sender {
    if ([_textViewObservations.text isEqualToString:PLACEHOLDER_OBSERVATION]) {
        [self showAlertWithMessage:@"Ingresa la observación."];
        return;
    }
    if (!dictOfPrescription) {
        dictOfPrescription=[NSMutableDictionary new];
        [dictOfPrescription setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultationId"];
    }
    observationsArray=[NSMutableArray new];
    _textViewObservations.delegate=self;
    [observationsArray addObject:_textViewObservations.text];
    [dictOfPrescription setObject:observationsArray forKey:@"observations"];
    [addViewObservations removeFromSuperview];
    [_tvPrescriptionItems reloadData];
}
-(void)getHealthDataArray{
    InvokeService *invoke=[InvokeService new];
    [invoke getHealthCondition:[dictPatientPresential objectForKey:@"id"] AndConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        self->healthConditionDrugsArray=[[NSMutableArray alloc] initWithArray:responseData];
    }];
}
- (IBAction)tappedAdd:(id)sender {
    UIButton *btnPressed=(UIButton *)sender;
    int indexDiagnose=(int)btnPressed.tag;
    for (NSDictionary *dictDiagnose in healthConditionDrugsArray) {
        if ([[dictDiagnose objectForKey:@"description"] isEqualToString:[[healthAndDrugsArray objectAtIndex:indexDiagnose] objectForKey:@"name"]]) {
            diagnoseSelected=(int)[healthConditionDrugsArray indexOfObject:dictDiagnose];
        }
    }
    
//    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
//        InvokeService *invoke=[InvokeService new];
//        [invoke getHealthCondition:[dictPatientPresential objectForKey:@"id"] AndConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
//            healthConditionDrugsArray=[[NSMutableArray alloc] initWithArray:responseData];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                    addViewMedicament=[[UIView alloc] init];
                    addViewMedicament=[[[NSBundle mainBundle] loadNibNamed:@"AddMedicineView" owner:self options:nil] objectAtIndex:0];
                    [addViewMedicament setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
                    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                           initWithTarget:self
                           action:@selector(hideKeyboard)];
                    [addViewMedicament addGestureRecognizer:tap2];
                    [self.view addSubview:addViewMedicament];
                    
//                    pickerDiagnostic = [[UIPickerView alloc]init];
//                    pickerDiagnostic.tintColor = [UIColor whiteColor];
//                    pickerDiagnostic.dataSource = self;
//                    pickerDiagnostic.delegate = self;
//                    [_txtDiagnose setInputView:pickerDiagnostic];
//                    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
//                    toolBar.barStyle = UIBarStyleDefault;
//                    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingDiagnose)];
//                    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
//                    toolBar.tintColor = [UIColor darkGrayColor];
//                    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
//                    _txtDiagnose.inputAccessoryView = toolBar;
//                    _txtDiagnose.delegate=self;
    _txtDiagnose.text=[[healthAndDrugsArray objectAtIndex:indexDiagnose] objectForKey:@"name"];
    [_txtDiagnose setEnabled:NO];
//                }];
    
//            });
//        }];
//    }];
    

}
- (IBAction)tappedAddLab:(id)sender {
    addViewStudy=[[UIView alloc] init];
    addViewStudy=[[[NSBundle mainBundle] loadNibNamed:@"AddStudyView" owner:self options:nil] objectAtIndex:0];
    [addViewStudy setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addViewStudy addGestureRecognizer:tap2];
    studyToAdd=studyLab;
    _lblTitleStudy.text=@"Estudios de Laboratorio";
    [self.view addSubview:addViewStudy];
    [_txtNameStudy becomeFirstResponder];

}
- (IBAction)tappedAddGabinete:(id)sender {
    addViewStudy=[[UIView alloc] init];
    addViewStudy=[[[NSBundle mainBundle] loadNibNamed:@"AddStudyView" owner:self options:nil] objectAtIndex:0];
    [addViewStudy setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addViewStudy addGestureRecognizer:tap2];
    studyToAdd=studyGab;
    _lblTitleStudy.text=@"Estudios de Gabinete";
    [self.view addSubview:addViewStudy];
    [_txtNameStudy becomeFirstResponder];

}
- (IBAction)tappedAddObservations:(id)sender {
    addViewObservations=[[UIView alloc] init];
    addViewObservations=[[[NSBundle mainBundle] loadNibNamed:@"AddObservationsView" owner:self options:nil] objectAtIndex:0];
    [addViewObservations setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [addViewObservations addGestureRecognizer:tap2];
    [self.view addSubview:addViewObservations];
    _textViewObservations.delegate=self;
    _textViewObservations.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    _textViewObservations.layer.cornerRadius=10;
    _textViewObservations.layer.borderWidth=1;
    [_textViewObservations becomeFirstResponder];
}
- (IBAction)actionBackAddItemPrescription:(id)sender{
    if ([[self.view subviews] containsObject:addViewMedicament]||[[self.view subviews] containsObject:addViewStudy]||[[self.view subviews] containsObject:addViewObservations]) {
        [addViewMedicament removeFromSuperview];
        [addViewStudy removeFromSuperview];
        [addViewObservations removeFromSuperview];
    }
}
- (IBAction)actionCheckSign:(id)sender{
    _btnCheckSign.selected=!_btnCheckSign.selected;
}
-(void)saveCompletePrescription{
    if ([drugsArray count]==0&&[labArray count]==0&&[gabineteArray count]==0&&[observationsArray count]==0) {
        [self showAlertWithMessage:@"Añade medicamentos o estudios médicos para generar la orden de tratamiento"];
        return;
    }
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        NSMutableArray *completeMedicalStudies=[NSMutableArray new];
        for (NSString *nameStudy in self->labArray) {
            NSMutableDictionary *dictMedical=[NSMutableDictionary new];
            [dictMedical setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
            [dictMedical setObject:nameStudy forKey:@"name"];
            [dictMedical setObject:@"laboratory_study" forKey:@"study_type"];
            //[dictMedical setObject:nameStudy forKey:@"description"];
            [dictMedical setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
            [completeMedicalStudies addObject:dictMedical];
        }
        for (NSString *nameStudy in self->gabineteArray) {
            NSMutableDictionary *dictMedical=[NSMutableDictionary new];
            [dictMedical setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
            [dictMedical setObject:nameStudy forKey:@"name"];
            [dictMedical setObject:@"imaging_study" forKey:@"study_type"];
            //[dictMedical setObject:nameStudy forKey:@"description"];
            [dictMedical setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
            [completeMedicalStudies addObject:dictMedical];
        }
        if ([completeMedicalStudies count]>0) {
            for (int i=0; i<[completeMedicalStudies count]; i++) {
                InvokeService *invoke2=[[InvokeService alloc] init];
                [invoke2 addMedicalStudyWithData:[completeMedicalStudies objectAtIndex:i] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    if (i==[completeMedicalStudies count]-1) {
                        [self generatePrescription];
                    }
                }];
            }
        }
        else{
            [self generatePrescription];
        }
        
    }];

}
-(void)generatePrescription{
    NSMutableDictionary *dictPrescription=[NSMutableDictionary new];
    [dictPrescription setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
    //[dictPrescription setObject:@"ninguna" forKey:@"observations"];
    if ([observationsArray count]>0) {
        [dictPrescription setObject:[observationsArray objectAtIndex:0] forKey:@"observations"];
    }
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke addPrescritionWithData:dictPrescription WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        if ([self->drugsArray count]>0) {
            NSMutableDictionary *dictPrescription=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
            for (int i=0; i<[self->drugsArray count]; i++) {
                
                NSMutableDictionary *dictTreatment=[NSMutableDictionary new];
                [dictTreatment setObject:[[[self->drugsArray objectAtIndex:i] objectForKey:@"healthCondition"]objectForKey:@"id"] forKey:@"health_condition"];
                [dictTreatment setObject:[[self->drugsArray objectAtIndex:i] objectForKey:@"administration"] forKey:@"administration"];
                [dictTreatment setObject:[[self->drugsArray objectAtIndex:i] objectForKey:@"name"] forKey:@"medicament"];
                [dictTreatment setObject:[[self->drugsArray objectAtIndex:i] objectForKey:@"dose"] forKey:@"dose"];
                [dictTreatment setObject:[dictPrescription objectForKey:@"id"] forKey:@"prescription"];
                [dictTreatment setObject:[[self->drugsArray objectAtIndex:i] objectForKey:@"frequency"] forKey:@"frequency"];
                [dictTreatment setObject:[[self->drugsArray objectAtIndex:i] objectForKey:@"duration"] forKey:@"duration"];
                
                InvokeService *invoke2=[[InvokeService alloc] init];
                [invoke2 addTreatmentWithData:dictTreatment WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    if (i==[self->drugsArray count]-1) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[SharedFunctions sharedInstance] removeLoadingView];
                            self->drugsArray=[NSMutableArray new];
                            self->labArray=[NSMutableArray new];
                            self->gabineteArray=[NSMutableArray new];
                            self->observationsArray=[NSMutableArray new];
                                dictOfPrescription=[NSMutableDictionary new];
                                [self getPrescriptionsWS];
                            [self->_tvPrescriptionItems reloadData];
                                [self sendPdfOfPrescription:[dictPrescription objectForKey:@"id"]];
                          //  }];
                        });
                    }
                }];
            }
            
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                self->drugsArray=[NSMutableArray new];
                self->labArray=[NSMutableArray new];
                self->gabineteArray=[NSMutableArray new];
                self->observationsArray=[NSMutableArray new];
                    dictOfPrescription=[NSMutableDictionary new];
                    [self getPrescriptionsWS];
                [self->_tvPrescriptionItems reloadData];
                    [self sendPdfOfPrescription:[dictPrescription objectForKey:@"id"]];
               // }];
            });
        }
    }];

}

-(void)sendEmailPdfOfPrescription:(NSString *)idPrescription{
        NSString *signature=@"false";
        if (_btnCheckSign.selected) {
            signature=@"true";
        }
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            InvokeService *invoke=[InvokeService new];
            [invoke downloadPdfPrescription:idPrescription AndSignature:signature WithCompletion:^(NSString * _Nullable path, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        [self sendEventInEmailWithPath:path];
                    }];
                });
            }];
        }];
}
-(void)sendPdfOfPrescription:(NSString *)idPrescription{
//    NSString *signature=@"false";
//    if (_btnCheckSign.selected) {
//        signature=@"true";
//    }
//    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
//        InvokeService *invoke=[InvokeService new];
//        [invoke downloadPdfPrescription:idPrescription AndSignature:signature WithCompletion:^(NSString * _Nullable path, NSError * _Nullable error) {
//           // dispatch_async(dispatch_get_main_queue(), ^{
//                //[[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
//                    [self sendEventInEmailWithPath:path];
//               // }];
//            //});
//        }];
//    }];
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Orden de tratamiento creada con éxito"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:NO completion:nil];
}
-(void)showWebViewPrescription{
    
}
- (IBAction)actionSendPrescription:(id)sender{
    [self saveCompletePrescription];
    
}
- (void)sendPrescription:(UIButton *) sender{
    int index=(int)sender.tag;
    [self sendEmailPdfOfPrescription:[[prescriptionArray objectAtIndex:index] objectForKey:@"id"]];
   // [self openViewer:[[prescriptionArray objectAtIndex:index] objectForKey:@"id"]];
}
- (void)openViewer:(NSString *)idPrescription {
    pdfViewer=[[UIView alloc] init];
    pdfViewer=[[[NSBundle mainBundle] loadNibNamed:@"PrescriptionViewer" owner:self options:nil] objectAtIndex:0];
    [pdfViewer setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    //UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
    //                                initWithTarget:self
    //                                action:@selector(hideKeyboard)];
   // [pdfViewer addGestureRecognizer:tap2];
    [self.view addSubview:pdfViewer];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/prescription/%@/pdf/",BASE_URL,idPrescription]];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc] initWithURL:url];
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"GET"];
    [_myWebView loadRequest:request];
    _myWebView.delegate=self;
    [[SharedFunctions sharedInstance] showLoadingView];
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[SharedFunctions sharedInstance] removeLoadingView];
}
#pragma mark - tab Notas
-(void)getNotes{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getNoteOfConsultation:[presentialConsultation objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        self->notesArray=[[NSMutableArray alloc] initWithArray:[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]]];
                        self->_tvNotes.dataSource=self;
                        self->_tvNotes.delegate=self;
                        [self->_tvNotes reloadData];
                    }];
                });
        }];
    }];
}
- (void)openNote:(NSString *)noteDesc {
    NotesViewerVC *noteViewer =(NotesViewerVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"NotesViewerVC"];
    [[SharedFunctions sharedInstance] topMostController].definesPresentationContext = YES;
    noteViewer.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    noteViewer.modalPresentationStyle= UIModalPresentationOverFullScreen;
    [[[SharedFunctions sharedInstance] topMostController] presentViewController:noteViewer animated:YES completion:nil];
    noteViewer.txtViewNote.text=noteDesc;
}
- (IBAction)closeNoteDesc:(id)sender{
    [noteViewer removeFromSuperview];
}
- (IBAction)saveNote:(id)sender {
    [self.view endEditing:YES];
    BOOL isPrivate=NO;
    if ([_checkNotePersonal isSelected]) {
        isPrivate=YES;
    }
    if ([_txtViewNote.text isEqualToString:PLACEHOLDER_NOTES]) {
        [self showAlertWithMessage:@"Ingresa la nota."];
        return;
    }
    //if (_txtViewNote.text>0 && ![_txtViewNote.text isEqualToString:PLACEHOLDER_NOTES]) {
        NSMutableDictionary *dictNote=[[NSMutableDictionary alloc] init];
        [dictNote setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
        [dictNote setObject:_txtViewNote.text forKey:@"text"];
        [dictNote setObject:[NSNumber numberWithBool:isPrivate]  forKey:@"is_private"];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [invoke addNoteConsultationWithData:dictNote WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        [self->addView removeFromSuperview];
                        [self getNotes];
                        self->_txtViewNote.text=PLACEHOLDER_NOTES;
                    }];
                });
            }];
        }];
    //}
    
}

- (IBAction)actionCheckPrivateNote:(id)sender {
    _checkNotePersonal.selected=!_checkNotePersonal.selected;
}
#pragma mark - Tab Historial
- (void)performAndWait:(void (^)(dispatch_semaphore_t semaphore))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    perform(semaphore);
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}
- (void)performAndWait2:(void (^)(dispatch_semaphore_t semaphore2))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);
    perform(semaphore2);
    dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_FOREVER);
}
-(void)getHistoric{
    historicArray=[NSMutableArray new];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        self->completeHistoricArray=[[NSMutableArray alloc] init];
        [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
            
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getConsultationsByPatientWithInfo:dictPatientPresential WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                self->historicArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:responseData] mutableCopy];
                NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"meeting_date" ascending:NO];
                NSArray *arraySorted = [self->historicArray sortedArrayUsingDescriptors:@[descriptor]];
                self->historicArray = [arraySorted mutableCopy];
                
                dispatch_semaphore_signal(semaphore2);
                
                
            }];
        }];
        
//        [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
//            if ([historicArray count]>0) {
//                for(int i=0;i<[historicArray count];i++) {
//                    [self performAndWait:^(dispatch_semaphore_t semaphore) {
//                        InvokeService *invoke3=[[InvokeService alloc] init];
//                        NSString *patientId=[dictPatientPresential objectForKey:@"id"];
//
//                        [invoke3 getHealthCondition:patientId AndConsultation:[[historicArray objectAtIndex:i] objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
//
//                            NSMutableDictionary *dictComplete2=[[NSMutableDictionary alloc] initWithDictionary:[historicArray objectAtIndex:i]];
//                            [dictComplete2 setObject:[[SharedFunctions sharedInstance]arrayByReplacingNullsWithBlanks:responseData] forKey:@"diagnoseArray"];
//
//                            [completeHistoricArray addObject:dictComplete2];
//                            dispatch_semaphore_signal(semaphore);
//                        }];
//                    }];
//                }
//                dispatch_semaphore_signal(semaphore2);
//            }
//            else{
//                dispatch_semaphore_signal(semaphore2);
//            }
//
//        }];
        [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                   // historicArray=completeHistoricArray;
                    [self->_tvHistoric reloadData];
                }];
                
            });
            dispatch_semaphore_signal(semaphore2);
        }];
        
        
    }];
}
-(NSString *)formatDateToHistoric:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yyyy"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
- (IBAction)openPrescription:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayPresc=[[historicArray objectAtIndex:index] objectForKey:@"prescription_set"];
    if ([arrayPresc count]>0) {
        if ([arrayPresc count]==1) {
            NSDictionary *dictPresc=[arrayPresc objectAtIndex:0];
            NSString *idPres=[dictPresc objectForKey:@"id"];
            [self openViewer:idPres];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige tu orden de tratamiento a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingPrescription) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            prescriptionListArray=[[NSMutableArray alloc] initWithArray:arrayPresc];
            
            pickerPrescriptionList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,60, frame.frame.size.width-20,100)];
            pickerPrescriptionList.tintColor = [UIColor whiteColor];
            pickerPrescriptionList.layer.cornerRadius=10;
            pickerPrescriptionList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerPrescriptionList.dataSource = self;
            pickerPrescriptionList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerPrescriptionList];
        }
    }
}
- (IBAction)openNotes:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayNotes=[[historicArray objectAtIndex:index] objectForKey:@"notes"];
    if ([arrayNotes count]>0) {
        if ([arrayNotes count]==1) {
            NSDictionary *dictnote=[arrayNotes objectAtIndex:0];
            NSString *textNote=[dictnote objectForKey:@"text"];
            [self openNote:textNote];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige la a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingNote) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            notesListArray=[[NSMutableArray alloc] initWithArray:arrayNotes];
            
            pickerNotesList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,80, frame.frame.size.width-20,100)];
            pickerNotesList.tintColor = [UIColor whiteColor];
            pickerNotesList.layer.cornerRadius=10;
            pickerNotesList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerNotesList.dataSource = self;
            pickerNotesList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerNotesList];
        }
    }
}
#pragma mark - ClosingDiagnose
- (IBAction)backClosingDiagnose:(id)sender {
    [diagnoseClosingView removeFromSuperview];
}

- (IBAction)actionNextClosingDiagnose:(id)sender {
    
    
    if (_txtClosingDiagnose.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa una valoración médica"];
        return;
    }
    if (_txtClosingPrognosis.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa un pronóstico"];
        return;
    }
    NSMutableDictionary *dictHealthCondition=[[NSMutableDictionary alloc] init];
    [dictHealthCondition setObject:[dictPatientPresential objectForKey:@"id"] forKey:@"patient"];
    [dictHealthCondition setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
    [dictHealthCondition setObject:@"other" forKey:@"condition_type"];
    [dictHealthCondition setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"start_date"];
    [dictHealthCondition setObject:_txtClosingDiagnose.text forKey:@"description"];
    
    
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        NSMutableDictionary *dictConsultation=[[NSMutableDictionary alloc] init];
        [dictConsultation setObject:self->_txtClosingPrognosis.text forKey:@"prognosis"];
        if (![self->_txtClosingExitReason.text isEqualToString:@"Pendiente"]) {
            [dictConsultation setObject:[[self->exitReasonArray objectAtIndex:self->exitTypeSelected] objectForKey:@"valueRequest"] forKey:@"exit_reason"];
        }
        [dictConsultation setObject:[presentialConsultation objectForKey:@"id"] forKey:@"id"];
        if (self->closeNoPayConsultation) {
            [dictConsultation setObject:@"closed" forKey:@"status"];
            [dictConsultation setObject:@"notrequired" forKey:@"payment_status"];
        }
        if ([self->arrayOfStatus count]>0) {
            if (self->statusIndexSelected<[self->arrayOfStatus count]) {
                NSString *status=[[self->arrayOfStatus objectAtIndex:self->statusIndexSelected] objectForKey:@"id"];
                [dictConsultation setObject:status forKey:@"light"];
            }
            
        }
        
        [invoke updateConsultationWithData:dictConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->closeNoPayConsultation) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                            needRefreshConsultations=YES;
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                    });
                    return;
                }
                
                if (self->_txtClosingDiagnose.enabled) {
                    InvokeService *invoke=[[InvokeService alloc] init];
                    [invoke addHealthConditionWithData:dictHealthCondition WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                [self showClosingPaymentView];
                                
                            }];
                        });
                    }];
                }
                else{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        [self showClosingPaymentView];
                        
                    }];
                }
                
            });
            
        }];
        
        
        
    }];
    
    
    
    
    
    
}
#pragma mark - view PaymentClosing
-(void)showClosingPaymentView{
    closingPaymentView=[[UIView alloc] init];
    closingPaymentView=[[[NSBundle mainBundle] loadNibNamed:@"PaymentView" owner:self options:nil] objectAtIndex:0];
    [closingPaymentView setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
    [self.view addSubview:closingPaymentView];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    [closingPaymentView addGestureRecognizer:tap2];
    _layoutHeightContainerResume.constant=0;
    _viewContainerResumePrice.hidden=YES;
    _lblNegativeAmount.hidden=YES;
    _viewContainerTotalPrice.layer.cornerRadius=5;
    _viewContainerTotalPrice.layer.borderWidth=1;
    _viewContainerTotalPrice.layer.borderColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor;
    _viewContainerPriceConsultation.layer.cornerRadius=5;
    _viewContainerPriceConsultation.layer.borderWidth=1;
    _viewContainerPriceConsultation.layer.borderColor=[UIColor colorWithRed:0.796 green:0.796 blue:0.796 alpha:1.00].CGColor;
    serviceArray=[NSMutableArray new];
    _tvAdditionalServices.dataSource=self;
    //_tvAdditionalServices.delegate=self;
    paymentTypeArray=[[NSMutableArray alloc] initWithArray:@[@"Efectivo",@"iZettle",@"Otro"]];
    pickerPaymentType = [[UIPickerView alloc]init];
    pickerPaymentType.tintColor = [UIColor whiteColor];
    pickerPaymentType.dataSource = self;
    pickerPaymentType.delegate = self;
    [_txtPaymentType setInputView:pickerPaymentType];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingPaymentType)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    toolBar.tintColor = [UIColor darkGrayColor];
    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _txtPaymentType.inputAccessoryView = toolBar;
    _txtPaymentType.delegate=self;
    _txtPriceConsult.delegate=self;
    _txtTotalCost.delegate=self;
}
- (IBAction)actionAddService:(id)sender {
    
    
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke getServicesWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedFunctions sharedInstance] removeLoadingView];
        if ([responseData count]>0) {
            self->addViewService=[[UIView alloc] init];
            self->addViewService=[[[NSBundle mainBundle] loadNibNamed:@"AddServiceView" owner:self options:nil] objectAtIndex:0];
            [self->addViewService setFrame:CGRectMake(0,0, self.viewContainer.frame.size.width, self.view.frame.size.height) ];
            UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(hideKeyboard)];
            [self->addViewService addGestureRecognizer:tap2];
            [self.view addSubview:self->addViewService];
            self->_txtPriceService.delegate=self;
            
            self->_txtPriceService.enabled=NO;
            
            
            self->serviceListArray=[[NSMutableArray alloc] initWithArray:responseData];
            
            
            self->pickerServiceList = [[UIPickerView alloc]init];
            self->pickerServiceList.tintColor = [UIColor whiteColor];
            self->pickerServiceList.dataSource = self;
            self->pickerServiceList.delegate = self;
            [self->_txtNameService setInputView:self->pickerServiceList];
            UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
            toolBar.barStyle = UIBarStyleDefault;
            UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingService)];
            UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            toolBar.tintColor = [UIColor darkGrayColor];
            [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
            self->_txtNameService.inputAccessoryView = toolBar;
            self->_txtNameService.delegate=self;
            [self->_txtNameService becomeFirstResponder];
        }
        else{
           // [addViewService removeFromSuperview];
            [self showAlertWithMessage:@"Agrega un servicio desde Mi Perfil"];
        }
        });
    }];
    
    
}

- (IBAction)actionCloseAddService:(id)sender {
    [addViewService removeFromSuperview];
}

- (IBAction)closeWebView:(id)sender {
    [pdfViewer removeFromSuperview];
}

- (IBAction)actionBackClosing:(id)sender {
    [closingPaymentView removeFromSuperview];
}

- (IBAction)actionAddServiceForm:(id)sender {
    [closingPaymentView endEditing:YES];
    if (_txtNameService.text.length<=0) {
        [self showAlertWithMessage:@"Ingresa el nombre del servicio"];
        return;
    }
    if ([_txtPriceService.text isEqualToString:@"$0.00"]) {
        [self showAlertWithMessage:@"Ingresa el precio del servicio"];
        return;
    }
    if (![_txtTotalCost.text isEqualToString:@"$0.00"]&&[serviceArray count]<=0) {
        _txtPriceConsult.text=_txtTotalCost.text;
    }
    NSMutableDictionary *dictService=[NSMutableDictionary new];
    [dictService setObject:_txtNameService.text forKey:@"name"];
    [dictService setObject:_txtPriceService.text forKey:@"price"];
    [serviceArray addObject:dictService];
    [addViewService removeFromSuperview];
    [_tvAdditionalServices reloadData];
    _layoutHeightContainerResume.constant=200;
    _viewContainerResumePrice.hidden=NO;
    _txtTotalCost.enabled=NO;
    [self calculateTotalAmount];
    
   
    
    
}

- (IBAction)actionCloseWithPayment:(id)sender {
    if ([_txtTotalCost.text isEqualToString:@"$0.00"]) {
        [self showAlertWithMessage:@"Ingresa los servicios prestados"];
        return;
    }
//    if(_txtPaymentType.text.length<=0){
//        [self showAlertWithMessage:@"Selecciona un método de pago"];
//        return;
//    }
    
        if ([serviceArray count]>0) {
            [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
                for (NSMutableDictionary *dict in self->serviceArray) {
                [dict setObject:[NSString stringWithFormat:@"%.02f",[self getFloatFromPriceText:[dict objectForKey:@"price"]]] forKey:@"price"];
            }
                for (int i=0; i<[self->serviceArray count]; i++) {
                NSDictionary *dictServiceInfo;
                    for (NSDictionary *dict in self->serviceListArray) {
                        if ([[dict objectForKey:@"name"] isEqualToString:[[self->serviceArray objectAtIndex:i] objectForKey:@"name"]]) {
                        dictServiceInfo=[[NSDictionary alloc] initWithDictionary:dict];
                        break;
                    }
                }
                if (dictServiceInfo ) {
                    NSMutableDictionary *dictToConsultation=[NSMutableDictionary new];
                    [dictToConsultation setObject:[dictServiceInfo objectForKey:@"id"] forKey:@"service"];
                    [dictToConsultation setObject:[dictServiceInfo objectForKey:@"price"] forKey:@"price"];
                    [dictToConsultation setObject:[presentialConsultation objectForKey:@"id"] forKey:@"consultation"];
                    InvokeService *invoke2=[[InvokeService alloc] init];
                    [invoke2 addServiceToConsultationWithData:dictToConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                        if (i==[self->serviceArray count]-1) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                    [self closeWithPayment];
                                }];
                            });
                        }
                        
                        
                    }];
                }
                
                
            }
        }];
        
        }
        else{
            [self closeWithPayment];
        }
    
    
}
-(void)closeWithPayment{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        NSMutableDictionary *dictConsultation=[[NSMutableDictionary alloc] init];
        [dictConsultation setObject:@"closed" forKey:@"status"];
        [dictConsultation setObject:[presentialConsultation objectForKey:@"id"] forKey:@"id"];
        
        if ([self->serviceArray count]>0) {
            [dictConsultation setObject:[NSString stringWithFormat:@"%.02f",[self getFloatFromPriceText:self->_txtPriceConsult.text]] forKey:@"base_cost"];
            [dictConsultation setObject:[NSString stringWithFormat:@"%.02f",[self getFloatFromPriceText:self->_txtTotalCost.text]-[self getFloatFromPriceText:self->_txtPriceConsult.text]] forKey:@"extra_cost"];
        }
        else{
            [dictConsultation setObject:[NSString stringWithFormat:@"%.02f",[self getFloatFromPriceText:self->_txtTotalCost.text]] forKey:@"base_cost"];
            [dictConsultation setObject:@"0.00" forKey:@"extra_cost"];
        }
        [dictConsultation setObject:@"card" forKey:@"payment_type"];
        if ([[presentialConsultation objectForKey:@"interaction_type"] isEqualToString:@"on-site"]) {
            [dictConsultation setObject:@"card" forKey:@"payment_type"];
        }
        
        [dictConsultation setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"payment_fulfilled_date"];
        [dictConsultation setObject:@"in_progess" forKey:@"payment_status"];
        [invoke updateConsultationWithData:dictConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            });
            
        }];
        
    }];
    
}
- (void)removeService:(id)sender {
    UIImageView *imageView=sender;
    int indexSelected=(int)imageView.tag;
    [serviceArray removeObjectAtIndex:indexSelected];
    if ([serviceArray count]==0) {
        _layoutHeightContainerResume.constant=0;
        _viewContainerResumePrice.hidden=YES;
        _txtTotalCost.enabled=YES;
        _txtTotalCost.text=_txtPriceConsult.text;
    }
    else{
        [self calculateTotalAmount];
    }
    [_tvAdditionalServices reloadData];
}
-(void)calculateTotalAmount{
    float consultPrice=[self getFloatFromPriceText:_txtPriceConsult.text];
    if ([serviceArray count]>0) {
        for (NSDictionary *dictService in serviceArray) {
            consultPrice+=[self getFloatFromPriceText:[dictService objectForKey:@"price"]];
        }
        _txtTotalCost.text=[self getFormatPriceFromFloat:consultPrice];
        float meditecComission=11.6;
        _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:meditecComission]];
        float conektaTax=(((consultPrice/100)*2.9)+2.5)*1.16;
        float totalTax=meditecComission+conektaTax;
        _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:totalTax]];
        float totalLessComissions=consultPrice-meditecComission-conektaTax;
        _lblTotalLessComissions.text=[self getFormatPriceFromFloat:totalLessComissions];
        if (totalLessComissions<0) {
            _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
            _txtTotalCost.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
            _lblNegativeAmount.hidden=NO;
            [_btnClosePayment setEnabled:NO];
        }
        else{
            _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
            _txtTotalCost.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
            _lblNegativeAmount.hidden=YES;
            [_btnClosePayment setEnabled:YES];
            if (consultPrice==0) {
                _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:0]];
                _lblTotalLessComissions.text=[self getFormatPriceFromFloat:0];
            }
        }
        
        
    }
}
-(void)calculateTotalAmountWithoutServices{
    float consultPrice=[self getFloatFromPriceText:_txtTotalCost.text];
    
    _txtTotalCost.text=[self getFormatPriceFromFloat:consultPrice];
    float meditecComission=11.6;
    float conektaTax=(((consultPrice/100)*2.9)+2.5)*1.16;
    float totalTax=meditecComission+conektaTax;
    _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:totalTax]];
    float totalLessComissions=consultPrice-meditecComission-conektaTax;
    _lblTotalLessComissions.text=[self getFormatPriceFromFloat:totalLessComissions];
    if (totalLessComissions<0) {
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
        _txtTotalCost.textColor=[UIColor colorWithRed:0.714 green:0.204 blue:0.149 alpha:1.00];
        _lblNegativeAmount.hidden=NO;
        [_btnClosePayment setEnabled:NO];
    }
    else{
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
        _txtTotalCost.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
        _lblNegativeAmount.hidden=YES;
        [_btnClosePayment setEnabled:YES];
        
    }
    if (consultPrice==0) {
        _lblComisionMeditec.text=[NSString stringWithFormat:@"- %@",[self getFormatPriceFromFloat:0]];
        _lblTotalLessComissions.text=[self getFormatPriceFromFloat:0];
        _lblTotalLessComissions.textColor=[UIColor colorWithRed:0.000 green:0.796 blue:0.980 alpha:1.00];
    }
    
}
-(float)getFloatFromPriceText:(NSString *)priceText{
    float amount=0;
    amount=[[[priceText substringFromIndex:1] stringByReplacingOccurrencesOfString:@"," withString:@""] floatValue];
    return amount;
}
-(NSString *)getFormatPriceFromFloat:(float)priceFloat{
    NSNumber *formatedValue;
    formatedValue = [[NSNumber alloc] initWithFloat:priceFloat];
    NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
    [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [_currencyFormatter stringFromNumber:formatedValue];
    //return [NSString stringWithFormat:@"$%.02f", priceFloat];;
}
#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([tableView isEqual:_tvPrescriptionItems]) {
        return 4;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_tvAllerg]) {
        return [allergyArray count];
    }
    else if ([tableView isEqual:_tvSurgery]) {
        return [surgeryArray count];
    }
    else if ([tableView isEqual:_tvTransfusion]) {
        return [transfusionArray count];
    }
    else if ([tableView isEqual:_tvTraumatism]) {
        return [injuryArray count];
    }
    else if ([tableView isEqual:_tvFamiliarBg]) {
        return [familiarArray count];
    }
    else if ([tableView isEqual:_tvNotes]) {
        return [notesArray count];
    }
    else if ([tableView isEqual:_tvHistoric]) {
        return [historicArray count];
    }
    else if ([tableView isEqual:_tvPrescriptionsHistory]) {
        return [prescriptionArray count];
    }
    else if ([tableView isEqual:_tvAdditionalServices]) {
        return [serviceArray count];
    }
    else if ([tableView isEqual:_tvPrescriptionItems]) {
        switch (section) {
            case tableSectionDrugs:
                return [healthAndDrugsArray count];
                break;
            case tableSectionLab:
                return [labArray count];
                break;
            case tableSectionGabinete:
                return [gabineteArray count];
                break;
            case tableSectionObservations:
                return [observationsArray count];
                break;
            default:
                break;
        }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ([tableView isEqual:_tvPrescriptionItems]) {
        return 60;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0,0)];
    if ([tableView isEqual:_tvPrescriptionItems]) {
        CGRect frame = tableView.frame;
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width,60)];
        [headerView setBackgroundColor:[UIColor whiteColor]];
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 300, 30)];
        title.font=[UIFont fontWithName:@"MyriadPro-Regular" size:15];
        title.textColor=[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];

        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 30, 30)];
        [headerView addSubview:imageView];
        
        UIView *separator=[[UIView alloc] initWithFrame:CGRectMake(0, 59, frame.size.width, 1)];
        [separator setBackgroundColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00]];

        UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-40, 15, 30,30)];
        [addButton setImage:[UIImage imageNamed:@"addIcon"] forState:UIControlStateNormal];
        

        switch (section) {
            case 0:{
                title.text =@"Valoraciones Médicas";
                [addButton addTarget:self action:@selector(actionAddDiagnose:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case 1:
                title.text =@"Estudios de Laboratorio";
                [addButton addTarget:self action:@selector(tappedAddLab:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 2:
                title.text =@"Estudios de Gabinete";
                [addButton addTarget:self action:@selector(tappedAddGabinete:) forControlEvents:UIControlEventTouchUpInside];
                break;
            case 3:
                title.text =@"Observaciones";
                [addButton addTarget:self action:@selector(tappedAddObservations:) forControlEvents:UIControlEventTouchUpInside];
                break;
            default:
                break;
        }
        [headerView addSubview:title];
        [headerView addSubview:addButton];
        [headerView addSubview:separator];
    }


    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if ([tableView isEqual:_tvAllerg]) {
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:14];
        cell.textLabel.text=[[allergyArray objectAtIndex:indexPath.row] objectForKey:@"description"];
    }
    else if ([tableView isEqual:_tvSurgery]) {
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:14];
        cell.textLabel.text=[[surgeryArray objectAtIndex:indexPath.row] objectForKey:@"description"];
        
    }
    else if ([tableView isEqual:_tvTransfusion]) {
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:14];
        cell.textLabel.text=[[transfusionArray objectAtIndex:indexPath.row] objectForKey:@"description"];
        
    }
    else if ([tableView isEqual:_tvTraumatism]){
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:14];
    cell.textLabel.text=[[injuryArray objectAtIndex:indexPath.row] objectForKey:@"description"];
        
    }
    else if ([tableView isEqual:_tvFamiliarBg]){
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:14];
        cell.textLabel.text=[[familiarArray objectAtIndex:indexPath.row] objectForKey:@"description"];
        
    }
    else if ([tableView isEqual:_tvNotes]){
        cell.backgroundColor=[UIColor clearColor];
        UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgTextField"]];
        [tempImageView setFrame:cell.frame];
        cell.backgroundView = tempImageView;
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:18];
        cell.textLabel.text=[[notesArray objectAtIndex:indexPath.row] objectForKey:@"text"];
        
    }
    else if ([tableView isEqual:_tvPrescriptionsHistory]){
        cell.backgroundColor=[UIColor clearColor];
        UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgTextField"]];
        [tempImageView setFrame:cell.frame];
        cell.backgroundView = tempImageView;
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:18];
        cell.textLabel.text=[NSString stringWithFormat:@"Orden de tratamiento %ld",indexPath.row+1];
        UIButton *btnSend=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [btnSend setImage:[UIImage imageNamed:@"btnSend"] forState:UIControlStateNormal];
        [btnSend addTarget:self action:@selector(sendPrescription:) forControlEvents:UIControlEventTouchUpInside];
        btnSend.tag = indexPath.row;
        cell.accessoryView=btnSend;
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] init]; gesture.cancelsTouchesInView = NO;
        [_tvPrescriptionsHistory addGestureRecognizer:gesture];
        
    }
    else if ([tableView isEqual:_tvAdditionalServices]){
        cell.textLabel.textColor=[UIColor colorWithRed:0.290 green:0.290 blue:0.290 alpha:1.00];
        cell.textLabel.font=[UIFont fontWithName:@"MYriadPro-Regular" size:18];
        cell.textLabel.text=[NSString stringWithFormat:@"    %@",[[serviceArray objectAtIndex:indexPath.row] objectForKey:@"name"]];
        UILabel *price=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        price.textAlignment=NSTextAlignmentRight;
        price.font=[UIFont fontWithName:@"MYriadPro-Regular" size:18];
        price.textColor=[UIColor colorWithRed:0.290 green:0.290 blue:0.290 alpha:1.00];
        price.text=[[serviceArray objectAtIndex:indexPath.row] objectForKey:@"price"];
        cell.accessoryView=price;
        
        UIButton *btnDelete=[[UIButton alloc] initWithFrame:CGRectMake(0, 8, 30, 30)];
        [btnDelete setImage:[UIImage imageNamed:@"lessIcon"] forState:UIControlStateNormal];
        [btnDelete addTarget:self action:@selector(removeService:) forControlEvents:UIControlEventTouchUpInside];
        btnDelete.tag = indexPath.row;
        [cell addSubview:btnDelete];
        
    }
    else if ([tableView isEqual:_tvHistoric]){
        static NSString *simpleTab = @"ConsultationPatientTVC";
        ConsultationPatientTVC *cell = (ConsultationPatientTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
            cell.lblDate.text=[self formatDateToHistoric:[[historicArray objectAtIndex:indexPath.row] objectForKey:@"meeting_date"]];
        cell.lblNamePatient.text=[[NSString stringWithFormat:@"DR. %@ %@ %@",[[[historicArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"first_name"],[[[historicArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"last_name"],[[[historicArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"second_last_name"]] uppercaseString];
        cell.lblDiagnose.text=@"No especificado";
        cell.lblDiagnose.text=@"";
        NSString *lblDiagnose=@"";
        cell.btnPill.enabled=NO;
        if ([[[historicArray objectAtIndex:indexPath.row] objectForKey:@"prescription_set"] count]>0) {
            cell.btnPill.enabled=YES;
            cell.btnPill.tag=indexPath.row;
            [cell.btnPill addTarget:self action:@selector(openPrescription:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[historicArray objectAtIndex:indexPath.row] objectForKey:@"notes"] count]>0) {
            cell.btnNotes.hidden=NO;
            cell.btnNotes.tag=indexPath.row;
            [cell.btnNotes addTarget:self action:@selector(openNotes:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[historicArray objectAtIndex:indexPath.row] objectForKey:@"healthcondition_set"] count]>0) {
            for (NSDictionary *dictDiagnose in [[historicArray objectAtIndex:indexPath.row] objectForKey:@"healthcondition_set"]) {
                lblDiagnose=[NSString stringWithFormat:@"%@,%@",lblDiagnose,[dictDiagnose objectForKey:@"description"]];
            }
            if (lblDiagnose.length>0) {
                lblDiagnose=[lblDiagnose substringFromIndex:1];
            }
            if (lblDiagnose.length>0&&[[lblDiagnose substringFromIndex:lblDiagnose.length-1] isEqualToString:@","]) {
                lblDiagnose=[lblDiagnose substringToIndex:lblDiagnose.length-1];
            }
            cell.lblDiagnose.text=[lblDiagnose uppercaseString];
        }
        if (lblDiagnose.length<=0) {
            cell.lblDiagnose.text=@"PENDIENTE";
        }
            cell.lblSuffering.text=[[historicArray objectAtIndex:indexPath.row] objectForKey:@"topic"];
        if ([[[[historicArray objectAtIndex:indexPath.row]objectForKey:@"doctor"] objectForKey:@"profile_pic"] length]>0) {
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getImageFromUrl:[[[historicArray objectAtIndex:indexPath.row]objectForKey:@"doctor"] objectForKey:@"profile_pic"]  WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        [cell.imgPatient setImage:imageDownloaded];
                        cell.imgPatient.contentMode=UIViewContentModeScaleAspectFill;
                        [self roundImage:cell.imgPatient];
                    }
                    
                });
                
            }];
        }
        return cell;
    }
    else if ([tableView isEqual:_tvPrescriptionItems]) {
        if (indexPath.section==tableSectionDrugs) {
            static NSString *simpleTableIdentifier = @"DrugAddTVC";
            
            DrugAddTVC *cell = (DrugAddTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DrugAddTVC" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.lblDesc.text=[[healthAndDrugsArray objectAtIndex:indexPath.row] objectForKey:@"name"];
                cell.btnAddDrug.tag=indexPath.row;
                [cell.btnAddDrug addTarget:self action:@selector(tappedAdd:) forControlEvents:UIControlEventTouchUpInside];
                if ([[[healthAndDrugsArray objectAtIndex:indexPath.row] objectForKey:@"type"] isEqualToString:@"drug"]) {
                    [cell.btnAddDrug setHidden:YES];
                    cell.layoutConstraintLeftMargin.constant=15;
                }
            }
            return cell;
        }
        static NSString *simpleTableIdentifier = @"PrescriptionTVC";
        
        PrescriptionTVC *cell = (PrescriptionTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PrescriptionTVC" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            if (indexPath.section==tableSectionDrugs) {
                cell.lblName.text=@"  MEDICAMENTO";
                cell.lblDesc.text=[[healthConditionDrugsArray objectAtIndex:indexPath.row] objectForKey:@"description"];
            }
            else if (indexPath.section==tableSectionLab) {
                cell.lblName.text=@"  ESTUDIO";
                cell.lblDesc.text=[labArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.section==tableSectionGabinete) {
                cell.lblName.text=@"  ESTUDIO";
                cell.lblDesc.text=[gabineteArray objectAtIndex:indexPath.row];
            }
            else if (indexPath.section==tableSectionObservations) {
                cell.lblName.text=@"  OBSERVACIÓN";
                cell.lblDesc.text=[observationsArray objectAtIndex:indexPath.row];
            }
        }
        
        return cell;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:_tvHistoric]) {
        return 100;
    }
    else if ([tableView isEqual:_tvPrescriptionItems]){
        return 30;
    }
    else if ([tableView isEqual:_tvPrescriptionsHistory]){
        return 50;
    }
    else if ([tableView isEqual:_tvNotes]){
        return 50;
    }
    return 30;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:_tvPrescriptionsHistory]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self openViewer:[[prescriptionArray objectAtIndex:indexPath.row] objectForKey:@"id"]];
    }
    else if ([tableView isEqual:_tvNotes]){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self openNote:[[notesArray objectAtIndex:indexPath.row] objectForKey:@"text"]];
    }
}
#pragma mark - CollectionView Methods
-(void)setFlowCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    float widthItem=80;
    [flowLayout setItemSize:CGSizeMake(widthItem, 80)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    [_collectionStatus.collectionViewLayout invalidateLayout];
    [_collectionStatus setCollectionViewLayout:flowLayout ];
    
}
-(NSInteger)numberOfSectionsInCollectionView:
(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrayOfStatus count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier2 = @"statusConsultationIdentifier";
    StatusConsultationCVC *cell2 = [collectionView
                         dequeueReusableCellWithReuseIdentifier:cellIdentifier2
                         forIndexPath:indexPath];
    if (!cell2) {
        cell2=[[StatusConsultationCVC alloc]init];
    }
    
    cell2.lblNameStatus.text =[[arrayOfStatus objectAtIndex:indexPath.row] objectForKey:@"description"];

    UIImage *originalImage = [UIImage imageNamed:@"circleBlack"];
    UIImage *tintedImage = [originalImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell2.imgCircleStatusColor setImage:tintedImage];
    cell2.imgCircleStatusColor.tintColor = [self colorFromHexString:[[arrayOfStatus objectAtIndex:indexPath.row] objectForKey:@"hexadecimal"]];
    cell2.viewBg.backgroundColor=[UIColor whiteColor];
    if (statusIndexSelected==(int)indexPath.row) {
        cell2.viewBg.backgroundColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    }
    return cell2;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    StatusConsultationCVC *selectedCell =
    (StatusConsultationCVC *)[collectionView cellForItemAtIndexPath:indexPath];
    statusIndexSelected=(int)indexPath.row;
    [_collectionStatus reloadData];
    //selectedCell.viewBg.backgroundColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    _lblStatus.text=selectedCell.lblNameStatus.text;
    }
#pragma mark - textfield Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isEqual:_txtDiagnose]) {
        if ([healthConditionDrugsArray count]==0) {
            [_txtNameStudy becomeFirstResponder];
            [self showAlertWithMessage:@"Agrega una valoración médica"];
            
        }
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtTotalCost]||[textField isEqual:_txtPriceConsult]||[textField isEqual:_txtPriceService]) {
        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSInteger centValue = [cleanCentString intValue];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        NSNumber *myNumber = [f numberFromString:cleanCentString];
        NSNumber *result;
        
        if([textField.text length] < 16){
            if (string.length > 0)
            {
                centValue = centValue * 10 + [string intValue];
                double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            else
            {
                centValue = centValue / 10;
                double intermediate = [myNumber doubleValue]/10;
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            
            myNumber = result;
            NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
            NSNumber *formatedValue;
            formatedValue = [[NSNumber alloc] initWithDouble:[myNumber doubleValue]/ 100.0f];
            
            if(formatedValue.doubleValue < 5000.01){
                NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
                [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                textField.text = [_currencyFormatter stringFromNumber:formatedValue];
                if ([textField isEqual:_txtPriceConsult]) {
                    [self calculateTotalAmount];
                }
                else if([textField isEqual:_txtTotalCost]){
                    [self calculateTotalAmountWithoutServices];
                }
            }else{
                [UIView animateWithDuration:0.1 animations:^(void){
                    [textField resignFirstResponder];
                } completion:^(BOOL finished) {
                    [self showAlertWithMessage:@"El monto máximo es de $5,000 pesos"];
                }];
                return NO;
                
                
            }
            return NO;
        }else{
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:@0];
            
            return NO;
        }
    }
    else{
        return YES;
    }
    
   
    return YES;
}
#pragma mark - textview Methods
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:PLACEHOLDER_NOTES]||[textView.text isEqualToString:PLACEHOLDER_OBSERVATION]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = PLACEHOLDER_NOTES;
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    if ([textView isEqual:_textViewObservations] && [textView.text isEqualToString:@""]) {
        textView.text=PLACEHOLDER_OBSERVATION;
    }
    [textView resignFirstResponder];
}
#pragma mark - Picker Methods
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if ([pickerView isEqual:pickerExitReasonsType]) {
        return [exitReasonArray count];
    }
    if ([pickerPaymentType isEqual:pickerView]) {
        return [paymentTypeArray count];
    }
    else if ([pickerView isEqual:pickerServiceList]){
        return [serviceListArray count];
    }
    else if ([pickerView isEqual:bloodPicker]){
        return [arrayBloodTypes count];
    }
    else if ([pickerView isEqual:pickerPrescriptionList]){
        return [prescriptionListArray count];
    }
    else if ([pickerView isEqual:pickerNotesList]) {
        return [notesListArray count];
    }
    return healthConditionDrugsArray.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if ([pickerPaymentType isEqual:pickerView]) {
        return paymentTypeArray[row];
    }
    else if ([pickerView isEqual:pickerServiceList]){
        return [[serviceListArray objectAtIndex:row] objectForKey:@"name"];
    }
    else if ([pickerView isEqual:pickerExitReasonsType]){
        NSString *completeName=[NSString stringWithFormat:@"%@",[[exitReasonArray objectAtIndex:row] objectForKey:@"title"]];
        return completeName;
    }
    else if ([pickerView isEqual:bloodPicker]){
        return arrayBloodTypes[row];
    }
    else if ([pickerView isEqual:pickerPrescriptionList]){
        NSString *title = [NSString stringWithFormat:@"Orden de tratamiento %ld",(long)row+1];;
        return title;
    }
    else if ([pickerView isEqual:pickerNotesList]) {
        NSString *title = [NSString stringWithFormat:@"Nota %ld",(long)row+1];;
        return title;
    }
    return [healthConditionDrugsArray[row] objectForKey:@"description"];
    
}
- (void) donePickingDiagnose {
    int row = (int) [pickerDiagnostic selectedRowInComponent:0];
    diagnoseSelected=row;
    _txtDiagnose.text =  [self pickerView:pickerDiagnostic titleForRow:row forComponent:0];
    [self hideKeyboard];
}
- (void) donePickingBlood {
    int row = (int) [bloodPicker selectedRowInComponent:0];
    _txtfieldAddBlood.text =  [self pickerView:bloodPicker titleForRow:row forComponent:0];
    [self hideKeyboard];
}
-(void)donePickingPaymentType{
    int row = (int) [pickerPaymentType selectedRowInComponent:0];
    paymentTypeSelected=row;
    _txtPaymentType.text =  [self pickerView:pickerPaymentType titleForRow:row forComponent:0];
    [self hideKeyboard];
}
-(void)donePickingService{
    int row = (int) [pickerServiceList selectedRowInComponent:0];
    serviceSelected=row;
    _txtNameService.text =  [[serviceListArray objectAtIndex:serviceSelected] objectForKey:@"name"];
    _txtPriceService.text =  [NSString stringWithFormat:@"$%@",[[serviceListArray objectAtIndex:serviceSelected] objectForKey:@"price"]];
    [self hideKeyboard];
}
- (void) donePickingPrescription {
    int row = (int) [pickerPrescriptionList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictPresc=[prescriptionListArray objectAtIndex:row];
    NSString *idPres=[dictPresc objectForKey:@"id"];
    [self openViewer:idPres];
    
}
- (void) donePickingNote {
    int row = (int) [pickerNotesList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictnote=[notesListArray objectAtIndex:row];
    NSString *textNote=[dictnote objectForKey:@"text"];
    [self openNote:textNote];
    
}
#pragma mark - Pdf and Mail
- (void) sendEventInEmailWithPath:(NSString *)nameReceta
{
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    [picker setToRecipients:@[[dictPatientPresential objectForKey:@"username"]]];
    picker.mailComposeDelegate =self;
    
    if(picker != nil && [MFMailComposeViewController canSendMail]){
        //NSString *paciente=[NSString stringWithFormat:@"%@ %@ %@",[dictPatientPresential objectForKey:@"first_name"],[dictPatientPresential objectForKey:@"last_name"],[dictPatientPresential objectForKey:@"second_last_name"]];

        
//        NSString *cedulaProfesional=@"";
//        if ([doctorInfo objectForKey:@"professional_card"]) {
//            cedulaProfesional = [doctorInfo objectForKey:@"professional_card"];
//        }
        [picker setSubject:@"Orden de tratamiento"];
        NSString *Title=@"Dr. ";
        if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
            Title=@"Dra. ";
        }
        NSString *emailBody=[NSString stringWithFormat:@"Imprime y presenta en tu farmacia la orden de tratamiento adjunta.\n\n*Archivo pdf adjunto\n\nSaludos,\n%@ %@\n\nNota: Es posible que en la farmacia te pidan pasar con su médico de guardia para validarla.",Title,[NSString stringWithFormat:@"%@ %@ %@",[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"],[doctorInfo objectForKey:@"second_last_name"]]];
        [picker setMessageBody:emailBody isHTML:NO];
        
        NSString *recetaPath =nameReceta;
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        if ([fileMgr fileExistsAtPath:recetaPath]){
            NSData * pdfData = [NSData dataWithContentsOfFile:recetaPath];
            NSString *Title=@"Dr. ";
            if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
                Title=@"Dra. ";
            }
            [picker addAttachmentData:pdfData mimeType:@"application/pdf" fileName:[NSString stringWithFormat:@"Receta%@%@%@.pdf",Title,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"]]];
            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                [self presentViewController:picker animated:true completion:nil];
            }];
        }else{
            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                [self showAlertWithMessage:@"No se ha podido guardar la orden de tratamiento "];
            }];
        }
        
        
    }else{
        [self showAlertWithMessage:@"Tu celular no cuenta con una cuenta de mail asociada por lo que no se puede mandar la orden de tratamiento. Asocia una cuenta a tu dispositivo para poder mandar la orden de tratamiento"];
    }
    
    
    
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    [controller dismissViewControllerAnimated:true completion:nil];

    [self dismissViewControllerAnimated:NO completion:nil];
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            //[self sendToApi];
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}
-(CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}
@end
