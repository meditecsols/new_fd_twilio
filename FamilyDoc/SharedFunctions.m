//
//  SharedFunctions.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "ConsultaPresencialVC.h"
#import "Reachability.h"
#import "NetMessageView.h"
#import "AppDelegate.h"
#import "HomeViewsContainerVC.h"
#import "VideoStartVC.h"
#import "Config.h"
#import "RMCustomViewActionController.h"
#import "HomePacientesVC.h"
#import "SuscriptionVC.h"


#define  DEFAULT_PHOTO_MAX_SIZE 500

@interface SharedFunctions(){
    NetMessageView *netView;
}
@property (strong,nonatomic) ConsultaPresencialVC *viewControllerCP;
@property (nonatomic) Reachability *internetReachability;
@end
@implementation SharedFunctions
@synthesize viewLoading,viewControllerCP;
+(SharedFunctions *)sharedInstance{
    
    static SharedFunctions *_sharedInstance =  nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[SharedFunctions alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init{
    self = [super init];
    if(self){
        
    }
    return self;
}
-(void)returnToHomeView{
    if (![[self topMostController] isKindOfClass:[HomeViewsContainerVC class]]) {
        [[self topMostController] dismissViewControllerAnimated:YES completion:^(void){
            [self returnToHomeView];
        }];
    }
}
-(void)goToPatientsView{
    if (![[self topMostController] isKindOfClass:[HomeViewsContainerVC class]]) {
        [[self topMostController] dismissViewControllerAnimated:YES completion:^(void){
            [self goToPatientsView];
        }];
    }
    else{
        HomeViewsContainerVC *homeContainer = (HomeViewsContainerVC *)[self topMostController];
        [homeContainer moveToSecondView];  
    }
}
-(void)goToPatientsViewWithPatientId:(int)idPatient{
    if (![[self topMostController] isKindOfClass:[HomeViewsContainerVC class]]) {
        [[self topMostController] dismissViewControllerAnimated:YES completion:^(void){
            [self goToPatientsViewWithPatientId:idPatient];
        }];
    }
    else{
        HomeViewsContainerVC *homeContainer = (HomeViewsContainerVC *)[self topMostController];
        [homeContainer moveToSecondView];
        for (UIViewController *vcChild in [homeContainer childViewControllers]) {
            if ([vcChild isKindOfClass:[HomePacientesVC class]]) {
                HomePacientesVC *pacientesVC=(HomePacientesVC *)vcChild;
                [pacientesVC openChatWithPatientId:idPatient];
            }
        }
       
    }
}
-(void)goToPatientsViewWithPatientName:(NSString *)namePatient{
    if (![[self topMostController] isKindOfClass:[HomeViewsContainerVC class]]) {
        [[self topMostController] dismissViewControllerAnimated:YES completion:^(void){
            [self goToPatientsViewWithPatientName:namePatient];
        }];
    }
    else{
        HomeViewsContainerVC *homeContainer = (HomeViewsContainerVC *)[self topMostController];
        [homeContainer moveToSecondView];
        for (UIViewController *vcChild in [homeContainer childViewControllers]) {
            if ([vcChild isKindOfClass:[HomePacientesVC class]]) {
                HomePacientesVC *pacientesVC=(HomePacientesVC *)vcChild;
                [pacientesVC openChatWithPatientName:namePatient];
            }
        }
        
    }
}
-(void)openChatWithPatientId:(int)idPatient{
    [self goToPatientsViewWithPatientId:idPatient];
   
    
}
-(void)openChatWithPatientName:(NSString *)namePatient{
    [self goToPatientsViewWithPatientName:namePatient];
}
-(UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

-(void)showLoadingView{
    UIViewController *top=[self topMostController];
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
    viewLoading=[mainstoryboard instantiateViewControllerWithIdentifier:@"LoadingVC"];
    viewLoading.providesPresentationContextTransitionStyle = YES;
    viewLoading.definesPresentationContext = YES;
    [viewLoading setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [top presentViewController:viewLoading animated:NO completion:nil];
}
-(void)showLoadingViewWithCompletition:(void (^)(void))completionBlock{
    UIViewController *top=[self topMostController];
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
    viewLoading=[mainstoryboard instantiateViewControllerWithIdentifier:@"LoadingVC"];
    viewLoading.providesPresentationContextTransitionStyle = YES;
    viewLoading.definesPresentationContext = YES;
    [viewLoading setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    if (![top isKindOfClass:[LoadingVC class]]) {
        [top presentViewController:viewLoading animated:NO completion:^{
            completionBlock();
        }];
    }else{
        completionBlock();
    }
   
    
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"FamilyDoc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [[self topMostController] presentViewController:alert animated:NO completion:nil];
}
-(void)showShareWithPatient{
  //  [self showLoadingView];
    NSString *url=@"";
    if (![[doctorInfo objectForKey:@"public_profile_preference"] boolValue]) {
       url=[NSString stringWithFormat:@"%@/redirect-app/patient/?doctor=%@",BASE_URL,[doctorInfo objectForKey:@"id"]];
    }
    else{
        url=[NSString stringWithFormat:@"%@/directorio/doctor/%@",BASE_URL,[doctorInfo objectForKey:@"full_name_slug"]];
    }
    [self showActivityWithUrl:url];
    //        InvokeService *invoke=[[InvokeService alloc] init];
    //        NSString *slashTagDoctor=[NSString stringWithFormat:@"InviteDoctor%@",[doctorInfo objectForKey:@"id"]];
    //        [invoke getShortUrlWithSlashTag:slashTagDoctor AndCompletition:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
    //            NSLog(@"response %@",responseData);
    //            if ([responseData isKindOfClass:[NSArray class]]) {
    //                if ([responseData count]>0) {
    //                    NSDictionary *dictOfUrlData=[[NSDictionary alloc] initWithDictionary:[responseData objectAtIndex:0]];
    //                    NSString *urlShort=[dictOfUrlData objectForKey:@"shortUrl"];
    //                    NSLog(@"shortUrl: %@",urlShort);
    //                    [self showActivityWithUrl:urlShort];
    //                }
    //                else{
    //                    InvokeService *invoke=[[InvokeService alloc] init];
    //                    [invoke postShortUrlWithUrl:url AndCompletition:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
    //                        NSLog(@"response %@",responseData);
    //                        if([responseData objectForKey:@"shortUrl"]){
    //                            if ([[responseData objectForKey:@"shortUrl"] length]>0) {
    //                                NSString *urlShort=[responseData objectForKey:@"shortUrl"];
    //                                NSLog(@"shortUrl: %@",urlShort);
    //                                [self showActivityWithUrl:urlShort];
    //                            }
    //                        }
    //                    }];
    //                }
    //            }
    //            else{
    //                InvokeService *invoke=[[InvokeService alloc] init];
    //                [invoke postShortUrlWithUrl:url AndCompletition:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
    //                    if([responseData objectForKey:@"shortUrl"]){
    //                        if ([[responseData objectForKey:@"shortUrl"] length]>0) {
    //                            NSString *urlShort=url;
    //                            //NSLog(@"shortUrl: %@",urlShort);
    //                            [self showActivityWithUrl:urlShort];
    //                        }
    //                        else{
    //                            dispatch_async(dispatch_get_main_queue(), ^{
    //                                [self removeLoadingView];
    //                                [self showAlertWithMessage:@"Error al generar link"];
    //                            });
    //                        }
    //                    }
    //                    else{
    //                        dispatch_async(dispatch_get_main_queue(), ^{
    //                            [self removeLoadingView];
    //                            [self showAlertWithMessage:@"Error al generar link"];
    //                        });
    //                    }
    //                }];
    //            }
    //        }];
}
-(void)showActivityWithUrl:(NSString *)url{
    
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getUrlsTermsPrivacyWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        NSString *urlAndroid;
        NSString *urlAppstore;
        if([responseData isKindOfClass:[NSArray class]]){
            for (NSDictionary *dict in responseData) {
                if ([[dict objectForKey:@"label"] isEqualToString:@"download_patient_android"]) {
                    urlAndroid=[dict objectForKey:@"url"];
                }
                else if ([[dict objectForKey:@"label"] isEqualToString:@"download_patient_ios"]){
                    urlAppstore=[dict objectForKey:@"url"];
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self removeLoadingViewWithCompletition:^{
                    NSString *prefix=@"Dr.";
                    if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
                        prefix= @"Dra.";
                    }
                    NSString *message=[NSString stringWithFormat:
                                       @"Hola, ahora podré cuidar tu salud sin importar donde me encuentre\n"
                                       @"\n"
                                       @"Descarga Family Doc y da clic en este link\n"
                                       @"\n"
                                       @"👉 %@ 👈\n"
                                       @"\n"
                                       @"Atte: %@ %@ %@\n"
                                       @"------------------------------\n"
                                       @"\n"
                                       @"Son tres pasos:\n"
                                       @"1. Da clic en mi link\n"
                                       @"2. Descarga la app FamilyDocPaciente y regístrate\n"
                                       @"3. Vuelve a dar clic en mi link para agregarme a tu lista de médicos\n"
                                       @"\n"
                                       @"👉 %@ 👈\n"
                                       @"\n"
                                       @"¡Descárgala ahora! 📲\n",url,prefix,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"],url];
                    NSArray *activityItems = @[message];
                    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
                    activityViewControntroller.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToWeibo, UIActivityTypeSaveToCameraRoll, UIActivityTypeCopyToPasteboard, UIActivityTypeAirDrop,UIActivityTypePostToTwitter,UIActivityTypeAssignToContact,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypeOpenInIBooks];
                    
                    [[self topMostController] presentViewController:activityViewControntroller animated:true completion:nil];
                }];
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showAlertWithMessage:@"Error al generar link"];
            });
        }
        
    }];
    
    
    

}
-(void)removeLoadingView{
    [viewLoading dismissViewControllerAnimated:NO completion:nil];
}
-(void)removeLoadingViewWithCompletition:(void (^)(void))completionBlock{
    if ([[self topMostController] isKindOfClass:[LoadingVC class]]) {
        [[self topMostController] dismissViewControllerAnimated:NO completion:^{
            completionBlock();
        }];
    }else{
        completionBlock();
    }
    
}
-(void)removeRegisterViews{
    if ([[UIApplication sharedApplication].keyWindow.rootViewController isKindOfClass:[HomeViewsContainerVC class]]) {
        
        [[self topMostController] dismissViewControllerAnimated:NO completion:^{
            [self removeRegisterViews];
        }];
        
    }
    else{
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
            UIViewController *home= [storyboard instantiateViewControllerWithIdentifier:@"HomeViewsContainerVC"];
            home.modalPresentationStyle= UIModalPresentationOverFullScreen;
            home.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [[self topMostController] presentViewController:home animated:YES completion:nil];

    }
}


-(void)saveUser:(NSString *)usr andPass:(NSString *)pass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:usr forKey:@"usr"];
    [def setObject:pass forKey:@"pass"];
    usr=usr;
    psw=pass;
    [def synchronize];
}
-(void)getUsrAndPass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    usr = [def valueForKey:@"usr"];
    psw =[def valueForKey:@"pass"];
}
-(void)saveUserData:(NSDictionary *)usrInfo{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:[self dictionaryByReplacingNullsWithStrings:usrInfo] forKey:@"usrInfo"];
    [def setObject:[self dictionaryByReplacingNullsWithStrings:accountInfo] forKey:@"accessToken"];
    [def synchronize];
}
-(void)getUsrInfo{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    doctorInfo=[def objectForKey:@"usrInfo"];
    accountInfo=[def objectForKey:@"accessToken"];
    
}
-(void)clearUsrAndPass{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:@"" forKey:@"usr"];
    [def setObject:@"" forKey:@"pass"];
    [def setObject:nil forKey:@"usrInfo"];
    usr=@"";
    psw=@"";
    doctorInfo=nil;
    accountInfo=nil;
    [def synchronize];
}
-(BOOL) isiPad{
    static BOOL isIPad = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isIPad = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
    });
    return isIPad;
}
-(NSString *)getCurrentStoryboard{
    NSString *nameStoryboard;
    if ([self isiPad]) {
        nameStoryboard =@"iPadStoryboard";
    }
    else{
        nameStoryboard = @"Main";
    }
    
    return nameStoryboard;
}
-(void)updatePushToken:(NSString *)token{
    if (!token) {
        return;
    }
    if ([token length]<=0) {
        return;
    }
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setObject:token forKey:@"push_token"];
    [dict setObject:[doctorInfo objectForKey:@"id"] forKey:@"id"];
    
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke updateDoctorWithData:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        NSLog(@"updating fcmToken: %@",responseData);
        
    }];
    
}
-(void)saveDeviceAndVersion{
    InvokeService *invoke4=[[InvokeService alloc] init];
    NSMutableDictionary *dictUser=[[NSMutableDictionary alloc] init];
    [dictUser setObject:[doctorInfo objectForKey:@"id"] forKey:@"id"];
    [dictUser setObject:@"ios" forKey:@"os"];
    [dictUser setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"version"];
    [invoke4 updateDoctorWithData:dictUser WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        NSLog(@"save device login");
    }];
}
-(NSString *)formatDateToWS:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
-(NSDictionary *) dictionaryByReplacingNullsWithStrings : (NSDictionary *) dictionary{
    @try {
        const NSMutableDictionary *replaced = [NSMutableDictionary dictionaryWithDictionary: dictionary];
        
        if([dictionary isKindOfClass:[NSArray class]]){
            return [replaced copy];
        }
        const id nul = [NSNull null];
        const NSString *blank = @"";
        
        for (NSString *key in dictionary) {
            const id object = [dictionary objectForKey: key];
            if (object == nul) {
                [replaced setObject: blank forKey: key];
            }
            else if ([object isKindOfClass: [NSDictionary class]]) {
                [replaced setObject: [self dictionaryByReplacingNullsWithStrings:object] forKey: key];
            }else if ([object isKindOfClass: [NSArray class]]){
                [replaced setObject:[self arrayByReplacingNullsWithBlanks:object] forKey:key];
            }
        }
        return [NSDictionary dictionaryWithDictionary: (NSDictionary *) replaced];
    } @catch (NSException *exception) {
        return [[NSDictionary alloc] init];
    }
   
}

-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array{
    @try {
        NSMutableArray *replaced = [array mutableCopy];
        if([array isKindOfClass:[NSDictionary class]]){
            return replaced;
        }
        
        const id nul = [NSNull null];
        const NSString *blank = @"";
        for (int idx = 0; idx < [replaced count]; idx++) {
            id object = [replaced objectAtIndex:idx];
            if (object == nul) [replaced replaceObjectAtIndex:idx withObject:blank];
            else if ([object isKindOfClass:[NSDictionary class]])
                [replaced replaceObjectAtIndex:idx withObject:[self dictionaryByReplacingNullsWithStrings:object]];
            else if ([object isKindOfClass:[NSArray class]])
                [replaced replaceObjectAtIndex:idx withObject:[self arrayByReplacingNullsWithBlanks: object]];
        }
        return [replaced copy];
    } @catch (NSException *exception) {
        return [[NSArray alloc] init];
    }
   
}
- (void)tappedOpenInfo{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"¿Deseas hablar con un asistente?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
        [alert dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alert addAction:noButton];
    UIAlertAction* siButton = [UIAlertAction
                               actionWithTitle:@"Si"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action)
                               {
        
        NSURL* mailURL = [NSURL URLWithString:@"https://api.whatsapp.com/send?phone=5215544668127&text=Hola,%20necesito%20ayuda%20con%20Family%20Doc%20Doctor"];
        if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
            [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:nil];
        }
        
    }];
    
    [alert addAction:siButton];
    [[self topMostController] presentViewController:alert animated:YES completion:nil];
}
#pragma mark QRReader
- (void)tappedQR {
//    if ([self verifyIfNeedShowSuscription]) {
//        [self showSuscriptionVCFromScanner:YES];
//        return;
//    }
    [self flowQROpen];
}
-(void)flowQROpen{
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:NO showTorchButton:NO];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [[self topMostController] presentViewController:vc animated:YES completion:NULL];
    }
    else {
        //device not supported
    }
}
-(void)flowRemoteConsultationOpen{
    if ([[dictPatientPresential objectForKey:@"non_self_creator"] intValue]>0) {
        [self verifyAvaliablePaymentsForPatient:[ NSString stringWithFormat:@"%d",[[dictPatientPresential objectForKey:@"id"] intValue]] comeFromScan:NO andParentId:[ NSString stringWithFormat:@"%d",[[dictPatientPresential objectForKey:@"non_self_creator"] intValue]] ];
    }
    else{
        [self verifyAvaliablePaymentsForPatient:[ NSString stringWithFormat:@"%d",[[dictPatientPresential objectForKey:@"id"] intValue]] comeFromScan:NO andParentId:nil];
    }
}
#pragma mark - QRCodeReader Delegate Methods
- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    //isReturningFromScan=YES;
    [reader stopScanning];
    
    NSArray *components=[result componentsSeparatedByString:@"-"];
    if ([components count]<2 || ![[components objectAtIndex:0] isEqualToString:@"FD"]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Escanea un código QR válido."
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Aceptar"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [[self topMostController] dismissViewControllerAnimated:true completion:nil];
                                   }];
        
        [alert addAction:noButton];
        [[self topMostController] dismissViewControllerAnimated:true completion:^{
            [[self topMostController] presentViewController:alert animated:YES completion:nil];
        }];
        
        return;
    }
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke addPatientRelationWithId:[components objectAtIndex:1] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        InvokeService *invoke2=[[InvokeService alloc] init];
        [invoke2 getPatientById:[components objectAtIndex:1] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            NSDictionary *dictFilter= [[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:[responseData copy]];
            if ([[dictFilter objectForKey:@"non_self_creator"] intValue]>0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                    
                    [self verifyAvaliablePaymentsForPatient:[components objectAtIndex:1] comeFromScan:YES andParentId:[dictFilter objectForKey:@"non_self_creator"]];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                    
                    [self verifyAvaliablePaymentsForPatient:[components objectAtIndex:1] comeFromScan:YES andParentId:nil];
                });
            }
        }];
       
        
    }];
   
}
-(void)verifyAvaliablePaymentsForPatient:(NSString *)idPatient comeFromScan:(BOOL)isPresentialConsultation andParentId:(nullable NSString *)parentId{
    [self showLoadingViewWithCompletition:^{
        InvokeService *invoke=[[InvokeService alloc] init];
        NSString *idPatientForPayment=idPatient;
        if (parentId) {
            idPatientForPayment=parentId;
        }
        [invoke getPatientAviliablePaymentsById:idPatientForPayment IsRemote:!isPresentialConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"response %@",responseData);
                
                NSMutableDictionary *responseDataFilter=[[NSMutableDictionary alloc] init];
                responseDataFilter =[[self dictionaryByReplacingNullsWithStrings:[responseData copy]] mutableCopy];
                [self removeLoadingViewWithCompletition:^{
                    NSMutableArray *options=[[NSMutableArray alloc] init];
                        if ([[responseDataFilter objectForKey:@"open_remote_consultations"] count]>0||[[responseDataFilter objectForKey:@"open_onsite_consultations"] count]>0) {
                            if ([[responseDataFilter objectForKey:@"open_remote_consultations"] count]>0) {
                                if (isPresentialConsultation==NO) {
                                    
                                    if ([self verifyIfDoctorHasConsultationsOpenWithArray:[responseDataFilter objectForKey:@"open_remote_consultations"]]) {
                                        InvokeService *invoke2=[[InvokeService alloc] init];
                                        [invoke2 getPatientProfileFromWS:idPatient WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if ([[dictPatientPresential allKeys] count]<=0) {
                                                    [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    return;
                                                }
                                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
                                                self->viewControllerCP = [storyboard instantiateViewControllerWithIdentifier:@"ConsultaPresencialVC"];
                                                if (!isPresentialConsultation) {
                                                    self->viewControllerCP.isRemote=YES;
                                                }
                                                
                                                if([[self topMostController] isKindOfClass:[LoadingVC class]]){
                                                    [[self topMostController] dismissViewControllerAnimated:NO completion:^{
                                                        self->viewControllerCP.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                        [[self topMostController] presentViewController:self->viewControllerCP animated:NO completion:nil];
                                                    }];
                                                }
                                                else{
                                                    self->viewControllerCP.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                    [[self topMostController]
                                                     presentViewController:self->viewControllerCP animated:NO completion:nil];
                                                }
                                                return;
                                            });
                                            
                                        }];
                                        
                                    }
                                    else{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            RMActionControllerStyle style = RMActionControllerStyleWhite;
                                            RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
                                            
                                            if ([[responseDataFilter objectForKey:@"free_consultations"] intValue]>0) {
                                                [options addObject:@"free"];
                                                RMAction *freeConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta Gratuita" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"free"];
                                                    }];
                                                    
                                                }];
                                                [actionController addAction:freeConsultation];
                                            }
                                            if ([[responseDataFilter objectForKey:@"familybusiness_consultations_left"] intValue]>0) {
                                                [options addObject:@"suscription"];
                                                RMAction *suscriptionConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta FamilyDoc Business" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"subscription"];
                                                    }];
                                                    
                                                }];
                                                [actionController addAction:suscriptionConsultation];
                                            }
                                            
                                            [options addObject:@"card"];
                                            RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta por cobrar" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                if ([responseDataFilter objectForKey:@"has_default_card"]) {
                                                    if ([[responseDataFilter objectForKey:@"has_default_card"] boolValue]) {
                                                        [actionController dismissViewControllerAnimated:NO completion:^{
                                                            [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"card"];
                                                        }];
                                                    }
                                                    else{
                                                        [actionController dismissViewControllerAnimated:NO completion:^{
                                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                        }];
                                                        
                                                    }
                                                }
                                                else{
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }];
                                                }
                                                
                                            }];
                                            [actionController addAction:payConsultation];
                                                
                                            if ([responseDataFilter objectForKey:@"no_cost"]) {
                                                if ([[responseDataFilter objectForKey:@"no_cost"] boolValue]) {
                                                    [options addObject:@"no_cost"];
                                                    RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Sin cobro" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                        [actionController dismissViewControllerAnimated:NO completion:^{
                                                            [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"no_cost"];
                                                        }];
                                                        
                                                    }];
                                                    [actionController addAction:payConsultation];
                                                }
                                            }
                                            
                                            if ([options count]>0) {
                                                RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
                                                    
                                                }];
                                                
                                                
                                                actionController.title = @"¿Cómo deseas abrir la consulta?";
                                                actionController.message = @"";
                                                
                                                [actionController addAction:cancelAction];
                                                
                                                [[self topMostController] presentViewController:actionController animated:YES completion:nil];
                                            }
                                            else{
                                                if ([responseDataFilter isKindOfClass:[NSDictionary class]]) {
                                                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                                        if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                            [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                            [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                        }
                                                        else{
                                                            [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                        }
                                                    }
                                                    else{
                                                        if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                            [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                        }
                                                        else{
                                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                        }
                                                    }
                                                    
                                                }
                                                else{
                                                    if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                        [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                    else{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                }
                                            }
                                        });
                                        
                                    }
                                    
                                    
                                }
                                else{
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        RMActionControllerStyle style = RMActionControllerStyleWhite;
                                        RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
                                        
                                        if ([[responseDataFilter objectForKey:@"free_consultations"] intValue]>0) {
                                            [options addObject:@"free"];
                                            RMAction *freeConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta Gratuita" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    
                                                    [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"free"];
                                                }];
                                                
                                            }];
                                            [actionController addAction:freeConsultation];
                                        }
                                        if ([[responseDataFilter objectForKey:@"familybusiness_consultations_left"] intValue]>0) {
                                            [options addObject:@"suscription"];
                                            RMAction *suscriptionConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta FamilyDoc Business" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"subscription"];
                                                }];
                                                
                                            }];
                                            [actionController addAction:suscriptionConsultation];
                                        }
                                       RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta por cobrar" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                            if ([responseDataFilter objectForKey:@"has_default_card"]) {
                                                if ([[responseDataFilter objectForKey:@"has_default_card"] boolValue]) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"card"];
                                                    }];
                                                }
                                                else{
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }];
                                                }
                                            }
                                            else{
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }];
                                            }
                                            
                                        }];
                                        [actionController addAction:payConsultation];
                                        if ([responseDataFilter objectForKey:@"no_cost"]) {
                                            if ([[responseDataFilter objectForKey:@"no_cost"] boolValue]) {
                                                [options addObject:@"no_cost"];
                                                RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Sin cobro" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"no_cost"];
                                                    }];
                                                    
                                                }];
                                                [actionController addAction:payConsultation];
                                            }
                                        }
                                        
                                        if ([options count]>0) {
                                            RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
                                                
                                            }];
                                            
                                            
                                            actionController.title = @"¿Cómo deseas abrir la consulta?";
                                            actionController.message = @"";
                                            
                                            [actionController addAction:cancelAction];
                                            
                                            [[self topMostController] presentViewController:actionController animated:YES completion:nil];
                                        }
                                        else{
                                            if ([responseDataFilter isKindOfClass:[NSDictionary class]]) {
                                                if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                                    if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                        [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                        [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    }
                                                    else{
                                                        [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    }
                                                }
                                                else{
                                                    if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                        [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                    else{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                }
                                                
                                            }
                                            else{
                                                if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                    [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }
                                                else{
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }
                                            }
                                        }
                                    });
                                    
                                }
                            }
                            else if ([[responseDataFilter objectForKey:@"open_onsite_consultations"] count]>0){
                                if (isPresentialConsultation==YES) {
                                    
                                    if ([self verifyIfDoctorHasConsultationsOpenWithArray:[responseDataFilter objectForKey:@"open_remote_consultations"]]) {
                                        InvokeService *invoke2=[[InvokeService alloc] init];
                                        [invoke2 getPatientProfileFromWS:idPatient WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if ([[dictPatientPresential allKeys] count]<=0) {
                                                    [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    return;
                                                }
                                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
                                                self->viewControllerCP = [storyboard instantiateViewControllerWithIdentifier:@"ConsultaPresencialVC"];
                                                self->viewControllerCP.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                if (!isPresentialConsultation) {
                                                    self->viewControllerCP.isRemote=YES;
                                                }
                                                
                                                if([[self topMostController] isKindOfClass:[LoadingVC class]]){
                                                    [[self topMostController] dismissViewControllerAnimated:NO completion:^{
                                                        [[self topMostController] presentViewController:self->viewControllerCP animated:NO completion:nil];
                                                    }];
                                                }
                                                else{
                                                    [[self topMostController] presentViewController:self->viewControllerCP animated:NO completion:nil];
                                                }
                                                return;
                                            });
                                            
                                        }];
                                        
                                    }
                                    
                                    
                                    
                                }
                                else{
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        RMActionControllerStyle style = RMActionControllerStyleWhite;
                                        RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
                                        
                                        if ([[responseDataFilter objectForKey:@"free_consultations"] intValue]>0) {
                                            [options addObject:@"free"];
                                            RMAction *freeConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta Gratuita" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    
                                                    [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"free"];
                                                }];
                                                
                                            }];
                                            [actionController addAction:freeConsultation];
                                        }
                                        if ([[responseDataFilter objectForKey:@"familybusiness_consultations_left"] intValue]>0) {
                                            [options addObject:@"suscription"];
                                            RMAction *suscriptionConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta FamilyDoc Business" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"subscription"];
                                                }];
                                                
                                            }];
                                            [actionController addAction:suscriptionConsultation];
                                        }
                                        RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta por cobrar" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                            if ([responseDataFilter objectForKey:@"has_default_card"]) {
                                                if ([[responseDataFilter objectForKey:@"has_default_card"] boolValue]) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"card"];
                                                    }];
                                                }
                                                else{
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }];
                                                }
                                            }
                                            else{
                                                [actionController dismissViewControllerAnimated:NO completion:^{
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }];
                                            }
                                            
                                        }];
                                        [actionController addAction:payConsultation];
                                        if ([responseDataFilter objectForKey:@"no_cost"]) {
                                            if ([[responseDataFilter objectForKey:@"no_cost"] boolValue]) {
                                                [options addObject:@"no_cost"];
                                                RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Sin cobro" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                                    [actionController dismissViewControllerAnimated:NO completion:^{
                                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"no_cost"];
                                                    }];
                                                    
                                                }];
                                                [actionController addAction:payConsultation];
                                            }
                                        }
                                        if ([options count]>0) {
                                            RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
                                                
                                            }];
                                            
                                            
                                            actionController.title = @"¿Cómo deseas abrir la consulta?";
                                            actionController.message = @"";
                                            
                                            [actionController addAction:cancelAction];
                                            
                                            [[self topMostController] presentViewController:actionController animated:YES completion:nil];
                                        }
                                        else{
                                            if ([responseDataFilter isKindOfClass:[NSDictionary class]]) {
                                                if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                                    if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                        [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                        [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    }
                                                    else{
                                                        [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                                    }
                                                }
                                                else{
                                                    if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                        [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                    else{
                                                        [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                    }
                                                }
                                                
                                            }
                                            else{
                                                if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                    [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }
                                                else{
                                                    [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                                }
                                            }
                                        }
                                    });
                                    
                                }
                            }
                            
                        }
                        else{
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if ([[responseDataFilter objectForKey:@"familybusiness_consultations_left"] intValue]>0) {
                                    if (isFDBusinessDoc) {
                                        [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"subscription"];
                                        return;
                                    }
                                    
                                }
                                RMActionControllerStyle style = RMActionControllerStyleWhite;
                                RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
                                
                                if ([[responseDataFilter objectForKey:@"free_consultations"] intValue]>0) {
                                    [options addObject:@"free"];
                                    RMAction *freeConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta Gratuita" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                        [actionController dismissViewControllerAnimated:NO completion:^{
                                            
                                            [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"free"];
                                        }];
                                        
                                    }];
                                    [actionController addAction:freeConsultation];
                                }
                                
                                RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Consulta por cobrar" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                    if ([responseDataFilter objectForKey:@"has_default_card"]) {
                                        if ([[responseDataFilter objectForKey:@"has_default_card"] boolValue]) {
                                            [actionController dismissViewControllerAnimated:NO completion:^{
                                                [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"card"];
                                            }];
                                        }
                                        else{
                                            [actionController dismissViewControllerAnimated:NO completion:^{
                                                [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                            }];
                                        }
                                    }
                                    else{
                                        [actionController dismissViewControllerAnimated:NO completion:^{
                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                        }];
                                    }
                                    
                                }];
                                [actionController addAction:payConsultation];
                                if ([responseDataFilter objectForKey:@"no_cost"]) {
                                    if ([[responseDataFilter objectForKey:@"no_cost"] boolValue]) {
                                        [options addObject:@"no_cost"];
                                        RMAction *payConsultation = [RMAction<UIView *> actionWithTitle:@"Sin cobro" style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
                                            [actionController dismissViewControllerAnimated:NO completion:^{
                                                [self createConsultationOfPatient:idPatient comeFromScan:isPresentialConsultation andPaymentSelection:@"no_cost"];
                                            }];
                                            
                                        }];
                                        [actionController addAction:payConsultation];
                                    }
                                }
                                
                                if ([options count]>0) {
                                    RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
                                        
                                    }];
                                    
                                    
                                    actionController.title = @"¿Cómo deseas abrir la consulta?";
                                    actionController.message = @"";
                                    
                                    [actionController addAction:cancelAction];
                                    
                                    [[self topMostController] presentViewController:actionController animated:YES completion:nil];
                                }
                                else{
                                    if ([responseDataFilter isKindOfClass:[NSDictionary class]]) {
                                        if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                            if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                            }
                                            else{
                                                [self showAlertWithMessage:@"Error al obtener datos del paciente"];
                                            }
                                        }
                                        else{
                                            if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                                [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                                [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                            }
                                            else{
                                                [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                            }
                                        }
                                        
                                    }
                                    else{
                                        if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                                            [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                        }
                                        else{
                                            [self showAlertWithMessage:@"Solicita a tu Paciente que registre alguna tarjeta de crédito o débito"];
                                        }
                                    }
                                }
                            });
                            
                                
                            
                        }
                    
                    
                    
                }];
                
                
            });
            
            
            
            
        }];
    }];
}
-(BOOL)verifyIfDoctorHasConsultationsOpenWithArray:(NSMutableArray *)arrayConsultions{
    BOOL hasOpen=NO;
    for (NSDictionary *dictConsultation in arrayConsultions) {
        if ([[dictConsultation objectForKey:@"doctor"] intValue] == [[doctorInfo objectForKey:@"id"] intValue]) {
            return YES;
        }
    }
    return hasOpen;
}
-(void)createConsultationOfPatient:(NSString *)idPatient comeFromScan:(BOOL)isPresentialConsultation andPaymentSelection:(NSString *)paymentType{
    [self showLoadingViewWithCompletition:^{
        InvokeService *invoke2=[[InvokeService alloc] init];
        NSMutableDictionary *dictConsultation=[[NSMutableDictionary alloc] init];
        [dictConsultation setObject:[self getDateFormatToWS] forKey:@"meeting_date"];
        [dictConsultation setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [dictConsultation setObject:idPatient forKey:@"patient"];
        [dictConsultation setObject:@"Consulta" forKey:@"topic"];
        [dictConsultation setObject:@"on-site" forKey:@"interaction_type"];
        [dictConsultation setObject:paymentType forKey:@"payment_type"];
        
        if (!isPresentialConsultation) {
            [dictConsultation setObject:@"remote" forKey:@"interaction_type"];
        }
        [dictConsultation setObject:@"test" forKey:@"device_session_id"];
        //  [dictConsultation setObject:@YES forKey:@"diagnosis_confirmed"];
        
        
        [invoke2  addAConsultationWithData:dictConsultation WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            comeFromScan=isPresentialConsultation;
            if ([[presentialConsultation allKeys] count]>1) {
                needRefreshConsultations=YES;
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getPatientProfileFromWS:idPatient WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self removeLoadingViewWithCompletition:^{
                           // [[self topMostController] dismissViewControllerAnimated:NO completion:^{
                                if ([[dictPatientPresential allKeys] count]>0) {
                                    needRefreshPatients=YES;
                                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
                                    self->viewControllerCP = [storyboard instantiateViewControllerWithIdentifier:@"ConsultaPresencialVC"];
                                    self->viewControllerCP.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                    if (!isPresentialConsultation) {
                                        self->viewControllerCP.isRemote=YES;
                                    }
                                    
                                    if([[self topMostController] isKindOfClass:[LoadingVC class]]){
                                        [[self topMostController] dismissViewControllerAnimated:NO completion:^{
                                            [[self topMostController] presentViewController:self->viewControllerCP animated:NO completion:^{
                                                [self->viewControllerCP openPrediagnosticView];
                                            }];
                                        }];
                                    }else{
//                                        if (!isPresentialConsultation) {
//                                            [[self topMostController].navigationController pushViewController:viewController animated:YES];
//                                            [viewController openPrediagnosticView];
//                                        }
//                                        else{
                                       
                                        [[self topMostController] presentViewController:self->viewControllerCP animated:NO completion:^{
                                                if (![presentialConsultation objectForKey:@"wasOpen"]) {
                                                    [self->viewControllerCP openPrediagnosticView];
                                                }
                                                
                                            }];
                                        //}
                                        
                                    }
                                    
                                    
                                }
                                else{
                                    
                                }
                          //  }];
                        }];
                    });
                }];
                
            }
            else{
            dispatch_async(dispatch_get_main_queue(), ^{
                  
               
                if ([[self topMostController] isKindOfClass:[QRCodeReaderViewController class]]) {
                    [(QRCodeReaderViewController *)[self topMostController ] startScanning];
                    [self showAlertWithMessage:@"Error al crear la consulta."];
                }
                else{
                    
                    [self removeLoadingViewWithCompletition:^{
                        [self showAlertWithMessage:@"Error al crear la consulta."];
                    }];
                    
                }
            });
                
            }
            
        }];
    }];
}
-(NSString *)getDateFormatToWS {
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setLocale:enUSPOSIXLocale];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:[NSDate date]];
    return dateFinal;
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [[self topMostController] dismissViewControllerAnimated:YES completion:NULL];
}
-(void)starReachabilityNotifier{
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self updateStatusReachability:self.internetReachability];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateStatusReachability:curReach];
}
- (void)updateStatusReachability:(Reachability *)reachability{
    
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    switch (netStatus)
    {
        case NotReachable:        {
            statusReachability=NO;
            [self showNoInternetBanner];
            break;
        }
            
        case ReachableViaWWAN:        {
            statusReachability=YES;
            [self removeInternetBanner];
            break;
        }
        case ReachableViaWiFi:        {
            statusReachability=YES;
            [self removeInternetBanner];
            break;
        }
    }
    
}
-(void)verifyInternetStatusForBanner{
    if (!statusReachability) {
        [self showNoInternetBanner];
    }
    else{
        [self removeInternetBanner];
    }
}
-(void)showNoInternetBanner{

        CGRect viewCordinates = CGRectMake(0,0,[self topMostController].view.frame.size.width,145);
        NSNumber *tag = [NSNumber numberWithInt:1];
        netView = [[NetMessageView alloc] initWithFrame:viewCordinates Message:@"No hay conexión a internet." Tag:tag Color:[UIColor colorWithRed:0.816 green:0.008 blue:0.106 alpha:0.80]];
    [netView initializeController];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:netView];
    
    
}
-(void)removeInternetBanner{
    [netView removeFromSuperview];
}
+ (UIImage *)scaleImageProportionally:(UIImage *)image {
    
    if (MAX(image.size.height, image.size.width) <= DEFAULT_PHOTO_MAX_SIZE) {
        return image;
    }
    else {
        CGFloat targetWidth = 0;
        CGFloat targetHeight = 0;
        if (image.size.height > image.size.width) {
            CGFloat ratio = image.size.height / image.size.width;
            targetHeight = DEFAULT_PHOTO_MAX_SIZE;
            targetWidth = roundf(DEFAULT_PHOTO_MAX_SIZE/ ratio);
        }
        else {
            CGFloat ratio = image.size.width / image.size.height;
            targetWidth = DEFAULT_PHOTO_MAX_SIZE;
            targetHeight = roundf(DEFAULT_PHOTO_MAX_SIZE/ ratio);
        }
        
        CGSize targetSize = CGSizeMake(targetWidth, targetHeight);
        
        UIImage *sourceImage = image;
        UIImage *newImage = nil;
        
        CGSize imageSize = sourceImage.size;
        CGFloat width = imageSize.width;
        CGFloat height = imageSize.height;
        
        targetWidth = targetSize.width;
        targetHeight = targetSize.height;
        
        CGFloat scaleFactor = 0.0;
        CGFloat scaledWidth = targetWidth;
        CGFloat scaledHeight = targetHeight;
        
        CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
        
        if (!CGSizeEqualToSize(imageSize, targetSize)) {
            
            CGFloat widthFactor = targetWidth / width;
            CGFloat heightFactor = targetHeight / height;
            
            if (widthFactor < heightFactor)
                scaleFactor = widthFactor;
            else
                scaleFactor = heightFactor;
            
            scaledWidth = roundf(width * scaleFactor);
            scaledHeight = roundf(height * scaleFactor);
            
            // center the image
            if (widthFactor < heightFactor) {
                thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
            } else if (widthFactor > heightFactor) {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
        
        UIGraphicsBeginImageContext(targetSize);
        
        CGRect thumbnailRect = CGRectZero;
        thumbnailRect.origin = thumbnailPoint;
        thumbnailRect.size.width = scaledWidth;
        thumbnailRect.size.height = scaledHeight;
        
        [sourceImage drawInRect:thumbnailRect];
        
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (newImage == nil) NSLog(@"could not scale image");
        
        return newImage;
    }
}
-(BOOL)verifyIfNeedShowSuscription{
    BOOL needToShowSuscription=NO;
    if (!isFDBusinessDoc) {
        if (!isSuscriptionValid) {
            if ([self check30daysRegisterWithDate:[doctorInfo objectForKey:@"date_joined"]]) {
                needToShowSuscription=YES;
            }
            
        }
    }
    return needToShowSuscription;
}
-(void)showSuscriptionVCFromScanner:(BOOL)comeFromScan{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[self getCurrentStoryboard] bundle:nil];
    SuscriptionVC *viewController = (SuscriptionVC *)[storyboard instantiateViewControllerWithIdentifier:@"SuscriptionVC"];
    viewController.comeFromScan=comeFromScan;
    [self topMostController].definesPresentationContext = YES;
    viewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [[self topMostController] presentViewController:viewController animated:YES completion:nil];
    
}
-(BOOL)check30daysRegisterWithDate:(NSString *)registerDate{
    BOOL hasMore30days=NO;
    NSString *shortDateString=[[registerDate componentsSeparatedByString:@"."] firstObject];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:shortDateString];
    NSDate *date_End=[NSDate date];
    NSCalendar *cal=[NSCalendar currentCalendar];
    NSDateComponents *components=[cal components:NSCalendarUnitDay fromDate:formattedDate toDate:date_End options:0];
    int days=(int)[components day];
    if(days>30){
        hasMore30days=YES;
    }
    
    return hasMore30days;
}
@end
