//
//  HomeAjustesVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 24/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface HomeAjustesVC : UIViewController
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTopHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblVersion;
@property (strong, nonatomic) IBOutlet UILabel *lblDev;

- (IBAction)actionOpenAbout:(id)sender;
- (IBAction)actionCloseSession:(id)sender;
- (IBAction)actionOpenQR:(id)sender;
- (IBAction)actionOpenPacientes:(id)sender;
- (IBAction)actionOpenConsultas:(id)sender;
- (IBAction)actionOpenInfo:(id)sender;
- (IBAction)actionOpenMyProfile:(id)sender;
@end
