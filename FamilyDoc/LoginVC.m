//
//  LoginVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 16/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "LoginVC.h"
#import "LanguageManagerSelector.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "Register2VC.h"
@interface LoginVC ()

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    _txtFieldPass.delegate=self;
    _txtFieldEmail.delegate=self;
    [_txtFieldEmail becomeFirstResponder];
//#ifndef NDEBUG
//    _txtFieldEmail.text=@"drm.gonzalez@meditec.com";
//    _txtFieldPass.text=@"holamundo246";
//#endif
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
}
-(void)dismissKeyboard {
    [_txtFieldEmail resignFirstResponder];
    [_txtFieldPass resignFirstResponder];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)validatePassWithString:(NSString *)pass{
    if (pass.length>0) {
        return YES;
    }
    return NO;
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    if (_txtFieldEmail.text.length<=0) {
        return;
    }
    if (_txtFieldPass.text.length<=0) {
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        return;
    }
    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionShowPass:(id)sender {
    if (self.txtFieldPass.secureTextEntry) {
        self.txtFieldPass.enabled = NO;
        self.txtFieldPass.secureTextEntry=NO;
        self.txtFieldPass.enabled = YES;
        [self.txtFieldPass becomeFirstResponder];
        [self.btnShowPass setTitle:@"OCULTAR CONTRASEÑA" forState:UIControlStateNormal];
    }else{
        self.txtFieldPass.enabled = NO;
        self.txtFieldPass.secureTextEntry=YES;
        self.txtFieldPass.enabled = YES;
        [self.txtFieldPass becomeFirstResponder];
        [self.btnShowPass setTitle:@"MOSTRAR CONTRASEÑA" forState:UIControlStateNormal];
    }
}


- (IBAction)actionShowTerms:(id)sender {
}

- (IBAction)actionNext:(id)sender {
    [self dismissKeyboard];
    if (_txtFieldEmail.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar un Email"];
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        [self showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    if (_txtFieldPass.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar una contraseña"];
        return;
    }
    InvokeService *invoke=[[InvokeService alloc] init];
    NSMutableDictionary *dict=[NSMutableDictionary new];
    [dict setObject:_txtFieldEmail.text forKey:@"usr"];
    [dict setObject:_txtFieldPass.text forKey:@"psw"];
    
    NSString *email=_txtFieldEmail.text;
    NSString *pss=_txtFieldPass.text;
    
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [invoke loginWithCompletion:dict WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                if ([[responseData objectForKey:@"client_id"] length]>0 && [[responseData objectForKey:@"client_secret"] length]>0) {
                   // [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
                        InvokeService *invoke2=[[InvokeService alloc] init];
                        [dict setObject:[responseData objectForKey:@"client_id"] forKey:@"client"];
                        [dict setObject:[responseData objectForKey:@"client_secret"] forKey:@"secret"];
                        [dict setObject:[responseData objectForKey:@"user_id"] forKey:@"user_id"];
                        [invoke2 tokenWithInfo:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                            
                                
                                    if ([[responseData objectForKey:@"access_token"] length]>0) {
                                        [dict setObject:responseData forKey:@"tokenInfo"];
                                        accountInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dict]];
                                        InvokeService *invoke3=[[InvokeService alloc] init];
                                        [invoke3 getDoctorData:[dict objectForKey:@"user_id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                                            if([[responseData allKeys] count]>20){
                                                doctorInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
                                                [[SharedFunctions sharedInstance] saveUserData:doctorInfo];
                                                [[SharedFunctions sharedInstance] saveUser:email andPass:pss];
                                                [[SharedFunctions sharedInstance] saveDeviceAndVersion];
                                                if ([[doctorInfo objectForKey:@"date_of_birth"] length]<=0) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        UIStoryboard *storyboard = self.storyboard;
                                                        UIViewController *registerNext= [storyboard instantiateViewControllerWithIdentifier:@"Register2VC"];
                                                        registerNext.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                                        registerNext.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                        [self presentViewController:registerNext animated:YES completion:nil];
                                                    });
                                                }
                                                else{
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        comeFromLogin=YES;
                                                        [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                                            UIStoryboard *storyboard = self.storyboard;
                                                            
                                                            UIViewController *next= [storyboard instantiateViewControllerWithIdentifier:@"HomeViewsContainerVC"];
                                                            next.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                            [self presentViewController:next animated:YES completion:^{
                                                                [[SharedFunctions sharedInstance] updatePushToken:fcmTokenRetrieved];
                                                            }];
                                                        }];
                                                    });
                                                }
                                                
                                                
                                            }
                                            else{
                                                if ([responseData isKindOfClass:[NSDictionary class]]) {
                                                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]) {
                                                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Por favor inicia sesión desde la app de paciente."];
                                                    }
                                                }
                                                else{
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al Iniciar Sesión"];
                                                    });
                                                }
                                            }
                                        }];
                                        
                                    }
                                    else{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [self showAlertWithMessage:@"Error al Iniciar Sesión"];
                                        });
                                    }
                            
                            
                            
                            
                        }];
                 //   }];
                    
                    
                }else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self showAlertWithMessage:@"Usuario o Contraseña incorrecto."];
                    });
                }
            }];
            });
            
            
            
            
        }];
    }];
    
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];

}
- (IBAction)actionForgetPass:(id)sender{
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
}
@end
