//
//  AppDelegate.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 16/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <StoreKit/StoreKit.h>
//#import <TwilioChatClient/TwilioChatClient.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver>

@property (strong, nonatomic) SKProductsRequest *productsRequest;
@property (strong, nonatomic) SKPaymentQueue *defaultQueue;

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSString *updatedPushToken;
@property (nonatomic, strong) NSDictionary *receivedNotification;
//@property (nonatomic, strong) TwilioChatClient *chatClient;


@end

