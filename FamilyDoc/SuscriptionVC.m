//
//  SuscriptionVC.m
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 8/7/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import "SuscriptionVC.h"
#import "NSString+Base64.h"
#import "Config.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"

@interface SuscriptionVC (){
    NSArray *validProducts;
}

@end

@implementation SuscriptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}
-(void)viewWillAppear:(BOOL)animated{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self fetchAvailableProducts];
    }];
    
}

- (IBAction)actionSuscribe:(id)sender{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Membresía Premium mensual \nFamily Doc"
                                 message:@"Política: Puedes cancelar en cualquier momento desde Configuración hasta un día antes de la fecha de renovación. \nEl plan se renueva automáticamente hasta que se cancele."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Confirmar"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action)
                               {
                                   //[alert dismissViewControllerAnimated:YES completion:nil];
                                   self.defaultQueue = [SKPaymentQueue defaultQueue];
                                   [self.defaultQueue addTransactionObserver:self];
                                   [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
                                       [self purchaseMyProduct:[self->validProducts objectAtIndex:0]];
                                   }];
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:NO completion:nil];
    
    
    
    
    
}
- (IBAction)actionLater:(id)sender{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"En caso de no querer unirte a la suscripción podrás continuar utilizando la versión gratuita con chat abierto para tener comunicación constante con tus pacientes."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* cancelAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   if ([[SharedFunctions sharedInstance] check30daysRegisterWithDate:[doctorInfo objectForKey:@"date_joined"]]) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                   }
                                   else{
                                       if (self.comeFromScan) {
                                           [self dismissViewControllerAnimated:NO completion:^{
                                               [[SharedFunctions sharedInstance] flowQROpen];
                                           }];
                                           
                                       }
                                       else{
                                           [self dismissViewControllerAnimated:NO completion:^{
                                               [[SharedFunctions sharedInstance] flowRemoteConsultationOpen];
                                           }];
                                           
                                       }
                                   }
                                   
                               }];
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Quiero Suscribirme"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:cancelAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:NO completion:nil];
    
   
}
- (IBAction)actionRestore:(id)sender{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }];
}

-(void)fetchAvailableProducts {
    if ([self canMakePurchases]) {
        NSSet *productIdentifiers = [NSSet
                                     setWithObjects:SUSCRIPTION_PRODUCT_ID,nil];
        self.productsRequest = [[SKProductsRequest alloc]
                                initWithProductIdentifiers:productIdentifiers];
        self.productsRequest.delegate = self;
        [self.productsRequest start];
    }
}

- (BOOL)canMakePurchases {
    return [SKPaymentQueue canMakePayments];
}

- (void)purchaseMyProduct:(SKProduct*)product {
    if ([self canMakePurchases]) {
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Las compras están desactivadas en tu dispositivo"];
    }
}

#pragma mark StoreKit Delegate

-(void)paymentQueue:(SKPaymentQueue *)queue
updatedTransactions:(NSArray *)transactions {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
            for (SKPaymentTransaction *transaction in transactions) {
                switch (transaction.transactionState) {
                    case SKPaymentTransactionStatePurchasing:{
                        NSLog(@"Purchasing");
                        break;
                    }
                    case SKPaymentTransactionStatePurchased:{
                        if ([transaction.payment.productIdentifier
                             isEqualToString:SUSCRIPTION_PRODUCT_ID]) {
                            NSLog(@"Purchased ");
                            isSuscriptionValid=YES;
                            UIAlertController * alert=  [UIAlertController
                                                         alertControllerWithTitle:@"Family Doc"
                                                         message:@"Suscripción realizada con éxito"
                                                         preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* okAction = [UIAlertAction
                                                       actionWithTitle:@"Entendido"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           if (self.comeFromScan) {
                                                               [self dismissViewControllerAnimated:NO completion:^{
                                                                   [[SharedFunctions sharedInstance] flowQROpen];
                                                               }];
                                                               
                                                           }
                                                           else{
                                                               [self dismissViewControllerAnimated:NO completion:^{
                                                                   [[SharedFunctions sharedInstance] flowRemoteConsultationOpen];
                                                               }];
                                                               
                                                           }
                                                           
                                                       }];
                            
                            [alert addAction:okAction];
                            [self presentViewController:alert animated:YES completion:nil];
                        }
                        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                        break;
                    }
                    case SKPaymentTransactionStateRestored:{
                        NSLog(@"Restored ");
                        isSuscriptionValid=YES;
                        UIAlertController * alert=  [UIAlertController
                                                     alertControllerWithTitle:@"Family Doc"
                                                     message:@"Tu compra se ha restaurado"
                                                     preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* okAction = [UIAlertAction
                                                   actionWithTitle:@"Entendido"
                                                   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action)
                                                   {
                                                       if (self.comeFromScan) {
                                                           [self dismissViewControllerAnimated:NO completion:^{
                                                               [[SharedFunctions sharedInstance] flowQROpen];
                                                           }];
                                                           
                                                       }
                                                       else{
                                                           [self dismissViewControllerAnimated:NO completion:^{
                                                               [[SharedFunctions sharedInstance] flowRemoteConsultationOpen];
                                                           }];
                                                           
                                                       }
                                                       
                                                   }];
                        
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                        break;
                    }
                    case SKPaymentTransactionStateFailed:{
                        NSLog(@"Purchase failed ");
                        [self dismissViewControllerAnimated:YES completion:nil];
                        break;
                    }
                    default:
                        break;
                }
            }
        }];
    });
    
}
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"error: %@",error.description);
}
-(void)productsRequest:(SKProductsRequest *)request
    didReceiveResponse:(SKProductsResponse *)response {
    [[SharedFunctions sharedInstance] removeLoadingView];
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    
    if (count>0) {
        validProducts = response.products;
        validProduct = [response.products objectAtIndex:0];
        
        if ([validProduct.productIdentifier
             isEqualToString:SUSCRIPTION_PRODUCT_ID]) {
            NSLog(@"Product Title: %@",validProduct.localizedTitle);
            NSLog(@"Product Desc: %@",validProduct.localizedDescription);
            NSLog(@"Product Price: %@",validProduct.price);
            _lblPrice.text=[NSString stringWithFormat:@"Membresía mensual $%@ %@ ",validProduct.price,[validProduct.priceLocale objectForKey:NSLocaleCurrencyCode]];

                                          // [self purchaseMyProduct:validProduct];
                                        
        }
    } else {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al obtener la suscripción"];
    }
    
    //purchaseButton.hidden = NO;
}
- (void)isValidReceiptForTransaction:(void (^_Nullable)(BOOL isValid, NSError * _Nullable error))completion{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    NSError *error=nil;
    if (!receipt) {
        completion(NO,error);
        return;
    }
    NSError *jsonError = nil;
    NSString *receiptBase64 = [NSString base64StringFromData:receipt length:(int)[receipt length]];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                receiptBase64,@"receipt-data",
                                                                SHARED_SECRET_SUSCRIPTION_KEY,@"password",
                                                                nil]
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonError
                        ];
    if (jsonError) {
        completion(NO,error);
        return;
    }
    NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (jsonError) {
        completion(NO,error);
        return;
    }
    NSLog(@"JSON: %@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:parsedData
                                                          options:0
                                                            error:&error];
    
    if (!requestData) {
        completion(NO,error);
        return;
    }
    
    NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
    if ([BASE_URL  isEqualToString:URL_SANDBOX]) {
        storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt" ];
    }
    
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:storeRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        completion(NO,error);
                                                        return;
                                                    } else {
                                                        
                                                        NSError *error;
                                                        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                        
                                                        if (!jsonResponse) {
                                                            completion(NO,error);
                                                            return;
                                                        }
                                                        if ([jsonResponse objectForKey:@"latest_receipt_info"]) {
                                                            if ([[jsonResponse objectForKey:@"latest_receipt_info"] count]>0) {
                                                                NSDictionary *dictSuscription=[[jsonResponse objectForKey:@"latest_receipt_info"] lastObject];
                                                                NSLog(@"suscription: %@",dictSuscription);
                                                                NSString *msTime=[dictSuscription objectForKey:@"expires_date_ms"];
                                                                BOOL isExpired=[self compareDate:msTime];
                                                                if (isExpired) {
                                                                    completion(NO,error);
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    completion(YES,error);
                                                }];
    [dataTask resume];
}
-(BOOL)compareDate:(NSString *)timestampString{
    BOOL isExpired= NO;
    double timestampDouble= [timestampString doubleValue];
    double timeStamp=timestampDouble/1000;
    NSTimeInterval seconds = timeStamp;
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    NSDate *today = [NSDate date];
    if ( [epochNSDate compare:today] == NSOrderedAscending) {
        isExpired=YES;
    }
    return isExpired;
}
-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue{
    NSLog(@"here");
}
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    NSLog(@"here");
}
- (IBAction)actionOpenChat:(id)sender{
    NSURL* mailURL = [NSURL URLWithString:@"https://api.whatsapp.com/send?phone=5215544668127&text=Hola,%20deseo%20m%C3%A1s%20informaci%C3%B3n%20respecto%20a%20la%20suscripci%C3%B3n%20PREMIUM%20para%20Family%20Doc"];
    if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
        [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:^(BOOL success) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }
}
@end
