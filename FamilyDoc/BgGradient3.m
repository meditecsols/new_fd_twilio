//
//  BgGradient3.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 27/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "BgGradient3.h"

@implementation BgGradient3

- (void)drawRect:(CGRect)rect {
    // Drawing code
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.039 green:0.498 blue:0.675 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.090 green:0.133 blue:0.302 alpha:1.00].CGColor];
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
