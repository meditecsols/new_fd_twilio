//
//  RegisterVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 05/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutTopView;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldCedula;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldSecondLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldPass;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPass;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollContent;
@property (strong, nonatomic) IBOutlet UIView *viewContainerScroll;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionShowPass:(id)sender;
- (IBAction)actionCheckBox:(id)sender;
- (IBAction)actionShowTerms:(id)sender;
- (IBAction)actionNext:(id)sender;
@end
