//
//  SharedFunctions.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 03/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LoadingVC.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "QRCodeReaderDelegate.h"



@interface SharedFunctions : NSObject<QRCodeReaderDelegate>
+(SharedFunctions *) sharedInstance;
@property(nonatomic,retain) LoadingVC *viewLoading;
-(UIViewController*)topMostController;
-(void)returnToHomeView;
-(void)showLoadingView;
-(void)removeLoadingView;
-(void)getUsrInfo;
-(void)saveUserData:(NSDictionary *)usrInfo;
-(void)clearUsrAndPass;
-(NSString *)formatDateToWS:(NSDate *)date ;
-(void)saveUser:(NSString *)usr andPass:(NSString *)pass;
-(void)getUsrAndPass;
-(NSString *)getCurrentStoryboard;
-(NSArray *)arrayByReplacingNullsWithBlanks  : (NSArray *) array;
-(NSDictionary *) dictionaryByReplacingNullsWithStrings : (NSDictionary *) dictionary;
- (void)tappedQR;
-(void)showLoadingViewWithCompletition:(void (^)(void))completionBlock;
-(void)removeLoadingViewWithCompletition:(void (^)(void))completionBlock;
-(NSString *)getDateFormatToWS;
-(void)starReachabilityNotifier;
-(void)verifyInternetStatusForBanner;
-(void)verifyAvaliablePaymentsForPatient:(NSString *)idPatient comeFromScan:(BOOL)isPresentialConsultation andParentId:(nullable NSString *)parentId;
- (void)tappedOpenInfo;
-(void)updatePushToken:(NSString *)token;
-(void)showAlertWithMessage:(NSString *)message;
-(void)removeRegisterViews;
-(void)showShareWithPatient;
+ (UIImage *)scaleImageProportionally:(UIImage *)image;
-(void)openChatWithPatientId:(int)idPatient;
-(void)openChatWithPatientName:(NSString *)namePatient;
-(void)goToPatientsView;
-(void)saveDeviceAndVersion;
-(BOOL)verifyIfNeedShowSuscription;
-(void)showSuscriptionVCFromScanner:(BOOL)comeFromScan;
-(void)flowQROpen;
-(void)flowRemoteConsultationOpen;
-(BOOL)check30daysRegisterWithDate:(NSString *)registerDate;
@end
