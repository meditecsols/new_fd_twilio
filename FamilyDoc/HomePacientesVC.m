//
//  HomePacientesVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 19/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.

#import "HomePacientesVC.h"
#import "PatientTVC.h"
#import "SharedFunctions.h"
#import "InvokeService.h"
#import "GlobalMembers.h"
#import "LoginVC.h"
#import "PatientHistoricVC.h"
#import "ChatVC.h"
#import "HomeViewsContainerVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppDelegate.h"
#import <SendBirdSDK/SendBirdSDK.h>
#import "Config.h"

@interface HomePacientesVC ()<SBDChannelDelegate>{
    NSMutableArray *patientsArray;
    UIRefreshControl *refreshControl;
    BOOL searchEnabled;
    NSMutableArray *patientsNamesArray;
    NSMutableArray *consultasArray;
    NSMutableArray *channelsArray;
    NSMutableArray *newMessagesArray;
    int numOfChannelsChecked;
}
@end

@implementation HomePacientesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _patientsTableView.dataSource=self;
    _patientsTableView.delegate=self;
    
    self.searchResult = [[NSMutableArray alloc] init];
    channelsArray=[[NSMutableArray alloc] init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh1:) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor whiteColor];
    [_patientsTableView addSubview:refreshControl];
    
    _patientSearch.delegate=self;
    _patientsTableView.backgroundColor = [UIColor whiteColor];
    _patientsTableView.backgroundView.backgroundColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];
    
    [_btnDifusionList setHidden:YES];
    [_viewLineSeparator setHidden:YES];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getCompaniesWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        if ([responseData isKindOfClass:[NSArray class]]) {
            NSMutableArray *companiesArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
            if ([companiesArray count]>0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    isFDBusinessDoc=YES;
                    [self->_btnDifusionList setHidden:NO];
                    [self->_btnInvitePatients setHidden:YES];
                    [self->_viewLineSeparator setHidden:NO];
                    self->_layoutConstraintHeightButtonInvite.constant=0;
                });
            }
        }
        
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableForNewMessages) name:@"reloadTableForNewMessages" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadPatientList) name:@"reloadPatientList" object:nil];
    
    
    
    
    patientsArray=[[NSMutableArray alloc] init];
    if (!comeFromLogin&&firstTimeEntry) {
        [[SharedFunctions sharedInstance] getUsrAndPass];
        InvokeService *invoke=[[InvokeService alloc] init];
        NSMutableDictionary *dict=[NSMutableDictionary new];
        [dict setObject:usr forKey:@"usr"];
        [dict setObject:psw forKey:@"psw"];
        
       // [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [invoke loginWithCompletion:dict WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
                if ([[responseData objectForKey:@"client_id"] length]>0 && [[responseData objectForKey:@"client_secret"] length]>0) {
                    [dict setObject:[responseData objectForKey:@"client_id"] forKey:@"client"];
                    [dict setObject:[responseData objectForKey:@"client_secret"] forKey:@"secret"];
                    [dict setObject:[responseData objectForKey:@"user_id"] forKey:@"user_id"];
                    InvokeService *invoke=[[InvokeService alloc] init];
                    [invoke tokenWithInfo:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                        if ([[responseData objectForKey:@"access_token"] length]>0) {
                            [dict setObject:responseData forKey:@"tokenInfo"];
                            accountInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dict]];
                            InvokeService *invoke2=[[InvokeService alloc] init];
                            [invoke2 getDoctorData:[dict objectForKey:@"user_id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                                if([[responseData allKeys] count]>0){                                    doctorInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
                                    [[SharedFunctions sharedInstance] saveUserData:doctorInfo];
                                    
                                    [[SharedFunctions sharedInstance] saveDeviceAndVersion];
                                    
                                    if ([[doctorInfo objectForKey:@"date_of_birth"] length]<=0) {
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                                                UIStoryboard *storyboard = self.storyboard;
                                                UIViewController *registerNext= [storyboard instantiateViewControllerWithIdentifier:@"Register2VC"];
                                                registerNext.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                                registerNext.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                                [self presentViewController:registerNext animated:YES completion:nil];
                                            }];
                                            
                                        });
                                    }
                                    else{
                                        [self getPatients];
                                        firstTimeEntry=NO;
                                        InvokeService *invoke3=[[InvokeService alloc] init];
                                        [invoke3 getCompaniesWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                                            if ([responseData isKindOfClass:[NSArray class]]) {
                                                NSMutableArray *companiesArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
                                                if ([companiesArray count]>0) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        isFDBusinessDoc=YES;
                                                        [self->_btnDifusionList setHidden:NO];
                                                        [self->_btnInvitePatients setHidden:YES];
                                                        [self->_viewLineSeparator setHidden:NO];
                                                        self->_layoutConstraintHeightButtonInvite.constant=0;
                                                    });
                                                }
                                            }
                                            
                                        }];
                                    }
                                    
                                    return;
                                }
                            }];
                        }
                        
                    }];
                }
            }];
       // }];
        
    }
    else if (comeFromLogin){
        comeFromLogin=NO;
    }
    //[[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
    [self getPatients];
    [self connectSendBird];
    //}];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    if (needRefreshPatients) {
        needRefreshPatients=NO;
        [self getPatients];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (pageHomeSelected !=2) {
        return;
    }
    [self checkNewMessages];
}
- (void)refresh1:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self getPatients];
    }];
}
-(void)reloadPatientList{
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self getPatients];
    }];
}
-(void)reloadTableForNewMessages{
    [self checkNewMessages];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}


-(void)getPatients{
   // if (firstTimeEntry) {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->refreshControl endRefreshing];
        [self->_patientsTableView setContentOffset:CGPointMake(0, 0)];
        
    });
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getPatientsByDoctorWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->_patientsTableView setContentOffset:CGPointMake(0, 0)];
                [self->refreshControl endRefreshing];
            });
            if ([responseData isKindOfClass:[NSDictionary class]]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                });
                return;
            }
            self->patientsArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
            
            NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"last_name" ascending:YES];
            NSArray *arraySorted = [self->patientsArray sortedArrayUsingDescriptors:@[descriptor]];
            self->patientsArray = [arraySorted mutableCopy];
           
            
            self->patientsNamesArray=[[NSMutableArray alloc] init];
            self->channelsArray=[[NSMutableArray alloc] init];
            for (NSDictionary *dict in self->patientsArray) {
                [self->patientsNamesArray addObject:[NSString stringWithFormat:@"%@ %@ %@",[dict objectForKey:@"first_name"],[dict objectForKey:@"last_name"],[dict objectForKey:@"second_last_name"]]];
                NSString *channel=[NSString stringWithFormat:@"%@__%@",[dict objectForKey:@"id"],[doctorInfo objectForKey:@"id"]];
                [self->channelsArray addObject:channel];
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->refreshControl endRefreshing];
                [self->_patientsTableView scrollsToTop];
                [[SharedFunctions sharedInstance] removeLoadingView];
                [self->_patientsTableView reloadData];
                [self checkNewMessages];
            });
        }];
        return;
//    }
//    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
//        InvokeService *invoke=[[InvokeService alloc] init];
//        [invoke getPatientsByDoctorWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
//            patientsArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
//            patientsNamesArray=[[NSMutableArray alloc] init];
//            for (NSDictionary *dict in patientsArray) {
//                [patientsNamesArray addObject:[NSString stringWithFormat:@"%@ %@ %@",[dict objectForKey:@"first_name"],[dict objectForKey:@"last_name"],[dict objectForKey:@"second_last_name"]]];
//            }
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [[SharedFunctions sharedInstance] removeLoadingView];
//                [_patientsTableView reloadData];
//            });
//        }];
//    }];
}
- (void)swipe:(UISwipeGestureRecognizer *)swipeRecogniser
{
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionRight)
//    {
//        UIStoryboard *storyboard = self.storyboard;
//        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeAjustesVC"];
//        
//        
//        [self presentViewController:viewController animated:NO completion:^{
//        }];
//    }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionLeft)
//    {
//        
//        
//        UIStoryboard *storyboard = self.storyboard;
//        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ConsultasVC"];
//        
//        
//        [self presentViewController:viewController animated:NO completion:^{
//        }];
//    }
////    else if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionLeft)
////    {
////        _pageControlView.currentPage =_pageControlView.currentPage+1;
////        _segmentedControl.selectedSegmentIndex=_pageControlView.currentPage;
////    }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionUp)
//    {
//        _layoutConstraintTopHeader.constant=-76;
//        [self.view layoutIfNeeded];
//    }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionDown)
//    {
//        _layoutConstraintTopHeader.constant=0;
//        [self.view layoutIfNeeded];
//    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([patientsArray count]>0)
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        tableView.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"Por el momento no tienes \nningún paciente asociado";
    noDataLabel.numberOfLines=4;
    noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
        noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        return [self.searchResult count]+1;
    }
    else{
        if ([patientsArray count]>0) {
            return [patientsArray count]+1;
        }
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"PatientTVC";
    PatientTVC *cell = (PatientTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (searchEnabled) {
        if (indexPath.row<[_searchResult count]) {
            cell.lblDate.text=[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"profile_pic"];
            cell.lblName.text=[NSString stringWithFormat:@"%@ %@ %@",[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"first_name"],[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"last_name"],[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"second_last_name"]];
            cell.lblDesc.text=[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"lastMessage"];
            cell.lblDate.text=[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"lastMessageDate"];
            if ([[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"companysubscription"] length]>0) {
                cell.lblCompany.text=[NSString stringWithFormat:@"Compañia: %@",[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"companysubscription"]];
            }
            
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                                 initWithTarget:self
                                                 action:@selector(openHistoric:)];
            [cell.imgPatient setUserInteractionEnabled:YES];
            [cell.imgPatient addGestureRecognizer:tap];
            cell.imgPatient.tag=indexPath.row;
            
            [cell.imgNew setHidden:YES];
            if ([newMessagesArray count]>0) {
                for (NSMutableDictionary *patientPendingReading in newMessagesArray) {
                    if ([[NSString stringWithFormat:@"%@",[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"id"]] isEqualToString:[patientPendingReading objectForKey:@"id"]]) {
                        [cell.lblNumberMessages setHidden:NO];
                        [cell.lblNumberMessages setText:[NSString stringWithFormat:@"%d",[[patientPendingReading objectForKey:@"count"] intValue]]];
                        cell.lblNumberMessages.layer.cornerRadius = 10;
                        cell.lblNumberMessages.layer.masksToBounds = YES;
                    }
                }
            }
            
            [cell.lblNewPatiente setHidden:YES];
            [cell.lblPendingPayment setHidden:YES];
            NSString *idDoctor=[NSString stringWithFormat:@"%@",[doctorInfo objectForKey:@"id"]];
            if([[[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"debit"] valueForKey:idDoctor] boolValue]) {
                [cell.lblPendingPayment setHidden:NO];
            }
            
            if ([[[_searchResult objectAtIndex:indexPath.row] objectForKey:@"profile_pic"] length]>0) {
                [cell.imgPatient sd_setImageWithURL:[NSURL URLWithString:[[_searchResult objectAtIndex:indexPath.row]  objectForKey:@"profile_pic"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if (image) {
                        [self roundImage:cell.imgPatient];
                    }
                    else{
                        [cell.imgPatient setImage:[UIImage imageNamed:@"imgTestProfile"]];
                    }
                    
                }];
            }
        }
        else if (indexPath.row==[_searchResult count]) {
            static NSString *CellIdentifier = @"Cell";
            UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if(cell2==nil){
                cell2=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                //cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"campo1.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
                
            }
            [cell2 setBackgroundColor:[UIColor whiteColor]];
            return cell2;
            
        }
        
    }
    else{
    if (indexPath.row<[patientsArray count]) {
        //cell.lblDate.text=[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"profile_pic"];
        cell.lblName.text=[NSString stringWithFormat:@"%@ %@ %@",[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"first_name"],[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"last_name"],[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"second_last_name"]];
        cell.lblDesc.text=[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"lastMessage"];
        cell.lblDate.text=[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"lastMessageDate"];
        if ([[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"companysubscription"] length]>0) {
            cell.lblCompany.text=[NSString stringWithFormat:@"Compañia: %@",[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"companysubscription"]];
        }
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(openHistoric:)];
        [cell.imgPatient setUserInteractionEnabled:YES];
        [cell.imgPatient addGestureRecognizer:tap];
        cell.imgPatient.tag=indexPath.row;
        [cell.imgNew setHidden:YES];
        if ([newMessagesArray count]>0) {
            for (NSMutableDictionary *patientPendingReading in newMessagesArray) {
                if ([[NSString stringWithFormat:@"%@",[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"id"]] isEqualToString:[patientPendingReading objectForKey:@"id"]]) {
                    [cell.lblNumberMessages setHidden:NO];
                    [cell.lblNumberMessages setText:[NSString stringWithFormat:@"%d",[[patientPendingReading objectForKey:@"count"] intValue]]];
                    cell.lblNumberMessages.layer.cornerRadius = 10;
                    cell.lblNumberMessages.layer.masksToBounds = YES;
                }
            }
        }
        
        [cell.lblNewPatiente setHidden:YES];
        [cell.lblPendingPayment setHidden:YES];
        NSString *idDoctor=[NSString stringWithFormat:@"%@",[doctorInfo objectForKey:@"id"]];
        if([[[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"debit"] valueForKey:idDoctor] boolValue]) {
            [cell.lblPendingPayment setHidden:NO];
        }
        
        if ([[[patientsArray objectAtIndex:indexPath.row] objectForKey:@"profile_pic"] length]>0) {
            
            [cell.imgPatient sd_setImageWithURL:[NSURL URLWithString:[[patientsArray objectAtIndex:indexPath.row]  objectForKey:@"profile_pic"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (image) {
                    [self roundImage:cell.imgPatient];
                }
                else{
                    [cell.imgPatient setImage:[UIImage imageNamed:@"imgTestProfile"]];
                }
                
            }];
        }
    }
    else if (indexPath.row==[patientsArray count]) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell2==nil){
            cell2=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.backgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"campo1.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
            
        }
        [cell2 setBackgroundColor:[UIColor whiteColor]];
        return cell2;
        
    }
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSMutableArray *arrayToSelect=[[NSMutableArray alloc] initWithArray:patientsArray];
    if (indexPath.row>[arrayToSelect count]-1) {
        [tableView deselectRowAtIndexPath:indexPath animated:true];
        return;
    }
    if(searchEnabled){
        arrayToSelect=_searchResult;
    }
    
    
    
    indexSChatSelected=(int)indexPath.row;
    PatientTVC *cell = (PatientTVC*)[tableView cellForRowAtIndexPath:indexPath];
    if (![cell.imgNew isHidden]) {
        [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber-1;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    
    
    
    NSMutableDictionary *patientInfo=[[arrayToSelect objectAtIndex:indexPath.row] mutableCopy];
    if ([pathPdfFile length]>0) {
        NSArray *parts = [pathPdfFile componentsSeparatedByString:@"/"];
        NSString *nameFile=[parts lastObject];
        NSString *message=[NSString stringWithFormat:@"¿Deseas enviar el archivo %@ a esta persona?",nameFile];
        UIAlertController * alert=  [UIAlertController
                                     alertControllerWithTitle:@"Family Doc"
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction* okAction = [UIAlertAction
                                   actionWithTitle:@"Si"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       [self openChatWithPatient:patientInfo];
                                   }];
        UIAlertAction* noAction = [UIAlertAction
                                   actionWithTitle:@"No"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       pathPdfFile=@"";
                                       [self openChatWithPatient:patientInfo];
                                   }];
        
        [alert addAction:okAction];
        [alert addAction:noAction];
        [self presentViewController:alert animated:NO completion:nil];
        return;
    }
    
    
    [self openChatWithPatient:patientInfo];
}
-(void)openChatWithPatientName:(NSString *)patientName{
    for (NSDictionary *dictPatient in patientsArray) {
        NSString *nameDictPatient=[NSString stringWithFormat:@"%@ %@ %@",[dictPatient objectForKey:@"first_name"],[dictPatient objectForKey:@"last_name"],[dictPatient objectForKey:@"second_last_name"]];
        if ([[nameDictPatient lowercaseString] containsString:[patientName lowercaseString]]) {
            [self openChatWithPatient:[dictPatient mutableCopy]];
            return;
        }
    }
}
-(void)openChatWithPatientId:(int)patientId{
    for (NSDictionary *dictPatient in patientsArray) {
        if ([[dictPatient objectForKey:@"id"] intValue]== patientId) {
            [self openChatWithPatient:[dictPatient mutableCopy]];
        }
    }
}
-(void)openChatWithPatient:(NSMutableDictionary *)patientInfo{
    NSString *idStringDr=[NSString stringWithFormat:@"%d",[[doctorInfo objectForKey:@"id"] intValue] ];
    NSString *idStringPatient=[NSString stringWithFormat:@"%d",[[patientInfo objectForKey:@"id"] intValue] ];
    [SBDGroupChannel createChannelWithUserIds:@[idStringDr,idStringPatient] isDistinct:YES completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        [SBDGroupChannel getChannelWithUrl:channel.channelUrl completionHandler:^(SBDGroupChannel * _Nullable channel, SBDError * _Nullable error) {
            if (error != nil) { // Error.
                return;
            }
            channel.isPublic=YES;
            dictPatientPresential=patientInfo;
            ChatVC *vc = [ChatVC messagesViewController];
            vc.channel=channel;
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            nc.modalPresentationStyle= UIModalPresentationOverFullScreen;
            [self presentViewController:nc animated:YES completion:nil];
        }];
    }];
    
    
}
-(void)openHistoric:(id)sender{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    int indexSelected=(int)[tapRecognizer.view tag];
    NSMutableArray *arrayToSelect=patientsArray;
    if(searchEnabled){
        arrayToSelect=_searchResult;
    }
        if (indexSelected<[arrayToSelect count]) {
            [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
    
    
    
                //NSMutableArray *completeConsultasArray=[[NSMutableArray alloc] init];
                NSMutableDictionary *patientInfo=[[arrayToSelect objectAtIndex:indexSelected] mutableCopy];
    
    
                [self performAndWait:^(dispatch_semaphore_t semaphore) {
                    InvokeService *invoke=[[InvokeService alloc] init];
                    [invoke getConsultationsByPatientWithInfo:patientInfo WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                        self->consultasArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
                        dispatch_semaphore_signal(semaphore);
                    }];
                }];
    
    
                [self performAndWait:^(dispatch_semaphore_t semaphore) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        if ([self->consultasArray count]>0) {
                            UIStoryboard *storyboard = self.storyboard;
                            PatientHistoricVC *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PatientHistoricVC"];
                            viewController.consultasArray=self->consultasArray;
                            viewController.modalPresentationStyle= UIModalPresentationOverFullScreen;
                            [self presentViewController:viewController animated:NO completion:nil];
                        }
                        else{
                            [self showAlertWithMessage:@"No cuenta con consultas previas"];
                        }
    
    
                    });
                    dispatch_semaphore_signal(semaphore);
                }];
            }];
        }
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:NO completion:nil];
}
- (void)performAndWait:(void (^)(dispatch_semaphore_t semaphore))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    perform(semaphore);
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}
- (void)performAndWait2:(void (^)(dispatch_semaphore_t semaphore2))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);
    perform(semaphore2);
    dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_FOREVER);
}
#pragma mark - Search delegate methods
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"(first_name CONTAINS[c] %@)",
                                    searchText];
    NSPredicate *resultPredicate2 = [NSPredicate
                                    predicateWithFormat:@"(last_name CONTAINS[c] %@)",
                                    searchText];
    NSPredicate *resultPredicate3 = [NSPredicate
                                    predicateWithFormat:@"(second_last_name CONTAINS[c] %@)",
                                    searchText];
    NSPredicate *resultPredicate4 = [NSPredicate
                                     predicateWithFormat:@"(companysubscription CONTAINS[c] %@)",
                                     searchText];
    
    _searchResult = [[patientsArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    [_searchResult addObjectsFromArray:[patientsArray filteredArrayUsingPredicate:resultPredicate2]];
    [_searchResult addObjectsFromArray:[patientsArray filteredArrayUsingPredicate:resultPredicate3]];
    [_searchResult addObjectsFromArray:[patientsArray filteredArrayUsingPredicate:resultPredicate4]];
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:_searchResult];
    NSArray *arrayWithoutDuplicates = [orderedSet array];
    _searchResult=[arrayWithoutDuplicates mutableCopy];
    [_patientsTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.patientsTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchEnabled = YES;
    if(searchBar.text.length>0){
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_patientsTableView reloadData];
    
}
- (IBAction)actionOpenInfo:(id)sender{
    [[SharedFunctions sharedInstance] tappedOpenInfo];
}
- (IBAction)actionOpenQR:(id)sender {
    [[SharedFunctions sharedInstance] tappedQR];
}

- (IBAction)actionOpenMenu:(id)sender {
        HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
        [containerParent moveToFirstView];
}

- (IBAction)actionOpenConsultas:(id)sender {
        HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
        [containerParent moveToThirdView];
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if ([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        return YES;
    }
    if([touch.view isDescendantOfView:_patientsTableView]){
        return NO;
    }
    
    return YES;
    
}
#pragma mark - Chat methods
-(void)connectSendBird{
    [SBDMain initWithApplicationId:KEY_SENDBIRD];
    NSString *idStringDr=[NSString stringWithFormat:@"%d",[[doctorInfo objectForKey:@"id"] intValue] ];
    [SBDMain addChannelDelegate:self identifier:self.description];
    [SBDMain connectWithUserId:idStringDr completionHandler:^(SBDUser * _Nullable user, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        else{
            NSString *prefix=@"Dr.";
            if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
                prefix= @"Dra.";
            }
            NSString *nickname=[[NSString stringWithFormat:@"%@ %@ %@",prefix,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"]] uppercaseString];
            if ([[doctorInfo objectForKey:@"profile_pic"] length]>0) {
                [SBDMain updateCurrentUserInfoWithNickname:nickname profileUrl:[doctorInfo objectForKey:@"profile_pic"] completionHandler:^(SBDError * _Nullable error) {
                    if (error != nil) { // Error.
                        return;
                    }
                }];
                
            }
            else{
                [SBDMain updateCurrentUserInfoWithNickname:nickname profileImage:nil completionHandler:^(SBDError * _Nullable error) {
                    if (error != nil) { // Error.
                        return;
                    }
                }];
            }
            
            SBDUser *user = [SBDMain getCurrentUser];
            NSDictionary *metaData = user.metaData;
            if ([[metaData allKeys] count]<=0) {
                NSDictionary *data = @{
                                       @"type": @"doctor"
                                       };
                
                [user updateMetaData:data completionHandler:^(NSDictionary<NSString *,NSString *> * _Nullable metaData, SBDError * _Nullable error) {
                    if (error != nil) { // Error.
                        return;
                    }
                }];
            }
            [SBDMain registerDevicePushToken:[SBDMain getPendingPushToken] unique:YES completionHandler:^(SBDPushTokenRegistrationStatus status, SBDError * _Nullable error) {
                NSLog(@"register push token sendbird pending");
                
            }];
            
            [self checkNewMessages];
            
            
        }
    }];
}
-(void)checkNewMessages{
    newMessagesArray=[[NSMutableArray alloc] init];
    SBDGroupChannelListQuery *query = [SBDGroupChannel createMyGroupChannelListQuery];
    [query setOrder:SBDGroupChannelListOrderLatestLastMessage];
    query.includeEmptyChannel = NO;
    [query loadNextPageWithCompletionHandler:^(NSArray<SBDGroupChannel *> * _Nullable channels, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        NSMutableArray *arrayOrderPatientByChat=[[NSMutableArray alloc] init];
        for (SBDGroupChannel *channel in channels) {
            NSLog(@"channel unread %lu",(unsigned long)channel.unreadMessageCount);
            for (SBDMember *member in channel.members) {
                NSString *idMember= member.userId;
                if (![idMember isEqualToString:[NSString stringWithFormat:@"%@",[doctorInfo objectForKey:@"id"]]]) {
                    NSString *idPatient= member.userId;
                    for (NSDictionary *dictPatient in self->patientsArray) {
                        NSString *idPatientDict=[NSString stringWithFormat:@"%d",[[dictPatient objectForKey:@"id"] intValue]];
                        if ([idPatientDict isEqualToString:idPatient]) {
                            NSMutableDictionary *dictPatientMutable=[dictPatient mutableCopy];
                            NSString *dateString=[self formatSBDate:channel.lastMessage.createdAt];
                            [dictPatientMutable setObject:dateString forKey:@"lastMessageDate"];
                            if ([channel.lastMessage isKindOfClass:[SBDUserMessage class]]) {
                                SBDUserMessage *messageUser= (SBDUserMessage *)channel.lastMessage;
                                [dictPatientMutable setObject:messageUser.message forKey:@"lastMessage"];
                            }
                            else if ([channel.lastMessage isKindOfClass:[SBDFileMessage class]]) {
                                // Do something when the received message is a FileMessage.
                                SBDFileMessage *messageFile= (SBDFileMessage *)channel.lastMessage;
                                NSLog(@"message: %@",messageFile.type);
                                NSString *messageType= [[messageFile.type componentsSeparatedByString:@"/"] firstObject];
                                if ([messageType isEqualToString:@"image"]) {
                                    [dictPatientMutable setObject:@"Imagen" forKey:@"lastMessage"];
                                }
                                else if ([messageType isEqualToString:@"video"]) {
                                    [dictPatientMutable setObject:@"Video" forKey:@"lastMessage"];
                                }
                                else if ([messageType isEqualToString:@"audio"]) {
                                    [dictPatientMutable setObject:@"Audio" forKey:@"lastMessage"];
                                }
                                else if ([messageType containsString:@"application"]) {
                                    if ([messageFile.type containsString:@"pdf"]) {
                                        [dictPatientMutable setObject:@"Pdf" forKey:@"lastMessage"];
                                    }
                                }
                            }
                            
                            [arrayOrderPatientByChat addObject:dictPatientMutable];
                            break;
                        }
                    }
                }
            }
            
            if (channel.unreadMessageCount>0) {
                for (SBDMember *member in channel.members) {
                    NSString *idMember= member.userId;
                    if (![idMember isEqualToString:[NSString stringWithFormat:@"%@",[doctorInfo objectForKey:@"id"]]]) {
                        NSString *idPatient= member.userId;
                        NSMutableDictionary *dictPatient=[[NSMutableDictionary alloc] init];
                        [dictPatient setObject:idPatient forKey:@"id"];
                        NSNumber *numMessages=[NSNumber numberWithInteger:channel.unreadMessageCount];
                        [dictPatient setObject:numMessages forKey:@"count"];
                        [self->newMessagesArray addObject:dictPatient];
                        
                    }
                }
                
            }
            
        }
        for (NSDictionary *dictPatient in self->patientsArray) {
            int patientId=[[dictPatient objectForKey:@"id"] intValue];
            NSMutableArray *arrayIds=[[NSMutableArray alloc] init];
            for (NSMutableDictionary *dictPatientOrder in arrayOrderPatientByChat) {
                int patientIdOrdered=[[dictPatientOrder objectForKey:@"id"] intValue];
                [arrayIds addObject:[NSNumber numberWithInt:patientIdOrdered]];
            }
            if (![arrayIds containsObject:[NSNumber numberWithInt:patientId]]) {
                [arrayOrderPatientByChat addObject:dictPatient];
            }
        }
        self->patientsArray=arrayOrderPatientByChat;
        [self->_patientsTableView reloadData];
    }];
}
-(NSString *)formatSBDate:(long)timestampString{
    double timestampDouble= (double)timestampString;
    double timeStamp=timestampDouble/1000;
    NSTimeInterval seconds = timeStamp;
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:epochNSDate];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if (today) {
        [dateFormatter setDateFormat:@"hh:mm a"];
    }
    else if ([self numberOfWeek:[NSDate date]] == [self numberOfWeek:[NSDate dateWithTimeIntervalSinceNow: (60.0f*60.0f*24.0f*7.0f)]])
    {
        dateFormatter.dateFormat=@"EEEE";
    }
    else{
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    }
    NSString *dateString = [[dateFormatter stringFromDate:epochNSDate] capitalizedString];
    return dateString;
}
- (NSInteger)numberOfWeek:(NSDate *)date{
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [calender components:NSCalendarUnitWeekOfYear fromDate:date];
    return [components weekOfYear];
}
- (IBAction)actionInvitePatients:(id)sender{
    [[SharedFunctions sharedInstance] showShareWithPatient];
}
#pragma mark - SBDChannelDelegate

- (void)channel:(SBDBaseChannel * _Nonnull)sender didReceiveMessage:(SBDBaseMessage * _Nonnull)message {
    if ([sender isKindOfClass:[SBDGroupChannel class]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self checkNewMessages];
            if ([[[SharedFunctions sharedInstance] topMostController] isKindOfClass:[UINavigationController class]]) {
                if ([[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] count]>0) {
                    if ([[[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] objectAtIndex:0] isKindOfClass:[ChatVC class]]) {
                        [self verifyTypeMessageForSendLocalNotif:message];
                    }
                    else{
                        [self verifyTypeMessageForSendLocalNotif:message];
                    }
                }
                else{
                    [self verifyTypeMessageForSendLocalNotif:message];
                }
            }
            else{
                [self verifyTypeMessageForSendLocalNotif:message];
            }
            
        });
        
    }
}
-(void)verifyTypeMessageForSendLocalNotif:(SBDBaseMessage *)message{
    if ([message isKindOfClass:[SBDUserMessage class]]) {
        SBDUserMessage *messageUser= (SBDUserMessage *)message;
        NSString *idPatient=messageUser.sender.userId;
        if([[dictPatientPresential objectForKey:@"id"] intValue]!=[idPatient intValue]){
            [self sendLocalNotificationWithTitle:messageUser.sender.nickname AndMessage:messageUser.message];
        }
        
    }
    else if([message isKindOfClass:[SBDFileMessage class]]) {
        
        SBDFileMessage *messageFile= (SBDFileMessage *)message;
        NSString *idPatient=messageFile.sender.userId;
        if([[dictPatientPresential objectForKey:@"id"] intValue]!=[idPatient intValue]){
            [self sendLocalNotificationWithTitle:messageFile.sender.nickname AndMessage:@"Te ha enviado un archivo"];
        }
        
        
    }
}
-(void)sendLocalNotificationWithTitle:(NSString *)title AndMessage:(NSString *)message{
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    
    content.title = title;
    content.body =message;
    
    content.sound = [UNNotificationSound defaultSound];
    
    // Deliver the notification in five seconds.
    UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                  triggerWithTimeInterval:1 repeats:NO];
    UNNotificationRequest* request = [UNNotificationRequest requestWithIdentifier:@"Message"
                                                                          content:content trigger:trigger];
    
    // Schedule the notification.
    UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:nil];
}
@end
