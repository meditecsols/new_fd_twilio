//
//  StatusConsultationCVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 8/1/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StatusConsultationCVC : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgCircleStatusColor;
@property (strong, nonatomic) IBOutlet UILabel *lblNameStatus;
@property (strong, nonatomic) IBOutlet UIView *viewBg;
@end

NS_ASSUME_NONNULL_END
