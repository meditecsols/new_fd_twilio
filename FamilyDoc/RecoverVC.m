//
//  RecoverVC.m
//  
//
//  Created by Martin Gonzalez on 19/01/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "RecoverVC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"

@interface RecoverVC ()

@end

@implementation RecoverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];    _txtFieldEmail.delegate=self;
    [_txtFieldEmail becomeFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
}
-(void)dismissKeyboard {
    [_txtFieldEmail resignFirstResponder];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (_txtFieldEmail.text.length<=0) {
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        return;
    }
    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionNext:(id)sender {
    [self dismissKeyboard];
    if (_txtFieldEmail.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar un Email"];
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        [self showAlertWithMessage:@"Debes ingresar un Email válido"];
        return;
    }
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    NSMutableDictionary *dict=[NSMutableDictionary new];
    [dict setObject:_txtFieldEmail.text forKey:@"email"];
    
    [invoke recoverPassWithInfo:dict WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        NSLog(@"response");
        [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
            if ([[responseData allKeys] count]>0) {
                if ([[responseData objectForKey:@"email"] length]>0) {
                    UIAlertController * alert=  [UIAlertController
                                                 alertControllerWithTitle:@"Family Doc"
                                                 message:@"Revisa tu correo electrónico. Te hemos enviado las instrucciones para restaurar tu contraseña"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    
                    UIAlertAction* okAction = [UIAlertAction
                                               actionWithTitle:@"Entendido"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   NSURL* mailURL = [NSURL URLWithString:@"message://"];
                                                   if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
                                                       [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:^(BOOL success) {
                                                           [self dismissViewControllerAnimated:YES completion:nil];
                                                       }];
                                                   }
                                                   
                                               }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else{
                [self showAlertWithMessage:@"El email no ha sido registrado"];
            }
        }];
       
    }];
    
   
    
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"\n\n\nPadecimiento"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    NSString *text =[NSString stringWithFormat:@"\n\n\nPadecimiento\n\n"];
    NSString *text2=message;
    NSString *text3=[NSString stringWithFormat:@"%@%@",text,text2];
    NSMutableAttributedString *attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:19] range:[text3 rangeOfString:text]];
    NSRange blackTextRange = [text3 rangeOfString:text2];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15] range:blackTextRange];
    [alert setValue:attributedText forKey:@"attributedTitle"];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    
    
    
    [self presentViewController:alert animated:NO completion:^{
        int xPosition = alert.view.frame.size.width/2-20;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPosition,30,40,44)];
        imageView.image = [UIImage imageNamed:@"imgPadecimientoAlert"];
        [alert.view addSubview:imageView];
        
    }];
}
- (IBAction)actionForgetPass:(id)sender{
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
}
@end
