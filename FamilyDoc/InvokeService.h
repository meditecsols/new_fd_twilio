//
//  InvokeService.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface InvokeService : NSObject
- (void)loginWithCompletion:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;


- (void)tokenWithInfo:(NSDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;

- (void)registerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;

- (void)retrieveZipCodeData:(NSString *_Nullable)zipCode WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)retrieveCedulaInfo:(NSString *_Nullable)cedula WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateDoctorWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getDoctorData:(NSString *_Nullable)zipCode WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getAddressWithData:(NSString *_Nullable)idDoctor     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getSpecialtiesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addSpecialtyWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addSpecialtyToDoctorWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getConsultationsByDoctorWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getPatientsByDoctorWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getImageFromUrl:(NSString *_Nullable)url     WithCompletion:(void (^_Nullable)(UIImage * _Nullable imageDownloaded, NSError * _Nullable error))completion;
- (void)getPatientProfileFromWS:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getSpecialtiesByDoctor:(NSString *_Nullable)idDoctor WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getPatientById:(NSString*_Nonnull)idPatient WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addAConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getSpecialtiesListWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getHealthCondition:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addHealthConditionWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)deleteHealthCondition:(NSString *_Nullable)idHealth     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getNoteOfConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addNoteConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getPrescriptionsOfConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addPrescritionWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getHealthCondition:(NSString *_Nullable)idPatient AndConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addMedicalStudyWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addTreatmentWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)downloadPdfPrescription:(NSString *_Nullable)prescriptionId AndSignature:(NSString *_Nullable)addSignature    WithCompletion:(void (^_Nullable)(NSString * _Nullable path, NSError * _Nullable error))completion;
- (void)addServiceToConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addServiceWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addMembershipWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getServicesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)recoverPassWithInfo:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getConsultationsByPatientWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addMetricsWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addAChanelWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getMessagesByPatientWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getTokenChatWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)deleteService:(NSString *_Nullable)idHealth     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updateServiceWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getUrlsTermsPrivacyWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)updatePatientWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
-(NSString * _Nullable)parseToTinyURL:(NSString * _Nullable)urlToShare;
- (void)getPatientAviliablePaymentsById:(NSString * _Nullable)idPatient IsRemote:(BOOL)isRemote WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)postShortUrlWithUrl:(NSString * _Nullable)inviteDoctUrl AndCompletition:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getShortUrlWithSlashTag:(NSString * _Nullable)slashTagDoctor AndCompletition:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)addPatientRelationWithId:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getCompaniesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getStatusOfPatientWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion;
- (void)getTokenTwilioWithUrl:(NSString *)slashTagDoctor AndCompletition:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion;
@end
