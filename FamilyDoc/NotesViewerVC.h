//
//  NotesViewerVC.h
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 9/11/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotesViewerVC : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *txtViewNote;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)closeNoteDesc:(id)sender;
@end

NS_ASSUME_NONNULL_END
