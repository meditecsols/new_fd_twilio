//
//  ConsultasVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 18/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "ConsultasVC.h"
#import "ConsultationPatientTVC.h"
#import "InvokeService.h"
#import "GlobalMembers.h"
#import "LoginVC.h"
#import "SharedFunctions.h"
#import "ChatVC.h"
#import "HomeViewsContainerVC.h"
#import "Config.h"
#import "NotesViewerVC.h"

@interface ConsultasVC (){
    NSMutableArray *consultasArray;
    UIRefreshControl *refreshControl;
    UIView *pdfViewer;
    UIView *pickerContainer;
    UIPickerView *pickerPrescriptionList;
    int prescriptionSelected;
    NSMutableArray *prescriptionListArray;
    NSMutableArray *notesListArray;
    UIPickerView *pickerNotesList;
    UILabel *noDataLabel;
}

@end

@implementation ConsultasVC

- (void)viewDidLoad {
    [super viewDidLoad];

    
    consultasArray=[[NSMutableArray alloc] init];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh1:) forControlEvents:UIControlEventValueChanged];
    refreshControl.backgroundColor = [UIColor whiteColor];
    [_patientsTV addSubview:refreshControl];
    
    _patientsTV.backgroundView.backgroundColor=[UIColor whiteColor];
    _patientsTV.backgroundColor=[UIColor whiteColor];
    
    _patientsTV.dataSource=self;
    _patientsTV.delegate=self;
    if ([[doctorInfo allKeys] count]>0 ) {
        [self getConsultations];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    if (needRefreshConsultations) {
        needRefreshConsultations=NO;
        [self getConsultations];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if (pageHomeSelected !=3) {
//        return;
//    }
    if (comeFromScan) {
        return;
    }
    
    
    
}
- (void)refresh1:(UIRefreshControl *)refreshControl
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [refreshControl endRefreshing];
        [self->_patientsTV setContentOffset:CGPointMake(0, 0)];
        
    });
    [self getConsultations];
}
-(void)getConsultations{
    if (firstTimeEntry) {
        [self getAllDataConsultationsWithoutLoading];
        
    }else{
       // [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [self getAllDataConsultationsWithoutLoading];
      //  }];
        //[self getAllDataConsultations];
    }
}
- (void)performAndWait:(void (^)(dispatch_semaphore_t semaphore))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    perform(semaphore);
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}
- (void)performAndWait2:(void (^)(dispatch_semaphore_t semaphore2))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);
    perform(semaphore2);
    dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_FOREVER);
}
-(void)getAllDataConsultationsWithoutLoading{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->refreshControl endRefreshing];
        [self->_patientsTV setContentOffset:CGPointMake(0, 0)];
        
    });
        NSMutableArray *completeConsultasArray=[[NSMutableArray alloc] init];
        
        [self performAndWait:^(dispatch_semaphore_t semaphore) {
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getConsultationsByDoctorWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                self->consultasArray=responseData;
                self->consultasArray=[self filterClosedFromArray:self->consultasArray];
                dispatch_semaphore_signal(semaphore);
            }];
        }];
        [self performAndWait:^(dispatch_semaphore_t semaphore) {
            if ([self->consultasArray count]>0) {
                for(int i=0;i<[self->consultasArray count];i++) {
                    
                    NSDictionary *dict=[self->consultasArray objectAtIndex:i];
                    NSString *patientId=[dict objectForKey:@"patient"];
                    
                    [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
                        
                        InvokeService *invoke2=[[InvokeService alloc] init];
                        [invoke2 getPatientById:patientId WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                            NSMutableDictionary *dictComplete=[[NSMutableDictionary alloc] initWithDictionary:dict];
                            [dictComplete setObject:[[SharedFunctions sharedInstance]dictionaryByReplacingNullsWithStrings:responseData] forKey:@"patientDescription"];
                            [completeConsultasArray addObject:dictComplete];
                            dispatch_semaphore_signal(semaphore2);
                        }];
                    }];
                    [self performAndWait2:^(dispatch_semaphore_t semaphore2) {
                        InvokeService *invoke3=[[InvokeService alloc] init];
                        [invoke3 getHealthCondition:patientId AndConsultation:[[self->consultasArray objectAtIndex:i] objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                            if(!error && responseData){
                                NSMutableDictionary *dictComplete2=[[NSMutableDictionary alloc] initWithDictionary:[completeConsultasArray objectAtIndex:i]];
                                [dictComplete2 setObject:[[SharedFunctions sharedInstance]arrayByReplacingNullsWithBlanks:responseData] forKey:@"diagnoseArray"];
                                
                                [completeConsultasArray replaceObjectAtIndex:i withObject:dictComplete2];
                            }
                            
                           
                            dispatch_semaphore_signal(semaphore2);
                        }];
                        
                    }];
                }
                dispatch_semaphore_signal(semaphore);
            }
            else{
                dispatch_semaphore_signal(semaphore);
            }
            
        }];
        [self performAndWait:^(dispatch_semaphore_t semaphore) {
            self->consultasArray=completeConsultasArray;
            self->consultasArray=[self filterConsultationsTypeFromArray:self->consultasArray];
            NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"meeting_date" ascending:NO];
            NSArray *arraySorted = [self->consultasArray sortedArrayUsingDescriptors:@[descriptor]];
            self->consultasArray = [arraySorted mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->refreshControl endRefreshing];
                [[SharedFunctions sharedInstance] removeLoadingView];
                [self->_patientsTV reloadData];
            });
            dispatch_semaphore_signal(semaphore);
        }];

}
-(NSMutableArray*)filterClosedFromArray:(NSMutableArray *)arrayConsultations{
    NSMutableArray *filteredArray=[[NSMutableArray alloc] init];
    for (NSDictionary *dictToCheck in arrayConsultations) {
        if ([dictToCheck isKindOfClass:[NSDictionary class]]) {
            if (![[dictToCheck objectForKey:@"status"] isEqualToString:@"closed"]) {
                [filteredArray addObject:dictToCheck];
            }
        }
        
    }
    
    return filteredArray;
}
-(NSMutableArray*)filterConsultationsTypeFromArray:(NSMutableArray *)arrayConsultations{
    NSString *consultationType=@"remote";
    if (_segmentedConsultationSelected.selectedSegmentIndex ==1) {
        consultationType=@"on-site";
    }
    NSMutableArray *filteredArray=[[NSMutableArray alloc] init];
    for (NSDictionary *dictToCheck in arrayConsultations) {
        if ([dictToCheck isKindOfClass:[NSDictionary class]]) {
            if ([[dictToCheck objectForKey:@"interaction_type"] isEqualToString:consultationType]) {
                [filteredArray addObject:dictToCheck];
            }
        }
        
    }
    
    return filteredArray;
}
- (void)swipe:(UISwipeGestureRecognizer *)swipeRecogniser
{
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionRight)
//    {
//        UIStoryboard *storyboard = self.storyboard;
//        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomePacientesVC"];
//        
//        
//        [self presentViewController:viewController animated:NO completion:^{
//        }];
//    }
//    
//    
//    
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionUp)
//        {
//            _layoutConstraintTopHeader.constant=-76;
//            [self.view layoutIfNeeded];
//        }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionDown)
//    {
//        _layoutConstraintTopHeader.constant=0;
//        [self.view layoutIfNeeded];
//    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([consultasArray count]>0)
    {
        noDataLabel.hidden = YES;
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        tableView.backgroundView.backgroundColor = [UIColor whiteColor];
        
    }
    else
    {
        
        noDataLabel.hidden = NO;
        NSString *message=@"No tienes consultas a distancia \nabiertas.\nAbre una consulta dando clic\n en la cápsula, en la\n esquina superior derecha\n de la pantalla del chat.";
        if (_segmentedConsultationSelected.selectedSegmentIndex ==1) {
            message=@"No tienes consultas presenciales \nabiertas.\n Abre una presionando el ícono \nque aparece en la parte superior\n derecha de esta pantalla.";
        }
        noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = message;
        noDataLabel.numberOfLines=8;
        noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
        noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([consultasArray count]>0) {
        return [consultasArray count]+1;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"ConsultationPatientTVC";
    ConsultationPatientTVC *cell = (ConsultationPatientTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (indexPath.row<[consultasArray count]) {
    cell.lblDate.text=[self formatDateToView:[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"meeting_date"]];
    cell.lblNamePatient.text=[[NSString stringWithFormat:@"%@ %@ %@",[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"patientDescription"] objectForKey:@"first_name"],[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"patientDescription"] objectForKey:@"last_name"],[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"patientDescription"] objectForKey:@"second_last_name"]] uppercaseString];
        cell.lblDiagnose.text=@"No especificado";
        cell.lblDiagnose.text=@"";
        NSString *lblDiagnose=@"";
        cell.btnPill.enabled=NO;
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"prescriptions"] count]>0) {
            cell.btnPill.enabled=YES;
            cell.btnPill.tag=indexPath.row;
            [cell.btnPill addTarget:self action:@selector(openPrescription:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"notes"] count]>0) {
            cell.btnNotes.hidden=NO;
            cell.btnNotes.tag=indexPath.row;
            [cell.btnNotes addTarget:self action:@selector(openNotes:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"diagnoseArray"] count]>0) {
            for (NSDictionary *dictDiagnose in [[consultasArray objectAtIndex:indexPath.row] objectForKey:@"diagnoseArray"]) {
                lblDiagnose=[NSString stringWithFormat:@"%@,%@",lblDiagnose,[dictDiagnose objectForKey:@"description"]];
            }
            if (lblDiagnose.length>0) {
                lblDiagnose=[lblDiagnose substringFromIndex:1];
            }
            if (lblDiagnose.length>0&&[[lblDiagnose substringFromIndex:lblDiagnose.length-1] isEqualToString:@","]) {
                lblDiagnose=[lblDiagnose substringToIndex:lblDiagnose.length-1];
            }
            cell.lblDiagnose.text=[lblDiagnose uppercaseString];
        }
        if (lblDiagnose.length<=0) {
            cell.lblDiagnose.text=@"PENDIENTE";
        }
    
    cell.lblSuffering.text=[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"topic"];
    if ([[[[consultasArray objectAtIndex:indexPath.row]objectForKey:@"patientDescription"] objectForKey:@"profile_pic"] length]>0) {
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke getImageFromUrl:[[[consultasArray objectAtIndex:indexPath.row]objectForKey:@"patientDescription"] objectForKey:@"profile_pic"]  WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!error) {
                    [cell.imgPatient setImage:imageDownloaded];
                    cell.imgPatient.contentMode=UIViewContentModeScaleAspectFill;
                    [self roundImage:cell.imgPatient];
                }
                
            });
            
        }];
    }
    }
    else if (indexPath.row==[consultasArray count]) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell2==nil){
            cell2=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        [cell2 setBackgroundColor:[UIColor whiteColor]];
        return cell2;
        
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if (indexPath.row<[consultasArray count]) {
        NSDictionary *dict=[consultasArray objectAtIndex:indexPath.row];
        presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:dict];
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [invoke getPatientProfileFromWS:[[dict objectForKey:@"patientDescription"]objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
              dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                    if ([[dictPatientPresential allKeys] count]>0) {

                        UIStoryboard *storyboard = self.storyboard;
                        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"ConsultaPresencialVC"];
                        viewController.modalPresentationStyle= UIModalPresentationOverFullScreen;
                        [self presentViewController:viewController animated:NO completion:nil];

                    }
                }];
              });



            }];
        }];

    }
    
    
    

}
-(NSString *)formatDateToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yyyy"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
- (IBAction)actionOpenInfo:(id)sender{
    [[SharedFunctions sharedInstance] tappedOpenInfo];
}
- (IBAction)actionOpenQR:(id)sender {
    [[SharedFunctions sharedInstance] tappedQR];
}

- (IBAction)actionOpenMenu:(id)sender {
    HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
    [containerParent moveToFirstView];
}

- (IBAction)actionOpenPacientes:(id)sender {
    HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
    [containerParent moveToSecondView];
}
- (IBAction)actionChangeTypeConsultation:(id)sender {
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self getAllDataConsultationsWithoutLoading];
    }];
}

- (IBAction)openPrescription:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayPresc=[[consultasArray objectAtIndex:index] objectForKey:@"prescriptions"];
    if ([arrayPresc count]>0) {
        if ([arrayPresc count]==1) {
            NSDictionary *dictPresc=[arrayPresc objectAtIndex:0];
            NSString *idPres=[dictPresc objectForKey:@"id"];
            [self openViewer:idPres];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige tu orden de tratamiento a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingPrescription) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            prescriptionListArray=[[NSMutableArray alloc] initWithArray:arrayPresc];
            
            pickerPrescriptionList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,80, frame.frame.size.width-20,100)];
            pickerPrescriptionList.tintColor = [UIColor whiteColor];
            pickerPrescriptionList.layer.cornerRadius=10;
            pickerPrescriptionList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerPrescriptionList.dataSource = self;
            pickerPrescriptionList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerPrescriptionList];
        }
    }
}
- (IBAction)openNotes:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayNotes=[[consultasArray objectAtIndex:index] objectForKey:@"notes"];
    if ([arrayNotes count]>0) {
        if ([arrayNotes count]==1) {
            NSDictionary *dictnote=[arrayNotes objectAtIndex:0];
            NSString *textNote=[dictnote objectForKey:@"text"];
            [self openNoteViewer:textNote];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige la a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingNote) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            notesListArray=[[NSMutableArray alloc] initWithArray:arrayNotes];
            
            pickerNotesList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,80, frame.frame.size.width-20,100)];
            pickerNotesList.tintColor = [UIColor whiteColor];
            pickerNotesList.layer.cornerRadius=10;
            pickerNotesList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerNotesList.dataSource = self;
            pickerNotesList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerNotesList];
        }
    }
}
- (void)openViewer:(NSString *)idPrescription {
    pdfViewer=[[UIView alloc] init];
    pdfViewer=[[[NSBundle mainBundle] loadNibNamed:@"PrescriptionViewer" owner:self options:nil] objectAtIndex:0];
    [pdfViewer setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height) ];
    [self.view addSubview:pdfViewer];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/prescription/%@/pdf/",BASE_URL,idPrescription]];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc] initWithURL:url];
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"GET"];
    [_myWebView loadRequest:request];
    _myWebView.delegate=self;
    [[SharedFunctions sharedInstance] showLoadingView];
    
}
-(void)openNoteViewer:(NSString *)note{
    NotesViewerVC *noteViewer =(NotesViewerVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"NotesViewerVC"];
    [[SharedFunctions sharedInstance] topMostController].definesPresentationContext = YES;
    noteViewer.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    noteViewer.modalPresentationStyle= UIModalPresentationOverFullScreen;
    [[[SharedFunctions sharedInstance] topMostController] presentViewController:noteViewer animated:YES completion:nil];
    noteViewer.txtViewNote.text=note;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[SharedFunctions sharedInstance] removeLoadingView];
}
- (IBAction)closeWebView:(id)sender {
    [pdfViewer removeFromSuperview];
}
#pragma mark - Picker Methods
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerView isEqual:pickerNotesList]) {
        return [notesListArray count];
    }
    return [prescriptionListArray count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (void) donePickingPrescription {
    int row = (int) [pickerPrescriptionList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictPresc=[prescriptionListArray objectAtIndex:row];
    NSString *idPres=[dictPresc objectForKey:@"id"];
    [self openViewer:idPres];
    
}
- (void) donePickingNote {
    int row = (int) [pickerNotesList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictnote=[notesListArray objectAtIndex:row];
    NSString *textNote=[dictnote objectForKey:@"text"];
    [self openNoteViewer:textNote];
    
}
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:pickerNotesList]) {
        NSString *title = [NSString stringWithFormat:@"Nota %ld",(long)row+1];;
        NSAttributedString *attString =
        [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]}];
        
        return attString;
    }
    NSString *title = [NSString stringWithFormat:@"Orden de tratamiento %ld",(long)row+1];;
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]}];
    
    return attString;
}
@end
