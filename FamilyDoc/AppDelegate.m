//
//  AppDelegate.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 16/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "AppDelegate.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "ChatVC.h"
#import <SendBirdSDK/SendBirdSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "NSString+Base64.h"
#import "Config.h"

@import Firebase;

//@import VirgilCrypto;
//@import VirgilSDK;
//@import VirgilCryptoAPI;

@interface AppDelegate ()<FIRMessagingDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //    NSArray *fontFamilies = [UIFont familyNames];
    //
    //    for (int i = 0; i < [fontFamilies count]; i++)
    //    {
    //        NSString *fontFamily = [fontFamilies objectAtIndex:i];
    //        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
    //        NSLog (@"%@: %@", fontFamily, fontNames);
    //    }
    
    //    NSData *appPublicKey = [[NSData alloc] initWithBase64EncodedString:@"MCowBQYDK2VwAyEAljOYGANYiVq1WbvVvoYIKtvZi2ji9bAhxyu6iV/LF8M=" options:0];
    //
    //
    //
    //
    //
    //
    //    // save the Virgil Key into storage
    //    NSError *err;
    //    aliceKey = [virgil2.Keys loadKeyWithName:@"key_user_mardf34"
    //                                   password:@"ios_enc_asdwer345" error:&err];
    //    if (err || !aliceKey) {
    //        aliceKey = [virgil2.Keys generateKey];
    //        [aliceKey storeWithName:@"key_user_mardf34"
    //                       password:@"ios_enc_asdwer345" error:&err];
    //    }
    //
    //
    //
    //
    //    [virgil2.Cards publishCard:aliceCard completion:^(NSError *error) {
    //        //...
    //        NSLog(@"published ");
    //    }];
    //    [virgil2.Cards searchCardsWithIdentities:@[@"juan"]
    //                                 completion:^(NSArray<VSSVirgilCard *>* aliceCards, NSError *error) {
    //                                     //...
    //                                     NSLog(@"published ");
    //                                 }];
    self.defaultQueue = [SKPaymentQueue defaultQueue];
    [self.defaultQueue addTransactionObserver:self];
//    [self isValidReceiptForTransaction:^(BOOL isValid, NSError * _Nullable error) {
//        isSuscriptionValid=isValid;
//    }];
    
    
    
    //[ZDCChat initializeWithAccountKey:@"WFkrxGPgqcW3JyZsYdkEWhRjaNdvcpoi"];
    [Fabric with:@[[Crashlytics class]]];
    
    [[SharedFunctions sharedInstance] getUsrAndPass];
    [[SharedFunctions sharedInstance] getUsrInfo];
    firstTimeEntry=YES;
    statusReachability=YES;
    [[SharedFunctions sharedInstance] starReachabilityNotifier];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    //    [Flurry startSession:@"JN85DYW7Q7DN9GZ3783C"
    //      withSessionBuilder:[[[FlurrySessionBuilder new]
    //                           withCrashReporting:YES]
    //                          withLogLevel:FlurryLogLevelAll]];
    [FIRApp configure];
    //    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
    //        UIUserNotificationType allNotificationTypes =
    //        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    //        UIUserNotificationSettings *settings =
    //        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    //        [application registerUserNotificationSettings:settings];
    //    } else {
    // iOS 10 or later
    //#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    UNAuthorizationOptions authOptions =
    UNAuthorizationOptionAlert
    | UNAuthorizationOptionSound
    | UNAuthorizationOptionBadge;
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
    }];
    //#endif
    //    }
    [FIRMessaging messaging].delegate = self;
    [application registerForRemoteNotifications];
    
    
    
    //    UIUserNotificationType allNotificationTypes =
    //    (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
    //    UIUserNotificationSettings *settings =
    //    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    //    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    //    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    
    
    UIStoryboard *storyboard= [UIStoryboard storyboardWithName:[[SharedFunctions sharedInstance] getCurrentStoryboard] bundle:nil];
    if (usr.length>0&&psw.length>0 && [[doctorInfo allKeys]count]>0) {
        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewsContainerVC"];
        
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    }
    
    
    return YES;
}
- (void)isValidReceiptForTransaction:(void (^_Nullable)(BOOL isValid, NSError * _Nullable error))completion{
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    NSError *error=nil;
    if (!receipt) {
        completion(NO,error);
        return;
    }
    NSError *jsonError = nil;
    NSString *receiptBase64 = [NSString base64StringFromData:receipt length:(int)[receipt length]];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                receiptBase64,@"receipt-data",
                                                                SHARED_SECRET_SUSCRIPTION_KEY,@"password",
                                                                nil]
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&jsonError
                        ];
    if (jsonError) {
        completion(NO,error);
        return;
    }
    NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    if (jsonError) {
        completion(NO,error);
        return;
    }
    NSLog(@"JSON: %@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:parsedData
                                                          options:0
                                                            error:&error];
    
    if (!requestData) {
        completion(NO,error);
        return;
    }
    
    NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
    if ([BASE_URL  isEqualToString:URL_SANDBOX]) {
        storeURL = [NSURL URLWithString:@"https://sandbox.itunes.apple.com/verifyReceipt" ];
    }
    
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:storeRequest
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        completion(NO,error);
                                                        return;
                                                    } else {
                                                        
                                                        NSError *error;
                                                        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                        
                                                        if (!jsonResponse) {
                                                            completion(NO,error);
                                                            return;
                                                        }
                                                        if ([jsonResponse objectForKey:@"latest_receipt_info"]) {
                                                            if ([[jsonResponse objectForKey:@"latest_receipt_info"] count]>0) {
                                                                NSDictionary *dictSuscription=[[jsonResponse objectForKey:@"latest_receipt_info"] lastObject];
                                                                NSLog(@"suscription: %@",dictSuscription);
                                                                NSString *msTime=[dictSuscription objectForKey:@"expires_date_ms"];
                                                                BOOL isExpired=[self compareDate:msTime];
                                                                if (isExpired) {
                                                                    completion(NO,error);
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    completion(YES,error);
                                                }];
    [dataTask resume];
}
-(BOOL)compareDate:(NSString *)timestampString{
    BOOL isExpired= NO;
    double timestampDouble= [timestampString doubleValue];
    double timeStamp=timestampDouble/1000;
    NSTimeInterval seconds = timeStamp;
    NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
    NSDate *today = [NSDate date];
    if ( [epochNSDate compare:today] == NSOrderedAscending) {
        isExpired=YES;
    }
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setLocale:enUSPOSIXLocale];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTodayNow=[dateFormatter2 stringFromDate:today];
    NSString *dateRecibed=[dateFormatter2 stringFromDate:epochNSDate];
    NSLog(@"Compared dates: %@ vs %@",dateTodayNow, dateRecibed);
    return isExpired;
}
- (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    }
    return base64String;
}
-(void)processPushReceived:(NSDictionary *)userinfo{
    if ([[[userinfo objectForKey:@"sendbird"] allKeys] count]>0) {
        if ([[[[userinfo objectForKey:@"sendbird"]objectForKey:@"sender"] allKeys] count]>0) {
            if ([[[[userinfo objectForKey:@"sendbird"]objectForKey:@"sender"] objectForKey:@"id"] intValue]>0) {
                [[SharedFunctions sharedInstance] openChatWithPatientId:[[[[userinfo objectForKey:@"sendbird"]objectForKey:@"sender"] objectForKey:@"id"] intValue]];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTableForNewMessages" object:self userInfo:nil];
        }
    }
    else{
        if ([[userinfo objectForKey:@"name"] length]>0) {
            [[SharedFunctions sharedInstance] openChatWithPatientName:[userinfo objectForKey:@"name"]];
        }
    }
}
-(BOOL)application:(UIApplication *)application
           openURL:(NSURL *)url options:(nonnull NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    if (url != nil && [url isFileURL]) {
        pathPdfFile=[url relativeString];
        return YES;
    }
    return handled;
}
-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler{
    
    NSURL *url = userActivity.webpageURL;
    NSString * q = [url query];
    NSArray * pairs = [q componentsSeparatedByString:@"&"];
    NSMutableDictionary * kvPairs = [NSMutableDictionary dictionary];
    for (NSString * pair in pairs) {
        NSArray * bits = [pair componentsSeparatedByString:@"="];
        NSString * key = [[bits objectAtIndex:0] stringByRemovingPercentEncoding];
        NSString * value = [[bits objectAtIndex:1] stringByRemovingPercentEncoding];
        [kvPairs setObject:value forKey:key];
    }
    
    NSLog(@"params= %@", [kvPairs description]);
    
    return YES;
}
//-(BOOL) application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//
//    if (url != nil && [url isFileURL]) {
//        NSLog(@"get url %@",url);
//    }
//    return YES;
//
//}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    //    if ([[[SharedFunctions sharedInstance] topMostController] isKindOfClass:[UINavigationController class]]) {
    //        if ([[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] count]>0) {
    //            if ([[[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] objectAtIndex:0] isKindOfClass:[ChatVC class]]) {
    //                [[[(ChatVC *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] objectAtIndex:0] closeChat];
    //            }
    //        }
    //
    //
    //    }
}
//token firebase
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    if ([[doctorInfo allKeys] count]>20) {
        if (![[doctorInfo objectForKey:@"push_token"] isEqualToString:fcmToken]) {
            [[SharedFunctions sharedInstance] updatePushToken:fcmToken];
        }
        
    }
    else{
        fcmTokenRetrieved=fcmToken;
    }
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}
- (NSString *)hexadecimalStringFromData:(NSData *)data
{
  NSUInteger dataLength = data.length;
  if (dataLength == 0) {
    return nil;
  }

  const unsigned char *dataBuffer = (const unsigned char *)data.bytes;
  NSMutableString *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
  for (int i = 0; i < dataLength; ++i) {
    [hexString appendFormat:@"%02x", dataBuffer[i]];
  }
  return [hexString copy];
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSString *token = [self hexadecimalStringFromData:deviceToken];
    

    self.updatedPushToken = token;

    
    [SBDMain registerDevicePushToken:token unique:YES completionHandler:^(SBDPushTokenRegistrationStatus status, SBDError * _Nullable error) {
        if (error == nil) {
            if (status == SBDPushTokenRegistrationStatusPending) {
                // Registration is pending.
                // If you get this status, invoke `+ registerDevicePushToken:unique:completionHandler:` with `[SBDMain getPendingPushToken]` after connection.
            }
            else {
                // Registration succeeded
                NSLog(@"register push token sendbird sucess");
            }
        }
        else {
            // Registration failed.
            NSLog(@"register push token sendbird sucess");
        }
    }];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSLog(@"Failed to get token, error: %@", error);
    self.updatedPushToken = nil;
}
//- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
//    if(notificationSettings.types == UIUserNotificationTypeNone) {
//        NSLog(@"Failed to get token, error: Notifications are not allowed");
//        if (self.chatClient) {
//            [self.chatClient registerWithToken:nil];
//        } else {
//            self.updatedPushToken = nil;
//        }
//    } else {
//        [[UIApplication sharedApplication] registerForRemoteNotifications];
//    }
//}
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    // If your application supports multiple types of push notifications, you may wish to limit which ones you send to the TwilioChatClient here
//    if (self.chatClient && userInfo) {
//        // If your reference to the Chat client exists and is initialized, send the notification to it
//        [self.chatClient handleNotification:userInfo completion:^(TCHResult *result) {
//            if (![result isSuccessful]) {
//                // Handling of notification was not successful, retry?
//            }
//        }];
//    } else {
//        // Store the notification for later handling
//        self.receivedNotification = userInfo;
//        [self processPushReceived:userInfo];
//    }
//}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    // If you are receiving a notification message while your app is in the background,
//    // this callback will not be fired till the user taps on the notification launching the application.
//    // TODO: Handle data of notification
//    
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//    
//    // Print message ID.
////    if (userInfo[kGCMMessageIDKey]) {
////        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
////    }
//    
//    // Print full message.
//    NSLog(@"%@", userInfo);
//    
//        // Store the notification for later handling
//        self.receivedNotification = userInfo;
//        
//        
//        [self processPushReceived:userInfo];
//        completionHandler(UIBackgroundFetchResultNewData);
//    
//}

// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    NSLog(@"%@", userInfo);
    self.receivedNotification = userInfo;
    
    if ([[[SharedFunctions sharedInstance] topMostController] isKindOfClass:[UINavigationController class]]) {
        if ([[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] count]>0) {
            if ([[[(UINavigationController *)[[SharedFunctions sharedInstance] topMostController] childViewControllers] objectAtIndex:0] isKindOfClass:[ChatVC class]]) {
                NSString *idPatient=[[[userInfo objectForKey:@"channel_title"] componentsSeparatedByString:@"__"]objectAtIndex:1];
                if([[dictPatientPresential objectForKey:@"id"] intValue]==[idPatient intValue]){
                    completionHandler(UNNotificationPresentationOptionSound);
                    return;
                }
            }
        }
        
        
    }
    else{
        completionHandler(UNNotificationPresentationOptionAlert);
    }
    
    [self processPushReceived:userInfo];
    completionHandler(UNNotificationPresentationOptionAlert);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
#if defined(__IPHONE_11_0)
         withCompletionHandler:(void(^)(void))completionHandler {
#else
withCompletionHandler:(void(^)())completionHandler {
#endif
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    NSLog(@"%@", userInfo);
    if ([[userInfo allKeys] count]<=0) {
        if ([response.notification.request.content.title length]>0) {
            userInfo=@{@"name":response.notification.request.content.title};
        }
    }
    [self processPushReceived:userInfo];
    completionHandler();
}
#endif
    
- (void)applicationWillEnterForeground:(UIApplication *)application {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    
- (void)applicationDidBecomeActive:(UIApplication *)application {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    
- (void)applicationWillTerminate:(UIApplication *)application {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    @end
