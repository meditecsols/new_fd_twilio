//
//  Config.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#ifndef Config_h
#define Config_h


#endif /* Config_h */

#define URL_PROD @"https://business.family-doc.com"
#define URL_DEV @"https://dev-business.family-doc.com"
#define URL_SANDBOX @"https://sandbox-business.family-doc.com"
#define BASE_URL URL_PROD

#define KEY_SENDBIRD_PROD @"D2B04E2C-B0B5-4FFF-B5CB-38B2E773FEF0"
#define KEY_SENDBIRD_SANDBOX @"28C9BA3B-5D6E-4234-A78A-61458DB91461"
#define KEY_SENDBIRD_DEV @"FECB2EAC-231E-47E6-AD4E-946F4F35C55F"
#define KEY_SENDBIRD KEY_SENDBIRD_PROD



#define URL_ZIP_CODE @"https://api-codigos-postales.herokuapp.com/v2/codigo_postal/"

#define URL_SCHEME_PATIENT @"familydocpaciente://"
#define URL_APPSTORE_PATIENT @"https://itunes.apple.com/us/app/family-doc/id1125176990?mt=8"
#define URL_PLAYSTORE_PATIENT @"https://play.google.com/store/apps/details?id=mx.nyxtechnology.familydoc&hl=es_MX"


#define SUSCRIPTION_PRODUCT_ID @"com.meditec.FamilyDocDoctor.DoctorSuscriptionMonthly"
#define SHARED_SECRET_SUSCRIPTION_KEY @"d6ff1b97dfa24a3c91f84426eb1d7a5f"
