//
//  AddServiceVC.m
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 27/07/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "AddServiceVC.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "InvokeService.h"

@interface AddServiceVC ()

@end

@implementation AddServiceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    _txtFieldPrice.delegate=self;
    _txtFieldName.delegate=self;
    [_txtFieldName becomeFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    if ([[_dictService allKeys] count]>0) {
        _txtFieldName.text=[_dictService objectForKey:@"name"];
        _txtFieldPrice.text=[NSString stringWithFormat:@"$%@",[_dictService objectForKey:@"price"]];
        _lblTitle.text=@"Edita tu servicio";
    }
}
-(void)dismissKeyboard {
    [_txtFieldPrice resignFirstResponder];
    [_txtFieldName resignFirstResponder];
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (_txtFieldName.text.length<=0) {
        return;
    }
    if (_txtFieldPrice.text.length<=0) {
        return;
    }

    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
}
- (IBAction)actionNext:(id)sender{
    [self.view endEditing:YES];
    if ([_txtFieldName.text length]<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el nombre del servicio"];
        return;
    }
    if ([_txtFieldName.text isEqualToString:@"$0.00"]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar el precio del servicio"];
        return;
    }
    if ([[_dictService allKeys] count]>0) {
         NSMutableDictionary *dictServiceLocal=[[NSMutableDictionary alloc] init];
        [dictServiceLocal setObject:_txtFieldName.text forKey:@"name"];
        [dictServiceLocal setObject:[_txtFieldPrice.text substringFromIndex:1] forKey:@"price"];
        [dictServiceLocal setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [dictServiceLocal setObject:[_dictService objectForKey:@"id"] forKey:@"id"];
        
        
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke updateServiceWithData:dictServiceLocal WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        if (!error) {
                            if ([[responseData allKeys]count]>0) {
                                UIAlertController * alert=  [UIAlertController
                                                             alertControllerWithTitle:@""
                                                             message:@"Se ha actualizado el servicio exitosamente"
                                                             preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                UIAlertAction* okAction = [UIAlertAction
                                                           actionWithTitle:@"Entendido"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                                           {
                                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                               
                                                               
                                                           }];
                                
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:NO completion:nil];
                                
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar el servicio."];
                            }
                        }
                        else{
                            [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al actualizar el servicio."];
                        }
                    }];
                    
                });
            }];
        }];
    }
    else{
        NSMutableDictionary *dictServiceLocal=[[NSMutableDictionary alloc] init];
        [dictServiceLocal setObject:_txtFieldName.text forKey:@"name"];
        [dictServiceLocal setObject:[_txtFieldPrice.text substringFromIndex:1] forKey:@"price"];
        [dictServiceLocal setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke addServiceWithData:dictServiceLocal WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                        if (!error) {
                            if ([[responseData allKeys]count]>0) {
                                UIAlertController * alert=  [UIAlertController
                                                             alertControllerWithTitle:@""
                                                             message:@"Se ha guardado el servicio exitosamente"
                                                             preferredStyle:UIAlertControllerStyleAlert];
                                
                                
                                UIAlertAction* okAction = [UIAlertAction
                                                           actionWithTitle:@"Entendido"
                                                           style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * action)
                                                           {
                                                               [self dismissViewControllerAnimated:YES completion:nil];
                                                               
                                                               
                                                           }];
                                
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:NO completion:nil];
                                
                                
                            }
                            else{
                                [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar el servicio."];
                            }
                        }
                        else{
                            [[SharedFunctions sharedInstance] showAlertWithMessage:@"Error al registrar el servicio."];
                        }
                    }];
                    
                });
            }];
        }];
    }
    
    
    
}
- (IBAction)actionBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField isEqual:_txtFieldPrice]) {
        NSString *cleanCentString = [[textField.text componentsSeparatedByCharactersInSet: [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        NSInteger centValue = [cleanCentString intValue];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        NSNumber *myNumber = [f numberFromString:cleanCentString];
        NSNumber *result;
        
        if([textField.text length] < 16){
            if (string.length > 0)
            {
                centValue = centValue * 10 + [string intValue];
                double intermediate = [myNumber doubleValue] * 10 +  [[f numberFromString:string] doubleValue];
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            else
            {
                centValue = centValue / 10;
                double intermediate = [myNumber doubleValue]/10;
                result = [[NSNumber alloc] initWithDouble:intermediate];
            }
            
            myNumber = result;
            NSLog(@"%ld ++++ %@", (long)centValue, myNumber);
            NSNumber *formatedValue;
            formatedValue = [[NSNumber alloc] initWithDouble:[myNumber doubleValue]/ 100.0f];
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:formatedValue];

            
            return NO;
        }else{
            
            NSNumberFormatter *_currencyFormatter = [[NSNumberFormatter alloc] init];
            [_currencyFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            textField.text = [_currencyFormatter stringFromNumber:@0];
            
            return NO;
        }
    }
    else{
        return YES;
    }
    
    
    return YES;
}

@end
