//
//  HomeViewsContainerVC.m
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 06/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "HomeViewsContainerVC.h"
#import "GlobalMembers.h"

@interface HomeViewsContainerVC ()
@property (nonatomic) CGFloat lastContentOffset;
@end

@implementation HomeViewsContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _layoutConstraintWidthContainer.constant=self.view.frame.size.width*3;
    _scrollContainer.delegate=self;
    [self.view layoutIfNeeded];
    pageHomeSelected=2;
    
    UIStoryboard *storyboard = self.storyboard;
    
    UIViewController *viewController1 = [storyboard instantiateViewControllerWithIdentifier:@"HomeAjustesVC"];
    [self addChildViewController:viewController1];
    [_viewHome1 addSubview:viewController1.view];
    [viewController1 didMoveToParentViewController:self];
    
    UIViewController *viewController2 = [storyboard instantiateViewControllerWithIdentifier:@"HomePacientesVC"];
    [self addChildViewController:viewController2];
    [_viewHome2 addSubview:viewController2.view];
    [viewController2 didMoveToParentViewController:self];

    UIViewController *viewController3 = [storyboard instantiateViewControllerWithIdentifier:@"ConsultasVC"];
    [self addChildViewController:viewController3];
    [_viewHome3 addSubview:viewController3.view];
    [viewController3 didMoveToParentViewController:self];

    [_scrollContainer setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    self.lastContentOffset = scrollView.contentOffset.x;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (self.lastContentOffset < scrollView.contentOffset.x) {
        if (pageHomeSelected<3) {
            pageHomeSelected=pageHomeSelected+1;
        }
        [self.view endEditing:YES];
        // moved right
    } else if (self.lastContentOffset > scrollView.contentOffset.x) {
        // moved left
        if (pageHomeSelected>1) {
            pageHomeSelected=pageHomeSelected-1;
        }
        [self.view endEditing:YES];
    } else {
        // didn't move
    }
    CGFloat width = scrollView.frame.size.width;
    int page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    pageHomeSelected=page+1;
    if (pageHomeSelected==1) {
        [self setupButtonsFirstView];
    }
    else if (pageHomeSelected==2) {
        [self setupButtonsSecondView];
    }
    else if (pageHomeSelected==3) {
        [self setupButtonsThirdView];
    }
}
-(void)moveToFirstView{
    [self setupButtonsFirstView];
    [self.view endEditing:YES];
    pageHomeSelected=1;
    [self.scrollContainer setContentOffset:CGPointMake(0, 0)];
}
-(void)moveToSecondView{
    [self setupButtonsSecondView];
    [self.view endEditing:YES];
    pageHomeSelected=2;
    [self.scrollContainer setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
}
-(void)moveToThirdView{
    [self setupButtonsThirdView];
    [self.view endEditing:YES];
    pageHomeSelected=3;
    [self.scrollContainer setContentOffset:CGPointMake(self.view.frame.size.width*2, 0)];
}
-(void)setupButtonsFirstView{
    _btnMenu.selected=YES;
    _btnPacientes.selected=NO;
    _btnConsultas.selected=NO;
    _btnPacientes.highlighted=YES;
    _btnConsultas.highlighted=YES;
}
-(void)setupButtonsSecondView{
    _btnMenu.selected=NO;
    _btnPacientes.selected=YES;
    _btnConsultas.selected=NO;
    _btnPacientes.highlighted=NO;
    _btnConsultas.highlighted=NO;
}
-(void)setupButtonsThirdView{
    _btnMenu.selected=NO;
    _btnPacientes.selected=NO;
    _btnConsultas.selected=YES;
    _btnPacientes.highlighted=NO;
    _btnConsultas.highlighted=NO;
}
- (IBAction)actionOpenMenu:(id)sender {
    
    [self moveToFirstView];
}
- (IBAction)actionOpenPacientes:(id)sender {
    
    [self moveToSecondView];
}

- (IBAction)actionOpenConsultas:(id)sender {
    
    [self moveToThirdView];
}
@end
