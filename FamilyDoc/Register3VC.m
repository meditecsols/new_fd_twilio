//
//  Register3VC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 07/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "Register3VC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "SexyTooltip.h"

@interface Register3VC (){
    NSMutableArray *arrayNeighborHood;
    UIPickerView *neighbordHoodPicker;
}

@end

@implementation Register3VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.tintColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Buscar" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    _txtZipCode.inputAccessoryView = numberToolbar;
    [numberToolbar sizeToFit];
    _txtNeighbordHood.enabled=NO;
    _txtZipCode.delegate= self;
    _txtProvince.delegate=self;
    _txtState.delegate=self;
    _txtCountry.delegate=self;
    _txtNeighbordHood.delegate=self;
    _txtStreet.delegate=self;
    _txtExtNumb.delegate=self;
    _txtInt.delegate=self;
    
    _btnSkip.layer.borderColor=[UIColor whiteColor].CGColor;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(void)cancelNumberPad{
    [_txtZipCode resignFirstResponder];
    _txtZipCode.text = @"";
    _txtProvince.text=@"";
    _txtState.text=@"";
    _txtCountry.text=@"";
    _txtNeighbordHood.text=@"";
    [self verifyFields];
}
-(void)doneWithNumberPad{
    if (_txtZipCode.text.length==5) {
        _txtProvince.text=@"";
        _txtState.text=@"";
        _txtCountry.text=@"";
        _txtNeighbordHood.text=@"";
        
        
        [_txtZipCode resignFirstResponder];
        InvokeService *invoke=[[InvokeService alloc] init];
        [[SharedFunctions sharedInstance] showLoadingView];
        [invoke retrieveZipCodeData:_txtZipCode.text WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                NSLog(@"response:%@",[responseData description]);
                NSDictionary *data=[[NSDictionary alloc] initWithDictionary:responseData];
                self->_txtNeighbordHood.enabled=YES;
                self->_txtProvince.text=[data objectForKey:@"municipio"];
                self->_txtState.text=[data objectForKey:@"estado"];
                self->_txtCountry.text=@"México";
                self->arrayNeighborHood=[[NSMutableArray alloc] init];
                self->arrayNeighborHood=[data objectForKey:@"colonias"];

                if ([self->arrayNeighborHood count]>0) {
                    self->neighbordHoodPicker = [[UIPickerView alloc]init];
                    self->neighbordHoodPicker.tintColor = [UIColor whiteColor];
                    self->neighbordHoodPicker.dataSource = self;
                    self->neighbordHoodPicker.delegate = self;
                    [self->_txtNeighbordHood setInputView:self->neighbordHoodPicker];
                    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
                    toolBar.barStyle = UIBarStyleDefault;
                    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingNeightborHood)];
                    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
                    toolBar.tintColor = [UIColor darkGrayColor];
                    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
                    self->_txtNeighbordHood.inputAccessoryView = toolBar;
                    //bloodField = cell.txtField;
                    [self->_txtNeighbordHood becomeFirstResponder];
                }
                else{
                    [self showAlertWithMessage:@"No se encontraron datos para ese código postal."];
                }
            });
            
        }];
        
    }
    else{
        [self showAlertWithMessage:@"Ingrese un código postal válido."];
    }
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    

    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - Picker Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return arrayNeighborHood.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
        return arrayNeighborHood[row];
    
}
- (void) donePickingNeightborHood {
    int row = (int) [neighbordHoodPicker selectedRowInComponent:0];
    _txtNeighbordHood.text =  [self pickerView:neighbordHoodPicker titleForRow:row forComponent:0];
    [self hideKeyboard];
}
- (void) hideKeyboard{
    [self.view endEditing:YES];
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (_txtStreet.text.length<=0) {
        return;
    }
    if (_txtExtNumb.text.length<=0) {
        return;
    }
    if (_txtZipCode.text.length<=0) {
        return;
    }
    if (_txtNeighbordHood.text.length<=0) {
        return;
    }
    if (_txtProvince.text.length<=0) {
        return;
    }
    if (_txtState.text.length<=0) {
        return;
    }
    if (_txtCountry.text.length<=0) {
        return;
    }
    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionNext:(id)sender {
    if (_txtStreet.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar tu Calle"];
        return;
    }
    if (_txtExtNumb.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar el número de tu calle"];
        return;
    }
    if (_txtZipCode.text.length<5) {
        [self showAlertWithMessage:@"Debes ingresar tu Código Postal"];
        return;
    }
    if (_txtNeighbordHood.text.length<=0) {
        [self showAlertWithMessage:@"Debes seleccionar tu Colonia"];
        return;
    }

    NSMutableDictionary *dictInfo=[[NSMutableDictionary alloc] init];
    [dictInfo setObject:_txtStreet.text forKey:@"street"];
    [dictInfo setObject:_txtExtNumb.text forKey:@"street_num"];
    [dictInfo setObject:_txtCountry.text forKey:@"country"];
    [dictInfo setObject:@"0" forKey:@"int_num"];
    if (_txtInt.text.length>0) {
        [dictInfo setObject:_txtInt.text forKey:@"int_num"];
    }

    [dictInfo setObject:_txtProvince.text forKey:@"borough"];
    [dictInfo setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
    [dictInfo setObject:_txtProvince.text forKey:@"city"];
    [dictInfo setObject:_txtNeighbordHood.text forKey:@"neighbourhood"];
    [dictInfo setObject:_txtState.text forKey:@"state"];
    [dictInfo setObject:_txtZipCode.text forKey:@"postal_code"];
    [dictInfo setObject:@0 forKey:@"priority"];
    [dictInfo setObject:_txtZipCode.text forKey:@"postal_code"];
    
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke addAddressWithData:dictInfo WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                [[SharedFunctions sharedInstance] removeRegisterViews];
            }];
            
            

        });
        
    }];


    
}

- (IBAction)actionSkip:(id)sender {
//    UIStoryboard *storyboard = self.storyboard;
//    UIViewController *home= [storyboard instantiateViewControllerWithIdentifier:@"HomeViewsContainerVC"];
//    home.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:home animated:YES completion:nil];
    
     [[SharedFunctions sharedInstance] removeRegisterViews];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([textField isEqual:_txtZipCode]) {
        NSAttributedString *text=[[NSAttributedString alloc] initWithString:@"Ingresa tu código postal y presiona Buscar" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 18.0f]}];
        
        SexyTooltip *greetingsTooltip = [[SexyTooltip alloc] initWithAttributedString:text sizedToView:textField withPadding:UIEdgeInsetsMake(10, 10, 10, 10) andMargin:UIEdgeInsetsMake(0, 0, 0, 0)];
        greetingsTooltip.color=[UIColor whiteColor];
        [greetingsTooltip presentFromView:textField
                                   inView:textField
                               withMargin:25
                                 animated:YES];
        [greetingsTooltip dismissInTimeInterval:4];
    }
    
}
@end
