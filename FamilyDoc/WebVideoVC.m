//
//  WebVideoVC.m
//  FamilyDocBusiness
//
//  Created by Martin Gonzalez on 05/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "WebVideoVC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"


@interface WebVideoVC ()

@end

@implementation WebVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getUrlsTermsPrivacyWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        NSString *urlVideo;
        if([responseData isKindOfClass:[NSArray class]]){
            for (NSDictionary *dict in responseData) {
                if ([[dict objectForKey:@"label"] isEqualToString:@"video-ayuda-doc"]) {
                    urlVideo=[dict objectForKey:@"url"];
                    break;
                }
            }
            if (urlVideo.length>0) {
                if ([urlVideo containsString:@"/"]) {
                    NSArray *array=[urlVideo componentsSeparatedByString:@"/"];
                    NSString *identifier=[array lastObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                            NSDictionary *paramDic = @{
                                                       @"showinfo": @0
                                                       };
                            [self.playerView loadWithVideoId:identifier playerVars:paramDic];
                        }];
                    });
                }
                
            }
            
        }
    }];
}

- (IBAction)closeView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
