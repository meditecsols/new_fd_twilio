//
//  ClosingVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 12/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClosingVC : UIViewController

@property (strong, nonatomic) IBOutlet UIView *contentView;
- (IBAction)actionBack:(id)sender;
@end
