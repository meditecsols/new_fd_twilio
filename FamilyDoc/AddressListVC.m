//
//  AddressListVC.m
//  
//
//  Created by Martin Gonzalez on 17/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "AddressListVC.h"
#import "AddressTVC.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "AddressForm.h"

#define HEIGHT_ROW 80

@interface AddressListVC (){
    NSMutableArray *addressArray;
}

@end

@implementation AddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.myTableView setSeparatorColor:[UIColor whiteColor]];
    _myTableView.delegate=self;
    _myTableView.dataSource=self;
    _myTableView.backgroundView.backgroundColor= [UIColor whiteColor];
    _myTableView.backgroundColor = [UIColor whiteColor];
    _constraintTableHeight.constant=0;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getAdressList];
}
-(void)getAdressList{
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getAddressWithData:[doctorInfo objectForKey:@"id"] WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            self->addressArray=[[NSMutableArray alloc] initWithArray:responseData];
            [self->_myTableView reloadData];
            [self resizeTable];
        });
        
    }];
}
-(void)resizeTable{
    
    _constraintTableHeight.constant=HEIGHT_ROW *[addressArray count];
    _constraintViewContainerHeight.constant= _constraintTableHeight.constant + 60;
    _myTableView.scrollEnabled=NO;
    
    
    [self.view layoutSubviews];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionAddAddress:(id)sender {
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [addressArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_ROW;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"AddressTVC";
    AddressTVC *cell = (AddressTVC *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
        if ([[[addressArray objectAtIndex:indexPath.row] objectForKey:@"is_fiscal"] boolValue]) {
            cell.lblTitle.text=@"DIRECCIÓN FISCAL";
        }
        else if ([[[addressArray objectAtIndex:indexPath.row] objectForKey:@"priority"] intValue]==0){
            cell.lblTitle.text=@"CONSULTORIO PRINCIPAL";
        }
        else{
            cell.lblTitle.text=@"CONSULTORIO";
        }
        cell.lblDesc.text=[NSString stringWithFormat:@"%@ %@,Col. %@,%@ %@,%@",[[addressArray objectAtIndex:indexPath.row] objectForKey:@"street"],[[addressArray objectAtIndex:indexPath.row] objectForKey:@"street_num"],[[addressArray objectAtIndex:indexPath.row] objectForKey:@"neighbourhood"],[[addressArray objectAtIndex:indexPath.row] objectForKey:@"city"],[[addressArray objectAtIndex:indexPath.row] objectForKey:@"state"],[[addressArray objectAtIndex:indexPath.row] objectForKey:@"country"]];
        [cell.btnEdit addTarget:self action:@selector(editAddress:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnEdit.tag = indexPath.row;
        
    }

    return cell;
}
- (void)editAddress:(UIButton *) sender{
    NSMutableDictionary *addressDict=[[NSMutableDictionary alloc] initWithDictionary:[addressArray objectAtIndex:sender.tag]];
    UIStoryboard *storyboard = self.storyboard;
    AddressForm *form= [storyboard instantiateViewControllerWithIdentifier:@"AddressForm"];
    form.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    form.modalPresentationStyle= UIModalPresentationOverFullScreen;
    form.dictAddress=addressDict;
    form.comeToEdit=YES;
    [self presentViewController:form animated:YES completion:^{
        
    }];
    
    
}
@end
