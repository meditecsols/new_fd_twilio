//
//  ChatVC.m
//
//
//  Created by Martin Gonzalez on 24/04/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "ChatVC.h"
#import "InvokeService.h"
#import "GlobalMembers.h"
#import "Base64.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <NYTPhotoViewer/NYTPhotosViewController.h>
#import <NYTPhotoViewer/NYTPhotoViewerArrayDataSource.h>
#import "FamilyPhoto.h"
#import "SharedFunctions.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Config.h"
#import "MessageTransform.h"
#import "RMCustomViewActionController.h"
#import "Utils.h"
#import "ConnectionManager.h"
#import <PDFKit/PDFKit.h>
#import "PatientHistoricVC.h"


@interface ChatVC ()<ConnectionManagerDelegate>
{
    NSDictionary *users;
    NSMutableArray *messageBublees;
    BOOL isCameraMedia;
    NSString *dataImg;
    int cont;
    BOOL isVideo;
    NSString *documentsDirectoryURL;
    NSString *nameOfChatMember;
    //PDFViewController *pdfVC;
    NSMutableArray *messagesDataFilesToDownload;
    NSMutableArray *consultasArray;
}

@property (strong, nonatomic) NSString *identity;

@property (atomic) BOOL hasNext;
@property (atomic) long long minMessageTimestamp;
@property (strong, nonatomic) NSArray<SBDBaseMessage *> *dumpedMessages;
@property (strong, nonatomic) NSMutableArray<SBDBaseMessage *> *messages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, SBDBaseMessage *> *resendableMessages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, SBDBaseMessage *> *preSendMessages;
@property (strong, nonatomic) NSMutableDictionary<NSString *, NSDictionary<NSString *, NSObject *> *> *resendableFileData;
@property (strong, nonatomic) NSMutableDictionary<NSString *, NSDictionary<NSString *, NSObject *> *> *preSendFileData;
@property (atomic) BOOL cachedMessage;
@property (atomic) BOOL initialLoading;
@property (atomic) BOOL isLoading;
@property (atomic) BOOL keyboardShown;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typingIndicatorContainerViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView *typingIndicatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *typingIndicatorLabel;
@property (weak, nonatomic) IBOutlet UIView *typingIndicatorContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typingIndicatorImageHeight;

@property (strong, nonatomic) UIViewController *pdfVC;
@property (strong, nonatomic) PatientHistoricVC *patientHistoricController;

@end

@implementation ChatVC
@synthesize messages=_messages;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.messages=[[NSMutableArray alloc] init];
    messageBublees=[[NSMutableArray alloc] init];
    messagesDataFilesToDownload=[[NSMutableArray alloc] init];
    documentsDirectoryURL = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];

    self.senderId =[NSString stringWithFormat:@"%d",[[doctorInfo objectForKey:@"id"] intValue] ];
    NSString *prefix=@"Dr.";
    if ([[doctorInfo objectForKey:@"gender"] isEqualToString:@"female"]) {
        prefix= @"Dra.";
    }
    NSString *nickname=[[NSString stringWithFormat:@"%@ %@ %@",prefix,[doctorInfo objectForKey:@"first_name"],[doctorInfo objectForKey:@"last_name"]] uppercaseString];
    self.senderDisplayName =nickname;
    
    nameOfChatMember=[NSString stringWithFormat:@"%@ %@",[dictPatientPresential objectForKey:@"first_name"],[dictPatientPresential objectForKey:@"last_name"]];
    
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    _outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:0.25]];
    _incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.945 green:0.941 blue:0.941 alpha:1.00]];
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillTerminate:)
                                                 name:UIApplicationWillTerminateNotification
                                               object:nil];
    
    self.isLoading = NO;
    
    self.dumpedMessages = [Utils loadMessagesInChannel:self.channel.channelUrl];
    self.minMessageTimestamp = LLONG_MAX;
    self.cachedMessage = NO;
    [SBDMain addChannelDelegate:self identifier:self.description];
    if (self.dumpedMessages.count > 0) {
    }
    if ([SBDMain getConnectState] == SBDWebSocketClosed) {
        [ConnectionManager loginWithCompletionHandler:^(SBDUser * _Nullable user, NSError * _Nullable error) {
            if (error != nil) {
                return;
            }
        }];
    }
    else {
        [self loadPreviousMessage:YES];
    }
    
    [self setNavigationBarTitleViewWithName:nameOfChatMember];
    // InvokeService *invoke=[[InvokeService alloc] init];
    // [invoke addAChanelWithData:dictPatientPresential WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
    //    NSLog(@"response %@",[responseData objectForKey:@"channel"]);
    //}];
    if (@available(iOS 11.0, *)) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        NSLayoutConstraint *constraint = [self performSelector:@selector(toolbarBottomLayoutGuide)];
#pragma clang diagnostic pop
        constraint.priority = 999;
        [self.inputToolbar.bottomAnchor constraintLessThanOrEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor].active = YES;
         self.inputToolbar.contentView.backgroundColor = [UIColor whiteColor];
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect rect = CGRectMake(10, 0,40,55);
    UIButton *backButton = [[UIButton alloc] init];
    backButton.frame=rect;
    //[backButton setBackgroundImage:[UIImage imageNamed:@"buttonBackBlue"]
    //                 forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00] forState:UIControlStateNormal];
    backButton.contentMode = UIViewContentModeScaleAspectFit;
    [backButton setImage:[UIImage imageNamed:@"backBlueArrow"] forState:UIControlStateNormal];
    [backButton addTarget:self
                   action:@selector(closePressed:)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    self.navigationItem.hidesBackButton = YES;
    
    NSMutableArray<UIBarButtonItem *> *itemarray = [NSMutableArray new];
    CGRect rect2 = CGRectMake(10, 0,5,5);
    _consultationButton = [[UIButton alloc] init];
    _consultationButton.frame=rect2;
    _consultationButton.contentMode = UIViewContentModeScaleAspectFit;
    [_consultationButton setImage:[UIImage imageNamed:@"pillIcon"] forState:UIControlStateNormal];
    [_consultationButton addTarget:self
                   action:@selector(openConsultation)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *consultationButtonItem = [[UIBarButtonItem alloc] initWithCustomView:_consultationButton];
    //self.navigationItem.rightBarButtonItem = consultationButtonItem;
    [itemarray addObject:consultationButtonItem];
    
    CGRect rect3 = CGRectMake(40, 0,5,5);
    UIButton *videoButton = [[UIButton alloc] init];
    videoButton.frame=rect3;
    //[backButton setBackgroundImage:[UIImage imageNamed:@"buttonBackBlue"]
    //                 forState:UIControlStateNormal];
    [videoButton setTitleColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00] forState:UIControlStateNormal];
    videoButton.contentMode = UIViewContentModeScaleAspectFit;
    [videoButton setImage:[UIImage imageNamed:@"ic_video"] forState:UIControlStateNormal];
    [videoButton addTarget:self
                   action:@selector(openvideo:)
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *videoButtonItem = [[UIBarButtonItem alloc] initWithCustomView:videoButton];
    
     [itemarray addObject:videoButtonItem];
    
    self.navigationItem.rightBarButtonItems = itemarray;

    
}
-(void)viewWillDisappear:(BOOL)animated{
    comeFromChat=YES;
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state != UIApplicationStateBackground && state != UIApplicationStateInactive)
    {
        needRefreshPatients=YES;
    }
    [super viewWillDisappear:YES];
    [Utils dumpMessages:self.messages resendableMessages:self.resendableMessages resendableFileData:self.resendableFileData preSendMessages:self.preSendMessages channelUrl:self.channel.channelUrl];
}




- (void)applicationWillTerminate:(NSNotification *)notification {
    [Utils dumpMessages:self.messages resendableMessages:self.resendableMessages resendableFileData:self.resendableFileData preSendMessages:self.preSendMessages channelUrl:self.channel.channelUrl];
}
- (void)dealloc {
    [ConnectionManager removeConnectionObserver:self];
}

-(void)setNavigationBarTitleViewWithName:(NSString *)name{
    
    UIView *whole = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 100, 100)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, 40, 40)];
    
    // [imageView.layer setCornerRadius:25.0f];
    [imageView setClipsToBounds:YES];
    [imageView setContentMode:UIViewContentModeScaleToFill];
    
    
    [imageView setImage:[UIImage imageNamed:@"imgTestProfile"]];
 
        
     //
    if ([[dictPatientPresential objectForKey:@"profile_pic"] length]>0) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:[dictPatientPresential objectForKey:@"profile_pic"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image) {
                [self roundImage:imageView];
            }
            else{
                [imageView setImage:[UIImage imageNamed:@"imgTestProfile"]];
            }
            
        }];
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(openHistoric:)];
    [imageView setUserInteractionEnabled:YES];
    [imageView addGestureRecognizer:tap];
    
    //    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, [UIScreen mainScreen].bounds.size.width,18)];
    //    [titleLabel setText:@"Paciente"];
    //    [titleLabel setTextColor:[UIColor whiteColor]];
    //    [titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:16]];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15,whole.frame.size.width-60, 15)];
    [nameLabel setText:name];
    [nameLabel setTextColor:[UIColor colorWithRed:0.525 green:0.525 blue:0.525 alpha:1.00]];
    [nameLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:15]];
    
    //[whole addSubview:titleLabel];
    [whole addSubview:nameLabel];
    [whole addSubview:imageView];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor ];
    self.navigationItem.titleView = whole;
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor grayColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
#pragma mark - Avtar images area

//-(void)avatarImagesForUrlString:(NSString *)urlString andOnCompletion:(void(^)(UIImage *image))completion{
//
//    if([urlString isEqualToString:@""] || [urlString isEqual:[NSNull null]]){
//
//        completion(nil);
//
//    }else{
//
//        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:urlString] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
//
//            completion ([self cropAvatarImages:image]);
//        }];
//    }
//}

//-(UIImage *)cropAvatarImages:(UIImage *)image{
//
//    if(image){
//        return [JSQMessagesAvatarImageFactory circularAvatarImage:image withDiameter:48];
//    }else{
//        if([self.friendUserDetails.userDetails.gender isEqualToString:@"M"]){
//            return [UIImage imageNamed:@"no_image_male"];
//        }else{
//            return [UIImage imageNamed:@"no_image_female"];
//        }
//    }
//}
-(void)viewDidLayoutSubviews{
    //    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    //        CAGradientLayer *gradient = [CAGradientLayer layer];
    //        CGRect gradientFrame = self.navigationController.navigationBar.bounds;
    //        gradientFrame.size.height += [UIApplication sharedApplication].statusBarFrame.size.height;
    //        gradient.frame = gradientFrame;
    //        gradient.colors = @[(id)[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00].CGColor, (id)[UIColor colorWithRed:0.090 green:0.133 blue:0.302 alpha:1.00].CGColor];
    
    //  [self.navigationController.navigationBar setBackgroundImage:[self imageFromLayer:gradient] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:0.973 green:0.973 blue:0.973 alpha:1.00]];
   
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName : [UIFont fontWithName:@"MyriadPro-Light" size:20] ,
       NSForegroundColorAttributeName:[UIColor colorWithRed:0.525 green:0.525 blue:0.525 alpha:1.00]}];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
#pragma mark -
- (void)loadPreviousMessage:(BOOL)initial {
    long long timestamp = 0;
    if (initial) {
        self.hasNext = YES;
        timestamp = LLONG_MAX;
    }
    else {
        timestamp = self.minMessageTimestamp;
    }
    
    if (self.hasNext == NO) {
        return;
    }
    
    if (self.isLoading) {
        return;
    }
    
    self.isLoading = YES;
    
    
   
    
    
    [self.channel getPreviousMessagesByTimestamp:timestamp limit:30 reverse:!initial messageType:SBDMessageTypeFilterAll customType:@"" completionHandler:^(NSArray<SBDBaseMessage *> * _Nullable messages, SBDError * _Nullable error) {
        if (error != nil) {
            self.isLoading = NO;
            
            return;
        }
        
        self.cachedMessage = NO;
        
        if (messages.count == 0) {
            self.hasNext = NO;
        }
        
        if (initial) {
            [self.messages removeAllObjects];
            
            for (SBDBaseMessage *message in messages) {
                [self.messages addObject:message];
                
                if (self.minMessageTimestamp > message.createdAt) {
                    self.minMessageTimestamp = message.createdAt;
                }
            }
            
            NSArray *resendableMessagesKeys = [self.resendableMessages allKeys];
            for (NSString *key in resendableMessagesKeys) {
                [self.messages addObject:self.resendableMessages[key]];
            }
            
            NSArray *preSendMessagesKeys = [self.preSendMessages allKeys];
            for (NSString *key in preSendMessagesKeys) {
                [self.messages addObject:self.preSendMessages[key]];
            }
            
            [self.channel markAsRead];
            
            self.initialLoading = YES;
            
            if (messages.count > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (SBDBaseMessage *message in messages) {
                        [self processMessage:message];
                    }
                    [self finishReceivingMessage];
                    if ([self->messagesDataFilesToDownload count]>0) {
                        [self downloadMissingFiles];
                    }
                    
                });
            }
            
            self.initialLoading = NO;
            self.isLoading = NO;
        }
        else {
            if (messages.count > 0) {
                for (SBDBaseMessage *message in messages) {
                    [self.messages insertObject:message atIndex:0];
                    
                    if (self.minMessageTimestamp > message.createdAt) {
                        self.minMessageTimestamp = message.createdAt;
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    for (SBDBaseMessage *message in messages) {
                        [self processMessage:message];
                    }
                    [self finishReceivingMessage];
                    if ([self->messagesDataFilesToDownload count]>0) {
                        [self downloadMissingFiles];
                    }
                });
            }
            
            self.isLoading = NO;
        }
        
        if ([pathPdfFile length]>0) {
            [self sendPdfReceived];
        }
        self.messages=[messages mutableCopy];
    }];
    
    
}
-(void)processMessage:(SBDBaseMessage *)message{
    if ([message isKindOfClass:[SBDUserMessage class]]) {
        SBDUserMessage *messageUser= (SBDUserMessage *)message;
        NSLog(@"message: %@",messageUser.message);
        double timeStamp=(double)messageUser.createdAt/1000;
        NSTimeInterval seconds = timeStamp;
        NSDate *epochNSDate = [[NSDate alloc] initWithTimeIntervalSince1970:seconds];
        JSQMessage *messageBuble = [[JSQMessage alloc] initWithSenderId:messageUser.sender.userId
                                                      senderDisplayName:messageUser.sender.nickname
                                                                   date:epochNSDate
                                                                   text:messageUser.message];
        
        [self->messageBublees addObject:messageBuble];
    }
    else if ([message isKindOfClass:[SBDFileMessage class]]) {
        // Do something when the received message is a FileMessage.
        SBDFileMessage *messageFile= (SBDFileMessage *)message;
        NSLog(@"message: %@",messageFile.type);
        NSString *messageType= [[messageFile.type componentsSeparatedByString:@"/"] firstObject];
        if ([messageType isEqualToString:@"image"]) {
            NSString *identifier= [NSString stringWithFormat:@"%lld",messageFile.messageId];
            NSString *extension= [[messageFile.type componentsSeparatedByString:@"/"] lastObject];
            NSString *nameFile = [NSString stringWithFormat:@"%@.%@",identifier,extension];
            if ([self verifyFileExistWithIdentifier:nameFile]) {
                NSString *pathFile=[self getFilesDirectory];
                pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,nameFile];
                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithContentsOfFile:pathFile]];
                JSQMessage *photoMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:photoItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    photoItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                
                [messageBublees addObject:photoMessage];
            }
            else{
                JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                JSQMessage *photoMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:photoItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    photoItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                [messageBublees addObject:photoMessage];
                int indexMessage=(int)[messageBublees indexOfObject:photoMessage];
                
                NSMutableDictionary *dictMessageFile=[[NSMutableDictionary alloc] init];
                [dictMessageFile setObject:message forKey:@"messageSendbird"];
                [dictMessageFile setObject:[NSNumber numberWithInt:indexMessage] forKey:@"indexOfFile"];
                [messagesDataFilesToDownload addObject:dictMessageFile];
                
            }
        }
        else if ([messageType isEqualToString:@"video"]) {
            NSString *identifier= [NSString stringWithFormat:@"%lld",messageFile.messageId];
            //NSString *extension= [[messageFile.type componentsSeparatedByString:@"/"] lastObject];
            NSString *extension=@"mp4";
            NSString *nameFile = [NSString stringWithFormat:@"%@.%@",identifier,extension];
            if ([self verifyFileExistWithIdentifier:nameFile]) {
                NSString *pathFile=[self getFilesDirectory];
                pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,identifier];
                JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:pathFile] isReadyToPlay:YES];
                JSQMessage *videoMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:videoItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    videoItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                [messageBublees addObject:videoMessage];
            }
            else{
                JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:@""] isReadyToPlay:NO];
                JSQMessage *videoMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:videoItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    videoItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                [messageBublees addObject:videoMessage];
                int indexMessage=(int)[messageBublees indexOfObject:videoMessage];
                
                NSMutableDictionary *dictMessageFile=[[NSMutableDictionary alloc] init];
                [dictMessageFile setObject:message forKey:@"messageSendbird"];
                [dictMessageFile setObject:[NSNumber numberWithInt:indexMessage] forKey:@"indexOfFile"];
                [messagesDataFilesToDownload addObject:dictMessageFile];
                
            }
        }
        else if ([messageType isEqualToString:@"audio"]) {
            NSString *identifier= [NSString stringWithFormat:@"%lld",messageFile.messageId];
            //NSString *extension= [[messageFile.type componentsSeparatedByString:@"/"] lastObject];
            NSString *extension=@"mpeg";
            NSString *nameFile = [NSString stringWithFormat:@"%@.%@",identifier,extension];
            if ([self verifyFileExistWithIdentifier:nameFile]) {
                NSString *pathFile=[self getFilesDirectory];
                pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,nameFile];
                NSData *audio=[NSData dataWithContentsOfFile:pathFile];
                JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audio];
                JSQMessage *audioMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:audioItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    audioItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                [messageBublees addObject:audioMessage];
            }
            else{
                NSData *audio=nil;
                JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audio];
                JSQMessage *audioMessage = [JSQMessage
                                            messageWithSenderId:messageFile.sender.userId
                                            displayName:messageFile.sender.nickname
                                            media:audioItem];
                if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                    audioItem.appliesMediaViewMaskAsOutgoing=NO;
                }
                [messageBublees addObject:audioMessage];
                
                int indexMessage=(int)[messageBublees indexOfObject:audioMessage];
                
                NSMutableDictionary *dictMessageFile=[[NSMutableDictionary alloc] init];
                [dictMessageFile setObject:message forKey:@"messageSendbird"];
                [dictMessageFile setObject:[NSNumber numberWithInt:indexMessage] forKey:@"indexOfFile"];
                [messagesDataFilesToDownload addObject:dictMessageFile];
                
            }
        }
        else if ([messageType containsString:@"application"]) {
            if ([messageFile.type containsString:@"pdf"]) {
                NSString *identifier= [NSString stringWithFormat:@"%lld",messageFile.messageId];
                NSString *extension= @"pdf";
                NSString *nameFile = [NSString stringWithFormat:@"%@.%@",identifier,extension];
                if ([self verifyFileExistWithIdentifier:nameFile]) {
                    NSString *pathFile=[self getFilesDirectory];
                    pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,nameFile];
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"pdfImage"]];
                    JSQMessage *photoMessage = [JSQMessage
                                                messageWithSenderId:messageFile.sender.userId
                                                displayName:messageFile.sender.nickname
                                                media:photoItem];
                    if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                        photoItem.appliesMediaViewMaskAsOutgoing=NO;
                    }
                    [messageBublees addObject:photoMessage];
                }
                else{
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                    JSQMessage *photoMessage = [JSQMessage
                                                messageWithSenderId:messageFile.sender.userId
                                                displayName:messageFile.sender.nickname
                                                media:photoItem];
                    if ([messageFile.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                        photoItem.appliesMediaViewMaskAsOutgoing=NO;
                    }
                    [messageBublees addObject:photoMessage];
                    int indexMessage=(int)[messageBublees indexOfObject:photoMessage];
                    
                    NSMutableDictionary *dictMessageFile=[[NSMutableDictionary alloc] init];
                    [dictMessageFile setObject:message forKey:@"messageSendbird"];
                    [dictMessageFile setObject:[NSNumber numberWithInt:indexMessage] forKey:@"indexOfFile"];
                    [messagesDataFilesToDownload addObject:dictMessageFile];
                    
                }
            }
            
        }
    }
    else if ([message isKindOfClass:[SBDAdminMessage class]]) {
        // Do something when the received message is an AdminMessage.
    }
    
}
-(void)sendPdfReceived{
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
    JSQMessage *photoMessage = [JSQMessage
                                messageWithSenderId:self.senderId
                                displayName:self.senderDisplayName
                                media:photoItem];
    [messageBublees addObject:photoMessage];
    int indexMessage=(int)[messageBublees indexOfObject:photoMessage];
    [self finishSendingMessage];
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:pathPdfFile]];
    
    
    [self.channel sendFileMessageWithBinaryData:data filename:@"document.pdf" type:@"application/pdf" size:[data length] data:nil completionHandler:^(SBDFileMessage * _Nonnull fileMessage, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        NSString *identifier=[NSString stringWithFormat:@"%lld.pdf",fileMessage.messageId];
        [self saveFileIntoFolderWithIdentifier:[NSString stringWithFormat:@"%lld",fileMessage.messageId] AndExtension:@"pdf" AndData:data];
        NSString *pathFile=[self getFilesDirectory];
        pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,identifier];
        
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"pdfImage"]];
        JSQMessage *photoMessage2 = [JSQMessage
                                     messageWithSenderId:fileMessage.sender.userId
                                     displayName:fileMessage.sender.nickname
                                     media:photoItem];
        
        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:photoMessage2];
        [self->_messages addObject:fileMessage];
        [self finishSendingMessage];
        
        // ...
    }];
    
}
-(void)downloadMissingFiles{
    if ([messagesDataFilesToDownload count]>0) {
        NSDictionary *dictMessage=[messagesDataFilesToDownload objectAtIndex:0];
        SBDFileMessage *message=[dictMessage objectForKey:@"messageSendbird"];
        int indexMessage=[[dictMessage objectForKey:@"indexOfFile"] intValue];
        NSString *content=message.type;
        if ([content containsString:@"image"]) {
            NSString *url= message.url;
            NSURLSession *session = [NSURLSession sharedSession];
            __block NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error != nil) {
                    //[self hideImageViewerLoading];
                    
                    return;
                }
                NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
                if ([resp statusCode] >= 200 && [resp statusCode] < 300) {
                    NSString *identifier= [NSString stringWithFormat:@"%lld",message.messageId];
                    NSString *extension= [[message.type componentsSeparatedByString:@"/"] lastObject];
                    [self saveFileIntoFolderWithIdentifier:identifier AndExtension:extension AndData:data];
                    NSString *filePath=[NSString stringWithFormat:@"%@/%@.%@",[self getFilesDirectory],identifier,extension];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        
                        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithContentsOfFile:filePath]];
                        JSQMessage *photoMessage2 =[JSQMessage
                                                    messageWithSenderId:message.sender.userId
                                                    displayName:message.sender.nickname
                                                    media:photoItem];
                        if ([message.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                            photoItem.appliesMediaViewMaskAsOutgoing=NO;
                        }
                        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:photoMessage2];
                        [self finishReceivingMessageWithoutScroll];
                        if ([self->messagesDataFilesToDownload count]>0) {
                            [self->messagesDataFilesToDownload removeObjectAtIndex:0];
                        }
                        [self downloadMissingFiles];
                    });
                    
                }
            }] resume];
        }
        else if ([content containsString:@"video"]) {
            NSString *url= message.url;
            NSURLSession *session = [NSURLSession sharedSession];
            __block NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error != nil) {
                    //[self hideImageViewerLoading];
                    
                    return;
                }
                NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
                if ([resp statusCode] >= 200 && [resp statusCode] < 300) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *identifier= [NSString stringWithFormat:@"%lld",message.messageId];
                        //NSString *extension= [[message.type componentsSeparatedByString:@"/"] lastObject];
                        NSString *extension= @"mp4";
                        [self saveFileIntoFolderWithIdentifier:identifier AndExtension:extension AndData:data];
                        NSString *filePath=[NSString stringWithFormat:@"%@/%@.%@",[self getFilesDirectory],identifier,extension];
                        
                        JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:filePath] isReadyToPlay:YES];
                        JSQMessage *videoMessage2 =[JSQMessage
                                                    messageWithSenderId:message.sender.userId
                                                    displayName:message.sender.nickname
                                                    media:videoItem];
                        if ([message.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                            videoItem.appliesMediaViewMaskAsOutgoing=NO;
                        }
                        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:videoMessage2];
                        [self finishReceivingMessageWithoutScroll];
                        if ([self->messagesDataFilesToDownload count]>0) {
                            [self->messagesDataFilesToDownload removeObjectAtIndex:0];
                        }
                        [self downloadMissingFiles];
                    });
                    
                }
            }] resume];
            
        }
        if ([content containsString:@"audio"]) {
            
            NSString *url= message.url;
            NSURLSession *session = [NSURLSession sharedSession];
            __block NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error != nil) {
                    //[self hideImageViewerLoading];
                    
                    return;
                }
                NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
                if ([resp statusCode] >= 200 && [resp statusCode] < 300) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *identifier= [NSString stringWithFormat:@"%lld",message.messageId];
                        //NSString *extension= [[message.type componentsSeparatedByString:@"/"] lastObject];
                        NSString *extension= @"mpeg";
                        [self saveFileIntoFolderWithIdentifier:identifier AndExtension:extension AndData:data];
                        NSString *filePath=[NSString stringWithFormat:@"%@/%@.%@",[self getFilesDirectory],identifier,extension];
                        
                        NSData *audio=[NSData dataWithContentsOfFile:filePath];
                        JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audio];
                        JSQMessage *audioMessage2 = [JSQMessage
                                                     messageWithSenderId:message.sender.userId
                                                     displayName:message.sender.nickname
                                                     media:audioItem];
                        if ([message.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                            audioItem.appliesMediaViewMaskAsOutgoing=NO;
                        }
                        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:audioMessage2];
                        [self finishReceivingMessageWithoutScroll];
                        if ([self->messagesDataFilesToDownload count]>0) {
                            [self->messagesDataFilesToDownload removeObjectAtIndex:0];
                        }
                        [self downloadMissingFiles];
                    });
                    
                }
            }] resume];
            
        }
        else if ([content containsString:@"pdf"]) {
            NSString *url= message.url;
            NSURLSession *session = [NSURLSession sharedSession];
            __block NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (error != nil) {
                    //[self hideImageViewerLoading];
                    
                    return;
                }
                NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
                if ([resp statusCode] >= 200 && [resp statusCode] < 300) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSString *identifier= [NSString stringWithFormat:@"%lld",message.messageId];
                        NSString *extension= @"pdf";
                        [self saveFileIntoFolderWithIdentifier:identifier AndExtension:extension AndData:data];
                        
                        
                        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"pdfImage"]];
                        JSQMessage *photoMessage2 =[JSQMessage
                                                    messageWithSenderId:message.sender.userId
                                                    displayName:message.sender.nickname
                                                    media:photoItem];
                        if ([message.sender.userId intValue]!=[[doctorInfo objectForKey:@"id"] intValue]) {
                            photoItem.appliesMediaViewMaskAsOutgoing=NO;
                        }
                        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:photoMessage2];
                        [self finishReceivingMessageWithoutScroll];
                        if ([self->messagesDataFilesToDownload count]>0) {
                            [self->messagesDataFilesToDownload removeObjectAtIndex:0];
                        }
                        [self downloadMissingFiles];
                    });
                    
                }
            }] resume];
        }
    }
}
- (void)performAndWait:(void (^)(dispatch_semaphore_t semaphore))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    perform(semaphore);
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}
- (void)performAndWait2:(void (^)(dispatch_semaphore_t semaphore2))perform;
{
    NSParameterAssert(perform);
    dispatch_semaphore_t semaphore2 = dispatch_semaphore_create(0);
    perform(semaphore2);
    dispatch_semaphore_wait(semaphore2, DISPATCH_TIME_FOREVER);
}
-(NSDate*)castDateOfMessage:(NSString *)dateString{
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter2 setLocale:enUSPOSIXLocale];
    [dateFormatter2 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *dateCast=[dateFormatter2 dateFromString:dateString];
    return dateCast;
}

- (void)showActivityIndicator
{
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicatorView.frame = CGRectMake(0, 0, 22, 22);
    activityIndicatorView.color = [UIColor grayColor];
    [activityIndicatorView startAnimating];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"Conectando...";
    titleLabel.font = [UIFont fontWithName:@"MyriadPro-Light" size:20];
    titleLabel.textColor=[UIColor grayColor];
    
    CGSize fittingSize = [titleLabel sizeThatFits:CGSizeMake(200.0f, activityIndicatorView.frame.size.height)];
    titleLabel.frame = CGRectMake(activityIndicatorView.frame.origin.x + activityIndicatorView.frame.size.width + 8,
                                  activityIndicatorView.frame.origin.y,
                                  fittingSize.width,
                                  fittingSize.height);
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(-(activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width)/2,
                                                                 -(activityIndicatorView.frame.size.height)/2-3,
                                                                 activityIndicatorView.frame.size.width + 8 + titleLabel.frame.size.width,
                                                                 activityIndicatorView.frame.size.height)];
    [titleView addSubview:activityIndicatorView];
    [titleView addSubview:titleLabel];
    
    self.navigationItem.titleView = titleView;
}

- (void)hideActivityIndicator
{
    self.navigationItem.titleView = nil;
}
- (UIImage *)imageFromLayer:(CALayer *)layer
{
    UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

-(void)sendMessage:(NSString *)message{
    if (message > 0) {
        [self.channel endTyping];
        NSError *error = nil;
        NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
        if (error == nil) {
            NSArray *matches = [detector matchesInString:message options:0 range:NSMakeRange(0, message.length)];
            NSURL *url = nil;
            for (NSTextCheckingResult *match in matches) {
                url = [match URL];
                break;
            }
            
            //            if (url != nil) {
            //                OutgoingGeneralUrlPreviewTempModel *tempModel = [[OutgoingGeneralUrlPreviewTempModel alloc] init];
            //                tempModel.createdAt = (long long)([[NSDate date] timeIntervalSince1970] * 1000);
            //                tempModel.message = message;
            //
            //                [self.chattingView.messages addObject:tempModel];
            //                dispatch_async(dispatch_get_main_queue(), ^{
            //                    [self.chattingView.chattingTableView reloadData];
            //                    [self.chattingView scrollToBottomWithForce:YES];
            //
            //                    // Send preview;
            //                    [self sendUrlPreview:url message:message tempModel:tempModel];
            //                });
            //
            //                return;
            //            }
        }
        
        // self.chattingView.sendButton.enabled = NO;
        SBDUserMessage *preSendMessage = [self.channel sendUserMessage:message data:@"" customType:@"" targetLanguages:@[@"ar", @"de", @"fr", @"nl", @"ja", @"ko", @"pt", @"es", @"zh-CHS"] completionHandler:^(SBDUserMessage * _Nullable userMessage, SBDError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                SBDUserMessage *preSendMessage = (SBDUserMessage *)self.preSendMessages[userMessage.requestId];
                [self.preSendMessages removeObjectForKey:userMessage.requestId];
                
                if (error != nil) {
                    self.resendableMessages[userMessage.requestId] = userMessage;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self processMessage:(SBDBaseMessage *)userMessage];
                        [self finishReceivingMessage];
                    });
                    
                    return;
                }
                
                if (preSendMessage != nil) {
                    [self.messages replaceObjectAtIndex:[self.messages indexOfObject:preSendMessage] withObject:userMessage];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    //  [self.chattingView scrollToBottomWithForce:YES];
                });
            });
        }];
        
        self.preSendMessages[preSendMessage.requestId] = preSendMessage;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.preSendMessages[preSendMessage.requestId] == nil) {
                return;
            }
            [self.messages addObject:preSendMessage];
            
            //[self.chattingView.chattingTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[self.messages indexOfObject:preSendMessage] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //  [self.chattingView scrollToBottomWithForce:YES];
                //  self.chattingView.sendButton.enabled = YES;
            });
        });
    }
}
#pragma mark - Connection Manager Delegate
- (void)didConnect:(BOOL)isReconnection {
    [self loadPreviousMessage:NO];
    
    [self.channel refreshWithCompletionHandler:^(SBDError * _Nullable error) {
        if (error == nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setNavigationBarTitleViewWithName:self->nameOfChatMember];
            });
            
        }
    }];
}

- (void)didDisconnect {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNavigationBarTitleViewWithName:@"Conectando"];
        [self showActivityIndicator];
    });
    
    
}

#pragma mark - SBDChannelDelegate

- (void)channel:(SBDBaseChannel * _Nonnull)sender didReceiveMessage:(SBDBaseMessage * _Nonnull)message {
    if (sender == self.channel) {
        [self.channel markAsRead];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.messages addObject:message];
            [self processMessage:message];
            [self finishReceivingMessage];
            if ([message isKindOfClass:[SBDFileMessage class]]) {
                [self downloadMissingFiles];
            }
            
        });
    }
}

- (void)channelDidUpdateReadReceipt:(SBDGroupChannel * _Nonnull)sender {
    if (sender == self.channel) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }
}

//- (void)channelDidUpdateTypingStatus:(SBDGroupChannel * _Nonnull)sender {
//    if (sender == self.channel) {
//        if (sender.getTypingMembers.count == 0) {
//            [self endTypingIndicator];
//        }
//        else {
//            [self startTypingIndicator:[NSString stringWithFormat:@"%@ está escribiendo", sender.getTypingMembers[0].nickname]];
//
//        }
//    }
//}


//- (void)startTypingIndicator:(NSString *)text {
//    // Typing indicator
//    self.typingIndicatorContainerView.hidden = NO;
//    self.typingIndicatorLabel.text = text;
//
//    self.typingIndicatorContainerViewHeight.constant = 26;
//    self.typingIndicatorImageHeight.constant = 26;
//    [self.typingIndicatorContainerView layoutIfNeeded];
//
//    if (self.typingIndicatorImageView.animating == NO) {
//        NSMutableArray<UIImage *> *typingImages = [[NSMutableArray alloc] init];
//        for (int i = 1; i <= 50; i++) {
//            NSString *typingImageFrameName = [NSString stringWithFormat:@"%02d", i];
//            [typingImages addObject:[UIImage imageNamed:typingImageFrameName]];
//        }
//        self.typingIndicatorImageView.animationImages = typingImages;
//        self.typingIndicatorImageView.animationDuration = 1.5;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.typingIndicatorImageView startAnimating];
//        });
//    }
//}
//
//- (void)endTypingIndicator {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.typingIndicatorImageView stopAnimating];
//    });
//
//    self.typingIndicatorContainerView.hidden = YES;
//    self.typingIndicatorContainerViewHeight.constant = 0;
//    self.typingIndicatorImageHeight.constant = 0;
//
//    [self.typingIndicatorContainerView layoutIfNeeded];
//}

#pragma mark - FileManager
- (BOOL)verifyFileExistWithIdentifier:(NSString *)identifier {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:identifier];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([fileManager fileExistsAtPath: path])
    {
        return YES;
    }
    return NO;
}
-(NSString *)getFilesDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}
-(void)moveTemptoDocumentsWithIdentifier:(NSString *)identifier withExtension:(NSString *)extension{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *nameFile=[NSString stringWithFormat:@"%@.%@",identifier,extension];
    NSString *newPath = [documentsDirectory stringByAppendingPathComponent:nameFile];
    
    NSString *nameTemp=[NSString stringWithFormat:@"%@.temp",identifier];
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:nameTemp];
    
    
    NSError* error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if([fileManager moveItemAtPath:filePath toPath:newPath error:&error])
    {
        NSLog(@"sucess");
    }
    else
    {
        NSLog(@"%@", error);
    }
}
-(void)saveFileIntoFolderWithIdentifier:(NSString *)identifier AndExtension:(NSString *)extension AndData:(NSData *)data{
    NSString *documentsPath = [self getFilesDirectory];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@.%@",documentsPath,identifier,extension];
    [data writeToFile:filePath atomically:YES];
}
#pragma mark - Picking Media
- (void)didPressAccessoryButton:(UIButton *)sender
{
    RMActionControllerStyle style = RMActionControllerStyleWhite;
    RMCustomViewActionController *actionController = [RMCustomViewActionController actionControllerWithStyle:style];
    
    
    RMAction *takePhotoAction = [RMAction<UIView *> actionWithTitle:@"Tomar Foto" image:[UIImage imageNamed:@"iconCamera"] style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        
        UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            return;
        }
        [cameraController setSourceType:UIImagePickerControllerSourceTypeCamera];
        [cameraController setDelegate:self];
        self->isCameraMedia = YES;
        
        [actionController dismissViewControllerAnimated:NO completion:^{
            [self presentViewController:cameraController animated:YES completion:nil];
        }];
        
    }];
    
    RMAction *selectPhotoAction = [RMAction<UIView *> actionWithTitle:@"Seleccionar Foto" image:[UIImage imageNamed:@"iconFile"] style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            return;
        }
        [cameraController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [cameraController setDelegate:self];
        self->isCameraMedia = NO;
        [actionController dismissViewControllerAnimated:NO completion:^{
            cameraController.modalPresentationStyle= UIModalPresentationOverFullScreen;
            [self presentViewController:cameraController animated:YES completion:nil];
        }];
        
        
        
    }];
    
    RMAction *takeVideoAction = [RMAction<UIView *> actionWithTitle:@"Grabar Video" image:[UIImage imageNamed:@"videoRecorderIcon"] style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
            picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
            picker.videoMaximumDuration = 120.0f;
            self->isVideo = true;
            [actionController dismissViewControllerAnimated:NO completion:^{
                [self presentViewController:picker animated:YES completion:NULL];
            }];
        }
        
    }];
    
    RMAction *takeAudioAction = [RMAction<UIView *> actionWithTitle:@"Grabar Audio" image:[UIImage imageNamed:@"iconMic"] style:RMActionStyleDefault andHandler:^(RMActionController<UIView *> *controller) {
        
        IQAudioRecorderViewController *audiController = [[IQAudioRecorderViewController alloc] init];
        audiController.delegate = self;
        audiController.title = @"Grabar audio";
        //        controller.maximumRecordDuration = 10;
        //        controller.allowCropping = YES;
        audiController.barStyle = UIBarStyleDefault;
        audiController.navigationController.navigationBarHidden = false;
        [actionController dismissViewControllerAnimated:NO completion:^{
            [self presentBlurredAudioRecorderViewControllerAnimated:audiController];
        }];
    }];
    
    
    
    
    RMAction *cancelAction = [RMAction<UIView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIView *> *controller) {
        NSLog(@"Action controller was canceled");
    }];
    
    
    actionController.title = @"";
    actionController.message = @"";
    
    [actionController addAction:cancelAction];
    [actionController addAction:takeAudioAction];
    [actionController addAction:takeVideoAction];
    [actionController addAction:selectPhotoAction];
    [actionController addAction:takePhotoAction];
    
    
    
    
    
    [self presentViewController:actionController animated:YES completion:nil];
    
}
- (UIImage *)fixImageOrientation:(UIImage *)imagenAarreglar {
    
    // No-op if the orientation is already correct
    if (imagenAarreglar.imageOrientation == UIImageOrientationUp) return imagenAarreglar;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (imagenAarreglar.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, imagenAarreglar.size.width, imagenAarreglar.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, imagenAarreglar.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, imagenAarreglar.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (imagenAarreglar.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, imagenAarreglar.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, imagenAarreglar.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, imagenAarreglar.size.width, imagenAarreglar.size.height,
                                             CGImageGetBitsPerComponent(imagenAarreglar.CGImage), 0,
                                             CGImageGetColorSpace(imagenAarreglar.CGImage),
                                             CGImageGetBitmapInfo(imagenAarreglar.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (imagenAarreglar.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,imagenAarreglar.size.height,imagenAarreglar.size.width), imagenAarreglar.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,imagenAarreglar.size.width,imagenAarreglar.size.height), imagenAarreglar.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}
-(void)audioRecorderControllerDidCancel:(IQAudioRecorderViewController *)controller {
    //Notifying that user has clicked cancel.
    [controller dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    if (isVideo) {
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:@""] isReadyToPlay:NO];
        JSQMessage *videoMessage = [JSQMessage
                                    messageWithSenderId:self.senderId
                                    displayName:self.senderDisplayName
                                    media:videoItem];
        [messageBublees addObject:videoMessage];
        int indexMessage=(int)[messageBublees indexOfObject:videoMessage];
        [self finishSendingMessage];
        
        NSData *videoData = [NSData dataWithContentsOfFile:info[UIImagePickerControllerMediaURL]];
        dataImg = [Base64 encode:videoData];
        NSData *data = videoData;
        
        [self.channel sendFileMessageWithBinaryData:data filename:@"video.mp4" type:@"video/mp4" size:[data length] data:nil completionHandler:^(SBDFileMessage * _Nonnull fileMessage, SBDError * _Nullable error) {
            if (error != nil) { // Error.
                return;
            }
            NSString *identifier=[NSString stringWithFormat:@"%lld.mp4",fileMessage.messageId];
            [self saveFileIntoFolderWithIdentifier:[NSString stringWithFormat:@"%lld",fileMessage.messageId] AndExtension:@"mp4" AndData:data];
            NSString *pathFile=[self getFilesDirectory];
            pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,identifier];
            JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:pathFile] isReadyToPlay:YES];
            JSQMessage *videoMessage2 =[JSQMessage
                                        messageWithSenderId:fileMessage.sender.userId
                                        displayName:fileMessage.sender.nickname
                                        media:videoItem];
            
            [self->messageBublees replaceObjectAtIndex:indexMessage withObject:videoMessage2];
            [self->_messages addObject:fileMessage];
            [self finishSendingMessage];
            
            // ...
        }];
        
        
        
        
    }else{
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:nil];
        JSQMessage *photoMessage = [JSQMessage
                                    messageWithSenderId:self.senderId
                                    displayName:self.senderDisplayName
                                    media:photoItem];
        [messageBublees addObject:photoMessage];
        int indexMessage=(int)[messageBublees indexOfObject:photoMessage];
        [self finishSendingMessage];
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        image = [self fixImageOrientation:image];
        
        NSData *dataImage = UIImageJPEGRepresentation(image,0.10);
        
        dataImg = [Base64 encode:dataImage];
        
        if (isCameraMedia == YES)
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        
        // NSString *imagePath = [NSString stringWithFormat:@"%@/%lld.jpg", [FIRAuth auth].currentUser.uid, (long long)([[NSDate date] timeIntervalSince1970] * 1000.0)];
        
        //send image
        NSData *data = dataImage;
        
        // Prepare the upload stream and parameters
        [self.channel sendFileMessageWithBinaryData:data filename:@"image.jpeg" type:@"image/jpeg" size:[data length] data:nil completionHandler:^(SBDFileMessage * _Nonnull fileMessage, SBDError * _Nullable error) {
            if (error != nil) { // Error.
                return;
            }
            NSString *identifier=[NSString stringWithFormat:@"%lld.jpeg",fileMessage.messageId];
            [self saveFileIntoFolderWithIdentifier:[NSString stringWithFormat:@"%lld",fileMessage.messageId] AndExtension:@"jpeg" AndData:data];
            NSString *pathFile=[self getFilesDirectory];
            pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,identifier];
            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageWithContentsOfFile:pathFile]];
            JSQMessage *photoMessage2 = [JSQMessage
                                         messageWithSenderId:fileMessage.sender.userId
                                         displayName:fileMessage.sender.nickname
                                         media:photoItem];
            [self->messageBublees replaceObjectAtIndex:indexMessage withObject:photoMessage2];
            [self->_messages addObject:fileMessage];
            [self finishSendingMessage];
            
            // ...
        }];
        
        
        
        
    }
    
}

-(void)audioRecorderController:(IQAudioRecorderViewController *)controller didFinishWithAudioAtPath:(NSString *)filePath {
    //Do your custom work with file at filePath.
    //loading
    
    
    NSData *audioData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:filePath]];
    dataImg = [Base64 encode:audioData];
    
    NSData *data = audioData;
    
    JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:nil];
    JSQMessage *audioMessage = [JSQMessage
                                messageWithSenderId:self.senderId
                                displayName:self.senderDisplayName
                                media:audioItem];
    [messageBublees addObject:audioMessage];
    
    int indexMessage=(int)[messageBublees indexOfObject:audioMessage];
    [self finishSendingMessage];
    
    // Prepare the upload stream and parameters
    [self.channel sendFileMessageWithBinaryData:data filename:@"audio.mpeg" type:@"audio/mpeg" size:[data length] data:nil completionHandler:^(SBDFileMessage * _Nonnull fileMessage, SBDError * _Nullable error) {
        if (error != nil) { // Error.
            return;
        }
        NSString *identifier=[NSString stringWithFormat:@"%lld.jpeg",fileMessage.messageId];
        [self saveFileIntoFolderWithIdentifier:[NSString stringWithFormat:@"%lld",fileMessage.messageId] AndExtension:@"jpeg" AndData:data];
        NSString *pathFile=[self getFilesDirectory];
        pathFile=[NSString stringWithFormat:@"%@/%@",pathFile,identifier];
        JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
        JSQMessage *audioMessage2 = [JSQMessage
                                     messageWithSenderId:fileMessage.sender.userId
                                     displayName:fileMessage.sender.nickname
                                     media:audioItem];
        [self->messageBublees replaceObjectAtIndex:indexMessage withObject:audioMessage2];
        [self->_messages addObject:fileMessage];
        [self finishSendingMessage];
        
        // ...
    }];
    
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Actions
- (void)closePdf:(UIBarButtonItem *)sender
{
    [self.pdfVC.navigationController popViewControllerAnimated:YES];
}
- (void)closePressed:(UIBarButtonItem *)sender
{
    [self closeChat];
    
}
-(void)closeChat{
    [SBDMain removeChannelDelegateForIdentifier:self.description];
    [SBDMain removeConnectionDelegateForIdentifier:self.description];
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}
-(void)openConsultation{
//    if ([[SharedFunctions sharedInstance] verifyIfNeedShowSuscription]) {
//        [[SharedFunctions sharedInstance] showSuscriptionVCFromScanner:NO];
//        return;
//    }

    [[SharedFunctions sharedInstance] flowRemoteConsultationOpen];
    

   
}



#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                             senderDisplayName:senderDisplayName
                                                          date:date
                                                          text:text];
    
    [messageBublees addObject:message];
    [self sendMessage:text];
    [self finishSendingMessage];
}






#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [messageBublees objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [messageBublees objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return _outgoingBubbleImageData;
    }
    
    return _incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    // JSQMessage *message = [messages objectAtIndex:indexPath.item];
    
    //   if ([message.senderId isEqualToString:self.senderId]) {
    //        if (![NSUserDefaults outgoingAvatarSetting]) {
    //            return nil;
    //        }
    //   }
    // else {
    //        if (![NSUserDefaults incomingAvatarSetting]) {
    //            return nil;
    //        }
    //}
    
    
    //  return [self.demoData.avatars objectForKey:message.senderId];
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [messageBublees objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [messageBublees objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messageBublees objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:nameOfChatMember];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}


#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [messageBublees count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [messageBublees objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor colorWithRed:0.290 green:0.290 blue:0.290 alpha:1.00];
        }
        else {
            cell.textView.textColor = [UIColor colorWithRed:0.290 green:0.290 blue:0.290 alpha:1.00];
        }
        
        cell.textView.linkTextAttributes = @{ NSFontAttributeName : [UIFont fontWithName:@"MyriadPro-Light" size:10] ,
                                              NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

-(NSString *) urlFile:(NSString *)nameMessage {
    return [documentsDirectoryURL stringByAppendingPathComponent:nameMessage];
}

#pragma mark - JSQMessages collection view flow layout delegate
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        SBDBaseMessage *message = [self.messages objectAtIndex:indexPath.item];
        if ([message isKindOfClass:[SBDFileMessage class]]) {
            SBDFileMessage *fileMessage=(SBDFileMessage  *)message;
            NSString *content=fileMessage.type;
            if ([content containsString:@"image"]) {
                NSString *identifier= [NSString stringWithFormat:@"%lld",fileMessage.messageId];
                NSString *extension= [[fileMessage.type componentsSeparatedByString:@"/"] lastObject];
                NSMutableArray *photos = [NSMutableArray array];
                NSString *namePhoto=[NSString stringWithFormat:@"%@.%@",identifier,extension];
                NSString *pathPhoto=[NSString stringWithFormat:@"%@/%@",[self getFilesDirectory],namePhoto];
                UIImage * imageFile = [UIImage imageWithContentsOfFile:pathPhoto];
                FamilyPhoto *photo = [[FamilyPhoto alloc] init];
                photo.image = imageFile;
                [photos addObject:photo];
                
                NYTPhotoViewerArrayDataSource *dataSource = [[NYTPhotoViewerArrayDataSource alloc] initWithPhotos:photos];
                NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithDataSource:dataSource];
                photosViewController.modalPresentationStyle= UIModalPresentationOverFullScreen;
                [self presentViewController:photosViewController animated:YES completion:nil];
            }else if ([content containsString:@"video"])  {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *identifier= [NSString stringWithFormat:@"%lld",fileMessage.messageId];
                //NSString *extension= [[fileMessage.type componentsSeparatedByString:@"/"] lastObject];
                NSString *extension=@"mp4";
                NSString *nameVideo=[NSString stringWithFormat:@"%@.%@",identifier,extension];
                
                NSString *pathVideo=[NSString stringWithFormat:@"%@/%@",[self getFilesDirectory],nameVideo];
                if ([fileManager fileExistsAtPath:pathVideo]){
                    self.videoURL = [NSURL fileURLWithPath:pathVideo];
                    
                    _playerViewController = [[AVPlayerViewController alloc] init];
                    _playerViewController.player = [AVPlayer playerWithURL:self.videoURL];
                    _playerViewController.view.frame = self.view.bounds;
                    _playerViewController.showsPlaybackControls = YES;
                    [_playerViewController.player play];
                    _playerViewController.modalPresentationStyle= UIModalPresentationOverFullScreen;
                    [self presentViewController:_playerViewController animated:true completion:nil];
                }else{
                    //[GlobalMethods showAlertCancelIn:self initWithTitle:@"Descargando" message:@"Espere a que se termine de descargar para poder ver el video." cancelButtonTitle:NSLocalizedString(@"Aceptar",nil)];
                }
                
            }else if ([content containsString:@"audio"]) {
                
            }
            else if ([content containsString:@"pdf"]) {
                NSString *namePdf=[NSString stringWithFormat:@"%lld.pdf",fileMessage.messageId];
                
                
                PDFView *view = [[PDFView alloc] initWithFrame: self.view.bounds];
                view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
                view.autoScales = NO ;
                view.displayDirection = kPDFDisplayDirectionHorizontal;
                view.displayMode = kPDFDisplaySinglePageContinuous;
                view.displaysRTL = YES ;
                [view setDisplaysPageBreaks:YES];
                [view setDisplayBox:kPDFDisplayBoxTrimBox];
                [view zoomIn:self];
                // [self.view addSubview:view];
                
                NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
                documentsURL = [documentsURL URLByAppendingPathComponent:namePdf];
                
                PDFDocument * document = [[PDFDocument alloc] initWithURL: documentsURL];
                view.document = document;
                
                
                CGRect rect = CGRectMake(10, 0,5,5);
                UIButton *backButton = [[UIButton alloc] init];
                backButton.frame=rect;
                [backButton setTitleColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00] forState:UIControlStateNormal];
                backButton.contentMode = UIViewContentModeScaleAspectFit;
                [backButton setImage:[UIImage imageNamed:@"backBlueArrow"] forState:UIControlStateNormal];
                [backButton addTarget:self
                               action:@selector(closePdf:)
                     forControlEvents:UIControlEventTouchUpInside];
                self.pdfVC=[[UIViewController alloc] init];
                [self.pdfVC.view addSubview:view];
                UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
                self.pdfVC.navigationItem.leftBarButtonItem = backButtonItem;
                self.pdfVC.navigationItem.hidesBackButton = YES;
                self.pdfVC.navigationItem.title=[NSString stringWithFormat:@"Visor Pdf"];
                
                [self.navigationController pushViewController:self.pdfVC animated:true];
                
                
                
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"error");
        self.messages=[[NSMutableArray alloc] init];
        [self loadPreviousMessage:YES];
    }
   
    
}

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [messageBublees objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messageBublees objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}
-(void)openHistoric:(id)sender{
        [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
            [self performAndWait:^(dispatch_semaphore_t semaphore) {
                InvokeService *invoke=[[InvokeService alloc] init];
                [invoke getConsultationsByPatientWithInfo:dictPatientPresential WithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
                    self->consultasArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
                    dispatch_semaphore_signal(semaphore);
                }];
            }];
            
            
            [self performAndWait:^(dispatch_semaphore_t semaphore) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedFunctions sharedInstance] removeLoadingView];
                    if ([self->consultasArray count]>0) {
                        UIStoryboard *storyboard= [UIStoryboard storyboardWithName:[[SharedFunctions sharedInstance] getCurrentStoryboard] bundle:nil];
                        self.patientHistoricController = [storyboard instantiateViewControllerWithIdentifier:@"PatientHistoricVC"];
                        self.patientHistoricController.consultasArray=self->consultasArray;
                        self.patientHistoricController.modalPresentationStyle= UIModalPresentationOverFullScreen;
                        [self presentViewController:self.patientHistoricController animated:YES completion:nil];
                    }
                    else{
                        [[SharedFunctions sharedInstance] showAlertWithMessage:@"No cuenta con consultas previas"];
                    }
                    
                    
                });
                dispatch_semaphore_signal(semaphore);
            }];
        }];
    
}
- (void)openvideo:(UIBarButtonItem *)sender
{
UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
UIViewController *home= [storyboard instantiateViewControllerWithIdentifier:@"VideoConferenceVC"];
home.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
home.modalPresentationStyle= UIModalPresentationOverFullScreen;
[self presentViewController:home animated:YES completion:nil];
    
}
@end

