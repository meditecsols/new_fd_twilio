//
//  HomeAjustesVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 24/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "HomeAjustesVC.h"
#import "ConsultasVC.h"
#import "SharedFunctions.h"
#import "VideoStartVC.h"
#import "GlobalMembers.h"
#import "Config.h"
#import "HomeViewsContainerVC.h"
#import "InvokeService.h"
#import "WebTermsVC.h"
#import <SendBirdSDK/SendBirdSDK.h>



@interface HomeAjustesVC ()

@end

@implementation HomeAjustesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UISwipeGestureRecognizer *swipeTop = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeTop.direction = UISwipeGestureRecognizerDirectionUp;
//    [self.view addGestureRecognizer:swipeTop];
//    
//    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
//    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
//    [self.view addGestureRecognizer:swipeDown];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    _lblVersion.text=[NSString stringWithFormat:@"Versión %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    if ([BASE_URL isEqualToString:URL_DEV]) {
        _lblDev.text=@"Apuntando a Dev";
    }
    else if ([BASE_URL isEqualToString:URL_SANDBOX]) {
        _lblDev.text=@"Apuntando a Sandbox";
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
}
- (void)swipe:(UISwipeGestureRecognizer *)swipeRecogniser
{
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionLeft)
//    {
//        
//        
//        UIStoryboard *storyboard = self.storyboard;
//        UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"HomePacientesVC"];
//        
//        
//        [self presentViewController:viewController animated:NO completion:^{
//        }];
//    }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionUp)
//    {
//        _layoutConstraintTopHeader.constant=-76;
//        [self.view layoutIfNeeded];
//    }
//    if ([swipeRecogniser direction] == UISwipeGestureRecognizerDirectionDown)
//    {
//        _layoutConstraintTopHeader.constant=0;
//        [self.view layoutIfNeeded];
//    }
}


- (IBAction)actionOpenAbout:(id)sender {
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke getUrlsTermsPrivacyWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            if ([responseData count]>0) {

                UIStoryboard *storyboard = self.storyboard;
                WebTermsVC *terms= [storyboard instantiateViewControllerWithIdentifier:@"WebTermsVC"];
                terms.arrayUrls=responseData;
                terms.modalPresentationStyle= UIModalPresentationOverFullScreen;
                [self presentViewController:terms animated:YES completion:^{
                    terms.lblTitle.text=@"Acerca de";
                }];


            }



        });
    }];
    
    
}

- (IBAction)actionCloseSession:(id)sender {
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"¿Desea cerrar su sesión?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* noAction = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Si"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [[SharedFunctions sharedInstance] updatePushToken:@"closed-session"];
                                   [[SharedFunctions sharedInstance] clearUsrAndPass];
                                   needRefreshConsultations=YES;
                                   needRefreshPatients=YES;
                                   
                                   [SBDMain unregisterPushToken:[SBDMain getPendingPushToken] completionHandler:^(NSDictionary * _Nullable response, SBDError * _Nullable error) {
                                       if (error != nil) { // Error.
                                           return;
                                       }
                                       [SBDMain disconnectWithCompletionHandler:^{
                                           if (error != nil) { // Error.
                                               return;
                                           }
                                       }];
                                   }];
                                   

                                   UIStoryboard *storyboard = self.storyboard;
                                   UIViewController *next= [storyboard instantiateViewControllerWithIdentifier:@"VideoStartVC"];
                                   next.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                                   next.modalPresentationStyle= UIModalPresentationOverFullScreen;
                                   [self presentViewController:next animated:YES completion:nil];
                                   
                                   
                                   
                                   
                                   
                               }];
    [alert addAction:noAction];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)actionOpenInfo:(id)sender{
    [[SharedFunctions sharedInstance] tappedOpenInfo];
}
- (IBAction)actionOpenMyProfile:(id)sender{
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke3=[[InvokeService alloc] init];
    [invoke3 getDoctorData:[doctorInfo objectForKey:@"id"] WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        
        if ([responseData count]>20) {
            doctorInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
        }
        
        [[SharedFunctions sharedInstance] saveUserData:doctorInfo];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            
            UIStoryboard *storyboard = self.storyboard;
            UIViewController *home= [storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
            home.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            home.modalPresentationStyle= UIModalPresentationOverFullScreen;
            [self presentViewController:home animated:YES completion:nil];
            
        });
        
    }];
}
- (IBAction)actionOpenQR:(id)sender {
    [[SharedFunctions sharedInstance] tappedQR];
}

- (IBAction)actionOpenPacientes:(id)sender {
    HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
    [containerParent moveToSecondView];
}

- (IBAction)actionOpenConsultas:(id)sender {
    HomeViewsContainerVC *containerParent=(HomeViewsContainerVC *)self.parentViewController;
    [containerParent moveToThirdView];
}



@end
