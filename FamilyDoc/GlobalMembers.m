//
//  GlobalMembers.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 15/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "GlobalMembers.h"

@implementation GlobalMembers
NSString *fullNameDoctor;
NSString *usr;
NSString *psw;
BOOL firstTimeEntry;
BOOL comeFromLogin;
BOOL comeFromScan;
BOOL comeFromChat;
BOOL statusReachability;
BOOL needRefreshConsultations;
BOOL needRefreshPatients;
BOOL isSignTaked;
BOOL isFDBusinessDoc;
BOOL isSuscriptionValid;
NSMutableDictionary *accountInfo;
NSMutableDictionary *dictPatientPresential;
NSMutableDictionary *doctorInfo;
NSMutableDictionary *presentialConsultation;
NSMutableDictionary *dictOfPrescription;
int pageHomeSelected;
int indexSChatSelected;
NSString *pathPdfFile;
NSString *fcmTokenRetrieved;
@end
