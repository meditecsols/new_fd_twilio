//
//  RegisterVC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 05/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "RegisterVC.h"
#import "Register2VC.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "SexyTooltip.h"
#import "WebTermsVC.h"

@interface RegisterVC (){
    int movedHeight;
    UITextField *textFieldEditable;
    NSDictionary *data;
    BOOL cedulaSearch;
    NSString *genderByProffesionalCardConsultation;
}

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    self.txtFieldCedula.delegate=self;
    self.txtFieldName.delegate=self;
    self.txtFieldEmail.delegate=self;
    self.txtFieldPass.delegate=self;
    self.txtFieldLastName.delegate=self;
    self.txtFieldSecondLastName.delegate=self;
    [_scrollContent setScrollEnabled:YES];
    if (self.view.frame.size.height>=668) {
        [_scrollContent setScrollEnabled:NO];
    }
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.tintColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Buscar" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    _txtFieldCedula.inputAccessoryView = numberToolbar;
    [numberToolbar sizeToFit];
    
    
    [_txtFieldCedula becomeFirstResponder];
    _scrollContent.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    [_txtFieldName setEnabled:YES];
    [_txtFieldLastName setEnabled:YES];
    [_txtFieldSecondLastName setEnabled:YES];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSAttributedString *text=[[NSAttributedString alloc] initWithString:@"Ingresa tu cédula y presiona Buscar" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 18.0f]}];
    
    SexyTooltip *greetingsTooltip = [[SexyTooltip alloc] initWithAttributedString:text sizedToView:_txtFieldCedula withPadding:UIEdgeInsetsMake(10, 10, 10, 10) andMargin:UIEdgeInsetsMake(0, 0, 0, 0)];
    greetingsTooltip.color=[UIColor whiteColor];
    [greetingsTooltip presentFromView:_txtFieldCedula
                               inView:_txtFieldCedula
                           withMargin:25
                             animated:YES];
    [greetingsTooltip dismissInTimeInterval:3];
}
-(void)cancelNumberPad{
    [_txtFieldCedula resignFirstResponder];
    _txtFieldName.text = @"";
    _txtFieldLastName.text=@"";
    _txtFieldSecondLastName.text=@"";
    
    [self verifyFields];
}
-(void)doneWithNumberPad{
    if (_txtFieldCedula.text.length>5&&_txtFieldCedula.text.length<10) {
        _txtFieldName.text=@"";
        _txtFieldLastName.text=@"";
        _txtFieldSecondLastName.text=@"";
        [_txtFieldCedula resignFirstResponder];
        [[SharedFunctions sharedInstance] showLoadingView];
        
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke retrieveCedulaInfo:_txtFieldCedula.text WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"response:%@",[responseData description]);
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([responseData isKindOfClass:[NSDictionary class]]) {
                    if ([[responseData objectForKey:@"detail"] isEqualToString:@"Not found."]||[[responseData objectForKey:@"detail"] isEqualToString:@"No encontrado."]||[[responseData allKeys] count]<=0) {
                        [[SharedFunctions sharedInstance] removeLoadingView];
                        [self showAlertWithMessage:@"No se encontró información"];
                        self->_txtFieldName.text=@"";
                        self->_txtFieldLastName.text=@"";
                        self->_txtFieldSecondLastName.text=@"";
                        self->_txtFieldCedula.text=@"";
                        [self->_txtFieldName setEnabled:NO];
                        [self->_txtFieldLastName setEnabled:NO];
                        [self->_txtFieldSecondLastName setEnabled:NO];
                        return;
                    }
                }
                else{
                    self->data=[[NSDictionary alloc] initWithDictionary:[(NSArray *)responseData objectAtIndex:0]];
                    NSLog(@"data:%@",[self->data description]);
                    self->cedulaSearch=YES;
                    self->_txtFieldName.text=[self->data objectForKey:@"name"];
                    self->_txtFieldLastName.text=[self->data objectForKey:@"last_name"];
                    self->_txtFieldSecondLastName.text=[self->data objectForKey:@"second_last_name"];
                    [self->_txtFieldName setEnabled:YES];
                    [self->_txtFieldLastName setEnabled:YES];
                    [self->_txtFieldSecondLastName setEnabled:YES];
                    if ([[self->data objectForKey:@"gender"] intValue]==1) {
                        self->genderByProffesionalCardConsultation=@"male";
                    }
                    else if ([[self->data objectForKey:@"gender"] intValue]==2) {
                        self->genderByProffesionalCardConsultation=@"female";
                    }
                    [[SharedFunctions sharedInstance] removeLoadingView];
                }
                
            });
            
        }];
        
    }
    else{
        [self showAlertWithMessage:@"Ingrese una cédula válida."];
    }
}
- (BOOL)validateNameWithString:(NSString*)name
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ "] invertedSet];

    if ([name rangeOfCharacterFromSet:set].location != NSNotFound) {
        return NO;
    }
    return YES;
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(BOOL)validatePassWithString:(NSString *)pass{
    if (pass.length>0) {
        return YES;
    }
    return NO;
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (_txtFieldCedula.text.length<=0) {
        return;
    }
    if (_txtFieldEmail.text.length<=0) {
        return;
    }
    if (_txtFieldPass.text.length<=0) {
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        return;
    }
    if (![_btnCheckBox isSelected]) {
        return;
    }
    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionShowPass:(id)sender {
    if (self.txtFieldPass.secureTextEntry) {
        self.txtFieldPass.enabled = NO;
        self.txtFieldPass.secureTextEntry=NO;
        self.txtFieldPass.enabled = YES;
        [self.txtFieldPass becomeFirstResponder];
        [self.btnShowPass setTitle:@"OCULTAR CONTRASEÑA" forState:UIControlStateNormal];
    }else{
        self.txtFieldPass.enabled = NO;
        self.txtFieldPass.secureTextEntry=YES;
        self.txtFieldPass.enabled = YES;
        [self.txtFieldPass becomeFirstResponder];
        [self.btnShowPass setTitle:@"MOSTRAR CONTRASEÑA" forState:UIControlStateNormal];
    }
}

- (IBAction)actionCheckBox:(id)sender {
    if ([self.btnCheckBox isSelected]) {
        self.btnCheckBox.selected = NO;
    }
    else{
        self.btnCheckBox.selected = YES;
        
    }
    [self verifyFields];
}

- (IBAction)actionShowTerms:(id)sender {
    InvokeService *invoke=[[InvokeService alloc] init];
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke getUrlsTermsPrivacyWithCompletion:^(NSMutableArray *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            if ([responseData count]>0) {
                
                UIStoryboard *storyboard = self.storyboard;
                WebTermsVC *terms= [storyboard instantiateViewControllerWithIdentifier:@"WebTermsVC"];
                terms.arrayUrls=responseData;
                terms.modalPresentationStyle= UIModalPresentationOverFullScreen;
                [self presentViewController:terms animated:YES completion:^{
                    terms.lblTitle.text=@"Acerca de";
                }];
                
                
            }
            
            
            
        });
    }];
}

- (IBAction)actionNext:(id)sender {
    if (_txtFieldCedula.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar tu Cédula"];
        return;
    }
    if (_txtFieldEmail.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar un correo electrónico"];
        return;
    }
    if (![self validateEmailWithString:_txtFieldEmail.text]) {
        [self showAlertWithMessage:@"Debes ingresar un correo electrónico válido"];
        return;
    }
    if (_txtFieldPass.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar una contraseña"];
        return;
    }
    if (_txtFieldName.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Nombre"];
        return;
    }
    if (![self validateNameWithString:_txtFieldSecondLastName.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Tu Nombre no debe contener caracteres especiales o números"];
        return;
    }
    if (_txtFieldLastName.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Apellido Paterno"];
        return;
    }
    if (![self validateNameWithString:_txtFieldLastName.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Tu Apellido Paterno no debe contener caracteres especiales o números"];
        return;
    }
    if (_txtFieldSecondLastName.text.length<=0) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Debes ingresar un Apellido Materno"];
        return;
    }
    if (![self validateNameWithString:_txtFieldSecondLastName.text]) {
        [[SharedFunctions sharedInstance] showAlertWithMessage:@"Tu Apellido Materno no debe contener caracteres especiales o números"];
        return;
    }
    if (![_btnCheckBox isSelected]) {
        [self showAlertWithMessage:@"Debes aceptar los Términos y Condiciones"];
        return;
    }
    
    InvokeService *invoke=[[InvokeService alloc] init];
    NSMutableDictionary *dict=[NSMutableDictionary new];
    [dict setObject:_txtFieldEmail.text forKey:@"email"];
    [dict setObject:_txtFieldPass.text forKey:@"pass"];
    [dict setObject:_txtFieldName.text forKey:@"name"];
    [dict setObject:_txtFieldCedula.text forKey:@"cedula"];
    [dict setObject:_txtFieldLastName.text forKey:@"appPat"];
    [dict setObject:_txtFieldSecondLastName.text forKey:@"appMat"];
    [dict setObject:@"" forKey:@"gender"];
    if ([data objectForKey:@"title"]) {
        [dict setObject:[data objectForKey:@"title"] forKey:@"profesional_title"];
    }
    else{
        [dict setObject:@"" forKey:@"profesional_title"];
    }
    if ([data objectForKey:@"institution"]) {
        [dict setObject:[data objectForKey:@"institution"] forKey:@"university"];
    }
    else{
        [dict setObject:@"" forKey:@"university"];
    }
    
    if (genderByProffesionalCardConsultation.length>0) {
        [dict setObject:genderByProffesionalCardConsultation forKey:@"gender"];
    }
    
    
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke registerWithData:dict WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SharedFunctions sharedInstance] removeLoadingView];
            if ([[responseData allKeys] count]<=2) {
                if ([responseData objectForKey:@"username"] ) {
                    if ([[responseData objectForKey:@"username"] isKindOfClass:[NSArray class]] ) {
                        if ([[responseData objectForKey:@"username"] count]>=1) {
                            if ([[[responseData objectForKey:@"username"] objectAtIndex:0] isEqualToString:@"Ya existe un usuario con este nombre."]) {
                                [self showAlertWithMessage:@"Este correo electrónico ya ha sido registrado"];
                                return;
                            }
                        }
                    }
                }
                else{
                    [self showAlertWithMessage:@"Error al Registrarse"];
                }
                
                
            }
            else{
                if ([[responseData allKeys] count]>20) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                            UIAlertController * alert=  [UIAlertController
                                                         alertControllerWithTitle:@"Family Doc"
                                                         message:@"Para continuar con el registro abre el link que se te envió a tu correo electrónico"
                                                         preferredStyle:UIAlertControllerStyleAlert];
                            
                            
                            UIAlertAction* okAction = [UIAlertAction
                                                       actionWithTitle:@"Entendido"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                                       {
                                                           NSURL* mailURL = [NSURL URLWithString:@"message://"];
                                                           if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
                                                               [[UIApplication sharedApplication] openURL:mailURL options:@{} completionHandler:^(BOOL success) {
                                                                   [self dismissViewControllerAnimated:YES completion:nil];
                                                               }];
                                                           }
                                                           
                                                       }];
                            
                            [alert addAction:okAction];
                            [self presentViewController:alert animated:NO completion:nil];
                       // }];
                    });
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                    //    [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                            [self showAlertWithMessage:@"Error al Registrarse"];
                      //  }];
                    });
                    
                }
                
            }
            
            
//            NSMutableDictionary *account=[[NSMutableDictionary alloc] init];
//            [account setObject:_txtFieldEmail.text forKey:@"usr"];
//            [account setObject:_txtFieldPass.text forKey:@"psw"];
//            [[SharedFunctions sharedInstance] saveUser:_txtFieldEmail.text andPass:_txtFieldPass.text];
//            accountInfo=[[NSMutableDictionary alloc] initWithDictionary:account];
//            doctorInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:responseData]];
//            [[SharedFunctions sharedInstance] saveUserData:doctorInfo];
//            [[SharedFunctions sharedInstance] saveUser:_txtFieldEmail.text andPass:_txtFieldPass.text];
            
            
//            InvokeService *invoke2=[[InvokeService alloc] init];
//            [invoke2 loginWithCompletion:accountInfo WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
//                if ([[responseData allKeys] containsObject:@"client_id"] && [[responseData allKeys] containsObject:@"client_secret"]) {
//                    [accountInfo setObject:[responseData objectForKey:@"client_id"] forKey:@"client"];
//                    [accountInfo setObject:[responseData objectForKey:@"client_secret"] forKey:@"secret"];
//                    
//                    InvokeService *invoke3=[[InvokeService alloc] init];
//                    [invoke3 tokenWithInfo:accountInfo WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
//                        if ([[responseData objectForKey:@"access_token"] length]>0) {
//                            [dict setObject:responseData forKey:@"tokenInfo"];
//                            
//                            accountInfo=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dict]];
//                            
//                            InvokeService *invoke4=[[InvokeService alloc] init];
//                            NSMutableDictionary *dictMembership=[NSMutableDictionary new];
//                            [dictMembership setObject:@"1" forKey:@"institution"];
//                            [dictMembership setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
//                            [invoke4 addMembershipWithData:dictMembership WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
//                                NSLog(@"adding membership");
//                            }];
//                            
//                            
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                UIStoryboard *storyboard = self.storyboard;
//                                Register2VC *register2= [storyboard instantiateViewControllerWithIdentifier:@"Register2VC"];
//                                register2.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                                register2.email=_txtFieldEmail.text;
//                                register2.comeFromRegister=YES;
//                                [self presentViewController:register2 animated:YES completion:nil];
//                            });
//                        }
//                        else{
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [self showAlertWithMessage:@"Error al Iniciar Sesión"];
//                            });
//                        }
//                        
//                    }];
//                }
//                else{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self showAlertWithMessage:@"Error al Iniciar Sesión"];
//                    });
//                }
//                
//            }];
            
        });
    }];
    
    
    
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Aceptar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark-
#pragma mark Delegado TextField

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_scrollContent setScrollEnabled:YES];
    textFieldEditable=textField;
    if ([textField isEqual:_txtFieldCedula]) {
        cedulaSearch=NO;
        _txtFieldCedula.text=@"";
        _txtFieldName.text=@"";
        _txtFieldLastName.text=@"";
        _txtFieldSecondLastName.text=@"";
    }
    if (([textField isEqual:_txtFieldName]||[textField isEqual:_txtFieldLastName]||[textField isEqual:_txtFieldSecondLastName])&&_txtFieldCedula.text.length<=0) {
        NSAttributedString *text=[[NSAttributedString alloc] initWithString:@"Antes de ingresar nombre, debes ingresar tu cédula" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 18.0f]}];
        
        SexyTooltip *greetingsTooltip = [[SexyTooltip alloc] initWithAttributedString:text sizedToView:textField withPadding:UIEdgeInsetsMake(10, 10, 10, 10) andMargin:UIEdgeInsetsMake(0, 0, 0, 0)];
        greetingsTooltip.color=[UIColor whiteColor];
        [greetingsTooltip presentFromView:textField
                                   inView:textField
                               withMargin:25
                                 animated:YES];
        [greetingsTooltip dismissInTimeInterval:3];
    }
}

-(void)keyboardWillShow:(NSNotification*)notification{
    NSDictionary* info = [notification userInfo];
    
    CGRect kKeyBoardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    int KeyboardY=kKeyBoardFrame.origin.y;
    int endPositionText=textFieldEditable.frame.origin.y+textFieldEditable.frame.size.height+20;
    if (endPositionText<KeyboardY) {
        textFieldEditable=nil;
        return;
    }
    int animatedDistance =80+endPositionText-KeyboardY;
    const int movementDistance = animatedDistance;
    int movement = -movementDistance;
    movedHeight=-movement;
    [_scrollContent setContentOffset:CGPointMake(0, movedHeight) animated:YES];
   
    
}
-(void)keyboardWillHide:(NSNotification*)notification{
//    if (textFieldEditable==nil) {
//        return;
//    }
    [_scrollContent setContentOffset:CGPointMake(0,0) animated:YES];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
//    if (self.view.frame.size.height>=668) {
//        [_scrollContent setScrollEnabled:NO];
//    }
//    self.layoutTopView.constant=0;
    if (textField==_txtFieldCedula) {
        if (!cedulaSearch) {
            [_txtFieldName setEnabled:NO];
            [_txtFieldLastName setEnabled:NO];
            [_txtFieldSecondLastName setEnabled:NO];
            _txtFieldName.text=@"";
            _txtFieldLastName.text=@"";
            _txtFieldSecondLastName.text=@"";
        }
    }
}

@end
