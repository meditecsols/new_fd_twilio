//
//  MyProfileVC.h
//  
//
//  Created by Martin Gonzalez on 16/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>



@interface MyProfileVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtSpecialty;
@property (strong, nonatomic) IBOutlet UITextField *txtOfficePhone;
@property (strong, nonatomic) IBOutlet UITextField *txtCellPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblDr;
@property (strong, nonatomic) IBOutlet UILabel *lblNameDoctor;
@property (strong, nonatomic) IBOutlet UILabel *lblSpecialty;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightSpecialtyContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnService;
@property (strong, nonatomic) IBOutlet UILabel *lblSpecialtyList;
@property (strong, nonatomic) IBOutlet UILabel *lblGender;
@property (strong, nonatomic) IBOutlet UIImageView *imgGender;
@property (strong, nonatomic) IBOutlet UILabel *lblBirthDate;
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UIButton *btnAddSpecialty;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollContainer;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;

- (IBAction)actionAddress:(id)sender;
- (IBAction)actionSign:(id)sender;
- (IBAction)saveNewSpecialty:(id)sender;
- (IBAction)actionOpendAddSpecialty:(id)sender;
- (IBAction)actionEdit:(id)sender;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionSave:(id)sender;
- (IBAction)actionCheckBox:(id)sender;
- (IBAction)actionRestorePurchase:(id)sender;
@end
