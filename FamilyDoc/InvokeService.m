//
//  InvokeService.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 27/09/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "InvokeService.h"
#import "Config.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"
@import MobileCoreServices;


#define TIMEOUT_REQUEST 30

@implementation InvokeService
- (void)loginWithCompletion:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",};
    NSDictionary *parameters = @{ @"username": [dictInfo objectForKey:@"usr"],
                                  @"password": [dictInfo objectForKey:@"psw"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/login/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
- (void)tokenWithInfo:(NSDictionary *)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",[dictInfo objectForKey:@"client"] ,[dictInfo objectForKey:@"secret"]]];
    authString=[NSString stringWithFormat:@"Basic %@",authString];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization": authString, };
    NSDictionary *parameters = @{ @"grant_type": @"password",
                                  @"username": [dictInfo objectForKey:@"usr"],
                                  @"password": [dictInfo objectForKey:@"psw"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/o/token/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark RetrieveZIPCodeData
- (void)retrieveZipCodeData:(NSString *)zipCode WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@%@",URL_ZIP_CODE,zipCode];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                    
                                                        NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:dutf8 options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark RetrieveCedulaInfo
- (void)retrieveCedulaInfo:(NSString *)cedula WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctorverifier/%@/",BASE_URL,cedula];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                       // NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                        
                                                       // NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        //NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark Register
- (void)registerWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    //NSString *authString=[self encodeStringTo64:[NSString stringWithFormat:@"%@:%@",client,secret]];
   // authString=[NSString stringWithFormat:@"Basic %@",authString];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization": @"Basic d2d2RWhLQlV0VDF1djk1bFVYR3Z6RGFvOHhiSWY5Tk5iNmtsdmtVUDpRNjBOV1REWTJFWWE2a0xyc2pVVG1NTG9yV0ZtRXk2Q2VTTXZtelF1OHE5blVDSTZQWlkyY3NmRmRsTDI4MU9reVhONHU3Z2ExMTZJczJJWkJBVzlNMWhneFFhWnY1WEtRS0FMalVkMkg5WlAyWkhOamtxWTljQmNmNHViUnMxSg==" };
    NSDictionary *parameters = @{
                                 @"first_name":[dictInfo objectForKey:@"name"],
                                 @"password":[dictInfo objectForKey:@"pass"],
                                 @"professional_card":[dictInfo objectForKey:@"cedula"],
                                 @"last_name":[dictInfo objectForKey:@"appPat"],
                                 @"second_last_name":[dictInfo objectForKey:@"appMat"],
                                 @"username":[dictInfo objectForKey:@"email"],
                                 @"email":[dictInfo objectForKey:@"email"],
                                 @"gender":[dictInfo objectForKey:@"gender"],
                                 @"profesional_title":[dictInfo objectForKey:@"profesional_title"],
                                 @"university":[dictInfo objectForKey:@"university"]
                                 };
    
    
    
    
    
    
    
    {
        
    }
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/doctor/?app=familydoc",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark GetDoctor
- (void)getDoctorData:(NSString *_Nullable)idDoctor WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctor/%@/",BASE_URL,idDoctor];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        
                                                       // NSString *correctString = [NSString stringWithCString:[iso cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                                                        
                                                        //NSData *dutf8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
                                                        
                                                        
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[ws_responce mutableCopy];
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark UpdateDoctor
- (void)updateDoctorWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if ([[dictInfo objectForKey:@"clabe"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"clabe"] forKey:@"clabe"];
    }
    if ([[dictInfo objectForKey:@"date_of_birth"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"date_of_birth"] forKey:@"date_of_birth"];
    }
    
    if ([[dictInfo objectForKey:@"mobile_phone_number"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"mobile_phone_number"] forKey:@"mobile_phone_number"];
    }
    if ([[dictInfo objectForKey:@"work_phone_number"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"work_phone_number"] forKey:@"work_phone_number"];
    }
    if ([[dictInfo objectForKey:@"email"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"email"] forKey:@"email"];
    }
    if ([[dictInfo allKeys]containsObject:@"public_profile_preference"]) {
        [params setObject:[dictInfo objectForKey:@"public_profile_preference"] forKey:@"public_profile_preference"];
    }
    if ([[dictInfo objectForKey:@"os"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"os"] forKey:@"os"];
    }
    if ([[dictInfo objectForKey:@"version"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"version"] forKey:@"version"];
    }
    
    if ([[dictInfo objectForKey:@"push_token"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"push_token"] forKey:@"push_token"];
    }
    else if ([fcmTokenRetrieved length]>0) {
        [params setObject:fcmTokenRetrieved forKey:@"push_token"];
    }
    
    NSMutableArray *arrayPaths=[[NSMutableArray alloc] init];
    NSMutableArray *arrayParameterImages=[[NSMutableArray alloc] init];
    if ([[dictInfo objectForKey:@"pathSignatureImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathSignatureImage"]];
        [arrayParameterImages addObject:@"signature"];
    }
    if ([[dictInfo objectForKey:@"pathAccountImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathAccountImage"]];
        [arrayParameterImages addObject:@"income_document"];
    }
    if ([[dictInfo objectForKey:@"pathProfileImage"] length]>0) {
        [arrayPaths addObject:[dictInfo objectForKey:@"pathProfileImage"]];
        [arrayParameterImages addObject:@"profile_pic"];
    }
    
    
    NSString *boundary = [self generateBoundaryString];

    
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctor/%@/",BASE_URL,[doctorInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
   // [request setHTTPBody:postData];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:arrayPaths fieldNames:arrayParameterImages];
    
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([[ws_responce allKeys] count]>=35) {
                                                            doctorInfo=[[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:[ws_responce copy]] mutableCopy];
                                                        }
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addAddress
- (void)addAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    

    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark updateAddress
- (void)updateAddressWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/%@/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        int code=(int)[httpResponse statusCode];
                                                        if (code==200) {
                                                           // NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                            //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                            ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getAddress
- (void)getAddressWithData:(NSString *_Nullable)idDoctor     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
 
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctoraddress/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getConsultations
- (void)getConsultationsByDoctorWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultation/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                       // NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getPatients
- (void)getPatientsByDoctorWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctor/%@/patients/",BASE_URL,[doctorInfo objectForKey:@"id"]];
    //SString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctor/128/patients/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getPatient
- (void)getPatientById:(NSString*)idPatient WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/",BASE_URL,idPatient];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getSpecialtyByDoctor
- (void)getSpecialtiesByDoctor:(NSString *)idDoctor WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctorspeciality/%@/",BASE_URL,idDoctor];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                       // NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getSpecialtyOfDoctors
- (void)getSpecialtiesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctorspeciality/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getSpecialtyList
- (void)getSpecialtiesListWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/speciality/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addSpecialty
- (void)addSpecialtyWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/speciality/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addSpecialtyToDoctor
- (void)addSpecialtyToDoctorWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctorspeciality/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getImageFromUrl
- (void)getImageFromUrl:(NSString *_Nullable)url     WithCompletion:(void (^_Nullable)(UIImage * _Nullable imageDownloaded, NSError * _Nullable error))completion{
    //NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    //NSDictionary *headers = @{ @"content-type": @"application/json",
    //                           @"accept": @"application/json",
    //                           @"authorization":  authString};

    NSString *urlString=url;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
   // [request setAllHTTPHeaderFields:headers];
   // NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    UIImage *downloadedImage;
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        downloadedImage = [UIImage imageWithData:data];
                                                        
                                                    }
                                                    completion(downloadedImage,error);
                                                }];
    [dataTask resume];
}
#pragma mark getPatientProfile
- (void)getPatientProfileFromWS:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/",BASE_URL,idPatient];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([[ws_responce allKeys] count]>=15) {
                                                            dictPatientPresential=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:ws_responce]];
                                                        }
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addConsultation
- (void)addAConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultation/",BASE_URL];
    if ([[dictInfo objectForKey:@"payment_type"] isEqualToString:@"subscription"]) {
        urlString=[NSString stringWithFormat:@"%@/api/v0/consultationfb/",BASE_URL];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([[ws_responce allKeys] count]>=17) {
                                                            presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:ws_responce]];
                                                        }
                                                        else if([ws_responce objectForKey:@"open_remote_consultations"]){
                                                            if ([[[[ws_responce objectForKey:@"open_remote_consultations"]objectAtIndex:0] allKeys] count]>=17) {
                                                                ws_responce=[[[ws_responce objectForKey:@"open_remote_consultations"]objectAtIndex:0]mutableCopy];
                                                                [ws_responce setObject:@YES forKey:@"wasOpen"];
                                                                presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:ws_responce]];
                                                            }
                                                            
                                                        }
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getHealthCondition
- (void)getHealthCondition:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/healthcondition/?patient=%@",BASE_URL,idPatient];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getHealthConditionForConsultation
- (void)getHealthCondition:(NSString *_Nullable)idPatient AndConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/healthcondition/?patient=%@&consultation=%@",BASE_URL,idPatient,idConsultation];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addHealthCondition
- (void)addHealthConditionWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/healthcondition/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}

#pragma mark deletetHealthCondition
- (void)deleteHealthCondition:(NSString *_Nullable)idHealth     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/healthcondition/%@/",BASE_URL,idHealth];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"DELETE"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getNotes
- (void)getNoteOfConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultation/%@/notes/",BASE_URL,idConsultation];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addNoteConsultation
- (void)addNoteConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultationnote/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getPrescrition
- (void)getPrescriptionsOfConsultation:(NSString *_Nullable)idConsultation     WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultation/%@/prescriptions/",BASE_URL,idConsultation];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addPrescrition
- (void)addPrescritionWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/prescription/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addMedicalStudy
- (void)addMedicalStudyWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/medicalstudy/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addTreatment
- (void)addTreatmentWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/treatment/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark updateConsultation
- (void)updateConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    if (![[dictInfo objectForKey:@"payment_transaction_id"] isEqual:[NSNull null]]) {
        [dictInfo setObject:@"<null>" forKey:@"payment_transaction_id"];
    }
    if (![[dictInfo objectForKey:@"end_date"] isEqual:[NSNull null]]) {
        [dictInfo setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"end_date"];
    }
    if (![[dictInfo objectForKey:@"start_date"] isEqual:[NSNull null]]) {
        [dictInfo setObject:[[SharedFunctions sharedInstance] getDateFormatToWS] forKey:@"start_date"];
    }
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultation/%@/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        int code=(int)[httpResponse statusCode];
                                                        if (code==200) {
                                                            ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark downloadPdfPrescription
- (void)downloadPdfPrescription:(NSString *_Nullable)prescriptionId AndSignature:(NSString *_Nullable)addSignature    WithCompletion:(void (^_Nullable)(NSString * _Nullable path, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/prescription/%@/pdf/?signature=%@",BASE_URL,prescriptionId,addSignature];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    // NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        
                                                        
                                                        NSString *path;
                                                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                                        path = [paths objectAtIndex:0];
                                                        NSString *namePath=[NSString stringWithFormat:@"%@.pdf",prescriptionId];
                                                        path = [path stringByAppendingPathComponent:namePath];
                                                        
                                                        
                                                        
                                                        NSError *error;
                                                        BOOL sucess =[data writeToFile:path options:NSDataWritingAtomic error:&error];
                                                        if (sucess) {
                                                            completion(path,error);
                                                        }
                                                        
                                                    }
                                                    completion(@"",error);
                                                }];
    [dataTask resume];
}
#pragma mark addServiceConsultation
- (void)addServiceToConsultationWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultationservice/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addService
- (void)addServiceWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/service/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark updateService
- (void)updateServiceWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/service/%@/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [dictInfo removeObjectForKey:@"id"];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark deleteService
- (void)deleteService:(NSString *_Nullable)idHealth     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    NSMutableDictionary *dictInfo=[[NSMutableDictionary alloc] init];
    [dictInfo setObject:@false forKey:@"enabled"];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/service/%@/",BASE_URL,idHealth];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addMembership
- (void)addMembershipWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctormembership/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getServices
- (void)getServicesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/service/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark recoverPass
- (void)recoverPassWithInfo:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",};
    NSDictionary *parameters = @{ @"email": [dictInfo objectForKey:@"email"] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/login/recover/viaemail/",BASE_URL]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getConsultationsByPatient
- (void)getConsultationsByPatientWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/consultations/",BASE_URL,[dictInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                         NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addMetricsPatient
- (void)addMetricsWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patientmetrics/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getUrls
- (void)getUrlsTermsPrivacyWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/sysurl/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getPatientAvaliable
- (void)getPatientAviliablePaymentsById:(NSString*)idPatient IsRemote:(BOOL)isRemote WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/availablepayment/",BASE_URL,idPatient];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        ws_responce=[[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:ws_responce] mutableCopy];
                                                        
                                                        if (isRemote) {
                                                            if ([[ws_responce objectForKey:@"open_remote_consultations"] count]>0) {
                                                                if ([self verifyIfDoctorHasConsultationsOpenWithArray:[ws_responce objectForKey:@"open_remote_consultations"]])
                                                                {
                                                                    NSMutableDictionary *dictConsultation=[self getDoctorConsultationOpenWithArray:[ws_responce objectForKey:@"open_remote_consultations"]];
                                                                    if ([[dictConsultation allKeys] count]>=17) {
                                                                        [dictConsultation setObject:@YES forKey:@"wasOpen"];
                                                                        presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dictConsultation]];
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                
                                                            }
                                                        }
                                                        else{
                                                            if ([[ws_responce objectForKey:@"open_onsite_consultations"] count]>0) {
                                                                if ([self verifyIfDoctorHasConsultationsOpenWithArray:[ws_responce objectForKey:@"open_onsite_consultations"]])
                                                                {
                                                                    NSMutableDictionary *dictConsultation=[self getDoctorConsultationOpenWithArray:[ws_responce objectForKey:@"open_onsite_consultations"]];
                                                                    if ([[dictConsultation allKeys] count]>=17) {
                                                                        [dictConsultation setObject:@YES forKey:@"wasOpen"];
                                                                        presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:dictConsultation]];
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark addPatientRelation
- (void)addPatientRelationWithId:(NSString *_Nullable)idPatient     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/doctor/%@/relation/patient/%@/",BASE_URL,[doctorInfo objectForKey:@"id"],idPatient]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getCompanies
- (void)getCompaniesWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/company/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        //NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getStatusOfPatient
- (void)getStatusOfPatientWithCompletion:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};

    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/consultationlight/",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark - Chat
#pragma mark addChanel
- (void)addAChanelWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/channel/doctor/%@/",BASE_URL,[dictInfo objectForKey:@"id"],[doctorInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    //[request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        if ([[ws_responce allKeys] count]>=17) {
                                                            presentialConsultation=[[NSMutableDictionary alloc] initWithDictionary:[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:ws_responce]];
                                                        }
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getConsultationsByPatient
- (void)getMessagesByPatientWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/messages/doctor/%@/",BASE_URL,[dictInfo objectForKey:@"id"],[doctorInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getTokenChatWithInfo
- (void)getTokenChatWithInfo:(NSMutableDictionary *_Nullable)dictInfo WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/doctor/%@/apntoken/?app=familydoc",BASE_URL,[doctorInfo objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark UpdatePatient
- (void)updatePatientWithData:(NSMutableDictionary *_Nullable)dictInfo     WithCompletion:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"authorization":  authString};
    
    
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if ([[dictInfo objectForKey:@"date_of_birth"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"date_of_birth"] forKey:@"date_of_birth"];
    }
    if ([[dictInfo objectForKey:@"gender"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"gender"] forKey:@"gender"];
    }
    if ([[dictInfo objectForKey:@"blood_type"] length]>0) {
        [params setObject:[dictInfo objectForKey:@"blood_type"] forKey:@"blood_type"];
    }


    
    
    NSString *boundary = [self generateBoundaryString];
    
    
    //NSData *postData = [NSJSONSerialization dataWithJSONObject:dictInfo options:0 error:nil];
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/patient/%@/",BASE_URL,[dictPatientPresential objectForKey:@"id"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"PATCH"];
    [request setAllHTTPHeaderFields:headers];
    // [request setHTTPBody:postData];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:nil fieldNames:nil];
    
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session uploadTaskWithRequest:request fromData:httpBody completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"%@", httpResponse);
            
            
            NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
            NSLog(@"%@",iso);
#endif
            //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
            ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            if ([[ws_responce allKeys] count]>=20) {
                if ([[params objectForKey:@"username"] length]>0) {
                    usr=[params objectForKey:@"username"];
                    [[SharedFunctions sharedInstance] saveUser:usr andPass:psw];
                    
                }
                dictPatientPresential=[[[SharedFunctions sharedInstance] dictionaryByReplacingNullsWithStrings:[ws_responce copy]] mutableCopy];
            }
            
        }
        completion(ws_responce,error);
    }];
    [dataTask resume];
}
#pragma mark -
#pragma mark utilMethods
-(BOOL)verifyIfDoctorHasConsultationsOpenWithArray:(NSMutableArray *)arrayConsultions{
    BOOL hasOpen=NO;
    for (NSDictionary *dictConsultation in arrayConsultions) {
        if ([[dictConsultation objectForKey:@"doctor"] intValue] == [[doctorInfo objectForKey:@"id"] intValue]) {
            return YES;
        }
    }
    return hasOpen;
}
-(NSMutableDictionary *)getDoctorConsultationOpenWithArray:(NSMutableArray *)arrayConsultions{
    NSMutableDictionary *dicConsult=[[NSMutableDictionary alloc] init];
    for (NSDictionary *dictConsultation in arrayConsultions) {
        if ([[dictConsultation objectForKey:@"doctor"] intValue] == [[doctorInfo objectForKey:@"id"] intValue]) {
            return [dictConsultation mutableCopy];
        }
    }
    return dicConsult;
}
- (NSString *)mimeTypeForPath:(NSString *)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}
- (NSString *)generateBoundaryString {
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldNames:(NSArray *)fieldNames {
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    int i = 0;
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [fieldNames objectAtIndex:i], filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        i++;
    }
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}
- (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    }
    return base64String;
}
//- (void)elWithInfo:(NSArray *)info {
//    NSString *POST_BODY_BOURDARY=@"asdfsdf";
//    NSURL *url = [NSURL URLWithString:@"http://127.0.0.1/uploadimage.php"];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:5.0];
//    [request setHTTPMethod:@"POST"];
//    NSString *contentTypeValue = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", POST_BODY_BOURDARY];
//    [request addValue:contentTypeValue forHTTPHeaderField:@"Content-type"];
//    NSMutableData *dataForm = [NSMutableData alloc];
//    [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//    [dataForm appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param1\";\r\n\r\n%@", @"10001"] dataUsingEncoding:NSUTF8StringEncoding]];
//    if([info count] > 0) {
//        int i = 0;
//        for (NSDictionary *imageInfo in info) {
//            ///UIImage *image = [imageInfo valueForKey:UIImagePickerControllerOriginalImage];
//            //image = [self resizeImage:image minSize:700.0];
//            //NSData *imageData = UIImageJPEGRepresentation(image, 0.9);
//            [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fileToUpload[]\"; filename=\"%@%d.jpg\"\r\n", @"test", i] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[[NSString stringWithFormat:@"Content-Type: image/jpeg\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//            [dataForm appendData:[NSData dataWithData:imageData]];
//            i++;
//        }
//    }
//    [dataForm appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",POST_BODY_BOURDARY] dataUsingEncoding:NSUTF8StringEncoding]];
//    NSURLSessionUploadTask *uploadTask = [urlSession uploadTaskWithRequest:request fromData:dataForm];
//    [uploadTask resume];
//}
-(NSString *)parseToTinyURL:(NSString *)urlToShare{
    
    NSString *urlData =[NSString stringWithFormat:@"https://tinyurl.com/api-create.php?url=%@",urlToShare];
    
    NSError *error;
    
    NSString *shortURL = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlData]
                                                  encoding:NSASCIIStringEncoding
                                                     error:&error];
    return shortURL;
}
#pragma mark ShortUrl
- (void)postShortUrlWithUrl:(NSString *)inviteDoctUrl AndCompletition:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"apikey": @"f65d8d505d394c4794fadd8031a1da6d"};
    NSString *slashTagDoctor=[NSString stringWithFormat:@"InviteDoctor%@",[doctorInfo objectForKey:@"id"]];
    NSDictionary *parameters = @{
                                 @"destination": inviteDoctUrl,
                                 @"title": @"titulo",
                                 @"slashtag": slashTagDoctor,
                                 @"shortUrl": [NSString stringWithFormat:@"fdoc.xyz/%@",slashTagDoctor],
                                 @"domain": @{
                                         @"id": @"6e9c1bdd69504fc4bdf8e95cf37f0928",
                                         @"fullName": @"fdoc.xyz"
                                         }
                                 };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.rebrandly.com/v1/links"]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableDictionary *ws_responce=[[NSMutableDictionary alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark getShortUrl
- (void)getShortUrlWithSlashTag:(NSString *)slashTagDoctor AndCompletition:(void (^_Nullable)(NSMutableArray * _Nullable responseData, NSError * _Nullable error))completion{
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"apikey": @"f65d8d505d394c4794fadd8031a1da6d"};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"https://api.rebrandly.com/v1/links/?domain.id=6e9c1bdd69504fc4bdf8e95cf37f0928&slashtag=%@",slashTagDoctor];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
#pragma mark Twilio
- (void)getTokenTwilioWithUrl:(NSString *)room AndCompletition:(void (^_Nullable)(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error))completion{
      NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
      NSDictionary *headers = @{ @"content-type": @"application/json",
                                 @"accept": @"application/json",
                                 @"authorization":  authString};
    
    
    
    NSString *urlString=[NSString stringWithFormat:@"%@/api/v0/login/twilio?id=%@&room=%@",BASE_URL,[doctorInfo objectForKey:@"id"],room];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIMEOUT_REQUEST];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    NSMutableArray *ws_responce=[[NSMutableArray alloc] init];
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        NSString *iso = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
#if DEBUG
                                                        NSLog(@"%@",iso);
#endif
                                                        //NSData *dutf8 = [iso dataUsingEncoding:NSUTF8StringEncoding];
                                                        ws_responce=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                                        
                                                    }
                                                    completion(ws_responce,error);
                                                }];
    [dataTask resume];
}
@end
