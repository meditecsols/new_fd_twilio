//
//  DifusionListVC.m
//  FamilyDocDoctor
//
//  Created by Martin Gonzalez on 6/27/19.
//  Copyright © 2019 Martin Gonzalez. All rights reserved.
//

#import "DifusionListVC.h"
#import "GlobalMembers.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "CompanyCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChatDifusionVC.h"

@interface DifusionListVC (){
    NSMutableArray *companiesArray;
    NSMutableArray *patietsOfCompanySelectedArray;
    UIRefreshControl *refreshControl;
}

@end

@implementation DifusionListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _companiesTableView.dataSource=self;
    _companiesTableView.delegate=self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard)];
    tap.delegate=self;
    [self.view addGestureRecognizer:tap];
    
    companiesArray=[[NSMutableArray alloc] init];
    patietsOfCompanySelectedArray=[[NSMutableArray alloc] init];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh1:) forControlEvents:UIControlEventValueChanged];
    [_companiesTableView addSubview:refreshControl];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self getCompanies];
    }];
    
}
- (void)refresh1:(UIRefreshControl *)refreshControl
{
    [refreshControl endRefreshing];
    [[SharedFunctions sharedInstance] showLoadingViewWithCompletition:^{
        [self getCompanies];
    }];
}
-(void)getCompanies{
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getCompaniesWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->refreshControl endRefreshing];
            [[SharedFunctions sharedInstance] removeLoadingViewWithCompletition:^{
                self->companiesArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
                [self->_companiesTableView reloadData];
            }];
        });
    }];
    return;
}
-(void)getPatientsOfCompany:(NSDictionary *)companySelected{
    [[SharedFunctions sharedInstance] showLoadingView];
    InvokeService *invoke=[[InvokeService alloc] init];
    [invoke getPatientsByDoctorWithCompletion:^(NSMutableArray * _Nullable responseData, NSError * _Nullable error) {
        if ([responseData isKindOfClass:[NSDictionary class]]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
            });
            return;
        }
        NSMutableArray *patientsArray=[[[SharedFunctions sharedInstance] arrayByReplacingNullsWithBlanks:[responseData copy]] mutableCopy];
        
        
        for (NSDictionary *dict in patientsArray) {
            if ([[dict objectForKey:@"companysubscription"] length]>0) {
                if ([[dict objectForKey:@"companysubscription"] isEqualToString:[companySelected objectForKey:@"name"]] ) {
                    [self->patietsOfCompanySelectedArray addObject:dict];
                }
                
            }
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            ChatDifusionVC *vc = [ChatDifusionVC messagesViewController];
            vc.patientsOfCompany=self->patietsOfCompanySelectedArray;
            vc.companySelected=companySelected;
            vc.modalPresentationStyle= UIModalPresentationOverFullScreen;
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:nc animated:YES completion:nil];

        });
    }];
}
-(void)hideKeyboard{
    [self.view endEditing:YES];
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
       shouldReceiveTouch:(UITouch *)touch
{
    if ([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        return YES;
    }
    if([touch.view isDescendantOfView:_companiesTableView]){
        return NO;
    }
    
    return YES;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [companiesArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"CompanyCell";
    CompanyCell *cell = (CompanyCell *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblName.text=[[companiesArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.lblDesc.text=[NSString stringWithFormat:@"Número de Pacientes: %@",[[companiesArray objectAtIndex:indexPath.row] objectForKey:@"total_count_beneficiaries"]];
    if ([[[companiesArray objectAtIndex:indexPath.row] objectForKey:@"company_pic"] length]>0) {
        [cell.imgCompany sd_setImageWithURL:[NSURL URLWithString:[[companiesArray objectAtIndex:indexPath.row]  objectForKey:@"company_pic"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (image) {
                [self roundImage:cell.imgCompany];
            }
            else{
                [cell.imgCompany setImage:[UIImage imageNamed:@"imgCompany"]];
            }
            
        }];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self getPatientsOfCompany:[companiesArray objectAtIndex:indexPath.row]];}
-(IBAction)actionBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
