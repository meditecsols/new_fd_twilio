//
//  Register2VC.m
//  FamilyDoc
//
//  Created by Martin Gonzalez on 07/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "Register2VC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "CaptureSignVC.h"
#import "InvokeService.h"
#import "GlobalMembers.h"
#import "SharedFunctions.h"

@interface Register2VC (){
    int movedHeight;
    UITextField *textFieldEditable;
    BOOL isSavedAccountImage;
    UIView *popUpView;
    UIDatePicker *datePicker;
    NSDate *dateChosen;
}

@end

@implementation Register2VC
@synthesize imagePicker;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    self.txtDay.delegate=self;
    self.txtClabe.delegate=self;
    self.txtYear.delegate=self;
    self.txtMonth.delegate=self;
    self.txtPhone.delegate=self;
    

    [_scrollContent setScrollEnabled:YES];
    if (self.view.frame.size.height>=668) {
        [_scrollContent setScrollEnabled:NO];
    }
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"es-MX"];
    [datePicker setLocale:locale];
    NSString *dateString = @"01-01-1980";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    [datePicker setDate:date];
    [datePicker setMaximumDate:[NSDate date]];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    datePicker.tintColor = [UIColor whiteColor];
    [_txtDay setInputView:datePicker];
    [_txtMonth setInputView:datePicker];
    [_txtYear setInputView:datePicker];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingDate)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    toolBar.tintColor = [UIColor darkGrayColor];
    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _txtDay.inputAccessoryView = toolBar;
    _txtMonth.inputAccessoryView = toolBar;
    _txtYear.inputAccessoryView = toolBar;
    
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//    documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    //fileMgr = [NSFileManager defaultManager];
    NSError *error;
    if ([fileMgr fileExistsAtPath:documentsDirectory]){
        [fileMgr removeItemAtPath:documentsDirectory error:&error];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    if ([[doctorInfo objectForKey:@"signature"] length]>0){
        _lblSignStatus.text=@"         ¡LISTO!";
        UIImageView *imgChec=[[UIImageView alloc] initWithFrame:CGRectMake(_lblSignStatus.frame.origin.x, _lblSignStatus.frame.origin.y, 10, 10)];
        [imgChec setImage:[UIImage imageNamed:@"imgCheckWhite"]];
        [_scrollContent addSubview:imgChec];
    }
    //NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
    //NSData *imgData = [NSData dataWithContentsOfFile:documentsDirectory];
    //UIImage *image = [[UIImage alloc] initWithData:imgData];
    
    popUpView=[[UIView alloc] init];
    popUpView=[[[NSBundle mainBundle] loadNibNamed:@"Register2HelpView" owner:self options:nil] objectAtIndex:0];
    [popUpView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) ];
    if (_comeFromRegister) {
        [self.view addSubview:popUpView];
        _comeFromRegister=NO;
    }
    
    [self finishingTakeSing];
    
//    NSFileManager *fileMgr = [NSFileManager defaultManager];
//    if ([fileMgr fileExistsAtPath:documentsDirectory]){
//        [_viewSignature setBackgroundColor:[UIColor whiteColor]];
//        [_divViewSignature setBackgroundColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00]];
//        _lblViewSignature.textColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
//        [_imgViewSignature setImage:[UIImage imageNamed:@"imgSignature"]];
//    }
//    else{
//        [_viewSignature setBackgroundColor:[UIColor clearColor]];
//        [_divViewSignature setBackgroundColor:[UIColor whiteColor]];
//        _lblViewSignature.textColor=[UIColor whiteColor];
//        [_imgViewSignature setImage:[UIImage imageNamed:@"imgSignatureWhite"]];
//    }
}
- (void) donePickingDate {
    dateChosen =datePicker.date;
    _txtDay.text = [[[self formatDate:dateChosen] componentsSeparatedByString:@"/"] objectAtIndex:0];
    _txtMonth.text = [[[self formatDate:dateChosen] componentsSeparatedByString:@"/"] objectAtIndex:1];
    _txtYear.text = [[[self formatDate:dateChosen] componentsSeparatedByString:@"/"] objectAtIndex:2];
    [self dismissKeyboard];
}
-(NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"\n\n\nPadecimiento"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    NSString *text =[NSString stringWithFormat:@"\n\n\nPadecimiento\n\n"];
    NSString *text2=message;
    NSString *text3=[NSString stringWithFormat:@"%@%@",text,text2];
    NSMutableAttributedString *attributedText =[[NSMutableAttributedString alloc] initWithString:text3];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:19] range:[text3 rangeOfString:text]];
    NSRange blackTextRange = [text3 rangeOfString:text2];
    [attributedText addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15] range:blackTextRange];
    [alert setValue:attributedText forKey:@"attributedTitle"];
    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    
    
    
    [self presentViewController:alert animated:NO completion:^{
        int xPosition = alert.view.frame.size.width/2-20;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xPosition,30,40,44)];
        imageView.image = [UIImage imageNamed:@"imgPadecimientoAlert"];
        [alert.view addSubview:imageView];
        
    }];
}
-(void)selectPhotoAccount{
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Seleccione una opción:" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *destructiveAction = [UIAlertAction actionWithTitle:@"Tomar foto" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self->imagePicker = [[UIImagePickerController alloc] init];
            self->imagePicker.delegate = self;
            self->imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            self->imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            self->imagePicker.allowsEditing = NO;
            [self presentViewController:self->imagePicker animated:YES completion:nil];
            //newMedia = YES;
        }
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Seleccionar existente" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            self->imagePicker = [[UIImagePickerController alloc] init];
            self->imagePicker.delegate = self;
            self->imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            self->imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            self->imagePicker.allowsEditing = NO;
            self->imagePicker.modalPresentationStyle= UIModalPresentationOverFullScreen;
            [self presentViewController:self->imagePicker animated:YES completion:nil];
            //newMedia = NO;
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:destructiveAction];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated: YES completion: nil];
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width scaledToHeight:(float) i_height
{
    UIImage *newImage;
    if (sourceImage.size.width>i_width) {
        float oldWidth = sourceImage.size.width;
        float scaleFactor = i_width / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else{
        newImage=sourceImage;
    }
    
    if(newImage.size.height>i_height){
        float oldHeight = sourceImage.size.height;
        float scaleFactor = i_height / oldHeight;
        
        float newWidth = sourceImage.size.width * scaleFactor;
        float newHeight =oldHeight * scaleFactor;
        
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    return newImage;
}
-(void)takeSignature{
//    [_viewSignature setBackgroundColor:[UIColor whiteColor]];
//    [_divViewSignature setBackgroundColor:[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00]];
//    _lblViewSignature.textColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
//    [_imgViewSignature setImage:[UIImage imageNamed:@"imgSignature"]];
    
    UIStoryboard *storyboard = self.storyboard;
    CaptureSignVC *capture= [storyboard instantiateViewControllerWithIdentifier:@"CaptureSignVC"];
    capture.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    capture.modalPresentationStyle= UIModalPresentationOverFullScreen;
    //capture.doctorName=[NSString stringWithFormat:@"Dr. %@",_txtFullName.text];
    [self presentViewController:capture animated:YES completion:^{
     //   capture.doctorName=[NSString stringWithFormat:@"Dr. %@",_txtFullName.text];
    }];
}
- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)verifyFields{
    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (_txtClabe.text.length<18) {
        return;
    }
    if (_txtPhone.text.length<=0) {
        return;
    }
    if (_txtDay.text.length<=0) {
        return;
    }

    if (!isSavedAccountImage) {
        return;
    }
    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionNext:(id)sender {
    if (_txtClabe.text.length<18) {
        [self showAlertWithMessage:@"Debes ingresar número de CLABE Interbancaria de 18 dígitos"];
        return;
    }
    if (_txtDay.text.length==0) {
        [self showAlertWithMessage:@"Debes seleccion tu fecha de nacimiento"];
        return;
    }
    if (_txtPhone.text.length<10) {
        [self showAlertWithMessage:@"Debes ingresar tu teléfono a 10 dígitos."];
        return;
    }

    

    if (!isSavedAccountImage) {
        [self showAlertWithMessage:@"Es necesario que adjuntes tu estado de cuenta para utilizar los beneficios de la aplicación."];
        return;
    }
    
    InvokeService *invoke3=[[InvokeService alloc] init];
    NSMutableDictionary *dictToChange=[[NSMutableDictionary alloc] init];
    [dictToChange setObject:[[SharedFunctions sharedInstance] formatDateToWS:dateChosen] forKey:@"date_of_birth"];
    [dictToChange setObject:_txtClabe.text forKey:@"clabe"];
    if ([_btnCheckBox isSelected]) {
         [dictToChange setObject:@YES forKey:@"public_profile_preference"];
    }
    if (_txtPhone.text.length>0) {
        [dictToChange setObject:_txtPhone.text forKey:@"work_phone_number"];
    }
    [dictToChange setObject:[doctorInfo objectForKey:@"id"] forKey:@"id"];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:@"/ProfileInfo"];
    NSString *nameImage=[NSString stringWithFormat:@"accountPhoto.jpg"];
    NSString *path= [documentsDirectory stringByAppendingPathComponent:nameImage];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:documentsDirectory]){
        [dictToChange setObject:path forKey:@"pathAccountImage"];
    }
    
    
    documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
    fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:documentsDirectory]){
        [dictToChange setObject:documentsDirectory forKey:@"pathSignatureImage"];
    }
    
    
   
    

    
    [[SharedFunctions sharedInstance] showLoadingView];
    [invoke3 updateDoctorWithData:dictToChange WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[SharedFunctions sharedInstance] removeLoadingView];
            if ([[responseData allKeys] count]>0) {
                UIStoryboard *storyboard = self.storyboard;
                UIViewController *register3= [storyboard instantiateViewControllerWithIdentifier:@"Register3VC"];
                register3.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                register3.modalPresentationStyle= UIModalPresentationOverFullScreen;
                [self presentViewController:register3 animated:YES completion:nil];
//                UIStoryboard *storyboard = self.storyboard;
//                UIViewController *home= [storyboard instantiateViewControllerWithIdentifier:@"HomeViewsContainerVC"];
//                home.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//                [self presentViewController:home animated:YES completion:nil];
                
                
            }
            else{
                [self showAlertWithMessage:@"Error al actualizar sus datos"];
            }
            
        });
        
    }];

    
    
}
-(NSString*)convertImageBytes:(UIImage*)image{
//    int width=0;
//    int height=0;
//
//    if(image.size.width > image.size.height){
//        width = 300;
//        height = image.size.height * 300;
//        height = height / image.size.width;
//    }else{
//        height = 300;
//        width = image.size.width * 300;
//        width = width / image.size.height;
//    }
    
    NSData *data = UIImageJPEGRepresentation(image,0.0);
    NSString *base64Encoded = [data base64EncodedStringWithOptions:0];
    
    // Print the Base64 encoded string
    //NSLog(@"Encoded: %@", base64Encoded);
    
    if(base64Encoded.length==0){
        base64Encoded = [NSString stringWithFormat:@""];
    }
    return base64Encoded;
}
- (IBAction)actionDismissPopUp:(id)sender {
    [popUpView removeFromSuperview];
    [_txtClabe becomeFirstResponder];
}

#pragma mark-
#pragma mark Delegado TextField

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    textFieldEditable=textField;
}

-(void)keyboardWillShow:(NSNotification*)notification{
    NSDictionary* info = [notification userInfo];
    
    CGRect kKeyBoardFrame = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    int KeyboardY=kKeyBoardFrame.origin.y;
    int endPositionText=textFieldEditable.frame.origin.y+textFieldEditable.frame.size.height+20;
    if (endPositionText<KeyboardY) {
        textFieldEditable=nil;
        return;
    }
    int animatedDistance =50+endPositionText-KeyboardY;
    const int movementDistance = animatedDistance;
    int movement = -movementDistance;
    movedHeight=-movement;
    [_scrollContent setContentOffset:CGPointMake(0, movedHeight) animated:YES];
    
}
-(void)keyboardWillHide:(NSNotification*)notification{
//    if (textFieldEditable==nil) {
//        return;
//    }
    [_scrollContent setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    //self.layoutTopView.constant=0;
    [self verifyFields];
    if (self.view.frame.size.height>=668) {
        [_scrollContent setScrollEnabled:NO];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL cambiar=YES;
    if ([textField isEqual:_txtPhone]) {
        if (textField.text.length > 9 && range.length == 0)
        {
            cambiar=NO;
        }
        else
        {
            
            NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
            cambiar= [string isEqualToString:filtered];
            if (textField.text.length==9&&![string isEqualToString:@""]&&cambiar) {
                textField.text=[NSString stringWithFormat:@"%@%@",textField.text,string];
                [textField resignFirstResponder];
                
            }
            
            
        }
    }
    if ([textField isEqual:_txtClabe]) {
        if (textField.text.length > 17 && range.length == 0)
        {
            cambiar=NO;
        }
        else
        {
            
            NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
            cambiar= [string isEqualToString:filtered];
            if (textField.text.length==17&&![string isEqualToString:@""]&&cambiar) {
                textField.text=[NSString stringWithFormat:@"%@%@",textField.text,string];
                [textField resignFirstResponder];
                
            }
            
            
        }
    }
    return cambiar;
}
#pragma mark - Profile Image methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *image = [SharedFunctions scaleImageProportionally:[info objectForKey:UIImagePickerControllerOriginalImage]];
    //imgProfile.image = image;
    //[btnImagePicker setBackgroundImage:imageViewBack.image forState:UIControlStateNormal];
    //imageViewBack.hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSData *imageData = UIImageJPEGRepresentation(image, 0);
    NSError *error;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:@"/ProfileInfo"];
    if (![fileMgr fileExistsAtPath:documentsDirectory]){
        [fileMgr createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSString *nameImage=[NSString stringWithFormat:@"accountPhoto.jpg"];
    NSString *path= [documentsDirectory stringByAppendingPathComponent:nameImage];
    
    [imageData writeToFile:path options:NSDataWritingAtomic error:&error];
    
    _lblBankStatus.text=@"         ¡LISTO!";
    UIImageView *imgChec=[[UIImageView alloc] initWithFrame:CGRectMake(_lblBankStatus.frame.origin.x, _lblBankStatus.frame.origin.y, 10, 10)];
    [imgChec setImage:[UIImage imageNamed:@"imgCheckWhite"]];
    [_scrollContent addSubview:imgChec];
    isSavedAccountImage=YES;
    [self verifyFields];
}
-(void)finishingTakeSing{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
   // documentsDirectory = [NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    documentsDirectory=[documentsDirectory stringByAppendingString:[NSString stringWithFormat:@"/signing.png"]];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    //fileMgr = [NSFileManager defaultManager];
    if ([fileMgr fileExistsAtPath:documentsDirectory]){
        _lblSignStatus.text=@"         ¡LISTO!";
        UIImageView *imgChec=[[UIImageView alloc] initWithFrame:CGRectMake(_lblSignStatus.frame.origin.x, _lblSignStatus.frame.origin.y, 10, 10)];
        [imgChec setImage:[UIImage imageNamed:@"imgCheckWhite"]];
        [_scrollContent addSubview:imgChec];
        
        [self verifyFields];
    }
    
    
    
}
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if(error){
        [self showAlertWithMessage:error.description];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)actionCaptureBankAccount:(id)sender {
    [self selectPhotoAccount];
}

- (IBAction)actionSigning:(id)sender {
    [self takeSignature];
}
- (IBAction)actionCheckBox:(id)sender {
    if ([self.btnCheckBox isSelected]) {
        self.btnCheckBox.selected = NO;
    }
    else{
        self.btnCheckBox.selected = YES;
        
    }
    [self verifyFields];
}
@end
