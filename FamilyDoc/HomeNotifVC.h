//
//  HomeNotifVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 24/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeNotifVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTopHeader;
@property (strong, nonatomic) IBOutlet UITableView *NotifTV;

- (IBAction)actionOpenQR:(id)sender;


@end
