//
//  PatientHistoricVC.m
//  
//
//  Created by Martin Gonzalez on 01/03/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "PatientHistoricVC.h"
#import "ConsultationPatientTVC2.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"
#import "Config.h"
#import "NotesViewerVC.h"

@interface PatientHistoricVC (){
    UIRefreshControl *refreshControl;
    UIView *pdfViewer;
    UIView *pickerContainer;
    UIPickerView *pickerPrescriptionList;
    int prescriptionSelected;
    NSMutableArray *prescriptionListArray;
    NSMutableArray *notesListArray;
    UIPickerView *pickerNotesList;
}

@end

@implementation PatientHistoricVC
@synthesize consultasArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[SharedFunctions sharedInstance] verifyInternetStatusForBanner];
    _patientsTV.delegate=self;
    _patientsTV.dataSource=self;
    [_patientsTV reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if ([consultasArray count]>0)
    {
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        tableView.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height)];
        noDataLabel.text             = @"Por el momento no tienes \nninguna consulta abierta";
        noDataLabel.numberOfLines=4;
        noDataLabel.font=[UIFont fontWithName:@"MyriadPro-Regular" size:20];
        noDataLabel.textColor        = [UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        tableView.backgroundView = noDataLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [consultasArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTab = @"ConsultationPatientTVC2";
    ConsultationPatientTVC2 *cell = (ConsultationPatientTVC2 *)[tableView dequeueReusableCellWithIdentifier:simpleTab];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTab owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else{
        NSArray* subviews = [cell.contentView subviews];
        for (UIView* view in subviews)
        {
            [view removeFromSuperview];
        }
    }
    if (indexPath.row<[consultasArray count]) {
        cell.lblDate.text=[self formatDateToView:[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"meeting_date"]];
        cell.lblHour.text=[self formatHourToView:[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"meeting_date"]];
        cell.lblNamePatient.text=[[NSString stringWithFormat:@"DR. %@ %@ %@",[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"first_name"],[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"last_name"],[[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"doctor"] objectForKey:@"second_last_name"]] uppercaseString];
        cell.lblDiagnose.text=@"No especificado";
        cell.lblDiagnose.text=@"";
        NSString *lblDiagnose=@"";
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"healthcondition_set"] count]>0) {
            for (NSDictionary *dictDiagnose in [[consultasArray objectAtIndex:indexPath.row] objectForKey:@"healthcondition_set"]) {
                lblDiagnose=[NSString stringWithFormat:@"%@,%@",lblDiagnose,[dictDiagnose objectForKey:@"description"]];
            }
            if (lblDiagnose.length>0) {
                lblDiagnose=[lblDiagnose substringFromIndex:1];
            }
            if (lblDiagnose.length>0&&[[lblDiagnose substringFromIndex:lblDiagnose.length-1] isEqualToString:@","]) {
                lblDiagnose=[lblDiagnose substringToIndex:lblDiagnose.length-1];
            }
            cell.lblDiagnose.text=[lblDiagnose uppercaseString];
        }
        if (lblDiagnose.length<=0) {
            cell.lblDiagnose.text=@"PENDIENTE";
        }
        if (![[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"closed"]) {
            cell.lblStatus.text=@"ABIERTA";
        }
        else{
            cell.lblStatus.text=@"CERRADA";
        }
        cell.btnPill.enabled=NO;
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"prescription_set"] count]>0) {
            cell.btnPill.enabled=YES;
            cell.btnPill.tag=indexPath.row;
            [cell.btnPill addTarget:self action:@selector(openPrescription:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[consultasArray objectAtIndex:indexPath.row] objectForKey:@"notes"] count]>0) {
            cell.btnNotes.hidden=NO;
            cell.btnNotes.tag=indexPath.row;
            [cell.btnNotes addTarget:self action:@selector(openNotes:) forControlEvents:UIControlEventTouchUpInside];
        }
        if ([[[[consultasArray objectAtIndex:indexPath.row]objectForKey:@"doctor"] objectForKey:@"profile_pic"] length]>0) {
            InvokeService *invoke=[[InvokeService alloc] init];
            [invoke getImageFromUrl:[[[consultasArray objectAtIndex:indexPath.row]objectForKey:@"doctor"] objectForKey:@"profile_pic"]  WithCompletion:^(UIImage * _Nullable imageDownloaded, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (!error) {
                        [cell.imgPatient setImage:imageDownloaded];
                        cell.imgPatient.contentMode=UIViewContentModeScaleAspectFill;
                        [self roundImage:cell.imgPatient];
                    }
                    
                });
                
            }];
        }
    }
    else if (indexPath.row==[consultasArray count]) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell2==nil){
            cell2=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        [cell2 setBackgroundColor:[UIColor whiteColor]];
        return cell2;
        
    }
    return cell;
}
-(NSString *)formatDateToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd/MM/yyyy"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
-(NSString *)formatHourToView:(NSString *)inputDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSDate *formattedDate = [dateFormatter dateFromString:inputDate]
    ;
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"hh:mm a"];
    NSString *dateFinal=[dateFormatter2 stringFromDate:formattedDate];
    return dateFinal;
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)openPrescription:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayPresc=[[consultasArray objectAtIndex:index] objectForKey:@"prescription_set"];
    if ([arrayPresc count]>0) {
        if ([arrayPresc count]==1) {
            NSDictionary *dictPresc=[arrayPresc objectAtIndex:0];
            NSString *idPres=[dictPresc objectForKey:@"id"];
            [self openViewer:idPres];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige tu orden de tratamiento a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingPrescription) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            prescriptionListArray=[[NSMutableArray alloc] initWithArray:arrayPresc];
            
            pickerPrescriptionList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,60, frame.frame.size.width-20,100)];
            pickerPrescriptionList.tintColor = [UIColor whiteColor];
            pickerPrescriptionList.layer.cornerRadius=10;
            pickerPrescriptionList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerPrescriptionList.dataSource = self;
            pickerPrescriptionList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerPrescriptionList];
        }
    }
}
- (IBAction)openNotes:(id)sender {
    UIButton *btnSelected=(UIButton *)sender;
    int index=(int)btnSelected.tag;
    NSMutableArray *arrayNotes=[[consultasArray objectAtIndex:index] objectForKey:@"notes"];
    if ([arrayNotes count]>0) {
        if ([arrayNotes count]==1) {
            NSDictionary *dictnote=[arrayNotes objectAtIndex:0];
            NSString *textNote=[dictnote objectForKey:@"text"];
            [self openNoteViewer:textNote];
            
        }
        else{
            
            pickerContainer=[[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height )];
            pickerContainer.backgroundColor=[UIColor colorWithRed:0.004 green:0.004 blue:0.004 alpha:0.60];
            
            UIView *frame=[[UIView alloc] initWithFrame:CGRectMake(0,self.view.center.y-120, 300,260 )];
            frame.backgroundColor=[UIColor whiteColor];
            [frame setCenter:pickerContainer.center];
            
            UILabel *lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(10, 10, frame.frame.size.width-20, 70)];
            lblTitle.font= [UIFont fontWithName:@"MyriadPro-Regular" size: 23.0f];
            lblTitle.textColor=[UIColor darkGrayColor];
            lblTitle.text=@"Elige la a visualizar:";
            lblTitle.textAlignment=NSTextAlignmentCenter;
            lblTitle.numberOfLines=3;
            frame.layer.cornerRadius=10;
            
            UIButton *btnChose=[[UIButton alloc] initWithFrame:CGRectMake(frame.frame.size.width/2-50, 210, 100, 30)];
            [btnChose setTitle:@"Aceptar" forState:UIControlStateNormal];
            [btnChose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnChose setBackgroundColor:[UIColor colorWithRed:0.008 green:0.580 blue:0.710 alpha:1.00]];
            [btnChose.titleLabel setFont:[UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]];
            btnChose.layer.cornerRadius=10;
            [btnChose addTarget:self action:@selector(donePickingNote) forControlEvents:UIControlEventTouchUpInside];
            [frame addSubview:btnChose];
            
            notesListArray=[[NSMutableArray alloc] initWithArray:arrayNotes];
            
            pickerNotesList = [[UIPickerView alloc]initWithFrame:CGRectMake(10,80, frame.frame.size.width-20,100)];
            pickerNotesList.tintColor = [UIColor whiteColor];
            pickerNotesList.layer.cornerRadius=10;
            pickerNotesList.backgroundColor=[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.00];
            pickerNotesList.dataSource = self;
            pickerNotesList.delegate = self;
            [self.view addSubview:pickerContainer];
            [pickerContainer addSubview:frame];
            [frame addSubview:lblTitle];
            
            [frame addSubview:pickerNotesList];
        }
    }
}
- (void)openViewer:(NSString *)idPrescription {
    pdfViewer=[[UIView alloc] init];
    pdfViewer=[[[NSBundle mainBundle] loadNibNamed:@"PrescriptionViewer" owner:self options:nil] objectAtIndex:0];
    [pdfViewer setFrame:CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height) ];
    [self.view addSubview:pdfViewer];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@/api/v0/prescription/%@/pdf/",BASE_URL,idPrescription]];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc] initWithURL:url];
    NSString *authString=[NSString stringWithFormat:@"Bearer %@",[[accountInfo objectForKey:@"tokenInfo"] objectForKey:@"access_token"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"accept": @"application/json",
                               @"authorization":  authString};
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"GET"];
    [_myWebView loadRequest:request];
    _myWebView.delegate=self;
    [[SharedFunctions sharedInstance] showLoadingView];
    
}
-(void)openNoteViewer:(NSString *)note{
    NotesViewerVC *noteViewer =(NotesViewerVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"NotesViewerVC"];
    [[SharedFunctions sharedInstance] topMostController].definesPresentationContext = YES;
    noteViewer.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [[[SharedFunctions sharedInstance] topMostController] presentViewController:noteViewer animated:YES completion:nil];
    noteViewer.txtViewNote.text=note;
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [[SharedFunctions sharedInstance] removeLoadingView];
}
- (IBAction)closeWebView:(id)sender {
    [pdfViewer removeFromSuperview];
}
-(void)roundImage:(UIImageView *)imgProfile{
    if (imgProfile.frame.size.height!=imgProfile.frame.size.width) {
        [imgProfile setFrame:CGRectMake(imgProfile.frame.origin.x, imgProfile.frame.origin.y, imgProfile.frame.size.height, imgProfile.frame.size.height)];
    }
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.layer.borderWidth = 0.5f;
    imgProfile.contentMode=UIViewContentModeScaleAspectFill;
    imgProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    imgProfile.clipsToBounds = YES;
}
#pragma mark - Picker Methods
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if ([pickerView isEqual:pickerNotesList]) {
        return [notesListArray count];
    }
    return [prescriptionListArray count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (void) donePickingPrescription {
    int row = (int) [pickerPrescriptionList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictPresc=[prescriptionListArray objectAtIndex:row];
    NSString *idPres=[dictPresc objectForKey:@"id"];
    [self openViewer:idPres];
    
}
- (void) donePickingNote {
    int row = (int) [pickerNotesList selectedRowInComponent:0];
    [pickerContainer removeFromSuperview];
    NSDictionary *dictnote=[notesListArray objectAtIndex:row];
    NSString *textNote=[dictnote objectForKey:@"text"];
    [self openNoteViewer:textNote];
    
}
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if ([pickerView isEqual:pickerNotesList]) {
        NSString *title = [NSString stringWithFormat:@"Nota %ld",(long)row+1];;
        NSAttributedString *attString =
        [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]}];
        
        return attString;
    }
    NSString *title = [NSString stringWithFormat:@"Orden de tratamiento %ld",(long)row+1];;
    NSAttributedString *attString =
    [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:0.322 green:0.737 blue:0.816 alpha:1.00],NSFontAttributeName: [UIFont fontWithName:@"MyriadPro-Regular" size: 15.0f]}];
    
    return attString;
}
@end
