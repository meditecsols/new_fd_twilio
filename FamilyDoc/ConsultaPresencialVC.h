//
//  ConsultaPresencialVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 21/08/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ConsultaPresencialVC : UIViewController<UITabBarDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,MFMailComposeViewControllerDelegate,UIWebViewDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property BOOL isRemote;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (strong, nonatomic) IBOutlet UITextField *txtTest;
@property (strong, nonatomic) IBOutlet UITabBar *tabBarMenu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintTabBar;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldPreDiagnostic;

@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddHeight;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddWeight;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddBlood;

@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddTemp;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddHearthFreq;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddBloodPressure;
@property (strong, nonatomic) IBOutlet UITextField *txtfieldAddBreathFreq;

//tab Expediente
@property (strong, nonatomic) IBOutlet UIScrollView *scrollContainerExp;
@property (strong, nonatomic) IBOutlet UIView *allergView;
@property (strong, nonatomic) IBOutlet UIView *surgeryView;
@property (strong, nonatomic) IBOutlet UIView *transfusionView;
@property (strong, nonatomic) IBOutlet UIView *traumatismView;
@property (strong, nonatomic) IBOutlet UIView *familiarView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblGender;
@property (strong, nonatomic) IBOutlet UILabel *lblAge;
@property (strong, nonatomic) IBOutlet UILabel *lblWeight;
@property (strong, nonatomic) IBOutlet UILabel *lblHeight;
@property (strong, nonatomic) IBOutlet UILabel *lblTemp;
@property (strong, nonatomic) IBOutlet UILabel *lblBloodPressure;
@property (strong, nonatomic) IBOutlet UILabel *lblHeartFreq;
@property (strong, nonatomic) IBOutlet UILabel *lblBreathFreq;
@property (strong, nonatomic) IBOutlet UIView *viewAge;
@property (strong, nonatomic) IBOutlet UIView *viewWeight;
@property (strong, nonatomic) IBOutlet UIView *viewHeight;
@property (strong, nonatomic) IBOutlet UIView *viewTemp;
@property (strong, nonatomic) IBOutlet UIView *viewPressureBlood;
@property (strong, nonatomic) IBOutlet UIView *viewCardiacFrequency;
@property (strong, nonatomic) IBOutlet UIView *viewBreathingFrequency;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewBlood;
@property (strong, nonatomic) IBOutlet UILabel *lblBlood;
@property (strong, nonatomic) IBOutlet UILabel *lblBirthDate;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightAllerg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightSurgery;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTransfu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTraumatism;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightFamiliar;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTableAllerg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTableSurgery;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTableTransfu;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTableTraumatism;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightTableFamiliar;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintHeightSecondContainer;

@property (strong, nonatomic) IBOutlet UIButton *btnYesAllerg;
@property (strong, nonatomic) IBOutlet UIButton *btnYesSurgery;
@property (strong, nonatomic) IBOutlet UIButton *btnYesTransfusion;
@property (strong, nonatomic) IBOutlet UIButton *btnYesTraumatism;
@property (strong, nonatomic) IBOutlet UIButton *btnYesFamiliarBg;

@property (strong, nonatomic) IBOutlet UIButton *btnNoAllerg;
@property (strong, nonatomic) IBOutlet UIButton *btnNoSurgery;
@property (strong, nonatomic) IBOutlet UIButton *btnNoTransfusion;
@property (strong, nonatomic) IBOutlet UIButton *btnNoTraumatism;
@property (strong, nonatomic) IBOutlet UIButton *btnNoFamiliarBg;

@property (strong, nonatomic) IBOutlet UIButton *btnAddAllerg;
@property (strong, nonatomic) IBOutlet UIButton *btnAddSurgery;
@property (strong, nonatomic) IBOutlet UIButton *btnAddTransfusion;
@property (strong, nonatomic) IBOutlet UIButton *btnAddTraumatism;
@property (strong, nonatomic) IBOutlet UIButton *btnAddFamiliarBg;

@property (strong, nonatomic) IBOutlet UITableView *tvAllerg;
@property (strong, nonatomic) IBOutlet UITableView *tvSurgery;
@property (strong, nonatomic) IBOutlet UITableView *tvTransfusion;
@property (strong, nonatomic) IBOutlet UITableView *tvTraumatism;
@property (strong, nonatomic) IBOutlet UITableView *tvFamiliarBg;

- (IBAction)actionYesAllergy:(id)sender;
- (IBAction)actionNoAllergy:(id)sender;
- (IBAction)actionYesSurgery:(id)sender;
- (IBAction)actionNoSurgery:(id)sender;
- (IBAction)actionYesTransfuc:(id)sender;
- (IBAction)actionNoTransfuc:(id)sender;
- (IBAction)actionYesTraumatism:(id)sender;
- (IBAction)actionNoTraumatism:(id)sender;
- (IBAction)actionYesFamiliar:(id)sender;
- (IBAction)actionNoFamiliar:(id)sender;

- (IBAction)actionAddAllergy :(id)sender;
- (IBAction)actionAddSurgery :(id)sender;
- (IBAction)actionAddTransfuction :(id)sender;
- (IBAction)actionAddTraumatism :(id)sender;
- (IBAction)actionAddFamiliar :(id)sender;

- (void)openPrediagnosticView;
- (IBAction)backPrediagnostic:(id)sender;
- (IBAction)savePrediagnostic:(id)sender;

- (IBAction)backAddHeight:(id)sender;
- (IBAction)saveAddHeight:(id)sender;

- (IBAction)backAddWeight:(id)sender;
- (IBAction)saveAddWeight:(id)sender;

- (IBAction)backAddBlood:(id)sender;
- (IBAction)saveAddBlood:(id)sender;

- (IBAction)backAddTemp:(id)sender;
- (IBAction)saveAddTemp:(id)sender;

- (IBAction)backAddBloodPressure:(id)sender;
- (IBAction)saveAddBloodPressure:(id)sender;

- (IBAction)backAddHeartFreq:(id)sender;
- (IBAction)saveAddHeartFreq:(id)sender;

- (IBAction)backAddBreathFreq:(id)sender;
- (IBAction)saveAddBreathFreq:(id)sender;


//tab Receta
@property (strong, nonatomic) IBOutlet UIButton *btnReceta;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollRecetaItems;
@property (strong, nonatomic) IBOutlet UITableView *tvPrescriptionItems;
@property (strong, nonatomic) IBOutlet UITableView *tvPrescriptionsHistory;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckSign;
@property (strong, nonatomic) IBOutlet UITextField *txtDiagnose;
@property (strong, nonatomic) IBOutlet UITextField *txtNameMedicine;
@property (strong, nonatomic) IBOutlet UITextField *txtDose;
@property (strong, nonatomic) IBOutlet UITextField *txtAdministration;
@property (strong, nonatomic) IBOutlet UITextField *txtFrequency;
@property (strong, nonatomic) IBOutlet UITextField *txtDuration;
@property (strong, nonatomic) IBOutlet UITextView *textViewObservations;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleStudy;
@property (strong, nonatomic) IBOutlet UITextField *txtNameStudy;
@property (strong, nonatomic) IBOutlet UITextField *txtDiagnoseInput;
@property (strong, nonatomic) IBOutlet UIWebView *myWebView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintContainerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintTablePrescriptionHeight;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewHistoryPrescription;

- (IBAction)addObservation:(id)sender;

- (IBAction)addDiagnose:(id)sender;
- (IBAction)addMedicine:(id)sender;
- (IBAction)addStudy:(id)sender;
- (IBAction)addDiagnoseInput:(id)sender;
- (IBAction)actionCloseDiagnose:(id)sender;
- (IBAction)actionCheckSign:(id)sender;
- (IBAction)actionSendPrescription:(id)sender;
- (IBAction)actionBackAddItemPrescription:(id)sender;
- (IBAction)backAction:(id)sender;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionClose:(id)sender;
//- (IBAction)actionPayment:(id)sender;
- (IBAction)actionAddDiagnose:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnPayment;
@property (strong, nonatomic) IBOutlet UIView *viewButtonsContainer;

//tab notas
@property (strong, nonatomic) IBOutlet UIButton *checkNotePersonal;
@property (strong, nonatomic) IBOutlet UITextView *txtViewNote;
@property (strong, nonatomic) IBOutlet UITextView *txtViewNoteDesc;
@property (strong, nonatomic) IBOutlet UITableView *tvNotes;
- (IBAction)saveNote:(id)sender;
- (IBAction)closeNoteDesc:(id)sender;
- (IBAction)actionCheckPrivateNote:(id)sender;

//tabHistorial
@property (strong, nonatomic) IBOutlet UITableView *tvHistoric;




//view addHealth
@property (strong, nonatomic) IBOutlet UILabel *lblTitleHealth;
@property (strong, nonatomic) IBOutlet UITextField *txtDescHealth;
- (IBAction)cancelAddHealth:(id)sender;
- (IBAction)actionAddHealth:(id)sender;


//view Closing Diagnose
@property (strong, nonatomic) IBOutlet UITextField *txtClosingDiagnose;
@property (strong, nonatomic) IBOutlet UITextField *txtClosingPrognosis;
@property (strong, nonatomic) IBOutlet UITextField *txtClosingExitReason;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UIView *viewContainerStatus;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutConstraintStatusContainerHeight;

- (IBAction)backClosingDiagnose:(id)sender;
- (IBAction)actionNextClosingDiagnose:(id)sender;

//view PaymentView
@property (strong, nonatomic) IBOutlet UIView *viewContainerTotalPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtTotalCost;
@property (strong, nonatomic) IBOutlet UIView *viewContainerResumePrice;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *layoutHeightContainerResume;
@property (strong, nonatomic) IBOutlet UIView *viewContainerPriceConsultation;
@property (strong, nonatomic) IBOutlet UITextField *txtPriceConsult;
@property (strong, nonatomic) IBOutlet UITableView *tvAdditionalServices;
@property (strong, nonatomic) IBOutlet UITextField *txtPaymentType;
@property (strong, nonatomic) IBOutlet UITextField *txtNameService;
@property (strong, nonatomic) IBOutlet UITextField *txtPriceService;
@property (strong, nonatomic) IBOutlet UILabel *lblComisionMeditec;
@property (strong, nonatomic) IBOutlet UILabel *lblComisionConekta;
@property (strong, nonatomic) IBOutlet UILabel *lblNegativeAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalLessComissions;
@property (strong, nonatomic) IBOutlet UIButton *btnClosePayment;
- (IBAction)actionAddService:(id)sender;
- (IBAction)actionCloseAddService:(id)sender;
- (IBAction)closeWebView:(id)sender;

- (IBAction)actionBackClosing:(id)sender;
- (IBAction)actionAddServiceForm:(id)sender;
- (IBAction)actionCloseWithPayment:(id)sender;

@end
