//
//  GlobalMembers.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 15/10/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalMembers : NSObject
extern NSString *fullNameDoctor;
extern NSString *usr;
extern NSString *psw;
extern NSMutableDictionary *accountInfo;
extern BOOL firstTimeEntry;
extern BOOL comeFromLogin;
extern BOOL comeFromChat;
extern BOOL statusReachability;
extern BOOL needRefreshConsultations;
extern BOOL needRefreshPatients;
extern BOOL isSignTaked;
extern BOOL isFDBusinessDoc;
extern BOOL isSuscriptionValid;
extern NSMutableDictionary *doctorInfo;
extern NSMutableDictionary *dictPatientPresential;
extern NSMutableDictionary *presentialConsultation;
extern NSMutableDictionary *dictOfPrescription;
extern BOOL comeFromScan;
extern int pageHomeSelected;
extern int indexSChatSelected;
extern NSString *pathPdfFile;
extern NSString *fcmTokenRetrieved;
@end
