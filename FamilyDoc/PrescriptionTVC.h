//
//  PrescriptionTVC.h
//  FamilyDoc
//
//  Created by Martin Gonzalez on 11/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrescriptionTVC : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblDesc;
@end
