//
//  MessageTransform.m
//  
//
//  Created by Martin Gonzalez on 25/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import "MessageTransform.h"
#import "AESCrypt.h"

@implementation MessageTransform
+ (NSString*) encryptString: (NSString*) stringToEncrypt
{
    NSString *stringKey=[NSString stringWithFormat:@"F9M1lYD0C8U51n35"];
    NSString *encryptedData = [AESCrypt encrypt:stringToEncrypt password:stringKey];
    
    return encryptedData;
}

+(NSString *)decryptString:(NSString *)stringToDecrypt{
    NSString *stringKey=[NSString stringWithFormat:@"F9M1lYD0C8U51n35"];
    NSString *decrypted = [AESCrypt decrypt:stringToDecrypt password:stringKey];
    if ([decrypted length]>0) {
        return decrypted;
    }
    return stringToDecrypt;
}
@end
