//
//  MessageTransform.h
//  
//
//  Created by Martin Gonzalez on 25/06/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageTransform : NSObject

+(NSString *)encryptString:(NSString *)stringToEncrypt;
+(NSString *)decryptString:(NSString *)stringToDecrypt;

@end
