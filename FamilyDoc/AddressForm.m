//
//  AddressForm.m
//  
//
//  Created by Martin Gonzalez on 17/11/17.
//  Copyright © 2017 Martin Gonzalez. All rights reserved.
//

#import "AddressForm.h"
#import "InvokeService.h"
#import "SharedFunctions.h"
#import "GlobalMembers.h"

@interface AddressForm (){
    NSMutableArray *arrayNeighborHood;
    UIPickerView *neighbordHoodPicker;
    NSMutableArray *arrayTypeAddress;
    UIPickerView *typeAddressPicker;
}

@end

@implementation AddressForm

- (void)viewDidLoad {
    [super viewDidLoad];
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.tintColor=[UIColor colorWithRed:0.000 green:0.788 blue:0.969 alpha:1.00];
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancelar" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Buscar" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    
    _txtZipCode.inputAccessoryView = numberToolbar;
    [numberToolbar sizeToFit];
    _txtNeighbordHood.enabled=NO;
    _txtZipCode.delegate= self;
    _txtProvince.delegate=self;
    _txtState.delegate=self;
    _txtCountry.delegate=self;
    _txtNeighbordHood.delegate=self;
    _txtStreet.delegate=self;
    _txtTypeAddress.delegate=self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    typeAddressPicker = [[UIPickerView alloc]init];
    typeAddressPicker.tintColor = [UIColor whiteColor];
    typeAddressPicker.dataSource = self;
    typeAddressPicker.delegate = self;
    arrayTypeAddress=[[NSMutableArray alloc] initWithArray:@[@"Consultorio Principal",@"Consultorio",@"Dirección Fiscal"]];
    [_txtTypeAddress setInputView:typeAddressPicker];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
    toolBar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingType)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    toolBar.tintColor = [UIColor darkGrayColor];
    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
    _txtTypeAddress.inputAccessoryView = toolBar;
    //bloodField = cell.txtField;
    [_txtTypeAddress becomeFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[_dictAddress allKeys] count]>0){
        _lblTitle.text=@"Edita tu dirección";
        _txtStreet.text=[_dictAddress objectForKey:@"street"];
        _txtExtNumb.text=[_dictAddress objectForKey:@"street_num"];
        _txtIntNumb.text=[_dictAddress objectForKey:@"int_num"];
        _txtZipCode.text=[_dictAddress objectForKey:@"postal_code"];
        _txtNeighbordHood.text=[_dictAddress objectForKey:@"neighbourhood"];
        _txtProvince.text=[_dictAddress objectForKey:@"city"];
        _txtState.text=[_dictAddress objectForKey:@"state"];
        _txtCountry.text=[_dictAddress objectForKey:@"country"];
        if ([[_dictAddress objectForKey:@"is_fiscal"] boolValue]) {
            _txtTypeAddress.text=@"Dirección Fiscal";
        }
        else if ([[_dictAddress objectForKey:@"priority"] intValue]==0){
            _txtTypeAddress.text=@"Consultorio Principal";
        }
        else{
            _txtTypeAddress.text=@"Consultorio";
        }
        [self.view endEditing:YES];
    }
}
-(void)dismissKeyboard {
    [self.view endEditing:YES];
}
-(void)cancelNumberPad{
    [_txtZipCode resignFirstResponder];
    _txtZipCode.text = @"";
    _txtProvince.text=@"";
    _txtState.text=@"";
    _txtCountry.text=@"";
    _txtNeighbordHood.text=@"";
    [self verifyFields];
}
-(void)doneWithNumberPad{
    if (_txtZipCode.text.length==5) {
        _txtProvince.text=@"";
        _txtState.text=@"";
        _txtCountry.text=@"";
        _txtNeighbordHood.text=@"";
        
        
        [_txtZipCode resignFirstResponder];
        [[SharedFunctions sharedInstance] showLoadingView];
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke retrieveZipCodeData:_txtZipCode.text WithCompletion:^(NSMutableDictionary *responseData, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                NSLog(@"response:%@",[responseData description]);
                NSDictionary *data=[[NSDictionary alloc] initWithDictionary:responseData];
                self->_txtNeighbordHood.enabled=YES;
                self->_txtProvince.text=[data objectForKey:@"municipio"];
                self->_txtState.text=[data objectForKey:@"estado"];
                self->_txtCountry.text=@"México";
                self->arrayNeighborHood=[[NSMutableArray alloc] init];
                self->arrayNeighborHood=[data objectForKey:@"colonias"];
                
                if ([self->arrayNeighborHood count]>0) {
                    self->neighbordHoodPicker = [[UIPickerView alloc]init];
                    self->neighbordHoodPicker.tintColor = [UIColor whiteColor];
                    self->neighbordHoodPicker.dataSource = self;
                    self->neighbordHoodPicker.delegate = self;
                    [self->_txtNeighbordHood setInputView:self->neighbordHoodPicker];
                    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 340, 30)];
                    toolBar.barStyle = UIBarStyleDefault;
                    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Elegir",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickingNeightborHood)];
                    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
                    toolBar.tintColor = [UIColor darkGrayColor];
                    [toolBar setItems:[NSArray arrayWithObjects:flex,doneButton, nil]];
                    self->_txtNeighbordHood.inputAccessoryView = toolBar;
                    //bloodField = cell.txtField;
                    [self->_txtNeighbordHood becomeFirstResponder];
                }
                else{
                    [self showAlertWithMessage:@"No se encontraron datos para ese código postal."];
                }
            });
            
        }];
        
    }
    else{
        [self showAlertWithMessage:@"Ingrese un código postal válido."];
    }
}
-(void)showAlertWithMessage:(NSString *)message{
    UIAlertController * alert=  [UIAlertController
                                 alertControllerWithTitle:@"Family Doc"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    

    
    
    UIAlertAction* okAction = [UIAlertAction
                               actionWithTitle:@"Entendido"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - Picker Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView==typeAddressPicker) {
        return [arrayTypeAddress count];
    }
    return arrayNeighborHood.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView==typeAddressPicker) {
        return arrayTypeAddress [row];
    }
    return arrayNeighborHood[row];
    
}
- (void) donePickingNeightborHood {
    int row = (int) [neighbordHoodPicker selectedRowInComponent:0];
    _txtNeighbordHood.text =  [self pickerView:neighbordHoodPicker titleForRow:row forComponent:0];
    [self hideKeyboard];
}
-(void)donePickingType{
    int row = (int) [typeAddressPicker selectedRowInComponent:0];
    _txtTypeAddress.text =  [self pickerView:typeAddressPicker titleForRow:row forComponent:0];
    [self hideKeyboard];
    
}
- (void) hideKeyboard{
    [self.view endEditing:YES];
}
-(void)verifyFields{
//    [_btnNext setBackgroundColor:[UIColor colorWithRed:0.443 green:0.482 blue:0.576 alpha:1.00]];
//    [_btnNext setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (_txtStreet.text.length<=0) {
        return;
    }
    if (_txtZipCode.text.length<=0) {
        return;
    }
    if (_txtNeighbordHood.text.length<=0) {
        return;
    }
    if (_txtProvince.text.length<=0) {
        return;
    }
    if (_txtState.text.length<=0) {
        return;
    }
    if (_txtCountry.text.length<=0) {
        return;
    }
//    [_btnNext setBackgroundColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.0]];
//    [_btnNext setTitleColor:[UIColor colorWithRed:0.608 green:0.608 blue:0.608 alpha:1.00] forState:UIControlStateNormal];
}
-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    [self verifyFields];
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionSave:(id)sender {
    if (_txtStreet.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar tu Calle y Número"];
        return;
    }
    if (_txtExtNumb.text.length<=0) {
        [self showAlertWithMessage:@"Debes ingresar el número de tu calle"];
        return;
    }
    if (_txtZipCode.text.length<5) {
        [self showAlertWithMessage:@"Debes ingresar tu Código Postal"];
        return;
    }
    if (_txtNeighbordHood.text.length<=0) {
        [self showAlertWithMessage:@"Debes seleccionar tu Colonia"];
        return;
    }
    if (!_dictAddress) {
        _dictAddress=[[NSMutableDictionary alloc] init];
    }
    
    
    [_dictAddress setObject:_txtStreet.text forKey:@"street"];
    [_dictAddress setObject:_txtExtNumb.text forKey:@"street_num"];
    [_dictAddress setObject:_txtCountry.text forKey:@"country"];
    [_dictAddress setObject:@"0" forKey:@"int_num"];
    if (_txtIntNumb.text.length>0) {
        [_dictAddress setObject:_txtIntNumb.text forKey:@"int_num"];
    }
    
    [_dictAddress setObject:_txtProvince.text forKey:@"borough"];
    [_dictAddress setObject:_txtProvince.text forKey:@"city"];
    [_dictAddress setObject:_txtNeighbordHood.text forKey:@"neighbourhood"];
    [_dictAddress setObject:_txtState.text forKey:@"state"];
    [_dictAddress setObject:_txtZipCode.text forKey:@"postal_code"];
    if ([_txtTypeAddress.text isEqualToString:@"Dirección Fiscal"]) {
        [_dictAddress setObject:@YES forKey:@"is_fiscal"] ;
    }
    else if ([_txtTypeAddress.text isEqualToString:@"Consultorio Principal"]){
        [_dictAddress setObject:@0 forKey:@"priority"];
    }
    else{
        [_dictAddress setObject:@1 forKey:@"priority"];
    }
    
    if (_comeToEdit) {
        [[SharedFunctions sharedInstance] showLoadingView];
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke updateAddressWithData:_dictAddress WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([[responseData allKeys] count]>0) {
                    UIAlertController * alert=  [UIAlertController
                                                 alertControllerWithTitle:@""
                                                 message:@"Se ha actualizado tu dirección correctamente"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction* okAction = [UIAlertAction
                                               actionWithTitle:@"Aceptar"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:NO completion:nil];
                }
                else{
                    [self showAlertWithMessage:@"Error al actualizar tu dirección"];
                }
                
            });
            
        }];
    }
    else{
        [_dictAddress setObject:[doctorInfo objectForKey:@"id"] forKey:@"doctor"];
        [[SharedFunctions sharedInstance] showLoadingView];
        InvokeService *invoke=[[InvokeService alloc] init];
        [invoke addAddressWithData:_dictAddress WithCompletion:^(NSMutableDictionary * _Nullable responseData, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedFunctions sharedInstance] removeLoadingView];
                if ([[responseData allKeys] count]>0) {
                    UIAlertController * alert=  [UIAlertController
                                                 alertControllerWithTitle:@""
                                                 message:@"Se ha agregado tu dirección correctamente"
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction* okAction = [UIAlertAction
                                               actionWithTitle:@"Aceptar"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   [self dismissViewControllerAnimated:YES completion:nil];
                                                   
                                               }];
                    
                    [alert addAction:okAction];
                    [self presentViewController:alert animated:NO completion:nil];
                }
                else{
                    [self showAlertWithMessage:@"Error al agregar tu dirección"];
                }
                
            });
            
        }];
    }
    
    
   
    
}

@end
