//
//  ChatVC.h
//  
//
//  Created by Martin Gonzalez on 24/04/18.
//  Copyright © 2018 Martin Gonzalez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessages.h"
#import "JSQMessagesViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import <IQAudioRecorderController/IQAudioRecorderViewController.h>
#import <SendBirdSDK/SendBirdSDK.h>


@interface ChatVC : JSQMessagesViewController <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,IQAudioRecorderViewControllerDelegate,SBDChannelDelegate>

@property (strong, nonatomic) SBDGroupChannel *channel;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSURL *videoURL;
@property (nonatomic, retain) AVPlayerViewController *playerViewController;
@property(strong,nonatomic) UIButton *consultationButton;
-(void)processMessage:(SBDBaseMessage *)message;
//- (void)receiveMessagePressed:(UIBarButtonItem *)sender;
-(void)closeChat;
- (void)closePressed:(UIBarButtonItem *)sender;


@end
